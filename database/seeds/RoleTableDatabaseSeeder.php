<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\models\Role;
use Zizaco\Entrust\EntrustRole;

class RoleTableDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$admin_role_count = Role::where('name', '=', 'admin')->count();
		if($admin_role_count < 1)
        {
            $admin = new Role();
            $admin->name         = 'admin';
            $admin->display_name = 'Admin'; // optional
            $admin->description  = 'Admin is allowed to view all details'; // optional
            $admin->save();
        }
        
        $roles = Role::where('name', '=', 'customer')->count();
        if ($roles < 1) 
        {
            $p1 = new Role();
            $p1->id = '2';
            $p1->name         = 'customer';
            $p1->display_name = 'customer'; // optional
            $p1->description  = 'customer'; // optional
            $p1->save(); 
        }        
    }
}
