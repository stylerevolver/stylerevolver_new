<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('sales_date');
            $table->double('sgst');
            $table->double('cgst');
            $table->double('ammount',15,8);
            $table->date('delivery_date')->nullable()->change();
            $table->double('delivery_charges',15,8)->nullable()->change();
            $table->string('delivery_address')->nullable();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('offer_id')->nullable();
            $table->double('offer_amount',15,8)->nullable();
            $table->foreign('user_id')->references('id')->on('user');
            $table->string('status')->nullable()->comment("Pending,Complete,Cencel");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
