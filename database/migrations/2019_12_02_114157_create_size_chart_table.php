<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSizeChartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('size_chart', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('size_name');
            $table->double('length',15,8);
            $table->double('waist',15,8);
            $table->double('chest',15,8);
            $table->double('hips',15,8);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('size_chart');
    }
}
