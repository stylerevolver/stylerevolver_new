<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('product_name');
            $table->string('description')->nullable();
            $table->text('product_image');
            $table->double('product_price',15,8);
            $table->integer('product_qty');
            $table->bigInteger('subcategory_id')->unsigned();
            $table->foreign('subcategory_id')->references('id')->on('subcategory');
            $table->bigInteger('size_id')->unsigned();
            $table->foreign('size_id')->references('id')->on('size_chart');  
            $table->bigInteger('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('company');
            $table->string('color_id');
            $table->string('featured_filed')->nullable()->comment("Yes mean's featured");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
        //DB::statement('ALTER TABLE items MODIFY COLUMN color_id bigInteger');
    }
}
