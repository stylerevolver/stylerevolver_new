<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplainTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complain', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('complain_date');
            $table->text('description');
            $table->string('complain_status');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('sales_id')->unsigned()->nullable();
            $table->bigInteger('product_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('user');
            $table->foreign('sales_id')->references('id')->on('sales');
            $table->foreign('product_id')->references('id')->on('product');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complain');
    }
}
