<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomizeProductDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customize_product_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('description');
            $table->double('price',15,8);
            $table->bigInteger('customize_product_id')->unsigned();
            $table->foreign('customize_product_id')->references('id')->on('customize_product');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customize_product_details');
    }
}
