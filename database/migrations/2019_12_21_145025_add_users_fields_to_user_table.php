<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsersFieldsToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function (Blueprint $table) {
            $table->text('address');
            $table->string('security_question');
            $table->string('security_answer');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('gender');
            $table->date('dob');
            $table->bigInteger('contact_no')->unique();
            $table->string('profile_pic');
            $table->bigInteger('area_id')->nullable()->unsigned();
            $table->foreign('area_id')->references('id')->on('area');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function (Blueprint $table) {
            $table->dropColumn('address');
            $table->dropColumn('security_question');
            $table->dropColumn('security_answer');
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('gender');
            $table->dropColumn('dob');
            $table->dropColumn('contact_no');
            $table->dropColumn('profile_pic');
            $table->dropColumn('area_id');
            $table->dropForeign(['area_id']);
            $table->dropColumn('area_id');
        });
    }
}
