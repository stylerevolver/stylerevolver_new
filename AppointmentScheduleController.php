<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use App\Models\AppointmentSchedule;
use App\Models\AppointmentTiming;
use App\Models\AppointmentTimeBlock;
use App\Models\DocumentType;
use App\Models\ClientDocumentRequest;
use App\Models\ClientDocument;
use App\Models\ClientAppointment;
use App\Models\TeamMember;
use App\Models\BaseModel;
use App\Models\User;
use App\Models\Site;
use App\Models\Leading;
use App\Models\AppointmentRule;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ClientAppointmentExport;
use App\Events\AppointmentAssigned;
use App\Events\AppointmentStatusChange;
use App\Events\AppointmentScheduleChange;
use App\Mail\ClientAppoinmentStatusChange;
use App\Mail\ClientAppoinmentAssignTo;
use App\Mail\ClientAppoinmentReschedule;
use Carbon\Carbon;
use Validator;
use Session;
use DataTables;
use Exception;
use Eluceo\iCal\Component\Calendar;
use Eluceo\iCal\Component\Event;
use Uuid;

class AppointmentScheduleController extends Controller
{
    public function index()
    {
        return view('appointment.index');
    }

    public function indexJson(Request $request)
    {
        $appointments = AppointmentSchedule::site()->get();
        return \DataTables::of($appointments)
                ->editColumn('action', function($data) {
                    $html = '<a href="'. url('/edit-appointment-schedule/'.$data->id) .'" class="btn btn-info btn-icon margin-r-5"><i class="fa fa-edit"></i>Edit</a>';

                    $html .= '<form onsubmit="return confirm(\'Many Client Appointments may be set to this schedule, Are you sure to continue ?\');" action="'. url('/delete-appointment-schedule') .'" method="POST" style="display: inline;" >';
                    $html .= '<input type="hidden" name="_token" value="'.csrf_token().'">';
                    $html .= '<input type="hidden" value="'.$data->id.'" name="id">';
                    $html .= '<button type="submit" class="btn btn-danger btn-icon margin-r-5"><i class="fa fa-trash"></i>Delete</button>';
                    $html .= '</form>';
                    return $html;
                })
                ->rawColumns(['action'])
                ->make(true);
    }

    public function create()
    {
        $rules = AppointmentRule::site()->get();
        return view('appointment.create')->with('rules', $rules);
    }

    public function store(Request $request)
    {
        $site = BaseModel::getcurrentsiteid();
        $site_id = (isset($site)) ? $site->id : NULL;
        $data = $request->all();
        $block_data = array();
        $block = false;
        if(isset($data['block_fields']) && !empty($data['block_fields'])){
            $block_data = json_decode(stripslashes($data['block_fields']));
            $block = true;
        }
        
        $appointment_schedule = AppointmentSchedule::create([
            "name" => $data['name'],
            "site_id" => $site_id,
            "description" => $data['description'],
            "appointment_rule_id" => $data['rule_id'],
        ]);

        if(!isset($data['days']) && empty($data['days'])){
            $data['days'] = array();
        }

        foreach ($data['days'] as $day) {
            $app_time = AppointmentTiming::create([
                "appointment_schedule_id" => $appointment_schedule->id,
                "day" => $day,
                "from_hour" => $data['from_hour'],
                "to_hour" => $data['to_hour'],
                "interval" => $data['interval'],
                "appointment_per_interval" => $data['appointment_per_interval']
            ]);

            if($block){
                foreach ($block_data as $block_time) {
                    AppointmentTimeBlock::create([
                        "appointment_timing_id" => $app_time->id,
                        "block_from_hour" => $block_time->block_from_hour,    
                        "block_to_hour" => $block_time->block_to_hour
                    ]);
                }
            }
        }
        Session::flash('message', 'Appointment Schedule Created Successfully.');
        Session::flash('alert-class', 'bg-success');
        return response()->json(['status' => 'success', 'message' => 'Block Times successfully.', 'redirect_url' => url('/all-appointment-schedule')]);
    }

    public function edit($id)
    {
        if(isset($id))
        {
            $appointment = AppointmentSchedule::site()->where('id','=',$id)->first();
            $appointment_times = AppointmentTiming::where('appointment_schedule_id',$id)->get()->toArray();
            $added_days = array();
            foreach ($appointment_times as $key => $value) {
                $added_days[] = $value['day'];
            }
            $rules = AppointmentRule::site()->get();
            return view('appointment.edit')->with('appointment',$appointment)->with('appointment_times', $appointment_times)->with('added_days',$added_days)->with('rules', $rules);
        }
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'rule_id' => 'required',
        ]);

        $attributeNames = array(
            'name' => 'Appointment Schedule Name',
            'rule_id' => 'Appointment Rule',
        );

        $validator->setAttributeNames($attributeNames);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        if(isset($request->id))
        {
            $appointment_schedule = AppointmentSchedule::site()->where('id','=',$request->id)->update([
                'name' => $data['name'],
                'description' => $data['description'],
                'appointment_rule_id' => $data['rule_id'],
            ]);
        }
        
        Session::flash('message', 'Appointment Schedule Updated Successfully.');
        Session::flash('alert-class', 'bg-info');
        return redirect("/all-appointment-schedule");
    }

    public function destroy(Request $request)
    {
        $data = $request->all();
        $site = BaseModel::getcurrentsiteid();
        $site_id = (isset($site)) ? $site->id : NULL;
        if (isset($request->id))
        {
            $documentTypeCount=DocumentType::where('appointment_schedule_id','=',$request->id)->count();
            if($documentTypeCount == 0){
                AppointmentSchedule::site()->where('id','=',$request->id)->delete();
                Session::flash('message', 'Appointment Schedule Deleted Successfully.');
                Session::flash('alert-class', 'bg-danger');
            }else{
                Session::flash('message', "Sorry can't be deleted. This Appointment Schedule has been selected in one of document type.");
                Session::flash('alert-class', 'bg-warning');
            }
        }
        return redirect("/all-appointment-schedule");
    }

    public function viewTiming($id, $day)
    {
        $app_time = AppointmentTiming::where('appointment_schedule_id', $id)->where('day', $day)->first();
        $type = "view";
        if(!empty($app_time)){
            $time_block = AppointmentTimeBlock::where('appointment_timing_id', $app_time->id)->first();
            $time_block_fields = AppointmentTimeBlock::where('appointment_timing_id', $app_time->id)->select('id', 'block_from_hour', 'block_to_hour')->get()->toArray();

            $view = view('appointment.view_edit_render')->with('app_time', $app_time)->with('type', $type)->with('time_block',$time_block)->with('time_block_fields',$time_block_fields)->with('time_interval_block',0);
            echo $view->render();
        }else{
            return ['status' => 'error', 'message' => '! Some Error Occurred.'];
        }
    }
    
    public function addTiming($id, $day)
    {
        $app_time['appointment_schedule_id'] = $id;
        $app_time['id'] = null;
        $app_time['day'] = $day;
        $app_time['from_hour'] = null;
        $app_time['to_hour'] = null;
        $app_time['interval'] = null;
        $app_time['appointment_per_interval'] = null;
        $type = "add";

        $view = view('appointment.view_edit_render')->with('app_time', $app_time)->with('type', $type)->with('time_block_fields',"[]")->with('time_interval_block',0);
        echo $view->render();
    }

    public function editTiming($id, $day)
    {
        $app_time = AppointmentTiming::where('appointment_schedule_id', $id)->where('day', $day)->first();
        $type = "edit";
        if(!empty($app_time)){
            $time_interval_block=0;
            $count_client_appointment=ClientAppointment::where('appointment_timing_id','=',$app_time->id)->count();
            if($count_client_appointment > 0)
            {
               $countClientAppointmentStatus=ClientAppointment::where('appointment_timing_id','=',$app_time->id)
                    ->where('status','!=','Completed_call')->count(); 
                if($countClientAppointmentStatus > 0){
                    $time_interval_block=1;
                }
            }
            $time_block_fields = AppointmentTimeBlock::where('appointment_timing_id', $app_time->id)->select('id', 'block_from_hour', 'block_to_hour')->get()->toJson();
            $view = view('appointment.view_edit_render')->with('app_time', $app_time)->with('type', $type)->with('time_block_fields',$time_block_fields)->with('time_interval_block',$time_interval_block);
            echo $view->render();
        }else{
            return ['status' => 'error', 'message' => '! Some Error Occurred.'];
        }
    }

    public function addEditTimingDay(Request $request)
    {
        $data = $request->all();

        $schedule_id = $data['appointment_schedule_id'];
        $app_time_id = null;
        if(isset($data['app_time_id']) && !empty($data['app_time_id'])){
            $app_time_id = $data['app_time_id'];
        }
        $block_data = array();
        $block = false;
        if(isset($data['block_fields']) && !empty($data['block_fields'])){
            $block_data = json_decode(stripslashes($data['block_fields']));
            $block = true;
        }

        if(!isset($app_time_id) && empty($app_time_id)){
            $app_time = AppointmentTiming::create([
                "appointment_schedule_id" => $schedule_id,
                "day" => $data['day_key'],
                "from_hour" => $data['from_hour'],
                "to_hour" => $data['to_hour'],
                "interval" => $data['interval'],
                "appointment_per_interval" => $data['appointment_per_interval']
            ]);

            if($block){
                foreach ($block_data as $block_time) {
                    AppointmentTimeBlock::create([
                        "appointment_timing_id" => $app_time->id,
                        "block_from_hour" => $block_time->block_from_hour,    
                        "block_to_hour" => $block_time->block_to_hour
                    ]);
                }
            }
        }else{
            $boolean=true;
            $appointment_time_array=[];
            $status_array = ['Completed_call', 'Cancelled'];
            $appoTime = AppointmentTiming::find($app_time_id);
            $client_appointment=ClientAppointment::where('appointment_timing_id','=',$app_time_id)->get();
            $time_array=['01','02','03','04','05','06','07','08','09'];
            $block_time_array=[];
            if(count($client_appointment) > 0)
            {   
                $old_interval = $appoTime->interval;
                $new_interval = $data['interval'];

                if(count($block_data) > 0){
                    //convert 09:30 to 9:30 start
                    foreach ($block_data as $key => $block_value) {
                        $date = strtotime($block_value->block_from_hour);
                        if(in_array(date('H', $date),$time_array)){
                            $hours=date('H', $date);
                            $block_from_hour=ltrim($hours, $hours[0]).':'.date('i', $date).' '.date('a', $date);
                        }else{
                            $block_from_hour=$block_value->block_from_hour;
                        }

                        $to_date = strtotime($block_value->block_to_hour);
                        if(in_array(date('H', $to_date),$time_array)){
                            $to_hours=date('H', $to_date);
                            $block_to_hour=ltrim($to_hours, $to_hours[0]).':'.date('i', $to_date).' '.date('a', $to_date);
                        }else{
                            $block_to_hour=$block_value->block_to_hour;
                        }
                        $block_time_array[]=$block_from_hour.'-'.$block_to_hour;
                    //convert 09:30 to 9:30 end
                    }
                }
                $count=0;
                foreach ($client_appointment as $key => $value) {
                    if (!in_array($value->status, $status_array)) {
                        $time=explode('-',strtolower($value->appointment_time));
                        foreach ($time as $key => $time_value) {
                           if(strtotime($request->from_hour) <= strtotime($time_value) && strtotime($time_value) <= strtotime($request->to_hour)){
                            }else{
                                $boolean=false;
                                return response()->json(['status' => 'error', 'message' => "You Have One or More Client Appointments Existing Can't Update!"]);
                            }
                        }
                        if(count($block_data) > 0){
                            if(in_array(strtolower($value->appointment_time),$block_time_array)){
                                $boolean=false;
                                return response()->json(['status' => 'error', 'message' => "You Have One or More Client Appointments In Block Time Can't Update!"]);
                            }
                            foreach ($block_time_array as $key => $block_times_value) {
                                $block_start_end = explode('-', $block_times_value);
                                $db_time_start_end = explode('-', strtolower($value->appointment_time));

                                if(strtotime($block_start_end[0]) <= strtotime($db_time_start_end[0]) && strtotime($db_time_start_end[0]) < strtotime($block_start_end[1])){
                                    $boolean=false;
                                    return response()->json(['status' => 'error', 'message' => "You Have One or More Client Appointments In Block Time Can't Update!"]);
                                }
                                if(strtotime($block_start_end[0]) < strtotime($db_time_start_end[1]) && strtotime($db_time_start_end[1]) <= strtotime($block_start_end[1])){
                                    $boolean=false;
                                    return response()->json(['status' => 'error', 'message' => "You Have One or More Client Appointments In Block Time Can't Update!"]);
                                }
                            }
                        }
                        $count++;
                    }
                }
                if($count > 0){
                    if($old_interval != $new_interval){
                        $boolean=false;
                        return response()->json(['status' => 'error', 'message' => "You Can't Update Interval Because Client Appointments Exists For Old Interval!"]);
                    }   
                }
                if($request->appointment_per_interval < $count){
                    return response()->json(['status' => 'error', 'message' => "You Have More Than ".$data['appointment_per_interval']." Client Appointments In Interval Can't Update!"]);
                    $boolean=false; 
                }
            }
            if($boolean){
                if(!empty($appoTime)){
                    $appoTime->update([
                        "from_hour" => $data['from_hour'],
                        "to_hour" => $data['to_hour'],
                        "interval" => $data['interval'],
                        "appointment_per_interval" => $data['appointment_per_interval']
                    ]);

                    if($block){
                        $existed_field_ids = [];
                        foreach ($block_data as $value)
                        {
                            if(!isset($value->id)) {
                                $field = AppointmentTimeBlock::create([
                                    "appointment_timing_id" => $appoTime->id,
                                    "block_from_hour" => $value->block_from_hour,    
                                    "block_to_hour" => $value->block_to_hour
                                ]);
                                $existed_field_ids[] = $field->id;
                            } else {
                                $field = AppointmentTimeBlock::updateOrCreate(
                                    [
                                        'appointment_timing_id' => $appoTime->id,
                                        'id' => $value->id
                                    ],
                                    [
                                        "block_from_hour" => $value->block_from_hour,    
                                        "block_to_hour" => $value->block_to_hour
                                    ]
                                );
                                $existed_field_ids[] = $field->id;
                            }
                        }
                        AppointmentTimeBlock::where('appointment_timing_id', $appoTime->id)->whereNotIn('id', $existed_field_ids)->delete();
                    }
                }
            }
        }
        Session::flash('message', 'Appointment Timing Saved Successfully.');
        Session::flash('alert-class', 'bg-info');
        return response()->json(['status' => 'success', 'message' => 'Appointment Time Saved Successfully.', 'redirect_url' => url('/edit-appointment-schedule/'.$schedule_id.'')]);
    }

    public function removeTiming($id, $day)
    {
        $app_time = AppointmentTiming::where('appointment_schedule_id', $id)->where('day', $day)->first();
        if(!empty($app_time)){
            $countClientAppointment=ClientAppointment::where('appointment_timing_id','=',$app_time->id)->count();
            if($countClientAppointment == 0){
                $app_time->delete();
                return ['status' => 'success', 'message' => 'Timing has been removed successfully.'];
            }else{
                return ['status' => 'error', 'message' => "Sorry can't be deleted. This Appointment Schedule has been selected in one of client appointment."];
            }
        }else{
            return ['status' => 'error', 'message' => '! Some Error Occurred.'];
        }
    }

    public function clientAppointment(){
        $users = User::site()->orderBy('firstname')->get();
        $AppointmentSchedule = AppointmentSchedule::site()->get();
        return view('appointment.clientAppointments',compact('users','AppointmentSchedule'));
    }

    public function clientAppointmentJson(Request $request){ 
        $clientAppointmentJson=ClientAppointment::join('client_document_requests as cd','cd.id','=','client_appointment.client_document_request_id')
            ->leftjoin('users as u','u.id','=','client_appointment.assigned_user_id')
            ->join('appointment_timing','appointment_timing.id','=','client_appointment.appointment_timing_id')
            ->join('client_documents', 'client_appointment.client_document_id', '=', 'client_documents.id')
            ->select('cd.reference','cd.client_name','cd.client_surname','client_appointment.appointment_date','client_appointment.appointment_time','cd.phonenumber','cd.client_email','client_appointment.due_date','client_appointment.status','u.firstname','u.lastname','client_appointment.assigned_user_id','client_appointment.id as client_ap_id','appointment_timing.appointment_schedule_id as appointment_schedule_id','client_documents.appointment_note');
        return \DataTables::eloquent($clientAppointmentJson)
            ->filter(function($query) use ($request)
            { 
                if(!empty($request->get('columns')))
                {
                    $searchVal = $request->get('columns');
                    $searchKeyword = $request->get('search');
                    if(isset($searchKeyword))
                    {
                        $request->session()->put('ca_search_keyword', $searchKeyword['value']);
                    }
                    if($request->session()->exists('ca_search_keyword') && !empty($request->session()->get('ca_search_keyword')))
                    {
                        $keyword = $request->session()->get('ca_search_keyword');
                        $query->where(function ($query) use ($keyword) {
                            $query->orWhere('cd.reference', 'like', "%{$keyword}%")
                                ->orWhere('cd.client_name', 'like', "%{$keyword}%")
                                ->orWhere('cd.client_surname', 'like', "%{$keyword}%")
                                ->orWhere('cd.client_email', 'like', "%{$keyword}%")
                                ->orWhere('cd.phonenumber', 'like', "%{$keyword}%");
                        });
                    }

                    if(isset($searchVal[7]) && !empty($searchVal[7]['search']['value']))
                    {
                        $request->session()->put('appointment_user_id', $searchVal[7]['search']['value']);
                    }
                    if($request->session()->exists('appointment_user_id') && !empty($request->session()->get('appointment_user_id')) && ($request->session()->get('appointment_user_id') != "select_user"))
                    {
                        $query->where('client_appointment.assigned_user_id', $request->session()->get('appointment_user_id'));
                    }

                    if(isset($searchVal[8]) && !empty($searchVal[8]['search']['value']))
                    {
                          $request->session()->put('assign_status', $searchVal[8]['search']['value']);
                    }
                    if($request->session()->exists('assign_status') && !empty($request->session()->get('assign_status')) && ($request->session()->get('assign_status') != "assign_user_status"))
                    {
                        if($request->session()->get('assign_status') == "assigned"){
                            $query->whereNotNull('client_appointment.assigned_user_id');
                        }elseif($request->session()->get('assign_status') == "unassigned"){
                            $query->whereNull('client_appointment.assigned_user_id');
                        }
                    }

                    if(isset($searchVal[2])  && !empty($searchVal[2]['search']['value']))
                    {
                        $request->session()->put('ap_due_date', $searchVal[2]['search']['value']);
                    }
                    if($request->session()->exists('ap_due_date') && !empty($request->session()->get('ap_due_date'))){
                        $query->where('client_appointment.appointment_date', '=',$request->session()->get('ap_due_date'));
                    }


                    if(isset($searchVal[6]) && !empty($searchVal[6]['search']['value'])){
                        $request->session()->put('ca_status', $searchVal[6]['search']['value']);
                    }
                    if($request->session()->exists('ca_status') && !empty($request->session()->get('ca_status')) && !empty($request->session()->get('ca_status') != "Select_Status")){
                        $query->where('client_appointment.status', '=',$request->session()->get('ca_status'));
                    }
                    
                    if(isset($searchVal[9]) && !empty($searchVal[9]['search']['value'])){
                        $request->session()->put('appointment_schedule', $searchVal[9]['search']['value']);
                    }
                    if($request->session()->exists('appointment_schedule') && !empty($request->session()->get('appointment_schedule')) && !empty($request->session()->get('appointment_schedule') != "appointment_schedules")){
                        $query->where('appointment_schedule_id', '=',$request->session()->get('appointment_schedule'));
                    }
                }
            })
            ->editColumn('reference', function($data)
            {
                return $data->reference;
            })
            ->editColumn('client_name', function($data)
            {
                return $data->client_name.' '.$data->client_surname;
            })
            ->editColumn('appointment_date', function($data)
            {
                return Carbon::parse($data->appointment_date)->format('d/m/Y');
            })
            ->editColumn('appointment_time', function($data)
            {
                return explode('-',$data->appointment_time)[0];
            })
             ->editColumn('phonenumber', function($data)
            {
                return $data->phonenumber;
            })
            ->editColumn('client_email', function($data)
            {
                return $data->client_email;
            })
            ->editColumn('status', function($data)
            {
                return $data->status;
            })
            ->editColumn('userassign', function($data)
            {
                if($data->assigned_user_id != ""){
                    return $data->firstname.'  '.$data->lastname;
                }else{
                    return "-";
                }
            })
            ->editColumn('assigned_user_id', function($data)
            {
                if($data->assigned_user_id != ""){
                    return $data->firstname.'  '.$data->lastname;
                }else{
                    return "-";
                }
            })
            ->editColumn('appointment_schedule', function($data)
            {
                return $data->appointment_schedule_id;
            })
            ->editColumn('action', function($data)
            {
                $html = '';
                if(\Auth::user()->hasRole('superadmin') || \Auth::user()->hasRole('admin') || (\Auth::user()->hasRole('user') && \Auth::user()->appointment_admin)){
                    if($data->status != "Completed_call" && $data->status != "Cancelled"){
                        $html .= '<a href="javascript:void(0)" class="margin-r-5 btn btn-success assign_user_modal" data-id="'.$data->client_ap_id.'">Assign</i></a>
                        <a href="javascript:void(0)" class="margin-r-5 btn btn-info appointment_status_modal" data-id="'.$data->client_ap_id.'">Status</i></a>
                        <a href="javascript:void(0)" class="margin-r-5 btn btn-danger appointment_time_modal" data-id="'.$data->client_ap_id.'">Reschedule</i></a>';
                    }
                    if(!empty($data->appointment_note)){
                        $html .= '<a href="javascript:void(0)" class="margin-r-5 btn btn-primary appointment_note" data-note="'.$data->appointment_note.'">Note</i></a>';
                    }
                }
                return $html;
            })
            ->rawColumns(['reference','client_name','appointment_date','appointment_time','phonenumber','client_email','status','userassign','assigned_user_id','appointment_schedule','action'])
            ->make(true);
    }
    
    public function clearClientAppoinment(Request $request){
        if($request->session()->exists('search_keyword'))
        {
            $request->session()->forget('search_keyword');
        } 
        if($request->session()->exists('appointment_user_id'))
        {
            $request->session()->forget('appointment_user_id');
        } 
        if($request->session()->exists('assign_status'))
        {
            $request->session()->forget('assign_status');
        } 
        if($request->session()->exists('ap_due_date'))
        {
            $request->session()->forget('ap_due_date');
        } 
        if($request->session()->exists('ca_status'))
        {
            $request->session()->forget('ca_status');
        }
        if($request->session()->exists('appointment_schedule'))
        {
            $request->session()->forget('appointment_schedule');
        }
        return redirect(url('/clientAppointment'));
    }

    public function assign_user_model(Request $request){
        $id=$request->id;

        $client_appointment = ClientAppointment::find($id);
        $client_document = ClientDocument::find($client_appointment->client_document_id);
        $document_type = DocumentType::find($client_document->document_type_id);

        $teams = $document_type->teammembers->pluck('team_id')->toArray(); //Get the teams of document_type
        if(!empty($teams)){
            $team_users = TeamMember::where('member_type','user')->whereIn('team_id',$teams)->pluck('member_id')->toArray(); //Get teams users
            $users = User::site()
                ->join('role_user', 'role_user.user_id', '=', 'users.id')
                ->where('role_user.role_id', 2)
                ->orWhere('role_user.role_id', 3)->where('access_appointment',1)->orWhere('appointment_admin',1)
                ->whereIn('users.id',$team_users)
                ->get();
        }else{
            $users = User::site()
                ->join('role_user', 'role_user.user_id', '=', 'users.id')
                ->where('role_user.role_id', 2)
                ->orWhere('role_user.role_id', 3)->where('access_appointment',1)->orWhere('appointment_admin',1)
                ->get();
        }

        $assigned_user_id=ClientAppointment::where('id','=',$id)->value('assigned_user_id');
        if($assigned_user_id == ""){
            $assigned_user_id="";
        }
        return view('appointment.appointmentAssignedUser',compact('users','id','assigned_user_id'));exit();
    }

    public function assign_user_store(Request $request){
        $client_appointment=ClientAppointment::where('id','=',$request->client_appointment_id)->first();
        $oldUser=$client_appointment->assigned_user_id;
        if(!empty($client_appointment))
        {
            $client = $client_appointment->clientDocumetRequest;
            $site = Site::where('id', '=', $client['site_id'])->first();

            $site_name = env('APP_NAME', 'Fast Dox');
            $site_url = env('APP_URL');
            $site_fullurl = explode("//", $site_url);
            $site_color = '#262fa2';
            $site_id = NULL;
            if (isset($site))
            {
                $site_id = $site->id;
                $site_name = $site->name;
                $site_color = $site->color_code;
                $site_url = $site_fullurl[0]. '//' . $site_name . '.' . $site_fullurl[1];
            }
            $emailfromname = getEmailmsgFromNameBySiteId($site_id);
            \Config::set('mail.from.name', $emailfromname);

            if(!empty($client->lead_id)){
                $lead_source = Leading::find($client->lead_id);
                if(!empty($lead_source)){
                    if(!empty($lead_source->email_sender_name)){
                        $emailfromname = $lead_source->email_sender_name;
                    }
                    if(!empty($lead_source->color)){
                        $site_color = $lead_source->color; 
                    }
                }
            }

            $client_appointment->update([
                'assigned_user_id' => $request->user_id
            ]);

            if($oldUser != $request->user_id){

                event(new AppointmentAssigned($oldUser, $client_appointment));

                $data['appointment'] = $client_appointment;
                $data['assignUser'] = $client_appointment->assignedUser->firstname.'  '.$client_appointment->assignedUser->lastname;
                $data['user'] = $client_appointment->clientDocumetRequest->client_title.' '.$client_appointment->clientDocumetRequest->client_name;
                $data['check'] = 1;
                $data['client'] = $client;
                $data['sitedetails'] = $site;
                $data['sitename'] = $emailfromname;
                $data['sitecolor'] = $site_color;

                $data1['appointment'] = $client_appointment;
                $data1['client'] = $client;
                $data1['sitedetails'] = $site;
                $data1['sitename'] = $emailfromname;
                $data1['sitecolor'] = $site_color;
                $data1['assignUser'] = $client_appointment->assignedUser->firstname.'  '.$client_appointment->assignedUser->lastname;
                $data1['user'] = $client_appointment->clientDocumetRequest->client_title.' '.$client_appointment->clientDocumetRequest->client_name;
                $data1['check'] = 0;

                $mail[0]['email'] = $client_appointment->assignedUser->email;
                $mail[0]['data'] = $data;
                $mail[1]['email'] = $client_appointment->clientDocumetRequest->client_email;
                $mail[1]['data'] = $data1;
                foreach ($mail as $key => $value) {
                    try {
                        Mail::to($value['email'])->send(new ClientAppoinmentAssignTo($value['data']));
                    } catch (Exception $e) {
                        $email_string = "Appointment Assign user/client mail not sent for: ".$client->id. "\r\n \r\n";
                        $email_string .= "Exception Detail:".$e->getMessage();
                        Mail::raw($email_string, function ($message) {
                            $message->to('rainstreamweb.test@gmail.com');
                            $message->subject('FastDox URGENT : Error while sending Appointment Assign user/client.');
                        });
                    }
                }
            }
            if($client_appointment){
                echo "Done";exit;
            }else{
                echo "error";exit;
            }
        }else{
           echo "error";exit;
        }
    }

    public function assign_status_model(Request $request){
        $id=$request->id;
        $client_appointment_status=ClientAppointment::where('id','=',$id)->value('status');
        return view('appointment.appointmentStatusModal',compact('id','client_appointment_status'));exit();
    }

    public function assign_status_store(Request $request){
        $client_appointment=ClientAppointment::where('id','=',$request->client_appointment_id)->first();
        $oldStatus=$client_appointment->status;
        if(!empty($client_appointment))
        {
            $client = $client_appointment->clientDocumetRequest;
            $site = Site::where('id', '=', $client['site_id'])->first();
            $client_appointment->update([
                'status' => $request->status
            ]);

            $site_name = env('APP_NAME', 'Fast Dox');
            $site_url = env('APP_URL');
            $site_fullurl = explode("//", $site_url);
            $site_color = '#262fa2';
            $site_id = NULL;
            if (isset($site))
            {
                $site_id = $site->id;
                $site_name = $site->name;
                $site_color = $site->color_code;
                $site_url = $site_fullurl[0]. '//' . $site_name . '.' . $site_fullurl[1];
            }
            $emailfromname = getEmailmsgFromNameBySiteId($site_id);
            \Config::set('mail.from.name', $emailfromname);

            if(!empty($client->lead_id)){
                $lead_source = Leading::find($client->lead_id);
                if(!empty($lead_source)){
                    if(!empty($lead_source->email_sender_name)){
                        $emailfromname = $lead_source->email_sender_name;
                    }
                    if(!empty($lead_source->color)){
                        $site_color = $lead_source->color; 
                    }
                }
            }

            if($oldStatus != $request->status){
                event(new AppointmentStatusChange($oldStatus, $client_appointment));
                if($request->status != "Reschedule"){
                    $data['appointment'] = $client_appointment;
                    $data['client'] = $client;
                    $data['sitename'] = $emailfromname;
                    $data['sitedetails'] = $site;
                    $data['sitecolor'] = $site_color;
                    $data['oldStatus'] = $oldStatus;
                    $data['newStatus'] = $request->status;
                    try {
                        Mail::to($client_appointment->clientDocumetRequest->client_email)->send(new ClientAppoinmentStatusChange($data));
                    } catch (Exception $e) {
                        $email_string = "Appointment Status Mail Not Sent To Client: ".$client->id. "\r\n \r\n";
                        $email_string .= "Exception Detail:".$e->getMessage();
                        Mail::raw($email_string, function ($message) {
                            $message->to('rainstreamweb.test@gmail.com');
                            $message->subject('FastDox URGENT : Error while sending Appointment Status Change Mail.');
                        });
                    }
                }
            }
            if($client_appointment){
                echo "Done";exit;
            }else{
                echo "error";exit;
            }
        }else{
           echo "error";exit;
        }
    }

    public function  assign_datetime_model(Request $request){
        $id=$request->id;
        $client_appointment=ClientAppointment::where('id','=',$id)->select('appointment_time','appointment_date','appointment_timing_id','due_date')->first();
        $appo_schedule = $client_appointment->AppointmentTiming->AppointmentSchedule;

        $available_days = $appo_schedule->appointment_days()->pluck('day')->toArray();
        $all_days = ["0"=>"sunday", "1"=>"monday", "2"=>"tuesday", "3"=>"wednesday", "4"=>"thursday", "5"=>"friday", "6"=>"saturday"]; 
        $not_available_days = array();
        foreach ($all_days as $day_key => $day_value) {
            if(!in_array($day_value, $available_days)){
                $not_available_days[] = $day_key;
            }
        }
        $not_available_days = implode(',', $not_available_days);
        return view('appointment.appointmentDateTime',compact('id','client_appointment','not_available_days', 'appo_schedule'));exit();
    }

    public function assign_datetime_model_submit(Request $request){
        $time_interval=AppointmentTiming::where('id','=',$request->appoinment_timing_id)->value('appointment_per_interval');
        $appointment_date=Carbon::createFromFormat('Y-m-d', $request->appoinment_date)->format('Y-m-d');

        $client_appointment=ClientAppointment::where('id','=',$request->client_document_id)->first();
        if($client_appointment->appointment_date != $request->appointment_date && $client_appointment->appointment_time != $request->appointment_time){
            if(!empty($time_interval)){
                $FullTimeInterval=ClientAppointment::where('appointment_timing_id','=',$request->appoinment_timing_id)
                            ->where('appointment_date','=',$appointment_date)
                            ->where('appointment_time','=',$request->appointment_time)
                            ->select('appointment_time', \DB::raw('count(*) as total'))
                            ->groupBy('appointment_time')
                            ->get()->toArray();
                if(!empty($FullTimeInterval)){
                    if($FullTimeInterval[0]['total'] >= $time_interval){
                        echo "Sloat already book please select another time sloat";exit();
                    }else{
                        $boolean=true;
                    }
                }else{
                    $boolean=true;
                }
            }else{
                $boolean=true;
            }
        }else{
            $boolean=true;
        }
       
        if($boolean){
            $client_appointment=ClientAppointment::where('id','=',$request->client_document_id)->first();
            $oldTime=$client_appointment->appointment_time;
            $oldDate=$client_appointment->appointment_date;
            if(($client_appointment->appointment_date != $appointment_date) || $client_appointment->appointment_time != $request->appointment_time){
                $status="Reschedule";
            }else{
                $status=$client_appointment->status;
            }
            $client_appointment->update([
                'appointment_date'=>$appointment_date,
                'appointment_time' =>$request->appointment_time,
                'appointment_timing_id'=>$request->appoinment_timing_id,
                'status' => $status
            ]);
            if(($oldDate != $appointment_date) || $client_appointment->appointment_time != $oldTime){
                event(new AppointmentScheduleChange($oldTime,$oldDate,$client_appointment));

                $client = $client_appointment->clientDocumetRequest;
                $site = Site::where('id', '=', $client['site_id'])->first();

                $site_name = env('APP_NAME', 'Fast Dox');
                $site_url = env('APP_URL');
                $site_fullurl = explode("//", $site_url);
                $site_color = '#262fa2';
                $site_id = NULL;
                if (isset($site))
                {
                    $site_id = $site->id;
                    $site_name = $site->name;
                    $site_color = $site->color_code;
                    $site_url = $site_fullurl[0]. '//' . $site_name . '.' . $site_fullurl[1];
                }
                $emailfromname = getEmailmsgFromNameBySiteId($site_id);
                \Config::set('mail.from.name', $emailfromname);

                if(!empty($client->lead_id)){
                    $lead_source = Leading::find($client->lead_id);
                    if(!empty($lead_source)){
                        if(!empty($lead_source->email_sender_name)){
                            $emailfromname = $lead_source->email_sender_name;
                        }
                        if(!empty($lead_source->color)){
                            $site_color = $lead_source->color; 
                        }
                    }
                }
                $data['appointment'] = $client_appointment;
                $data['client'] = $client;
                $data['sitename'] = $emailfromname;
                $data['sitedetails'] = $site;
                $data['sitecolor'] = $site_color;
                $data['oldTime'] = $oldTime;
                $data['newTime'] = $client_appointment->appointment_time;
                $data['oldDate'] = Carbon::createFromFormat('Y-m-d', $oldDate)->format('d/m/Y');
                $data['newDate'] = $request->appoinment_date;
                try {
                    Mail::to($client_appointment->clientDocumetRequest->client_email)->send(new ClientAppoinmentReschedule($data));
                } catch (Exception $e) {
                    $email_string = "Appointment Rescheduled Mail Not Sent To Client: ".$client->id. "\r\n \r\n";
                    $email_string .= "Exception Detail:".$e->getMessage();
                    Mail::raw($email_string, function ($message) {
                        $message->to('rainstreamweb.test@gmail.com');
                        $message->subject('FastDox URGENT : Error while sending Appointment Rescheduled Mail.');
                    });
                }
            }
            echo "Done";exit();
        }else{
            echo "please try again";exit();
        }
    }

    public function appoinmentDateIntervalModal(Request $request){
        $appointment_schedule=AppointmentTiming::where('id','=',$request->appointment_timing_id)->first();
        $time_interwal=AppointmentTiming::where('day','=',$request->day_name)->where('appointment_schedule_id','=',$appointment_schedule->appointment_schedule_id)->first();
        if($time_interwal){
            $appointment_time_array=[];
            $appointment_time_blocks=AppointmentTimeBlock::where('appointment_timing_id','=',$time_interwal->id)->get();
            if($appointment_time_blocks){
                foreach ($appointment_time_blocks as $key => $appointment_time_block) {
                    $appointment_time_array[$key]['start_time']=$appointment_time_block->block_from_hour;
                    $appointment_time_array[$key]['end_time']=$appointment_time_block->block_to_hour;
                }
            }
            $FullTimeInterval=ClientAppointment::where('appointment_timing_id','=',$time_interwal->id)
                ->where('appointment_date','=',$request->selectedDate)
                ->whereNotIn('status', ['Cancelled', 'Completed_call'])
                ->select('appointment_time', \DB::raw('count(*) as total'))
                ->groupBy('appointment_time')
                ->get()->toArray();
            if(empty($FullTimeInterval)){
                $FullTimeInterval=[];
            }
            $at_time="";
            if($request->appoinetment_date == $request->selectedDate){
                $at_time=$request->appoinetment_time;
            }
            echo json_encode(['id'=>$time_interwal->id,'from_hour'=>$time_interwal->from_hour,'to_hour'=>$time_interwal->to_hour,'interval'=>$time_interwal->interval,'appointment_per_interval'=>$time_interwal->appointment_per_interval,'block_time_array'=>$appointment_time_array,'FullTimeInterval'=>$FullTimeInterval,'time_booked'=>$at_time]);exit();
        }else{
            echo "error";exit();
        }
    }

    public function exportClientAppoinment(){
        return Excel::download(new ClientAppointmentExport, 'clientAppointment.xlsx');
    }

    public function myAppointments()
    {
        $token = Uuid::generate();
        $user = User::find(\Auth::user()->id);

        if(empty($user->ical_token)){
            $user->update([
                'ical_token' => $token
            ]);
        }
        return view('appointment.myAppointments');
    }

    public function myAppointmentsJson(Request $request)
    {
        $myAppointment = ClientAppointment::join('client_document_requests as cd','cd.id','=','client_appointment.client_document_request_id')
            ->where('client_appointment.assigned_user_id', \Auth::user()->id)
            ->join('client_documents', 'client_appointment.client_document_id', '=', 'client_documents.id')
            ->select('cd.reference','cd.client_name','cd.client_surname','client_appointment.appointment_date','client_appointment.appointment_time','cd.phonenumber','cd.client_email','client_appointment.status','client_appointment.assigned_user_id','client_appointment.id as client_ap_id', 'client_documents.appointment_note');

        return \DataTables::eloquent($myAppointment)
            ->filter(function($query) use ($request)
            {
                $searchKeyword = $request->get('search');
                if(isset($searchKeyword))
                {
                    $keyword = $searchKeyword['value'];
                }
                if(!empty($keyword)){
                    $query->where(function ($query) use ($keyword) {
                        $query->orWhere('cd.reference', 'like', "%{$keyword}%")
                            ->orWhere('cd.client_name', 'like', "%{$keyword}%")
                            ->orWhere('cd.client_surname', 'like', "%{$keyword}%")
                            ->orWhere('cd.client_email', 'like', "%{$keyword}%")
                            ->orWhere('cd.phonenumber', 'like', "%{$keyword}%");
                    });
                }
            })
            ->editColumn('reference', function($data)
            {
                return $data->reference;
            })
            ->editColumn('client_name', function($data)
            {
                return $data->client_name.' '.$data->client_surname;
            })
            ->editColumn('appointment_date', function($data)
            {
                return Carbon::parse($data->appointment_date)->format('d/m/Y');
            })
            ->editColumn('appointment_time', function($data)
            {
                return explode('-',$data->appointment_time)[0];
            })
             ->editColumn('phonenumber', function($data)
            {
                return $data->phonenumber;
            })
            ->editColumn('client_email', function($data)
            {
                return $data->client_email;
            })
            ->editColumn('status', function($data)
            {
                return $data->status;
            })
            ->editColumn('action', function($data)
            {
                $html = '';
                if($data->status != "Completed_call" && $data->status != "Cancelled"){
                    $html .= '<a href="javascript:void(0)" class="margin-r-5 btn btn-info appointment_status_modal" data-id="'.$data->client_ap_id.'">Status</i></a>';
                }
                if(!empty($data->appointment_note)){
                    $html .= '<a href="javascript:void(0)" class="margin-r-5 btn btn-primary appointment_note" data-note="'.$data->appointment_note.'">Note</i></a>';
                }
                return $html;
            })
            ->rawColumns(['reference', 'client_name', 'appointment_date', 'appointment_time', 'phonenumber', 'client_email', 'status', 'action'])
            ->make(true);
    }

    public function myAppoIcal($token, Request $request)
    {
        $user = User::where('ical_token', $token)->first();
        if(!empty($user)){
            $time_zone = 'Etc/UTC';
            $site = Site::where('id', '=', $user->site_id)->first();
            if(!empty($site)){
                $timezone = $site->timezone;
                if(!empty($timezone)){
                    $time_zone = $timezone->timezone;
                }
            }

            $client_appointments = ClientAppointment::where('assigned_user_id', $user->id)->whereNotIn('status', ['Completed_call', 'Cancelled'])->get();

            if(!empty($client_appointments)){
                $vCalendar = new Calendar('www.example.com');
                $vCalendar->setName('Appointment Calendar');
                $vCalendar->setTimezone($time_zone);

                foreach ($client_appointments as $client_app) {
                    $client = $client_app->clientDocumetRequest;
                    $vEvent = new Event();
                    $vEvent
                        ->setDtStart(new \DateTime($client_app->appointment_date))
                        ->setDtEnd(new \DateTime($client_app->appointment_date))
                        ->setNoTime(true)
                        ->setSummary('Client Appointment')
                        ->setDescription('Appointment Time : '.explode('-', $client_app->appointment_time)[0].'<br>Client Name :'.$client->client_title.' '.$client->client_name.'<br>Client Email :'.$client->client_email.'<br>Client Mobile : '.$client->phonenumber);

                    $vCalendar->addComponent($vEvent);

                    header('Content-Type: text/calendar; charset=utf-8');
                    header('Content-Disposition: attachment; filename="appointment.ics"');
                }
                $ical = $vCalendar->render();
                echo $vCalendar->render();
            }
        }
    }
}
