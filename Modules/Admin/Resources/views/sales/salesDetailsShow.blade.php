@extends('admin::layouts.master')
@section('content')

	<div class="page-title">
      <div class="container-fluid">
        <div class="row title-class">
          <div class="col-lg-4 col-md-4 col-sm-12">
            <h4 class="page-name">Sales Details</h4>
            <h4></h4>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 btngrp">
            <h5 class="page-name">Customer Name: <u>{{$user->first_name.' '.$user->last_name}}</u> </h5>
          </div>
        </div>
      </div>
  </div>
  <div class="m-grid__item m-grid__item--fluid m-wrapper">
      <div class="m-content">
      <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card m-b-20">
                            <div class="card-body">
                                <!--<h4 class="mt-0 header-title">All Events</h4>-->
                               @if(session('success'))
                                <div class="alert alert-success alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                                  {{ session('success') }}
                                </div>
                              @elseif(session('error'))
                                  <div class="alert alert-error alert-dismissib">
                                    <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                                    {{ session('error') }}
                                </div>                              
                              @endif
                                <table id="salesDetails" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                      <tr>
                                        <th>Sales Id</th>
                                        <th>Product Name</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Total</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach($details as $data)
	                                    	<tr>
	                                    		<td>{{$data['sales_id']}}</td>
                                          <td>{{$data['product_name']}}</td>
	                                    		<td>₹{{$data['price']}}</td>
	                                    		<td>{{$data['qty']}}</td>
	                                    		<td>₹{{($data->price)*($data->qty)}}</td>
	                                    	</tr>
                                    	@endforeach
                                    </tbody>
                                     
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
      </div>
    </div>
    </div>

@endsection
@section('PageJS')
<script type="text/javascript">
    jQuery('#salesDetails').dataTable();
</script>
@endsection