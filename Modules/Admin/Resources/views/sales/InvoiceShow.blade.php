<!DOCTYPE html>
<html>
	<head>
		<title>Style Revolver</title>
		<style type="text/css">
		@page  {
            margin: 0cm 0cm;
        }
		.left_div{
			float:left;
			width: 30%;
			display: inline-block;
		}
		header {
                position: fixed;
                top: 0cm;
                left: 0cm;
                right: 0cm;
                height: 3cm;

                /** Extra personal styles **/
                text-align: center;
                line-height: 0.6cm;
            }
        footer {
            position: fixed; 
            bottom: 0cm; 
            left: 0cm; 
            right: 0cm;
            height: 2cm;
        }
        body {
            margin-top: 3cm;
            margin-left: 2cm;
            margin-right: 2cm;
            margin-bottom: 2cm;
        }
		</style>
	</head>
	<body>
	<header>
	    <div class="col-sm-12">
        	<h1 class="page-name"><center>INVOICE</center><br></h1>
		  	<div class="left_div">
		  		<img src="{{public_path('/images/style_logo.jpeg')}}" height="70" style="margin-left:10px">
		 	 </div>
		  	<div class="right_div">
	            <h5 class="page-name" style="text-align: right;"><strong>{{$company->company_name}}</strong>(Boutique Shop)<br>
				{{$company->company_address}}<br>
				Contact No:{{$company->company_contact}}<br>
				Email Address:{{$company->company_email}}</h5>
	        </div>
 	    </div>
	</header>
	<main>	
	 	<table border="2" class="table table-bordered:1;" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
           <thead>
              <tr>
                <th>Product Id</th>
                <th>Product Image</th>
                <th>Product Name</th>
                <th>Color Name</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Total</th>
              </tr>
            </thead>
            <tbody>
            	@foreach($product as $data)
            	<tr>
            		<td>{{$data->id}}</td>
            		<td><img src="{{public_path('/images/'.$data->product_image)}}" height="50" width="50" alt="{{$data->product_name}}"/></td>
            		<td>{{$data->product_name}}</td>
            		<td>{{$data->color_id}}</td>
            		<td>{{$data->product_qty}}</td>
            		<td>{{$data->product_price}}</td>
            		<td>{{($data->product_price)* ($data->product_qty)}}</td>
				</tr>
            	@endforeach
            </tbody>
        </table>
	</main>
	</body>
</html>