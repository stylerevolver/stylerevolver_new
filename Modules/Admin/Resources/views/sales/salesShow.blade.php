@extends('admin::layouts.master')
@section('content')

<div class="page-title">
      <div class="container-fluid">
        <div class="row title-class">
          <div class="col-lg-6 col-md-6 col-sm-12">
            <h4 class="page-name">Sales</h4>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 btngrp">
          </div>
        </div>
      </div>
  </div>
  <div class="m-grid__item m-grid__item--fluid m-wrapper">
      <div class="m-content">
      <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card m-b-20">
                            <div class="card-body">
                                <!--<h4 class="mt-0 header-title">All Events</h4>-->
                               @if(session('success'))
                                <div class="alert alert-success alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                                  {{ session('success') }}
            
                                </div>                              
                              @endif
                                <table id="sales_datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                      <tr>
                                        <th>Id</th>
                                        <th>CustomerName</th>
                                        <th>Contact</th>
                                        <th>Date</th>
                                        <th>Details</th>
                                        <th>Status</th>
                                        <th>SalesPayment</th>
                                        <th>Invoice</th>
                                      </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                     
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
      </div>
    </div>
    </div>
@endsection

@section('PageJS')
<script type="text/javascript">
    jQuery('#sales_datatable').dataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        "scrollX": true,
        ajax: '{{ url("/admin/all-sales-json") }}',
        columns: [
            { data: 'id' , name: 'id' },
            { data: 'first_name', name: 'first_name' },
            { data: 'contact_no', name: 'contact_no' },
            { data: 'sales_date', name:'sales_date'},
            { data: 'details', name: 'details' , orderable: false, searchable:false },
            { data: 'status', name: 'status'},
            { data: 'payment', name: 'payment' , orderable: false, searchable:false },
            { data: 'Invoice', name: 'Invoice' , orderable: false, searchable:false },
        ],
        order: [ [0, 'desc'] ]
    });
</script>
@endsection