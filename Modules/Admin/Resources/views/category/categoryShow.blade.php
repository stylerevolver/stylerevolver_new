@extends('admin::layouts.master')

@section('content')
  
  <div class="page-title">
      <div class="container-fluid">
        <div class="row title-class">
          <div class="col-lg-6 col-md-6 col-sm-12">
            <h4 class="page-name">Category</h4>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 btngrp">
            <a class="btn btn-info" href="{{ url('/admin/addCategory') }}" type="button"><i class="fa fa-plus-circle"></i> Create New</a>
          </div>
        </div>
      </div>
  </div>
  <div class="m-grid__item m-grid__item--fluid m-wrapper">
      <div class="m-content">
      <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card m-b-20">
                            <div class="card-body">
                                <!--<h4 class="mt-0 header-title">All Events</h4>-->
                              @if(session('success'))
                                <div class="alert alert-success alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                                  {{ session('success') }}
                                </div>
                              @elseif(session('error'))
                                  <div class="alert alert-error alert-dismissib">
                                    <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                                    {{ session('error') }}
                                </div>                              
                              @endif
                                <table id="category_datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                      <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Action</th>    
                                      </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                     
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
      </div>
    </div>
    </div>
@endsection

@section('PageJS')
<script type="text/javascript">
    jQuery('#category_datatable').dataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: '{{ url("/admin/all-category-json") }}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'category_name', name: 'category_name' },
            { data: 'action', name: 'action', orderable: false, searchable:false }
        ],
        order: [ [0, 'desc'] ]
  });
</script>
@endsection
