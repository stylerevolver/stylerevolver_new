@extends('admin::layouts.master')
@section('content')

	<div class="page-title">
      <div class="container-fluid">
        <div class="row title-class">
          <div class="col-lg-6 col-md-6 col-sm-12">
            <h4 class="page-name">Customized Product Details</h4>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 btngrp">
          </div>
        </div>
      </div>
  </div>
  <div class="m-grid__item m-grid__item--fluid m-wrapper">
      <div class="m-content">
      <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card m-b-20">
                            <div class="card-body">
                                <!--<h4 class="mt-0 header-title">All Events</h4>-->
                               @if(session('success'))
                                <div class="alert alert-success alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                                  {{ session('success') }}
                                </div>
                              @elseif(session('error'))
                                  <div class="alert alert-error alert-dismissib">
                                    <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                                    {{ session('error') }}
                                </div>                              
                              @endif
                                <table id="customize_product_details" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
<!-- //id,description,price,customize_product_id ,customize_product_details-->
                                    <thead>
                                      <tr>
                                        <th>Id</th>
                                        <th>customize Product Name</th>
                                        <th>Estimate Price</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach($details as $data)
	                                    	<tr>
	                                    		<td>{{$data['id']}}</td>	
	                                    		<td>{{$data['customize_product_name']}}</td>
	                                    		<td>{{$data['price']}}</td>
	                                    		<td>{{$data['description']}}</td>
	                                    		<td><a href="{{url('/admin/updateCustomizedProductDetails/'.$data->id)}}'" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x"><i class="fas fa-edit"></i></a></td>
	                                    	</tr>
                                    	@endforeach
                                    </tbody>
                                     
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
      </div>
    </div>
    </div>

@endsection
@section('PageJS')
<script type="text/javascript">
    jQuery('#customize_product_details').dataTable();
</script>
@endsection