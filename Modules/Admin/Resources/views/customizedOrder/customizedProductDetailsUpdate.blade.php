@extends('admin::layouts.master')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-content">
		<div class="content">
	        <div class="container-fluid">
				<div class="col-md-12">
					<div class="m-portlet m-portlet--tab">
						<div class="page-title">
					    	<div class="container-fluid">
					    		<div class="row title-class">
					    			<div class="col-lg-6 col-md-6 col-sm-12">
					    				<h4 class="page-name">Update Customize Product Details</h4>
					    			</div>
					    		</div>
					    	</div>
					    </div>
					</div>
					
					<div class="row justify-content-center">
						<div class="col-md-10">
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{ url('/admin/updateCustomizedProductDetails') }}" enctype="multipart/form-data">
								{{csrf_field()}}

								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="name" class="col-3 col-form-label">
										Customized Product Name
										</label>
										<div class="col-9">
											<input class="form-control m-input" name="name" id="name" type="text" tabindex="1" value="{{ $customize_product->customize_product_name}}" placeholder="Please enter Product name">
											@error('name')
                                       		<span class="text-danger" role="alert">
                                            	<strong>{{ $message }}</strong>
                                       		</span>
                                     		@enderror
										</div>
									</div>
								</div>
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="image" class="col-3 col-form-label">
										Customized Product	Image
										</label>
										<div class="col-9">
											<img src="{{asset('/images/'.$customize_product->customize_product_image)}}" height="200" width="200" alt="{{ $customize_product->customize_product_name}}">
											
										</div>
									</div>
								</div>

								<div class="m-portlet__body">
										<div class="form-group m-form__group row">
											<label for="description" class="col-3 col-form-label">
											Description
											</label>
											<div class="col-9">
												<textarea tabindex="2" class="form-control m-input" name="description" id="description"  placeholder="">{{ $customize_product->description}}</textarea>
												@error('description')
		                                    		<span class="text-danger" role="alert">
		                                       				<strong>{{ $message }}</strong>
		                                   			</span>
                                   				@enderror
											</div>
										</div>
								</div>


								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="price" class="col-3 col-form-label">
											Estimate Price
										</label>
										<div class="col-9">
											<input class="form-control m-input" name="price" id="price" type="text" tabindex="3" value="{{ $customize_product->price}}"placeholder="">
											@error('price')
	                                   		 <span class="text-danger" role="alert">
	                                   			<strong>{{ $message }}</strong>
	                           		 		</span>
	                             		 	@enderror
										</div>
									</div>
								</div>



								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<div class="row">
											<div class="col-3"></div>
											<div class="col-9">
                        						<input type="hidden" name="pro_image" id="pro_image" value="{{$customize_product->customize_product_image}}">
												<input type="hidden" name="id" value="{{$customize_product->id}}">
												<input type="hidden" name="app_id" value="{{$customize_product->appointment_id}}">
												<button type="submit" class="btn btn-success" tabindex="4">
													Submit
												</button>
												<button type="reset" class="btn btn-secondary" tabindex="5">
                                            		Cancel
                                        		</button>
											<!-- 	<a href="{{url('/admin/customizedProductDetails')}}" class="btn btn-secondary" tabindex='6'>Cancel</a> -->
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('PageJS')
<script type="text/javascript">

</script>
@endsection