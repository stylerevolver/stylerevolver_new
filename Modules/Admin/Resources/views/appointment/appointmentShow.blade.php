@extends('admin::layouts.master')

@section('content')

  <div class="page-title">
    <div class="container-fluid">
      <div class="row title-class">
        <div class="col-lg-6 col-md-6 col-sm-12">
          <h4 class="page-name">Appointment</h4>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 btngrp">
        </div>
      </div>
    </div>
  </div>
  <div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12">
              <div class="card m-b-20">
                <div class="card-body">
                  <!--<h4 class="mt-0 header-title">All Events</h4>-->
                  @if(session('success'))
                    <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                        {{ session('success') }}
                    </div>
                  @elseif(session('error'))
                    <div class="alert alert-error alert-dismissib">
                      <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                      {{ session('error') }}
                    </div>                              
                  @endif
                    <table id="appointment_datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                      <tr>
                                        <th>Id</th>
                                        <th>Customer Name</th>
                                        <th>complete Stastus</th>
                                        <th>Visited Stastus</th>
                                        <th>Appointment Date</th>
                                        <th>Appointment Time</th>
                                        <th>Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                     
                                    </tbody>
                                     
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
      </div>
    </div>
    </div>
@endsection

@section('PageJS')
<script type="text/javascript">
    jQuery('#appointment_datatable').dataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        "scrollX": true,
        ajax: '{{ url("/admin/all-appointment-json") }}',
        columns: [
            { data: 'id' , name: 'id' },
            { data: 'first_name', name: 'first_name' , },
            { data: 'complete_status', name: 'complete_status' },
            { data: 'visited_status', name: 'visited_status' },
            { data: 'appointment_date', name: 'appointment_date' , orderable: false, searchable:false },
            { data: 'appointment_time', name: 'appointment_time' },
            { data: 'action', name: 'action' },
        ],
        order: [ [0, 'desc'] ]
    });
</script>
@endsection