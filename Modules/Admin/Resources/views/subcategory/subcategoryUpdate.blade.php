@extends('admin::layouts.master')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-content">
		<div class="content">
	        <div class="container-fluid">
				<div class="col-md-12">
					<div class="m-portlet m-portlet--tab">
						<div class="page-title">
					    	<div class="container-fluid">
					    		<div class="row title-class">
					    			<div class="col-lg-6 col-md-6 col-sm-12">
					    				<h4 class="page-name">Update Subcategory</h4>
					    			</div>
					    		</div>
					    	</div>
					    </div>
					</div>
					
					<div class="row justify-content-center">
						<div class="col-md-10">
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{ url('/admin/updateSubcategory') }}" enctype="multipart/form-data">
								{{csrf_field()}}
								<!-- @if($errors->any())
						          	<div class="alert alert-danger">
						            	<ul>
						              		@foreach ($errors->all() as $error)
						                		<li>{{ $error }}</li>
						              		@endforeach
						            	</ul>
						          	</div>
						          	<br>
						        @endif -->
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="name" class="col-3 col-form-label">
											Subcategory Name
										</label>
										<div class="col-9">
											<input class="form-control m-input" name="name" id="name" type="text" tabindex="1" value="{{ $subcategory->subcategory_name}}" placeholder="Please enter Subcategory">
											@error('name')
	  	                    				<span class="text-danger" role="alert">
	                                 			<strong>{{ $message }}</strong>
	                              	 		</span>
	                                       	@enderror
										</div>
									</div>
								</div>
								<div class="m-portlet__body">
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-3 col-form-label">
												Category
											</label>
											<div class="col-9">
												<select name="categoryname" id="categoryname" class="form-control m-input m-input--air 	select2_class" tabindex="2" placeholder="Please enter Category name">
													<option></option>
													@foreach($category as $c)
                                                    	<option value="{{$c->id}}" {{ $subcategory->category_id == $c->id ? 'selected="selected"' : '' }}>{{$c->category_name}}</option>
                                                	@endforeach
												</select>
												@error('categoryname')
                                        		<span class="text-danger" role="alert">
                                            		<strong>{{ $message }}</strong>
                                        		</span>
                                        		@enderror
											</div>
										</div>
									</div>


								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<div class="row">
											<div class="col-3"></div>
											<div class="col-9">
												<input type="hidden" name="subcategory_id" value="{{$subcategory->id}}">
												<button type="submit" class="btn btn-success" tabindex="3">
													Submit
												</button>
												<a href="{{url('/admin/subcategory')}}" class="btn btn-secondary" tabindex='4'>Cancel</a>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('PageJS')
<script type="text/javascript">
	jQuery("#categoryname").select2({
	    placeholder: "Select a category",
	    allowClear: true
	});
</script>
@endsection

