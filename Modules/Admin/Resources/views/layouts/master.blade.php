<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Style Revolver Admin</title>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <link rel="shortcut icon" href="{{ asset('images/style_logo.jpeg') }}" />
        <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="{{ asset('admin-asset/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{ asset('css/select2.css')}}">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="{{ asset('admin-asset/css/style.css')}}">
        <link rel="stylesheet" href="{{ asset('admin-asset/css/custom-style.css')}}">

        <link rel="stylesheet" href="{{ asset('admin-asset/css/animate.css')}}">
        <!-- Font Awesome JS -->
       <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
       <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link  href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
        <link  href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
         <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />
         @yield('Pagecss')
       {{-- Laravel Mix - CSS File --}}
       {{-- <link rel="stylesheet" href="{{ mix('css/admin.css') }}"> --}}

    </head>
    <body>

    <div class="wrapper">
        <!-- Sidebar  -->
        @include("admin::layouts.leftmenu")

        <!-- Page Content  -->
        <div id="content">
            @include("admin::layouts.header")
              <div id="find_ajax_loading" style="" class="gif">
                  <div class="loader">
                      <img src="{{asset('/loading.gif')}}" width="200" height="200" >
                  </div>
              </div>
            @yield('content')
            
        </div>
    </div>
    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="{{ asset('admin-asset/js/jquery-3.3.1.slim.min.js')}} "></script>
    <script src="{{ asset('admin-asset/js/jquery.min.js')}}"></script>
    <script src="{{ asset('admin-asset/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('js/select2.min.js')}}"></script>

    <script src="{{ asset('admin-asset/js/jquery.dataTables.min.js')}}"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
     <script type="text/javascript" src="{{asset('admin-asset/js/custom-js.js')}}"></script>
    <script type="text/javascript">
        toastr.options = {
          "closeButton": true,
          "debug": false,
          "newestOnTop": false,
          "progressBar": false,
          "positionClass": "toast-top-right",
          "preventDuplicates": false,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $('#content').toggleClass('active');
            });
            $('#sidebarCollapsem').on('click', function () {
                $('#sidebar').toggleClass('active');
                $('#content').toggleClass('active');
            }); 
        });
        $(window).scroll(function() {
            if ($(this).scrollTop() > 50){  
                $('.navbar').addClass("sticky");
            }
            else{
                $('.navbar').removeClass("sticky");
            }
        });
    </script>
    @yield('PageJS')
</body>
</html>
