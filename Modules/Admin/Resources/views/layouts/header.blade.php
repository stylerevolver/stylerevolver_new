 <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <button type="button" id="sidebarCollapsem" class="btn">
            <!-- <i class="fas fa-align-left"></i> -->
            <i class="fas fa-circle" style="color:#eb1d67"></i>
           {{-- <img src="images/radio.png" class=""> --}}
        </button>
        
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="nav navbar-nav ml-auto">
                                            
                 <li class="user-details">
               <li class="nav-item dropdown u-pro">
                <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ asset('/images/'.Auth::guard('admin')->user()->profile_picture)}}" alt="user" class=""> <span class="hidden-md-down" style="color:#ffffff"><b>{{ Auth::guard('admin')->user()->name}}</b><i class="fa fa-angle-down"></i></span> </a>
                <div class="dropdown-menu dropdown-menu-right animated flipInY">
                    <ul class="dropdown-user">
                        <li>
                           <div class="dw-user-box">
                                <div class="u-img"><img src="{{ asset('/images/'.Auth::guard('admin')->user()->profile_picture)}}" alt="user"></div>
                                <div class="u-text">
                                    <h4>{{ Auth::guard('admin')->user()->name}}</h4>
                                    <p class="text-muted"> {{Auth::guard('admin')->user()->email}}</p><center><a href="{{'/admin/updateprofile'}}" class="btn btn-rounded btn-danger btn-sm">View Profile</a><br><a href="{{ url('/admin/resetPassword')}}" class="btn btn-rounded btn-danger btn-sm">Reset Password</a></center></div>
                            </div>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a class="dropdown-item" href="javascript:void(0)" id="logout"><i class="fa fa-power-off"></i> Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
            </li>
            </ul>
        </div>
    </div>
</nav>