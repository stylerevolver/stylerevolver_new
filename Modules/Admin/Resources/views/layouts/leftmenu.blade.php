<nav id="sidebar">
    <div class="sidebar-header">
        <a href="{{ url('/admin')}}"><h3><img src="{{ asset('/images/style_logo.jpeg') }}" width="40px" height="40px">Style Revolver</h3></a>
        <strong><img src="{{ asset('/images/style_logo.jpeg') }}" width="40px" height="40px"></strong>
    </div>
    <ul class="list-unstyled components">
        <li class="drop-li">
            <a href="{{ url('/home') }}" target="_blank">
              <img src="{{ asset('/images/style_logo.jpeg') }}" width="40px" height="40px" style="margin-right: 10px">   
               <span> View Site</span>
            </a>  
        </li>
    </ul>

    <!--user-->
    <ul class="list-unstyled components">
        <li class="drop-li">
            <a href="#user" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
              
              <i class="fa fa-users fa-lg" aria-hidden="true"> &nbsp; </i>  
               <span> User</span>
            </a>  
             <ul class="collapse list-unstyled custm-li" id="user">
                <li>
                    <a href="{{ url('/admin/user')}}">View User</a>
                </li>
            </ul>                  
        </li>
    </ul>

    <!--company-->
    <ul class="list-unstyled components">
        <li class="drop-li">
            <a href="#company" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
              <i class="fa fa-building-o fa-lg" aria-hidden="true"> &nbsp;</i>   
               <span> Company</span>
            </a>  
            <ul class="collapse list-unstyled custm-li" id="company">
                <li>
                    <a href="{{ url('/admin/addCompany/') }}">Add Company</a>
                </li>
                <li>
                    <a href="{{ url('/admin/company')}}">View Company</a>
                </li>
            </ul>                  
        </li>
    </ul>
    
    <ul class="list-unstyled components">
        <li class="drop-li">
            <a href="#city" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
              <i class="fas fa-city fa-lg" aria-hidden="true"> &nbsp;</i>   
               <span> City</span>
            </a>  
             <ul class="collapse list-unstyled custm-li" id="city">
                <li>
                    <a href="{{ url('/admin/addCity/') }}">Add City</a>
                </li>
                <li>
                    <a href="{{ url('/admin/city')}}">View City</a>
                </li>
            </ul>                  
        </li>
    </ul>
    <!--area-->

    <ul class="list-unstyled components">
        <li class="drop-li">
            <a href="#area" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
              <i class="fa fa-area-chart fa-lg" aria-hidden="true">&nbsp;</i>   
               <span> Area</span>
            </a>  
             <ul class="collapse list-unstyled custm-li" id="area">
                <li>
                    <a href="{{ url('/admin/addArea/') }}">Add Area</a>
                </li>
                <li>
                    <a href="{{ url('/admin/area')}}">View Area</a>
                </li>
            </ul>                  
        </li>
    </ul>

    <!--category-->
    <ul class="list-unstyled components">
        <li class="drop-li">
            <a href="#category" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
              <i class="fa fa-tag fa-lg" aria-hidden="true">&nbsp;</i>   
               <span> Category</span>
            </a>  
             <ul class="collapse list-unstyled custm-li" id="category">      
                <li>
                    <a href="{{ url('/admin/addCategory/') }}">Add Category</a>
                </li>
                <li>
                    <a href="{{ url('/admin/category')}}">View Category</a>
                </li>
            </ul>                  
        </li>
    </ul>

    <!--subcategory-->
    <ul class="list-unstyled components">
        <li class="drop-li">
            <a href="#subcategory" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
              <i class="fa fa-tag fa-lg" aria-hidden="true">&nbsp;</i>   
               <span> SubCategory</span>
            </a>  
             <ul class="collapse list-unstyled custm-li" id="subcategory">
                <li>
                    <a href="{{ url('/admin/addSubcategory/') }}">Add Subcategory</a>
                </li>
                <li>
                    <a href="{{ url('/admin/subcategory')}}">View Subcategory</a>
                </li>
            </ul>                  
        </li>
    </ul>

   <!--size_chart-->
    <ul class="list-unstyled components">
        <li class="drop-li">
            <a href="#size_chart" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
              <i class="fa fa-bar-chart fa-lg" aria-hidden="true">&nbsp;</i>   
               <span> Size Chart</span>
            </a>  
             <ul class="collapse list-unstyled custm-li" id="size_chart">
                <li>
                    <a href="{{ url('/admin/addSizechart/') }}">Add Size</a>
                </li>
                <li>
                    <a href="{{ url('/admin/size_chart')}}">View Size chart</a>
                </li>
            </ul>                  
        </li>
    </ul>

    <!--product-->
    <ul class="list-unstyled components">
        <li class="drop-li">
            <a href="#product" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
              <i class="material-icons fa-lg" aria-hidden="true">grid_on</i>   
               <span> Product</span>
            </a>  
            <ul class="collapse list-unstyled custm-li" id="product">      
                <li>
                    <a href="{{ url('/admin/addProduct/') }}">Add Product</a>
                </li>
                <li>
                    <a href="{{ url('/admin/product')}}">View Product</a>
                </li>
                <li>
                    <a href="{{ url('/admin/multiple_products')}}">Multiple Products</a>
                </li>
            </ul>                  
        </li>
    </ul>
    <!--color-->
    <ul class="list-unstyled components">
        <li class="drop-li">

            <a href="#color" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
              <i class="fa fa-tint fa-lg" aria-hidden="true">&nbsp;</i>   
               <span> Color</span>
            </a>  
             <ul class="collapse list-unstyled custm-li" id="color">      
                <li>
                    <a href="{{ url('/admin/addColor/') }}">Add Color</a>
                </li>
                <li>
                    <a href="{{ url('/admin/color')}}">View Color</a>
                </li>
            </ul>                  
        </li>
    </ul>
    <!--sales-->
    <ul class="list-unstyled components">
        <li class="drop-li">
            <a href="#sales" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
              <i class="fa fa-line-chart fa-lg" aria-hidden="true">&nbsp;</i>   
               <span>Sales</span>
            </a>  
             <ul class="collapse list-unstyled custm-li" id="sales">
                <li>
                    <a href="{{ url('/admin/sales')}}">View Sales</a>
                </li>
            </ul>                  
        </li>
    </ul>
    <!--appointment-->
    <ul class="list-unstyled components">
        <li class="drop-li">
            <a href="#appointment" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
              <i class="fa fa-calendar fa-lg" aria-hidden="true">&nbsp;</i>   
               <span> Appointment</span>
            </a>  
             <ul class="collapse list-unstyled custm-li" id="appointment">
                <li>
                    <a href="{{ url('/admin/Appointments')}}">View Appointment</a>
                </li>
            </ul>                  
        </li>
    </ul>
    <!--offer-->
    <ul class="list-unstyled components">
        <li class="drop-li">
            <a href="#offer" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
              <i class="fa fa-gift fa-lg">&nbsp;</i>   
               <span>Offer</span>
            </a>  
             <ul class="collapse list-unstyled custm-li" id="offer">
                <li>
                    <a href="{{ url('/admin/addOffer/') }}">Add Offer</a>
                </li>
                <li>
                    <a href="{{ url('/admin/offer')}}">View Offer</a>
                </li>
            </ul>                  
        </li>
    </ul>
    <!--payment_type-->
    <ul class="list-unstyled components">
        <li class="drop-li">
            <a href="#payment_type" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
              <i class="fa fa-credit-card fa-lg" aria-hidden="true">&nbsp;</i>   
               <span> Payment type</span>
            </a>  
             <ul class="collapse list-unstyled custm-li" id="payment_type">
                <li>
                    <a href="{{ url('/admin/addPaymentType/') }}">Add Payment type</a>
                </li>
                <li>
                    <a href="{{ url('/admin/paymentType')}}">View Payment type</a>
                </li>
            </ul>                  
        </li>
    </ul>

    <!--feedback--> 
    <ul class="list-unstyled components">
        <li class="drop-li">
            <a href="#feedback" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <i class=" fa fa-commenting-o fa-lg" aria-hidden="true">&nbsp;</i>
                <span>Feedback</span>
            </a>
            <ul class="collapse list-unstyled custm-li" id="feedback">
                <li>
                    <a href="{{ url('/admin/feedback')}}">View Feedback</a>
                </li>
            </ul>
        </li>
    </ul>
    
    <!--rating-->
    <ul class="list-unstyled components">
        <li class="drop-li">
            <a href="#rating" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <i class="fa fa-star-half-o fa-lg" aria-hidden="true">&nbsp;</i>
                <span>Rating</span>
            </a>
            <ul class="collapse list-unstyled custm-li" id="rating">
                <li>
                    <a href="{{ url('/admin/rating')}}">View Rating</a>
                </li>
            </ul>
        </li>
    </ul>

    <!--complain-->
    <ul class="list-unstyled components">
        <li class="drop-li">
            <a href="#complain" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <i class="fa fa-comments-o fa-lg" aria-hidden="true">&nbsp;</i>
                <span>Complain</span>
            </a>
            <ul class="collapse list-unstyled custm-li" id="complain">
                <li>
                    <a href="{{ url('/admin/complain')}}">View Complain</a>
                </li>
            </ul>
        </li>
    </ul>

<!--new reports-->

    <ul class="list-unstyled components">
        <li class="drop-li">
            <a href="#report" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <i class="fa fa-sticky-note-o fa-lg" aria-hidden="true">&nbsp;</i>
                <span>Reports</span>
            </a>
            <ul class="collapse list-unstyled custm-li" id="report">
                <li>
                    <a href="{{ url('/admin/rating_report')}}">Rating</a>
                    <a href="{{ url('/admin/feedbackReport')}}">Feedback</a>
                    <a href="{{ url('/admin/customeComplainReport')}}">Complain</a>
                    <a href="{{ url('/admin/customerReport')}}">Customer</a>
                    <a href="{{ url('/admin/customAppointment')}}">Appointment</a>
                    <a href="{{ url('/admin/salesreportshow')}}">Sales</a>
                    <a href="{{ url('/admin/productReportShow')}}">Product</a>
                    <a href="{{ url('/admin/CustomizeProduct')}}">Customize Product</a>
                </li>
            </ul>
        </li>
    </ul>

    
</nav>