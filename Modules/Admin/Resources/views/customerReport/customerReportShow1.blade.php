@extends('admin::layouts.master')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <div class="content">
            <div class="container-fluid">
                <div class="col-md-12">
                    <div class="m-portlet m-portlet--tab">
                        <div class="page-title">
                            <div class="container-fluid">
                                <div class="row title-class">
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <h4 class="page-name">Customer Report</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-10">
                            <form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{ url('/admin/customeCustomerReport') }}" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-3 col-form-label">
                                        Area Wise
                                        </label>
                                        <div class="col-9">
                                           <select name="areaname" id="areaname" class="form-control m-input m-input--air select2_class areaname" tabindex="1">
                                                <option></option>
                                                @foreach($area as $a)
                                                    <option value="{{$a->id}}">{{$a->area_name.' '.$a->pincode}}</option>
                                                @endforeach
                                            </select>
                                            @error('areaname')
                                             <span class="text-danger" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-3 col-form-label">
                                        Month Wise
                                        </label>
                                        <div class="col-9">
                                           <select name="monthname" id="monthname" class="form-control m-input m-input--air select2_class monthname" tabindex="2">
                                            <option>
                                                    <option value="1">January</option>
                                                    <option value="2">Febaury</option>
                                                    <option value="3">March</option>
                                                    <option value="4">April</option>
                                                    <option value="5">May</option>
                                                    <option value="6">June</option>
                                                    <option value="7">July</option>
                                                    <option value="8">August</option>
                                                    <option value="9">September</option>
                                                    <option value="10">October</option>
                                                    <option value="11">November</option>
                                                    <option value="12">December</option>
                                            </option>
                                            </select>
                                            @error('monthname')
                                             <span class="text-danger" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <div class="row">
                                            <div class="col-3"></div>
                                            <div class="col-9">
                                                <button type="submit" name='view' class="btn btn-secondary" tabindex="" value="view">View</button>
                                                <button type="submit" class="btn btn-success" tabindex="" name="download" value="download">Download</button>
                                                <button type="button" class="btn btn-warning" tabindex="6"  onClick="window.location.reload();">Clear All</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('PageJS')
<script type="text/javascript">
    jQuery("#areaname").select2({
        placeholder: "Select a area name",
        allowClear: true
    });
    jQuery("#monthname").select2({
        placeholder: "Select a month name",
        allowClear: true
    });
</script>
@endsection