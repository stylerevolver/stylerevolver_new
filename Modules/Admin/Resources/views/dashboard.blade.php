@extends('admin::layouts.master')

@section('content')
<div class="page-title">
  <div class="container-fluid">
    <div class="row title-class">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <h4 class="page-name">Dashboard</h4>
      </div>
    </div>
  </div>
</div>
<div class="m-content">
    <div class="m-portlet">
        @if(session('success'))
        <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
          {{ session('success') }}
        </div>
        @endif
        <div class="main-page-content">
            <div class="container-fluid">
                <div class="infobar">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                            <div class="infobarin">
                                <h2>{{ $product }}</h2>
                                <a href="{{url('/admin/product')}}" class="but-hover1 item_add Sh"><p>Products</p></a><br>
                                
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                            <div class="infobarin infobarin2">
                                <h2>{{ $offer }}</h2>
                                <a href="{{url('/admin/offer')}}" class="but-hover1 item_add Sh"><p>Offer</p></a><br>
                                
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                            <div class="infobarin">
                                <h2>{{ $userCount }}</h2>
                                <a href="{{url('/admin/user')}}" class="but-hover1 item_add Sh"><p>Customer</p></a><br>
                                
                            </div>
                       </div>
                        {{-- <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                            <div class="infobarin">
                                <h2>{{ $salescount }}</h2>
                                <a href="{{url('/admin/user')}}" class="but-hover1 item_add Sh"><p>Sales</p></a><br>
                            </div>
                        </div> --}}
                        <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                            <div class="infobarin">
                                <h2>{{ $complain }}</h2>
                                <a href="{{url('/admin/complain')}}" class="but-hover1 item_add Sh"><p>Complain</p></a><br>
                                
                            </div>
                        </div> 
                        <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                            <div class="infobarin infobarin2">
                                <h2>{{ $appointment }}</h2>
                                <a href="{{url('/admin/Appointments')}}" class="but-hover1 item_add Sh"><p>Appoinment</p></a><br>
                            </div>
                        </div>
                    </div>
                </div>       
            </div>
        </div>
    </div>
</div>
@endsection
@section('Page-Js')
@endsection
