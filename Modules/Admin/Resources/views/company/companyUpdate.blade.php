@extends('admin::layouts.master')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-content">
		<div class="content">
	        <div class="container-fluid">
				<div class="col-md-12">
					<div class="m-portlet m-portlet--tab">
						<div class="page-title">
					    	<div class="container-fluid">
					    		<div class="row title-class">
					    			<div class="col-lg-6 col-md-6 col-sm-12">
					    				<h4 class="page-name">Update Company</h4>
					    			</div>
					    		</div>
					    	</div>
					    </div>
					</div>
					
					<div class="row justify-content-center">
						<div class="col-md-10">
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{ url('/admin/updateCompany') }}" enctype="multipart/form-data">
								{{csrf_field()}}
								<!-- @if($errors->any())
						          	<div class="alert alert-danger">
						            	<ul>
						              		@foreach ($errors->all() as $error)
						                		<li>{{ $error }}</li>
						              		@endforeach
						            	</ul>
						          	</div>
						          	<br>
						        @endif -->
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="name" class="col-3 col-form-label">
											Company Name
										</label>
										<div class="col-9">
											<input class="form-control m-input" name="name" id="name" type="text" tabindex="1" value="{{ $company->company_name}}" placeholder="Please enter Company name">
											@error('name')
	  	                    				<span class="text-danger" role="alert">
	                                 			<strong>{{ $message }}</strong>
	                              	 		</span>
	                                       	@enderror
										</div>
									</div>
								</div>

								<div class="form-group m-form__group row">
										<label for="address" class="col-3 col-form-label">
											Company Address
										</label>
										<div class="col-9">
											<textarea class="form-control m-input" name="address" id="address"  tabindex="2" placeholder="Please enter commpany address">{{ $company->company_address}}</textarea>
											@error('address')
	  	                    				<span class="text-danger" role="alert">
	                                 			<strong>{{ $message }}</strong>
	                              	 		</span>
	                                       	@enderror
										</div>	
								</div>

									<div class="form-group m-form__group row">
										<label for="email" class="col-3 col-form-label">
											Company Email
										</label>
										<div class="col-9">
											<input class="form-control m-input" name="email" id="email" type="email" value="{{ $company->company_email}}" tabindex="3" placeholder="Please enter Email Id">
											@error('email')
	  	                    				<span class="text-danger" role="alert">
	                                 			<strong>{{ $message }}</strong>
	                              	 		</span>
	                                       	@enderror
										</div>
									</div>

									<div class="m-portlet__body">
											<div class="form-group m-form__group row">
												<label for="example-text-input" class="col-3 col-form-label">
												Contact
												</label>
												<div class="col-9">
													<input type="number" tabindex="4" class="form-control m-input" name="contact" value="{{ $company->company_contact}}" placeholder="Please enter Contact">
													@error('contact')
                                        			<span class="text-danger" role="alert">
                                           				<strong>{{ $message }}</strong>
                                       				</span>
                                       				@enderror
												</div>
											</div>
									</div>

									<div class="m-portlet__body">
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-3 col-form-label">
												Area Name
											</label>
											<div class="col-9">
												<select name="areaname" id="areaname" class="form-control m-input m-input--air 	select2_class" tabindex="5" placeholder="Please enter Area name">
													<option></option>
													@foreach($area as $a)
                                                    	<option value="{{$a->id}}" {{ $company->area_id == $a->id ? 'selected="selected"' : '' }}>{{$a->area_name}}</option>
                                                	@endforeach
												</select>
												@error('areaname')
	                                        	<span class="text-danger" role="alert">
	                                            	<strong>{{ $message }}</strong>
	                                        	</span>
                                        		@enderror
											</div>
										</div>
									</div>


								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<div class="row">
											<div class="col-3"></div>
											<div class="col-9">
												<input type="hidden" name="company_id" value="{{$company->id}}">
												<button type="submit" class="btn btn-success" tabindex="6">
													Submit
												</button>
												<a href="{{url('/admin/company')}}" class="btn btn-secondary" tabindex='7'>Cancel</a>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('PageJS')
<script type="text/javascript">
	jQuery("#areaname").select2({
		placeholder: "Select an Area",
		allowClear: true
	});
</script>>
@endsection