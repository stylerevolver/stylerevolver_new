@extends('admin::layouts.master')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <div class="content">
            <div class="container-fluid">
                <div class="col-md-12">
                    <div class="m-portlet m-portlet--tab">
                        <div class="page-title">
                            <div class="container-fluid">
                                <div class="row title-class">
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <h4 class="page-name">Customize Product Report</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-10">
                            <form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{ url('/admin/CustomizeProduct') }}" enctype="multipart/form-data">
                                {{csrf_field()}}

                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-3 col-form-label">
                                        User Wise
                                        </label>
                                        <div class="col-9">
                                           <select name="custname" id="custname" class="form-control m-input m-input--air select2_class custname" tabindex="1">
                                                <option value=""></option>
                                                @foreach($users->unique('first_name') as $u)
                                                    <option value="{{$u->id}}">{{$u->first_name .' '.$u->last_name }}</option>
                                                @endforeach
                                            </select>
                                            @error('custname')
                                             <span class="text-danger" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-3 col-form-label">
                                        Appointment Wise
                                        </label>
                                        <div class="col-9">
                                           <select name="appointmentdate" id="appointmentdate" class="form-control m-input m-input--air select2_class appointmentdate" tabindex="1">
                                                <option value=""></option>
                                                @foreach($app as $a)
                                                    <option value="{{$a->id}}">Date : {{$a->appointment_date }}</option>
                                                @endforeach
                                            </select>
                                            @error('appointmentdate')
                                             <span class="text-danger" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                            
                               
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <div class="row">
                                            <div class="col-3"></div>
                                            <div class="col-9">
                                                <button type="submit" name='view' class="btn btn-secondary" tabindex="" value="view">View</button>
                                                <button type="submit" class="btn btn-success" tabindex="" name="download" value="download">Download</button>
                                                <button type="button" class="btn btn-warning" tabindex="6"  onClick="window.location.reload();">Clear All</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('PageJS')
<script type="text/javascript">
    jQuery("#custname").select2({
        placeholder: "Select a user name",
        allowClear: true
    });
    jQuery("#appointmentdate").select2({
        placeholder: "Select Appointment Date",
        allowClear: true
    });
    
 
</script>
@endsection