@extends('admin::layouts.master')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <div class="content">
            <div class="container-fluid">
                <div class="col-md-12">
                    <div class="m-portlet m-portlet--tab">
                        <div class="page-title">
                            <div class="container-fluid">
                                <div class="row title-class">
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <h4 class="page-name">Product Report</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-10">
                            <form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{ url('/admin/productReportShow') }}" enctype="multipart/form-data">
                                {{csrf_field()}}
                            
                            
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group row">
                                        <label for="categoryname" class="col-3 col-form-label">
                                            Category
                                        </label>
                                        <div class="col-9">
                                            <select name="categoryname" id="categoryname" class="form-control m-input m-input--air select2_class categoryname" tabindex="1">
                                             <option></option>
                                             @foreach($category as $c)
                                                <option value="{{ $c->id }}">{{ $c->category_name }}</option>
                                             @endforeach    
                                            </select>
                                            @error('categoryname')
                                            <span class="text-danger" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group row">
                                        <label for="subcategoryname" class="col-3 col-form-label">
                                            Subcategory
                                        </label>
                                        <div class="col-9">
                                            <select name="subcategoryname" id="subcategoryname" class="form-control m-input m-input--air select2_class subcategoryname" tabindex="2">
                                             <option></option>
                                             @foreach($subcategory as $s)
                                                <option value="{{ $s->id }}">{{ $s->subcategory_name }}</option>
                                             @endforeach    
                                            </select>
                                            @error('subcategoryname')
                                            <span class="text-danger" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                



                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <div class="row">
                                            <div class="col-3"></div>
                                            <div class="col-9">
                                                <button type="button" class="btn btn-secondary" name="mostsales" tabindex="3" value="mostsales">Most Sales</button>
                                                <button type="submit" name='view' class="btn btn-secondary" tabindex="4" value="view">View</button>
                                                <button type="submit" class="btn btn-success" tabindex="5" name="download" value="download">Download</button>
                                                <button type="button" class="btn btn-warning" tabindex="6"  onClick="window.location.reload();">Clear All</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('PageJS')
<script type="text/javascript">
    jQuery("#subcategoryname").select2({
        placeholder: "Select a Subcategory",
        allowClear: true
    });
    jQuery("#categoryname").select2({
        placeholder: "Select a Category",
        allowClear: true
    });
 
</script>
@endsection