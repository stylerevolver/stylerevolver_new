@extends('admin::layouts.master')
@section('content')
<div class="m-grid_item m-grid_item--fluid m-wrapper">
    <div class="m-content">
        <div class="content">
            <div class="container-fluid">
                <div class="col-md-12">
                    <div class="m-portlet m-portlet--tab">
                        <div class="page-title">
                            <div class="container-fluid">
                                <div class="row title-class">
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <h4 class="page-name">Rating Report</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-10">
                            <form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{ url('/admin/customRatingReport') }}" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-3 col-form-label">
                                        User Name Wise
                                        </label>
                                        <div class="col-9">
                                           <select name="username" id="username" class="form-control m-input m-input--air select2_class username" tabindex="1">
                                                <option></option>
                                                @foreach($users as $u)
                                                    <option value="{{$u->user->id}}">{{$u->user->first_name.' '.$u->user->last_name}}</option>
                                                @endforeach
                                            </select>
                                            @error('username')
                                             <span class="text-danger" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-3 col-form-label">
                                        Product Name Wise
                                        </label>
                                        <div class="col-9">
                                           <select name="productname" id="productname" class="form-control m-input m-input--air select2_class username" tabindex="2">
                                                <option></option>
                                                @foreach($product as $r)
                                                    <option value="{{$r->product->id}}">{{$r->product->product_name}}</option>
                                                @endforeach
                                            </select>
                                            @error('productname')
                                             <span class="text-danger" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-3 col-form-label">
                                        Rating From Date
                                        </label>
                                        <div class="col-9">
                                           <select name="rating_from_date" id="rating_from_date" class="form-control m-input m-input--air select2_class username" tabindex="3">
                                                <option></option>
                                                @foreach($rating_date as $rd)
                                                    <option value="{{$rd->rating_date}}">{{date('d-m-Y', strtotime($rd->rating_date))}}</option>
                                                @endforeach
                                            </select>
                                            @error('rating_from_date')
                                             <span class="text-danger" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-3 col-form-label">
                                        Rating To Date
                                        </label>
                                        <div class="col-9">
                                           <select name="rating_to_date" id="rating_to_date" class="form-control m-input m-input--air select2_class username" tabindex="4">
                                                <option></option>
                                                @foreach($rating_date as $rd)
                                                    <option value="{{$rd->rating_date}}">{{date('d-m-Y', strtotime($rd->rating_date))}}</option>
                                                @endforeach
                                            </select>
                                            @error('rating_to_date')
                                             <span class="text-danger" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet_foot m-portlet_foot--fit">
                                    <div class="m-form__actions">
                                        <div class="row">
                                            <div class="col-3"></div>
                                            <div class="col-9">
                                                <button type="submit" name='view' class="btn btn-secondary" tabindex="5" value="view">View</button>
                                                <button type="submit" class="btn btn-success" tabindex="6" name="download" value="download">Download</button>
                                                <button type="button" class="btn btn-warning" tabindex="7"  onClick="window.location.reload();">Clear All</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('PageJS')
<script type="text/javascript">
    jQuery("#username").select2({
        placeholder: "Select a user name",
        allowClear: true
    });
    jQuery("#productname").select2({
        placeholder: "Select a product name",
        allowClear: true
    }); 
    jQuery("#rating_from_date").select2({
        placeholder: "Select a from date",
        allowClear: true
    });
    jQuery("#rating_to_date").select2({
        placeholder: "Select a to date",
        allowClear: true
    });
</script>
@endsection