@extends('admin::layouts.app')

@section('content')
	<div class="loginmain">
   		<div class="container">
   			<div class="logincon">
	   			<div class="loginin" style="background-image: url( {{ asset('/admin-asset/images/login_back.jpeg') }} );">
	   				<div class="loginlogo">
	   					<img src="{{asset('/images/style_logo.jpeg')}}">
	   					<h2>Login</h2>
	   				</div>
					@if ($errors->any())
		                <div class="col-lg-12">
		                    <div class="alert alert-danger">
		                        <ul>
		                            @foreach ($errors->all() as $error)
		                                <li>{{ $error }}</li>
		                            @endforeach
		                        </ul>
		                    </div>
		                </div>
		            @endif
					@if(session('success'))
                        <div class="alert alert-success alert-dismissible">
                          <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                          {{ session('success') }}
                        </div>
                 	@elseif(session('error'))
                        <div class="alert alert-danger alert-dismissib">
                            <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                            {{ session('error') }}
                        </div>                              
                    @endif
					<form action="{{ route('admin.auth.loginCheck') }}"  method="POST" class="admin_log_form">
						@csrf
						<div class="form-group">
						    <input type="email" class="form-control" id="email" name="email" placeholder="Email">
						</div>
						<div class="form-group">
						    <input type="password" class="form-control" id="pwd" placeholder="Password" name="password">
						</div>

						<div class="form-group">
                        	<center><button type="submit" class="btn btn-primary">Submit</button></center>
                        </div>
						<div class="form-group">
                        	<a class="btn btn-success custom_btn_log" href="{{  url('/admin/forgotPassword') }}">{{ __('Forgot Your Password?') }}</a>
                        	<a class="btn btn-success custom_btn_log" href="{{  url('/home') }}">Go To Home</a>
                        </div>
					</form>
				</div>
	   		</div>
   		</div>
   </div>
<style type="text/css">
.m-login__logo a img {
    width: 45%;
}
</style>
@endsection