@extends('admin::layouts.master')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-content">
		<div class="content">
	        <div class="container-fluid">
				<div class="col-md-12">
					<div class="m-portlet m-portlet--tab">
						<div class="page-title">
					    	<div class="container-fluid">
					    		<div class="row title-class">
					    			<div class="col-lg-6 col-md-6 col-sm-12">
					    				<h4 class="page-name">Update Color</h4>
					    			</div>
					    		</div>
					    	</div>
					    </div>
					</div>
					
					<div class="row justify-content-center">
						<div class="col-md-10">
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{ url('/admin/updateColor') }}" enctype="multipart/form-data">
								{{csrf_field()}}
								<!-- @if($errors->any())
						          	<div class="alert alert-danger">
						            	<ul>
						              		@foreach ($errors->all() as $error)
						                		<li>{{ $error }}</li>
						              		@endforeach
						            	</ul>
						          	</div>
						          	<br>
						        @endif -->
						        <div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="colorcolorname" class="col-3 col-form-label">
											Color Name
										</label>
										<div class="col-9">
											<input class="form-control m-input" name="colorname" id="colorname" type="text" value="{{ $color->color_name }}" tabindex="1" placeholder="Please enter Color name">
											@error('colorname')
	  	                    				<span class="text-danger" role="alert">
	                                 			<strong>{{ $message }}</strong>
	                              	 		</span>
	                                       	@enderror
										</div>
									</div>
								</div> 
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="name" class="col-3 col-form-label">
											Color
										</label>
										<div class="col-9">
											<input class="form-control m-input" name="name" id="name" type="color" tabindex="1" value="{{ $color->color_code}}" placeholder="Please enter Color name">
											@error('name')
	  	                    				<span class="text-danger" role="alert">
	                                 			<strong>{{ $message }}</strong>
	                              	 		</span>
	                                       	@enderror
										</div>
									</div>
								</div>
								<!-- <div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="image" class="col-3 col-form-label">
											Color Image
										</label>
										<div class="col-9">
											<input class="form-control m-input" name="image" id="image" type="file" tabindex="2" value="{{ $color->color_image}}"  placeholder="Please enter Color image">
											@error('image')
	  	                    				<span class="text-danger" role="alert">
	                                 			<strong>{{ $message }}</strong>
	                              	 		</span>
	                                       	@enderror
										</div>
									</div>
								</div> -->
								<div class="col-md-6">
								</div>
								<!-- <div class="col-md-6">
									<img src="{{asset('/images/'.$color->color_image)}}" height="150" width="150">
								</div> -->
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<div class="row">
											<div class="col-3"></div>
											<div class="col-9">
												<input type="hidden" name="color_id" value="{{$color->id}}">
												<input type="hidden" name="color_image_name" value="{{$color->color_image}}">
												<button type="submit" class="btn btn-success" tabindex="3">
													Submit
												</button>
												<a href="{{url('/admin/color')}}" class="btn btn-secondary" tabindex='4'>Cancel</a>												
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection


