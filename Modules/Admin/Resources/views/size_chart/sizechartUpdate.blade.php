@extends('admin::layouts.master')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-content">
		<div class="content">
	        <div class="container-fluid">
				<div class="col-md-12">
					<div class="m-portlet m-portlet--tab">
						<div class="page-title">
					    	<div class="container-fluid">
					    		<div class="row title-class">
					    			<div class="col-lg-6 col-md-6 col-sm-12">
					    				<h4 class="page-name">Update Size Chart</h4>
					    			</div>
					    		</div>
					    	</div>
					    </div>
					</div>
					
					<div class="row justify-content-center">
						<div class="col-md-10">
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{ url('/admin/updateSizechart') }}" enctype="multipart/form-data">
								{{csrf_field()}}
								<!-- @if($errors->any())
						          	<div class="alert alert-danger">
						            	<ul>
						              		@foreach ($errors->all() as $error)
						                		<li>{{ $error }}</li>
						              		@endforeach
						            	</ul>
						          	</div>
						          	<br>
						        @endif -->
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="name" class="col-3 col-form-label">
											Size Name
										</label>
										<div class="col-9">
											<input class="form-control m-input" name="name" id="name" type="text" tabindex="1" value="{{ $size_chart->size_name}}" placeholder="Enter size name">
										</div>
									</div>
								</div>

								<div class="form-group m-form__group row">
										<label for="length" class="col-3 col-form-label">
											Length
										</label>
										<div class="col-9">
											<input class="form-control m-input" name="length" id="length" type="text" value="{{ $size_chart->length}}" tabindex="2" placeholder="Please enter Waist">
										</div>
								</div>

								<div class="form-group m-form__group row">
										<label for="waist" class="col-3 col-form-label">
											Waist
										</label>
										<div class="col-9">
											<input class="form-control m-input" name="waist" id="waist" type="text" value="{{ $size_chart->waist}}" tabindex="3"placeholder="Please enter Waist">
										</div>
								</div>

								<div class="form-group m-form__group row">
										<label for="" class="col-3 col-form-label">
											Chest
										</label>
										<div class="col-9">
											<input class="form-control m-input" name="chest" id="chest" type="text" value="{{ $size_chart->chest}}" tabindex="4" placeholder="Please enter chest">
										</div>
								</div>
								<div class="form-group m-form__group row">
										<label for="" class="col-3 col-form-label">
											Hips
										</label>
										<div class="col-9">
											<input class="form-control m-input" name="hips" id="hips" type="text" value="{{ $size_chart->hips}}" tabindex="5" placeholder="Please enter Hips">
										</div>
								</div>
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="subcategory" class="col-3 col-form-label">Subcategory</label>
										<div class="col-9">
											<select name="subcategory" id="subcategory" class="form-control m-input m-input--air select2_class subcategory" tabindex="6" placeholder="Please enter Subcategory">
												<option></option>
												@foreach($subcategory as $s)
                                                    <option value="{{$s->id}}" {{$size_chart->subcategory_id == $s->id ? 'selected="selected"' : '' }}>{{$s->subcategory_name}}</option>
                                                @endforeach
											</select>
											@error('subcategory')
                                       		<span class="text-danger" role="alert">
                                            	<strong>{{ $message }}</strong>
                                       		</span>
                                     		@enderror
										</div>
									</div>
								</div>

								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<div class="row">
											<div class="col-3"></div>
											<div class="col-9">
												<input type="hidden" name="sizechart_id" value="{{$size_chart->id}}">
												<button type="submit" class="btn btn-success" tabindex="6">
													Submit
												</button>
												<a href="{{url('/admin/size_chart')}}" class="btn btn-secondary" tabindex='7'>Cancel</a>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('PageJS')
<script type="text/javascript">
	jQuery(".subcategory").select2({
		placeholder: "Select a Subcategory",
		allowClear: true
	});
</script>
@endsection