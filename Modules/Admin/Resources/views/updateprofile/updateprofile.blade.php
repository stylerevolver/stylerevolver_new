@extends('admin::layouts.master')
@section('content')

<div class="page-title">
    <div class="container-fluid">
        <div class="row title-class">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <h4 class="page-name">Update Profile</h4>
            </div>
        </div>
    </div>
</div>
 <div>
    @if(session('success'))
        <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
          {{ session('success') }}
        </div>
    @elseif(session('error'))
        <div class="alert alert-error alert-dismissib">
            <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
            {{ session('error') }}
        </div>                              
    @endif
    <div class="container">
        <form method="post" action="{{ url('/admin/updatepro') }}" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-12 login-do1">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <label for="name" class="col-3 col-form-label">
                                Name
                            </label>
                            <div class="col-9">
                                <input class="form-control m-input" name="name" id="name" type="text" value="{{$updateprofile->name}}" tabindex="1"placeholder="Please enter User name">
                                @error('name')
                                    <span class="text-danger" role="alert">
                                      <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Email</label>
                                <div class="col-9">
                                    <input type="email" class="form-control m-input" name="email" value="{{$updateprofile->email }}" tabindex="2" placeholder="Please enter Email id">
                                    @error('email')
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <label for="image" class="col-3 col-form-label">Profile picture</label>
                                <div class="col-9">
                                    <input type="file" class="form-control m-input" name="image" id="image" tabindex="3" value="{{ $updateprofile->profile_picture}}"  placeholder="Add profile picture">
                                    @error('image')
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                          <img src="{{asset('/images/'.$updateprofile->profile_picture)}}" height="250" width="250" alt="{{$updateprofile->name}}">
                          <input type="hidden" name="adminId" id="adminId" value="{{\Auth::guard('admin')->user()->id}}">
                          <input type="hidden" name="admin_image" id="admin_image" value="{{$updateprofile->profile_picture}}">
                        </div>  
                         <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <div class="row">
                                    <div class="col-3"></div>
                                    <div class="col-9">
                                        <button type="submit" class="btn btn-success" tabindex="4">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary" tabindex="5">
                                            Cancel
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection