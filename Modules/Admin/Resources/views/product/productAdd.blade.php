@extends('admin::layouts.master')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-content">
		<div class="content">
	        <div class="container-fluid">
				<div class="col-md-12">
					<div class="m-portlet m-portlet--tab">
						<div class="page-title">
					    	<div class="container-fluid">
					    		<div class="row title-class">
					    			<div class="col-lg-6 col-md-6 col-sm-12">
					    				<h4 class="page-name">Create Product</h4>
					    			</div>
					    		</div>
					    	</div>
					    </div>
					</div>
					<div class="row justify-content-center">
						<div class="col-md-10">
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{ url('/admin/storeProduct') }}" enctype="multipart/form-data">
								{{csrf_field()}}
								<!-- @if($errors->any())
						          	<div class="alert alert-danger">
						            	<ul>
						              		@foreach ($errors->all() as $error)
						                		<li>{{ $error }}</li>
						              		@endforeach
						            	</ul>
						          	</div>
						          	<br>
						        @endif -->
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="name" class="col-3 col-form-label">
											Product Name
										</label>
										<div class="col-9">
											<input class="form-control m-input" name="name" id="name" type="text" value="{{ old('name') }}" tabindex="1" placeholder="Please enter Product name">
											@error('name')
											<span class="text-danger" role="alert">
                                            	<strong>{{ $message }}</strong>
                                       		</span>
											@enderror
										</div>
									</div>
								</div>

								<div class="m-portlet__body">
									<div class="form-group m-form__group row">									
										<label for="image" class="col-3 col-form-label">
										Product Image
										</label>
										<div class="col-9">
											<input class="form-control m-input" name="image" id="image" type="file" value="{{ old('image') }}" tabindex="2"  placeholder="Please select Product image">
											@error('image')
                                       		<span class="text-danger" role="alert">
                                            	<strong>{{ $message }}</strong>
                                       		</span>
                                     		@enderror
										</div>
										<div id="image_preview"></div>
									</div>
								</div>

								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="price" class="col-3 col-form-label">
											Product Price
										</label>
										<div class="col-9">
											<input class="form-control m-input" min="0" name="price" id="price" type="number" value="{{ old('price') }}" tabindex="3" placeholder="Please enter Product price">
											@error('price')
                                       		<span class="text-danger" role="alert">
                                           		<strong>{{ $message }}</strong>
                                       		</span>
                                     		@enderror
										</div>
									</div>
								</div>

								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="qty" class="col-3 col-form-label">
											Product Quantity
										</label>
										<div class="col-9">
											<input class="form-control m-input" min="0" name="qty" id="qty" type="number" value="{{ old('qty') }}" tabindex="4" placeholder="Please enter Product qty">
											@error('qty')
                                       		<span class="text-danger" role="alert">
                                            	<strong>{{ $message }}</strong>
                                       		</span>
                                     		@enderror
										</div>
									</div>
								</div>
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="desc" class="col-3 col-form-label">
											Product Description
										</label>
										<div class="col-9">
											<textarea class="form-control m-input" name="desc" id="desc" tabindex="5" placeholder="Please enter Product Description">{{ old('desc') }}</textarea>
											@error('desc')
                                       		<span class="text-danger" role="alert">
                                            	<strong>{{ $message }}</strong>
                                       		</span>
                                     		@enderror
										</div>
									</div>
								</div>

								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-3 col-form-label">
											Subcategory
										</label>
										<div class="col-9">
											<select name="subcategory" id="subcategory" class="form-control m-input m-input--air select2_class subcategory" data-info="add" data-id="0" tabindex="6" placeholder="Please enter Subcategory">
												<option></option>
												@foreach($subcategory as $s)
                                                    <option value="{{$s->id}}" {{ old('subcategory')== $s->id ? 'selected="selected"' : '' }}>{{$s->subcategory_name}}</option>
                                                @endforeach
											</select>
											@error('subcategory')
                                       		<span class="text-danger" role="alert">
                                            	<strong>{{ $message }}</strong>
                                       		</span>
                                     		@enderror
										</div>
									</div>
								</div>
								@php
									if($errors->any()){
										$style='style="display:block"';
									}else{
										$style='style="display:block"';
									}
								@endphp
								<div class="m-portlet__body sizechart_div" {{$style}}>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-3 col-form-label">
											Size
										</label>
										<div class="col-9">
											<select name="sizechart" id="sizechart" class="form-control m-input m-input--air select2_class sizechart" tabindex="7" placeholder="Please enter Size">
											</select>
											@error('sizechart')
                                       		<span class="text-danger" role="alert">
                                            	<strong>{{ $message }}</strong>
                                       		</span>
                                     		@enderror
                                     		<span class="s_error_message"></span>
										</div>
									</div>
								</div>

								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-3 col-form-label">
											Company Name
										</label>
										<div class="col-9">
											<select name="company" id="company" class="form-control m-input m-input--air select2_class company" tabindex="8" placeholder="Please enter Company name">
												<option></option>
												@foreach($company as $c)
                                                    <option value="{{$c->id}}" {{ old('company')== $c->id ? 'selected="selected"' : '' }}>{{$c->company_name}}</option>
                                                @endforeach
											</select>
											@error('company')
                                       		<span class="text-danger" role="alert">
                                            	<strong>{{ $message }}</strong>
                                       		</span>
                                    		@enderror
										</div>
									</div>
								</div>

								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-3 col-form-label">
											Color
										</label>
										<div class="col-9">
											@foreach($color as $key=> $cl)
												<input type="checkbox" name="color[]" id="color{{$key}}" tabindex="9" value="{{$cl->id}}"><label style="background-color:{{$cl->color_name}};height: 25px;width: 25px;margin-left:10px;"></label>
                                           	@endforeach
											@error('color')
                                       		<span class="text-danger" role="alert">
                                            	<strong>{{ $message }}</strong>
                                       		</span>
                                     		@enderror
										</div>
									</div>
								</div>

								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<div class="row">
											<div class="col-3"></div>
											<div class="col-9">
												<button type="submit" class="btn btn-success pro_submit" tabindex="10" disabled>
													Submit
												</button>
												<button type="reset" class="btn btn-secondary" tabindex="11">
													Cancel
												</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('PageJS')
<script type="text/javascript">
	jQuery(".subcategory").select2({
		placeholder: "Select a Subcategory",
		allowClear: true
	});
	/*jQuery(".sizechart").select2({
		placeholder: "Select a Sizechart",
		allowClear: true
	});*/
	jQuery(".company").select2({
		placeholder: "Select a Company",
		allowClear: true
	});
	jQuery(".color").select2({
		placeholder: "Select a Color",
		allowClear: true
	});
</script>
@endsection