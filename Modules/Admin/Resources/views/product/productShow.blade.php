@extends('admin::layouts.master')

@section('content')
  
  <div class="page-title">
      <div class="container-fluid">
        <div class="row title-class">
          <div class="col-lg-6 col-md-6 col-sm-12">
            <h4 class="page-name">Product</h4>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 btngrp">
            <a class="btn btn-info" href="{{ url('/admin/addProduct') }}" type="button"><i class="fa fa-plus-circle"></i> Create New</a>
            <a class="btn btn-warning" href="{{ url('/admin/multiple_products') }}" type="button"><i class="fa fa-plus-circle"></i> Multiple Images</a>
          </div>
        </div>
      </div>
  </div>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card m-b-20">
                            <div class="card-body">
                                <!--<h4 class="mt-0 header-title">All Events</h4>-->
                              @if(session('success'))
                                <div class="alert alert-success alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                                  {{ session('success') }}
                                </div>
                              @elseif(session('error'))
                                  <div class="alert alert-error alert-dismissib">
                                    <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                                    {{ session('error') }}
                                  </div>                              
                              @endif
                                <table id="product_datatable_data" class="table table-bordered product_datatable_data" style="border-collapse: collapse; border-spacing: 0; width: 100%;display:inline-block;">
                                    <thead>
                                      <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Fetured</th>
                                        <th>Image</th>
                                        <th>More Images</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Description</th>
                                        <th>Subcategory</th>
                                        <th>Category</th> 
                                        <th>Size</th>
                                        <th>Company Name</th>
                                        <th>Color</th>
                                        <th>Action</th>    
                                      </tr>
                                    </thead>
                                    <tbody>
                                      
                                    </tbody>
                                     
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

  <div class="modal fade bd-example-modal-lg" id="product_images" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h2 class="modal-title" id="exampleModalLabel">Images</h2>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-12">
              <div class="images_div"></div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('PageJS')
<script type="text/javascript">
    $( document ).ready(function() {
      jQuery("#sidebarCollapsem").trigger('click');
    });
    var dt=jQuery('.product_datatable_data').dataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: '{{ url("/admin/all-product-json") }}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'product_name', name: 'product_name' },
            { data: 'fetured', name: 'fetured',orderable: false, searchable:false },
            { data: 'product_image', name: 'product_image',orderable: false, searchable:false },
            { data: 'product_images', name: 'product_images',orderable: false, searchable:false },
            { data: 'product_price', name: 'product_price' },
            { data: 'product_qty', name: 'product_qty' },
            { data: 'description', name: 'description' },
            { data: 'subcategory_name', name: 'subcategory_name' },
            { data: 'category_name', name: 'category_name' },
            { data: 'size_name', name: 'size_name' },
            { data: 'company_name', name: 'company_name' },
            { data: 'color_name', name: 'color_name',orderable: false, searchable:false },
            { data: 'action', name: 'action', orderable: false, searchable:false }
        ],
        order: [ [0, 'desc'] ]
  });

jQuery(document).on('click','.more_images',function(){
      var product_id=jQuery(this).data('id');
      $.ajax({
        url:'{{ url("/admin/moreProductImages")}}',
        type:'get',
        data:{
          product_id:product_id,
        },
        success:function(data){
          $(".images_div").html(data);
          $("#product_images").modal('show');
        }
      });
    });
    jQuery(document).on('click','.fetured',function(){
        var product_id=jQuery(this).data('id');
        if(product_id != ""){
            $.ajax({
                url:'{{ url("/admin/feturedProduct/")}}',
                type:'get',
                data:{
                    id:product_id,
                },
                success:function(responce){
                    var data=JSON.parse(jQuery.trim(responce));
                    var id="#fetured"+data.id;
                    var status=data.status;
                    if( status != "Yes"){
                        jQuery(id).css('color','#da9fa7');
                    }else{
                        jQuery(id).css('color','red');
                    }
                }
            });
        }
    })
     jQuery(document).on('click','.aDeleteImage',function(){
        var id=$(this).data('id');
        if(id != ""){
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                },
                url:'{{ url("/admin/deleteProductImages")}}',
                type:'post',
                data:{
                    id:id,
                    product_id:$(this).data('pro_id')
                },
                success:function(data){
                    $("#product_images").modal('hide');
                    location.reload();
                }
            });
        }
    })
</script>

@endsection

