@extends('admin::layouts.master')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <div class="content">
            <div class="container-fluid">
                <div class="col-md-12">
                    <div class="m-portlet m-portlet--tab">
                        <div class="page-title">
                            <div class="container-fluid">
                                <div class="row title-class">
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <h4 class="page-name">Multiple Products</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-10">
                            <form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{ url('/admin/multiple_products_store') }}" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-3 col-form-label">
                                        Product Name
                                        </label>
                                        <div class="col-9">
                                           <select name="productname" id="productname" class="form-control m-input m-input--air select2_class multiproduct" tabindex="2" required>
                                                <option></option>
                                                @foreach($product as $p)
                                                    <option value="{{$p->id}}">{{$p->product_name}}</option>
                                                @endforeach
                                            </select>
                                            @error('productname')
                                             <span class="text-danger" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-3 col-form-label">
                                        Product Image
                                        </label>
                                        <div class="col-9">
                                         <input class="form-control m-input" name="image[]" id="image" multiple type="file" value="{{ old('image') }}" tabindex="3"  placeholder="Please select Product image" required  accept="image/*">
                                            @error('image')
                                             <span class="text-danger" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                
                               
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <div class="row">
                                            <div class="col-3"></div>
                                            <div class="col-9">
                                                <input type="submit" name='view' class="btn btn-secondary" tabindex="5" value="Submit">
                                                <input type="reset" class="btn btn-success" tabindex="6" name="download" value="Cancel">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('PageJS')
<script type="text/javascript">
    jQuery("#productname").select2({
        placeholder: "Select a product name",
        allowClear: true
    });
</script>
@endsection