<div class="imggal-main">
	@foreach ($product as $key => $value)
		<div class="imggal-col" style="display: inline-block;">
            <div class="imggal-col-img"><img src="{{url('/images/'.$value->image_name)}}" width="150" height="150"></div>
            <a href="javascript:void(0)" data-id='{{$value->id}}' data-pro_id='{{$value->product_id}}' class="aDeleteImage"  onclick="return confirm('Are you  sure you want to delete this product image?');">X</a>
		</div>
    @endforeach
</div>

<style type="text/css">
.imggal-col {
    float: left;
    position: relative;
    margin-bottom: 15px;
}
.imggal-col a {
    position: absolute;
    top: 15px;
    right: 15px;
    padding: 0 7px;
    background: #FF0000;
    color: #fff;
    font-weight: bold;
}
.imggal-main:after {
    display: block;
    content: "";
    clear: both;
}
.imggal-col-img{
	display: inline-block;
	margin: 10px;
}
</style>