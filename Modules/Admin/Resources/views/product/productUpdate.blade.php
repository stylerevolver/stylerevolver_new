@extends('admin::layouts.master')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-content">
		<div class="content">
	        <div class="container-fluid">
				<div class="col-md-12">
					<div class="m-portlet m-portlet--tab">
						<div class="page-title">
					    	<div class="container-fluid">
					    		<div class="row title-class">
					    			<div class="col-lg-6 col-md-6 col-sm-12">
					    				<h4 class="page-name">Update Product</h4>
					    			</div>
					    		</div>
					    	</div>
					    </div>
					</div>
					
					<div class="row justify-content-center">
						<div class="col-md-10">
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{ url('/admin/updateProduct') }}" enctype="multipart/form-data">
								{{csrf_field()}}
								<!-- @if($errors->any())
						          	<div class="alert alert-danger">
						            	<ul>
						              		@foreach ($errors->all() as $error)
						                		<li>{{ $error }}</li>
						              		@endforeach
						            	</ul>
						          	</div>
						          	<br>
						        @endif -->
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="name" class="col-3 col-form-label">
										Product Name
										</label>
										<div class="col-9">
											<input class="form-control m-input" name="name" id="name" type="text" tabindex="1" value="{{ $product->product_name}}" placeholder="Please enter Product name">
											@error('name')
                                       		<span class="text-danger" role="alert">
                                            	<strong>{{ $message }}</strong>
                                       		</span>
                                     		@enderror
										</div>
									</div>
								</div>
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="image" class="col-3 col-form-label">
										Product	Image
										</label>
										<div class="col-9">
											<input class="form-control m-input" name="image" id="image" type="file" tabindex="2" value="{{ $product->product_image}}" placeholder="Please enter Product">
											@error('image')
                                       		<span class="text-danger" role="alert">
                                            	<strong>{{ $message }}</strong>
                                       		</span>
                                     		@enderror
										</div>
									</div>
								</div>

								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="price" class="col-3 col-form-label">
										Product Price
										</label>
										<div class="col-9">
											<input class="form-control m-input" name="price" id="price" type="number" value="{{ $product->product_price}}" tabindex="3" placeholde="Please enter Product price">
											@error('price')
                                       		<span class="text-danger" role="alert">
                                           		<strong>{{ $message }}</strong>
                                       		</span>
                                     		@enderror
										</div>
									</div>
								</div>

								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="qty" class="col-3 col-form-label">
										Product Quantity
										</label>
										<div class="col-9">
											<input class="form-control m-input" name="qty" id="qty" type="number" value="{{ $product->product_qty}}" tabindex="4" placeholder="Please enter Product quantity">
											@error('qty')
                                       		<span class="text-danger" role="alert">
                                            	<strong>{{ $message }}</strong>
                                       		</span>
                                     		@enderror
										</div>
									</div>
								</div>


								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="desc" class="col-3 col-form-label">
											Product Description
										</label>
										<div class="col-9">
											<textarea class="form-control m-input" name="desc" id="desc" tabindex="5" placeholder="Please enter Product Description">{{ $product->description}}</textarea>
											@error('desc')
                                       		<span class="text-danger" role="alert">
                                            	<strong>{{ $message }}</strong>
                                       		</span>
                                     		@enderror
										</div>
									</div>
								</div>


								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="subcategory" class="col-3 col-form-label">Subcategory</label>
										<div class="col-9">
											<select name="subcategory" id="subcategory" class="form-control m-input m-input--air select2_class subcategory" data-info="update" data-id="{{$product->size_id}}" tabindex="6" placeholder="Please enter Subcategory">
												<option></option>
												@foreach($subcategory as $s)
                                                    <option value="{{$s->id}}" {{$product->subcategory_id == $s->id ? 'selected="selected"' : '' }}>{{$s->subcategory_name}}</option>
                                                @endforeach
											</select>
											@error('subcategory')
                                       		<span class="text-danger" role="alert">
                                            	<strong>{{ $message }}</strong>
                                       		</span>
                                     		@enderror
										</div>
									</div>
								</div>

								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="sizechart" class="col-3 col-form-label">
											Size
										</label>
										<div class="col-9">
											<select name="sizechart" id="sizechart" class="form-control m-input m-input--air" tabindex="7" placeholder="Please enter Size">
												<option></option>
												@foreach($size_chart as $sc)
                                                    <option value="{{$sc->id}}" {{$product->size_id == $sc->id ? 'selected="selected"' : '' }}>{{$sc->size_name}}</option>
                                                @endforeach
											</select>
											@error('sizechart')
                                       		<span class="text-danger" role="alert">
                                            	<strong>{{ $message }}</strong>
                                       		</span>
                                     		@enderror
                                     		<span class="s_error_message"></span>
										</div>
									</div>
								</div>

								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="company" class="col-3 col-form-label">
											Company Name
										</label>
										<div class="col-9">
											<select name="company" id="company" class="form-control m-input m-input--air select2_class" tabindex="8" placeholder="Please enter Company name">
												<option></option>
												@foreach($company as $c)
                                                    <option value="{{$c->id}}" {{$product->company_id == $c->id ? 'selected="selected"' : '' }}>{{$c->company_name}}</option>
                                                @endforeach
											</select>
											@error('company')
                                       		<span class="text-danger" role="alert">
                                           	 	<strong>{{ $message }}</strong>
                                       		</span>
                                     		@enderror
										</div>
									</div>
								</div>

								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="color" class="col-3 col-form-label">
											Color
										</label>
										<div class="col-9">
											@php 
												$color_old=explode("|",$product->color_id);
											@endphp
											@foreach($color as $key=> $cl)
												<input type="checkbox" name="color[]" id="color{{$key}}" tabindex="9" value="{{$cl->id}}" {{in_array($cl->id,$color_old) ? 'checked' : '' }}><label style="background-color:{{$cl->color_name}};height: 25px;width: 25px;margin: 10px;"></label>
                                           	@endforeach
											</select>
											@error('color')
                                       		<span class="text-danger" role="alert">
                                            	<strong>{{ $message }}</strong>
                                       		</span>
                                     		@enderror
										</div>
									</div>
								</div>
								<div class="imggal-main custom_img">
										<div class="imggal-col" style="display: inline-block;">
								            <div class="imggal-col-img"><img src="{{url('/images/'.$product->product_image)}}" width="100" height="100"></div>
							        	</div>
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<div class="row">
											<label for="color" class="col-3 col-form-label">
											&nbsp;
											</label>
											<div class="col-9">
												<input type="hidden" name="product_id" value="{{$product->id}}">
												<input type="hidden" name="product_image_name" value="{{$product->product_image}}">
												<button type="submit" class="btn btn-success" tabindex="10">
													Submit
												</button>
												<a href="{{url('/admin/product')}}" class="btn btn-secondary" tabindex='11'>Cancel</a>
												
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('Pagecss')
	<style type="text/css">
		.imggal-col {
		    float: left;
		    position: relative;
		    margin-bottom: 15px;
		}
		.imggal-col a {
		    position: absolute;
		    top: 15px;
		    right: 15px;
		    padding: 0 7px;
		    background: #FF0000;
		    color: #fff;
		    font-weight: bold;
		}
		.imggal-main:after {
		    display: block;
		    content: "";
		    clear: both;
		}
		.imggal-col-img{
			display: inline-block;
			margin: 10px;
		}
	</style>
@endsection
@section('PageJS')
<script type="text/javascript">
	jQuery(".select2_class").select2({
		placeholder: "Select a option"
	});
	
</script>
@endsection