@extends('admin::layouts.master')
@section('content')

<div class="page-title">
    <div class="container-fluid">
        <div class="row title-class">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <h4 class="page-name"> Reset Password </h4>
            </div>
        </div>
    </div>
</div>


<div class="resetPassword">  
    <div class="container">
        <form method="post" action="{{ url('/admin/resetPassword') }}" enctype="multipart/form-data">
            <div class="col-md-9">
                <div class="panel panel-default">   
                    <div class="clearfix"> </div>
                    <div class="panel-body">
                        @if(session('msg'))
                            <div class="alert alert-success alert-dismissible">
                              <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                              {{ session('msg') }}
                            </div>
                        @endif
                        @csrf 
                        <div class="form-group row">
                            <label for="current_password" class="col-md-4 col-form-label text-md-right">Current Password</label>

                            <div class="col-md-6">
                                <input required id="current_password" type="password" class="form-control @error('current_password') is-invalid @enderror" name="current_password" autocomplete="current-password">
                                @error('current_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="new_password" class="col-md-4 col-form-label text-md-right">New Password</label>

                            <div class="col-md-6">
                                <input required id="new_password" type="password" class="form-control @error('new_password') is-invalid @enderror" name="new_password" autocomplete="current-password">
                                @error('new_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="new_confirm_password" class="col-md-4 col-form-label text-md-right">Confirm Password</label>
                            <div class="col-md-6">
                                <input required id="new_confirm_password" type="password" class="form-control @error('new_confirm_password') is-invalid @enderror" name="new_confirm_password" autocomplete="current-password">
                                @error('new_confirm_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class="form-group row mb-0">
                <div class="col-md-8 text-center">
                    <input type="hidden" name="adminId" id="adminId" value="{{\Auth::guard('admin')->user()->id}}">
                    <button type="submit" class="btn btn-warning" style="background-color: #20b2aa">
                        Update Password
                    </button>
                </div>
            </div>
        </form>    
    </div>
</div>
   




@endsection
@section('PageCSS')
    <style type="text/css">
        .resetPassword{
            padding: 5em;
        }
    </style>
@endsection