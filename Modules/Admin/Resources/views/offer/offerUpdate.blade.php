@extends('admin::layouts.master')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-content">
		<div class="content">
	        <div class="container-fluid">
				<div class="col-md-12">
					<div class="m-portlet m-portlet--tab">
						<div class="page-title">
					    	<div class="container-fluid">
					    		<div class="row title-class">
					    			<div class="col-lg-6 col-md-6 col-sm-12">
					    				<h4 class="page-name">Update Offer</h4>
					    			</div>
					    		</div>
					    	</div>
					    </div>
					</div>
					
					<div class="row justify-content-center">
						<div class="col-md-10">
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{ url('/admin/updateOffer') }}" enctype="multipart/form-data">
								{{csrf_field()}}
								<!-- @if($errors->any())
						          	<div class="alert alert-danger">
						            	<ul>
						              		@foreach ($errors->all() as $error)
						                		<li>{{ $error }}</li>
						              		@endforeach
						            	</ul>
						          	</div>
						          	<br>
						        @endif -->

						       
						        <div class="m-portlet__body">
						        	<div class="form-group m-form__group row">
						        		<label for="couponcode" class="col-3 col-form-label">
						        			Coupon Code
						        		</label>
						        		<div class="col-9">
						        			<input class="form-control m-input" name="couponcodet" id="couponcodet" type="text" value="{{ $offer->couponcode}}" tabindex="1" readonly>
						        			{{-- <button type="button" name="couponcode" class="couponcode" id="couponcode">Generate Code</button> --}}
											@error('couponcodet')
	  	                    				<span class="text-danger" role="alert">
	                                 			<strong>{{ $message }}</strong>
	                              	 		</span>
	                                       	@enderror
						        		</div>
						        	</div>
						        </div>
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="offerpercentage" class="col-3 col-form-label">
											Offer(%)
										</label>
										<div class="col-9">
											<input class="form-control m-input" name="offerpercentage" id="offerpercentage" type="number" tabindex="2" value="{{ $offer->offer_percentage}}" placeholder="Please enter Offer percentage" readonly>
											@error('offerpercentage')
	  	                    				<span class="text-danger" role="alert">
	                                 			<strong>{{ $message }}</strong>
	                              	 		</span>
	                                       	@enderror
										</div>
									</div>
								</div>

								<div class="form-group m-form__group row">
										<label for="startdate" class="col-3 col-form-label">
											Start Date
										</label>
										<div class="col-9">
                                         	<input class="form-control m-input" name="startdate" id="startdate" type="date" value="{{ $offer->end_date}}" tabindex="3" placeholder="Please enter Start Date">
                                         	@error('startdate')
	  	                    				<span class="text-danger" role="alert">
	                                 			<strong>{{ $message }}</strong>
	                              	 		</span>
	                                       	@enderror
										</div>
										
								</div>

								<div class="form-group m-form__group row">
										<label for="enddate" class="col-3 col-form-label">
											End Date
										</label>
										<div class="col-9">
											<input class="form-control m-input" name="enddate" id="enddate" type="date" value="{{ $offer->end_date}}" tabindex="4" placeholder="Please enter End Date">
											@error('enddate')
	  	                    				<span class="text-danger" role="alert">
	                                 			<strong>{{ $message }}</strong>
	                              	 		</span>
	                                       	@enderror
						 				</div>
								</div>

								

								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-3 col-form-label">
											Product Name
										</label>
										<div class="col-9">
											<select name="productname1" id="productname" class="form-control m-input m-input--air 	select2_class" tabindex="5" placeholder="Please enter Product name" disabled>
												<option></option>
												@foreach($product as $p)
                                                	<option value="{{$p->id}}" {{ $p->id == $offer->product_id ? 'selected="selected"' : '' }}>{{$p->product_name}}</option>
                                            	@endforeach
											</select>
											@error('productname')
                                    		<span class="text-danger" role="alert">
                                        		<strong>{{ $message }}</strong>
                                    		</span>
                                    		@enderror
										</div>
									</div>
								</div>
								<input type="hidden" name="productname" value="{{$offer->product_id}}">

								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<div class="row">
											<div class="col-3"></div>
											<div class="col-9">
												<input type="hidden" name="offer_id" value="{{$offer->id}}">
												<button type="submit" class="btn btn-success" tabindex="6">
													Submit
												</button>
												<a href="{{url('/admin/offer')}}" class="btn btn-secondary" tabindex='7'>Cancel</a>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('PageJS')
<script type="text/javascript">
	jQuery("#productname").select2({
	    placeholder: "Select a Product",
	    allowClear: true
	});
	

	$("button.couponcode").click(function(){
		length=10;
		var result           = '';
	    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	    var charactersLength = characters.length;
	    for ( var i = 0; i < length; i++ ) {
	    	result += characters.charAt(Math.floor(Math.random() * charactersLength));
	   	}
	   	$("#couponcodet").val(result);
  		//alert(result);
		});
</script>
@endsection