@extends('admin::layouts.master')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	{{-- <input pattern=".{0}|.{5,10}" required title="Either 0 OR (5 to 10 chars)">
<input pattern=".{0}|.{8,}"   required title="Either 0 OR (8 chars minimum)"> --}}
	<div class="m-content">
		<div class="content">
	        <div class="container-fluid">
				<div class="col-md-12">
					<div class="m-portlet m-portlet--tab">
						<div class="page-title">
					    	<div class="container-fluid">
					    		<div class="row title-class">
					    			<div class="col-lg-6 col-md-6 col-sm-12">
					    				<h4 class="page-name">Create Offer</h4>
					    			</div>
					    		</div>
					    	</div>
					    </div>
					</div>
					<div class="row justify-content-center">
						<div class="col-md-10">
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{ url('/admin/storeOffer') }}" enctype="multipart/form-data">
								{{csrf_field()}}
								<!-- @if($errors->any())
						          	<div class="alert alert-danger">
						            	<ul>
						              		@foreach ($errors->all() as $error)
						                		<li>{{ $error }}</li>
						              		@endforeach
						            	</ul>
						          	</div>
						          	<br>
						        @endif -->
						        <div class="m-portlet__body">
						        	<div class="form-group m-form__group row">
						        		<label for="couponcode" class="col-3 col-form-label">
						        			Coupon Code
						        		</label>
						        		<div class="col-9">
						        			<input class="form-control m-input" name="couponcodet" id="couponcodet" type="text" value="{{ old('couponcodet') }}" tabindex="1" readonly><button type="button" name="couponcode" class="couponcode" id="couponcode">Generate Code</button>
											@error('couponcodet')
	  	                    				<span class="text-danger" role="alert">
	                                 			<strong>{{ $message }}</strong>
	                              	 		</span>
	                                       	@enderror
						        		</div>
						        	</div>
						        </div>
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="offerpercentage" class="col-3 col-form-label">
											Offer(%)
										</label>
										<div class="col-9">
											<input class="form-control m-input" min="0" max="100" name="offerpercentage" id="offerpercentage" type="number" value="{{ old('offerpercentage') }}" tabindex="2" placeholder="Please enter Offer Percentage">
											@error('offerpercentage')
	  	                    				<span class="text-danger" role="alert">
	                                 			<strong>{{ $message }}</strong>
	                              	 		</span>
	                                       	@enderror
										</div>
									</div>
								</div>
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="startdate" class="col-3 col-form-label">
											Start Date
										</label>
										<div class="col-9">
											<input class="form-control m-input" name="startdate" id="startdate" type="date" value="{{ old('startdate') }}" tabindex="3" placeholder="Please enter Offer Start Date">
											@error('startdate')
	  	                    				<span class="text-danger" role="alert">
	                                 			<strong>{{ $message }}</strong>
	                              	 		</span>
	                                       	@enderror
										</div>
									</div>
								</div>
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="enddate" class="col-3 col-form-label">
											End Date
										</label>
										<div class="col-9">
											<input class="form-control m-input" name="enddate" id="enddate" type="date" value="{{ old('enddate') }}" tabindex="4" placeholder="Please enter Offer End Date">
											@error('enddate')
	  	                    				<span class="text-danger" role="alert">
	                                 			<strong>{{ $message }}</strong>
	                              	 		</span>
	                                       	@enderror
										</div>
									</div>
								</div>

								<div class="m-portlet__body">
											<div class="form-group m-form__group row">
												<label for="example-text-input" class="col-3 col-form-label">
												Product Name
												</label>
												<div class="col-9">
													<select name="productname" id="productname" class="form-control m-input m-input--air select2_class" placeholder="Please enter Product name" tabindex="5">
														<option></option>
														@foreach($product as $o)
                                                    		<option value="{{$o->id}}">{{$o->product_name}}</option>
                                                		@endforeach
													</select>
													@error('productname')
                                       		 		<span class="text-danger" role="alert">
                                            			<strong>{{ $message }}</strong>
                                       		 		</span>
                                       		 		@enderror
												</div>
											</div>
									</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<div class="row">
											<div class="col-3"></div>
											<div class="col-9">
												<button type="submit" class="btn btn-success" tabindex="6">
													Submit
												</button>
												<button type="reset" class="btn btn-secondary" tabindex="7">
													Cancel
												</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('PageJS')
<script type="text/javascript">
	jQuery("#productname").select2({
	    placeholder: "Select a Product",
	    allowClear: true
	});
	
	$("button.couponcode").click(function(){
		length=6;
		var result           = '';
	    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	    var charactersLength = characters.length;
	    for ( var i = 0; i < length; i++ ) {
	    	result += characters.charAt(Math.floor(Math.random() * charactersLength));
	   	}
	   	$("#couponcodet").val(result);
  		//alert(result);
		});
</script>
@endsection