@extends('admin::layouts.master')

@section('content')

<div class="page-title">
      <div class="container-fluid">
        <div class="row title-class">
          <div class="col-lg-6 col-md-6 col-sm-12">
            <h4 class="page-name">Rating</h4>
          </div>
        </div>
      </div>
  </div>
  <div class="m-grid__item m-grid__item--fluid m-wrapper">
      <div class="m-content">
      <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card m-b-20">
                            <div class="card-body">
                              @if(session('success'))
                                <div class="alert alert-success alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                                  {{ session('success') }}
                                </div>
                              @elseif(session('error'))
                                  <div class="alert alert-error alert-dismissib">
                                    <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                                    {{ session('error') }}
                                </div>                              
                              @endif
                                <table id="rating_datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                      <tr>
                                        <th>User Name</th>
                                        <th>Product Name</th>
                                        <th>Rating</th>
                                        <th>Date</th>  
                                      </tr>
                                    </thead>
                                    <tbody>
                                      
                                    </tbody>
                                     
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
      </div>
    </div>
    </div>
@endsection

@section('PageJS')
<script type="text/javascript">
    jQuery('#rating_datatable').dataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        "scrollX": true,
        ajax: '{{ url("/admin/all-rating-json") }}',
        columns: [
            { data: 'first_name' , name: 'first_name' },
            { data: 'product_name', name: 'product_name' },
            { data: 'rating', name: 'rating' },
            { data: 'rating_date', name: 'rating_date' }
        ],
        order: [ [0, 'desc'] ]
    });
</script>
@endsection
