@extends('admin::layouts.master')

@section('content')

<div class="page-title">
      <div class="container-fluid">
        <div class="row title-class">
          <div class="col-lg-6 col-md-6 col-sm-12">
            <h4 class="page-name">User</h4>
          </div>
           
        </div>
      </div>
  </div>
  <div class="m-grid__item m-grid__item--fluid m-wrapper">
      <div class="m-content">
      <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card m-b-20">
                            <div class="card-body">
                                <!--<h4 class="mt-0 header-title">All Events</h4>-->
                               @if(session('success'))
                                <div class="alert alert-success alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                                  {{ session('success') }}
                                </div>
                              @elseif(session('error'))
                                  <div class="alert alert-error alert-dismissib">
                                    <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                                    {{ session('error') }}
                                </div>                              
                              @endif
                                <table id="user_datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                      <tr>
                                        <th>Id</th>
                                        <!-- <th>User Name</th> -->
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Contact</th>
                                        <th>Area Name</th>
                                        <th>Address</th>
                                        <th>Date Of Birth</th>  
                                        <th>Gender</th>  
                                      </tr>
                                    </thead>
                                     
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
      </div>
    </div>
    </div>
@endsection

@section('PageJS')
<script type="text/javascript">
    jQuery('#user_datatable').dataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        "scrollX": true,
        ajax: '{{ url("/admin/all-user-json") }}',
        columns: [
            { data: 'id' , name: 'id' },
            /*{ data: 'name', name: 'name' },*/
            { data: 'first_name', name: 'first_name' },
            { data: 'email', name: 'email' },
            { data: 'contact_no', name: 'contact_no' },
            { data: 'area_name', name: 'area_name'},
            { data: 'address', name: 'address' },
            { data: 'dob', name: 'dob'},
            { data: 'gender', name: 'gender'},

        ],
        order: [ [0, 'desc'] ]
    });
</script>
@endsection
