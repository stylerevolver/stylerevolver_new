@extends('admin::layouts.app')
@section('content')


<div class="loginmain">
    <div class="container">
        <div class="logincon">
            <div class="loginin" style="background-image: url( {{ asset('/admin-asset/images/login_back.jpeg') }} );">
                <div class="loginlogo">
                    <img src="{{asset('/images/style_logo.jpeg')}}">
                    <h2>Forgot Password</h2>
                </div>
                @if(session('success'))
                    <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                        {{ session('success') }}
                    </div>
                @elseif(session('error'))
                    <div class="alert alert-error alert-dismissib">
                        <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                        {{ session('error') }}
                    </div>                             
                @endif
                @if (\Session::has('error'))
                    <div class="alert alert-danger">
                        <ul>
                            <li>{!! \Session::get('error') !!}</li>
                        </ul>
                    </div>
                @endif
                <form action="{{ url('admin/forgotPassword') }}"  method="POST" enctype="multipart/form-data" id="forgotPassword" class="m-form m-form--fit m-form--label-align-right admin_log_form">
                    @csrf
                    {{--<input type="hidden" name="token" value="{{ $token }}">--}}
                    <div class="form-group">
                        <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}"   placeholder="Email" autofocus >
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                    </div>
                    <div class="form-group col-md-3">
                        <center><button type="submit" class="btn btn-primary">{{ __('Send Forgot Password Link') }}</button></center>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<style type="text/css">

.m-login__logo a img 
{
    width: 45%;
}
</style>


@endsection

