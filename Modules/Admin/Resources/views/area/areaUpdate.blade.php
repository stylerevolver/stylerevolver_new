@extends('admin::layouts.master')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-content">
		<div class="content">
	        <div class="container-fluid">
				<div class="col-md-12">
					<div class="m-portlet m-portlet--tab">
						<div class="page-title">
					    	<div class="container-fluid">
					    		<div class="row title-class">
					    			<div class="col-lg-6 col-md-6 col-sm-12">
					    				<h4 class="page-name">Update Area</h4>
					    			</div>
					    		</div>
					    	</div>
					    </div>
					</div>
					
					<div class="row justify-content-center">
						<div class="col-md-10">
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{ url('/admin/updateArea') }}" enctype="multipart/form-data">
								{{csrf_field()}}
								<!-- @if($errors->any())
						          	<div class="alert alert-danger">
						            	<ul>
						              		@foreach ($errors->all() as $error)
						                		<li>{{ $error }}</li>
						              		@endforeach
						            	</ul>
						          	</div>
						          	<br>
						        @endif -->
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="name" class="col-3 col-form-label">
											Area Name
										</label>
										<div class="col-9">
											<input class="form-control m-input" name="name" id="name" type="text" tabindex="1" value="{{ $area->area_name}}" placeholder="Please enter Area name">
											@error('name')
	                                   		 <span class="text-danger" role="alert">
	                                   			<strong>{{ $message }}</strong>
	                           		 		</span>
	                             		 	@enderror
										</div>
									</div>
								</div>


								<div class="m-portlet__body">
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-3 col-form-label">
											Pincode
											</label>
											<div class="col-9">
												<input type="number" tabindex="2" class="form-control m-input" name="pincode" value="{{ $area->pincode}}"placeholder="Please enter Pincode">
												@error('pincode')
	                                    		<span class="text-danger" role="alert">
	                                       				<strong>{{ $message }}</strong>
	                                   			</span>
                                   				@enderror
											</div>
										</div>
								</div>

								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-3 col-form-label">
										City Name
										</label>
										<div class="col-9">
											<select name="cityname" id="cityname" class="form-control m-input m-input--air 	select2_class" tabindex="3"placeholder="Please enter City name">
												<option></option>
												@foreach($city as $a)
                                                	<option value="{{$a->id}}" {{ $area->city_id == $a->id ? 'selected="selected"' : '' }}>{{$a->city_name}}</option>
                                            	@endforeach
											</select>
											@error('cityname')
                                        	<span class="text-danger" role="alert">
                                            	<strong>{{ $message }}</strong>
                                        	</span>
                                        	@enderror
										</div>
									</div>
								</div>


								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<div class="row">
											<div class="col-3"></div>
											<div class="col-9">
												<input type="hidden" name="area_id" value="{{$area->id}}">
												<button type="submit" class="btn btn-success" tabindex="4">
													Submit
												</button>
												<a href="{{url('/admin/size_chart')}}" class="btn btn-secondary" tabindex='5'>Cancel</a>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('PageJS')
<script type="text/javascript">
	jQuery("#cityname").select2({
		placeholder: "Select a Area",
		allowClear: true
	});
</script>>
@endsection