@extends('admin::layouts.app')
@section('content')


<div class="loginmain">
    <div class="container">
        <div class="logincon">
            <div class="loginin" style="background-image: url( {{ asset('/admin-asset/images/login_back.jpeg') }} );">
                <div class="loginlogo">
                    <img src="{{asset('/images/style_logo.jpeg')}}">
                    <h2>Forgot Password</h2>
                </div>
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <br>
                @endif
                <form action="{{ url('admin/adminForgotPassword') }}"  method="POST" class="admin_log_form">
                    @csrf
                    <div class="form-group">
                        <input type="email" class="form-control" id="email" name="email" value="{{ $email ?? old('email') }}"  required placeholder="Email" value="{{$email}}" readonly>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <input type="password" class="form-control" id="password" required placeholder="Password" name="password" autocomplete="off">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="password-confirm" required placeholder="Confirm Password" name="password_confirmation">
                        @error('password_confirmation')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <center><button type="submit" class="btn btn-primary">{{ __('Reset Password') }}</button></center>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
