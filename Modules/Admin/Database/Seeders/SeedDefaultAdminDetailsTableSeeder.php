<?php

namespace Modules\Admin\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Admin\Entities\admin;
use Illuminate\Database\Eloquent\Model;

class SeedDefaultAdminDetailsTableSeeder extends Seeder
{
    protected $adminUsers = [
        [
            'name' => 'Suresh Chauhan',
            'email' => 'styleRevolver3999@gmail.com',
            'password' => 'admin@123'
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->adminUsers as $user) {
            if(!admin::where('email', '=', $user['email'])->exists())
            {
                $superadmin = admin::create(
                    [
                        'name' => $user['name'],
                        'email' => $user['email'],
                        'password' => bcrypt($user['password'])
                    ]
                );
            }
        }
    }
}