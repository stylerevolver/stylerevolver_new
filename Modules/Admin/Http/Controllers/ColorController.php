<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use App\models\color;
use DB;

class ColorController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
         return view('admin::color.colorShow');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::color.colorAdd');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
       // echo '<pre>';print_r($request->all());exit();
        $rules = array(
            'colorname' => 'required|unique:color,color_name',
            'name' => 'required|unique:color,color_code',
            //'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        );
        $customeMessage = array(
            'colorname.required' => 'Color name can not be an empty.',
            'name.required' => 'Color name can not be an empty.',
            //'image.required' => 'Color image can not be an empty.',
        );
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
       // $imageName = time().'.'.request()->image->getClientOriginalExtension();

        //if(request()->image->move(public_path('images'), $imageName))
        //{
            $color = color::create([
                'color_name' => ucfirst($request->colorname),
                'color_image' => 'color.jpg',
                'color_code' =>$request->name,
            ]);
       /* }else{
            $message="image not uploaded please try again.";
            return redirect()->back()->withErrors($message)->withInput();
        }*/
         return redirect('/admin/color')->with('success','Color created successfully');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show()
    {
        $color=color::get();
         return \DataTables::of($color)
            ->editColumn('id', function($data)
            {
                return $data->id;
            })
            ->editColumn('color_name', function($data)
            {
                return $data->color_name;
            })
            ->editColumn('color_code', function($data)
            {
                $html='<div style="background-color:'.$data->color_code.';width:50px;height:50px;border:1px solid #fff;"></div>';
                return $html;
            })

             /*->editColumn('color_image', function($data)
            {
                $url = url('/images/'.$data->color_image);   
                $html='<img src="'.$url.'" height="70" width="70">';
                return $html;
            })*/
            ->editColumn('action', function($data)
            {
                $html = '<a href="'.url('/admin/editColor/'.$data->id) .'" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x"><i class="fas fa-edit"></i></a>
                <a href="'.url('/admin/deleteColor/'.$data->id) .'" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x" onclick="return confirm('."'Are you sure you want to delete this color?'".');"><i class="fas fa-trash"></i></a>
                ';
                return $html;
            })
            ->rawColumns(['id','color_name','color_code','action'])
            ->make(true);
    }
    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $color=color::where('id','=',$id)->first();
        return view('admin::color.colorUpdate',compact('color'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request)
    {
        if ($request->hasFile('image')){
            $rules = array(
                'colorname' => 'required|unique:color,color_name',
                'name' => 'required|unique:color,color_code,'.$request->color_id.'|max:255',
               // 'image' => 'required|image|mimes:jpeg,png,jpg',
            );
            $customeMessage = array(
                'colorname.required' => 'Color name can not be an empty.',
                'name.required' => 'Color name can not be an empty.',
                //'image.required' => 'Color image can not be an empty.',
            );
        }else{
            $rules = array(
                'colorname' => 'required|unique:color,color_name',
                'name' => 'required|unique:color,color_code,'.$request->color_id.'|max:255'
            );
            $customeMessage = array(
                'colorname.required' => 'Color name can not be an empty.',
                'name.required' => 'Color name can not be an empty.',
            );
        }

        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        /*if ($request->hasFile('image')) {
             $imageName = time().'.'.request()->image->getClientOriginalExtension();
            if(request()->image->move(public_path('images'), $imageName)){
                $image_name=$imageName;
            }else{
                $message="image not uploaded please try again.";
                return redirect()->back()->withErrors($message)->withInput();
            }
        }else{
            $image_name=$request->color_image_name;
        }*/
        $color=color::where('id','=',$request->color_id)->first();

        if(!empty($color))
        {
            $color->update([
                'color_name' =>ucfirst($request->colorname),
                //'color_image' => $image_name,
                'color_code' => $request->name,
            ]);
            return redirect('admin/color')->with('success','Color updated successfully');
        }else
        {
            return redirect('admin/color')->with('success','Some error');
        }
    }


    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try{
            $affected = DB::delete("DELETE FROM color WHERE id = {$id}");
            return redirect('/admin/color')->with('success','Color deleted successfully');
        }catch(\Illuminate\Database\QueryException  $ex){
            return redirect('/admin/color')->with('error','Cannot delete this color');
        }
    }
}
