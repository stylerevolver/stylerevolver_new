<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use App\models\subcategory;
use App\models\category;
use DB;

class SubcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admin::subcategory.subcategoryShow');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
         $category=category::get();
        return view('admin::subcategory.subcategoryAdd',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required|regex:/^[a-zA-Z ]+$/u|unique:subcategory,subcategory_name|max:255|string',
            'categoryname' => 'required'
        );
        $customeMessage = array(
            'name.required' => 'Subcategory name can not be empty.',
            'name.string' => 'Subcategory name only varchar.',
            'categoryname.required' => 'Category can not be empty.',
        );
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $subcategory = subcategory::create([
            'subcategory_name' => ucfirst($request->name),
            'category_id' => $request ->categoryname,
        ]);
         /*session::flash('message','Subcategory successfully added');
        session::flash('alert-class','alert success');*/
         return redirect('/admin/subcategory')->with('success','Subcategory created successfully');
        /*$notification = array(
            'message' => 'Subcategory created successfully!',
            'alert-type' => 'success'
        );
        return redirect('admin/subcategory')->with($notification);*/
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show()
    {
        $subcategory=subcategory::join('category as c','subcategory.category_id','=','c.id')
        ->select('subcategory.id','subcategory.subcategory_name','c.category_name')
        ->get();

         return \DataTables::of($subcategory)
            ->editColumn('id', function($data)
            {
                return $data->id;
            })
            ->editColumn('subcategory_name', function($data)
            {
                return $data->subcategory_name;
            })
             
              ->editColumn('category_name', function($data)
            {
                return $data->category_name;
            })
            
            
            
            ->editColumn('action', function($data)
            {
                $html = '<a href="'.url('/admin/editSubcategory/'.$data->id) .'" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x"><i class="fas fa-edit"></i></a>
                <a href="'.url('/admin/deleteSubcategory/'.$data->id) .'" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x" onclick="return confirm('."'Are you sure you want to delete this subcategory?'".');"><i class="fas fa-trash"></i></a>
                ';
                return $html;
            })
            ->rawColumns(['id','subcategory_name','category_name','action'])
            ->make(true);
    }


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $category=category::get();
        $subcategory=subcategory::where('id','=',$id)->first();
        return view('admin::subcategory.subcategoryUpdate')->with('subcategory',$subcategory)->with('category',$category);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request )
    {
        
        $rules = array(
            'name' => 'required|regex:/^[a-zA-Z ]+$/u|unique:subcategory,subcategory_name,'.$request->subcategory_id.'|max:255',
            'categoryname' => 'required'
        );
        $customeMessage = array(
            'name.required' => 'Subcategory name can not be an empty.',
            'name.string' => 'Subcategory name only varchar.',
            'categoryname.required' => 'Category can not be empty.',

        );
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $subcategory=subcategory::where('id','=',$request->subcategory_id)->first();

        if(!empty($subcategory))
        {
            $subcategory->update([
                'subcategory_name' =>ucfirst($request->name),
                'category_id' => $request ->categoryname,
            ]);
            return redirect('admin/subcategory')->with('success','Subcategory updated successfully');
        }else
        {
            return redirect('admin/subcategory')->with('success','Some error');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try{
            $affected = DB::delete("DELETE FROM subcategory WHERE id = {$id}");
            return redirect('/admin/subcategory')->with('success','Subcategory deleted successfully.');
        }catch(\Illuminate\Database\QueryException  $ex){
            return redirect('/admin/subcategory')->with('error','Cannot delete this Subcategory.');
        }
    }
}
