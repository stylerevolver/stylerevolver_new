<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use App\models\company;
use App\models\area;
use DB;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admin::company.companyShow');
    }

    /**
     * Show the form for creating |a new resource.
     * @return Response
     */
    public function create()
    {
        $area=area::get();
        return view('admin::company.companyAdd',compact('area'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required|regex:/^[a-zA-Z ]+$/u|unique:company,company_name|max:255|string',
            'address' => 'required|string',
            'email' => 'required|email|max:255|unique:company,company_email',
            'contact' => 'required|regex:/^[0-9 ]+$/u|digits_between:10,11',
            'areaname' => 'required'
        );
        $customeMessage = array(
            'name.required' => 'Company name can not be empty.',
            'name.string' => 'Company name only varchar.',
            'address.required' => 'Address can not be empty.',
            'address.string' => 'Address must be varchar value.',
            'email.required' => 'Email can not be empty.',
            'contact.required'=>'Contact can not be empty',
            'contact.digits_between' => 'Contact must be 10 or 11 digits.',
            'areaname.required' => 'Area can not be empty.',
        );
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $company = company::create([
            'company_name' => ucfirst($request->name),
            'company_address' => $request ->address,
            'company_email' => $request ->email,
            'company_contact' => $request ->contact,
            'area_id' => $request ->areaname,
        ]);
        /*session::flash('message','Company successfully added');
        session::flash('alert-class','alert success');*/
         return redirect('/admin/company')->with('success','Company created successfully');
        /*$notification = array(
            'message' => 'Company created successfully!',
            'alert-type' => 'success'
        );
        return redirect('admin/company')->with($notification);*/

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show()
    {
        $company=company::join('area as a','a.id','=','company.area_id')
        ->select('company.id','company.company_name','company.company_address','company.company_email','company.company_contact','a.area_name')
        ->get();
         return \DataTables::of($company)
            ->editColumn('id', function($data)
            {
                return $data->id;
            })
            ->editColumn('company_name', function($data)
            {
                return $data->company_name;
            })
             ->editColumn('company_address', function($data)
            {
                return $data->company_address;
            })
              ->editColumn('company_email', function($data)
            {
                return $data->company_email;
            })
             ->editColumn('company_contact', function($data)
            {
                return $data->company_contact;
            })
              ->editColumn('area_name', function($data)
            {
                return $data->area_name;
            })
            
            
            
            ->editColumn('action', function($data)
            {
                $html = '<a href="'.url('/admin/editCompany/'.$data->id) .'" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x"><i class="fas fa-edit"></i></a>
                <a href="'.url('/admin/deleteCompany/'.$data->id) .'" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x" onclick="return confirm('."'Are you sure you want to delete this company?'".');"><i class="fas fa-trash"></i></a>
                ';
                return $html;
            })
            ->rawColumns(['id','company_name','company_address','company_email','company_contact','area_name','action'])
            ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $area=area::get();
        $company=company::where('id','=',$id)->first();
        return view('admin::company.companyUpdate')->with('company',$company)->with('area',$area);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request)
    {
        $rules = array(
            'name' => 'required|regex:/^[a-zA-Z ]+$/u|unique:company,company_name,'.$request->company_id.'|max:255',
            'address' => 'required|string',
            'email' => 'required|email|max:255|unique:company_email,'.$request->company_id,
            'contact' => 'required|regex:/^[0-9 ]+$/u|digits_between:10,11',
            'areaname' => 'required'
        );
        $customeMessage = array(
            'name.required' => 'Company name can not be an empty.',
            'name.string' => 'Company name only varchar',
            'address.required' => 'Address can not be empty',
            'address.string' => 'Address must be varchar value',
            'email.required' => 'Email can not be empty',
            'contact.required' => 'Contact can not be empty',
            'contact.digits_between' => 'Contact must be 10 or 11 digits.',
            'areaname.required' => 'Area can not be empty',

        );
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $company=company::where('id','=',$request->company_id)->first();

        if(!empty($company))
        {
            $company->update([
                'company_name' =>ucfirst($request->name),
                'company_address' => $request ->address,
                'company_email' => $request ->email,
                'company_contact' => $request ->contact,
                'area_id' => $request ->areaname,
            ]);
            return redirect('admin/company')->with('success','Company updated successfully');
        }else
        {
            return redirect('admin/company')->with('success','Some error');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try{
            $affected = DB::delete("DELETE FROM company WHERE id = {$id}");
            return redirect('/admin/company')->with('success','Company deleted successfully');
        }catch(\Illuminate\Database\QueryException  $ex){
            return redirect('/admin/company')->with('error','Cannot delete this company');
        }
    }
}
