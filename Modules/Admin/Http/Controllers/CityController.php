<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use App\models\city;
use App\models\area;
use App\User;
use DB;


class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function viewUser()
    {
        return view('admin::user.usershow',compact('user'));
    }

    public function jsonUser(){
        $user=User::join('area as a','a.id','=','users.area_id')
            ->select('users.id','users.first_name','users.email','users.contact_no','a.area_name','users.address','users.dob','users.gender')
            ->get();
         return \DataTables::of($user)
           /* ->editColumn('name', function($data)
            {
                return $data->name;
            })*/
            ->editColumn('first_name', function($data)
            {
                return $data->first_name;
            })
            
            ->editColumn('email', function($data)
            {
                return $data->email;
            }) 
            ->editColumn('contact_no', function($data)
            {
                return $data->contact_no;
            })
            ->editColumn('area_name', function($data)
            {
                return $data->area_name;
            })
            ->editColumn('address', function($data)
            {
                return $data->address;
            })
            ->editColumn('dob', function($data)
            {
                return date('d-m-Y',strtotime($data['dob']));
            })
             ->editColumn('gender', function($data)
            {
                return $data->gender;
            })
            ->rawColumns(['id','name','first_name','email','contact_no','area_name','address','dob','gender'])
            ->make(true);

    }
    public function index()
    {
        return view('admin::city.cityShow');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {

        return view('admin::city.cityAdd');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required|regex:/^[a-zA-Z ]+$/u|unique:city,city_name|max:255',
        );
        $customeMessage = array(
            'name.required' => 'City name can not be an empty.',
        );
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $city = city::create([
            'city_name' => ucfirst($request->name),
        ]);
         return redirect('/admin/city')->with('success','City created successfully');
        /*$notification = array(
            'message' => 'City created successfully!',
            'alert-type' => 'success'
        );
        return redirect('admin/city')->with($notification);*/
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show()
    {
        $city=city::get();
         return \DataTables::of($city)
            ->editColumn('id', function($data)
            {
                return $data->id;
            })
            ->editColumn('city_name', function($data)
            {
                return $data->city_name;
            })
            ->editColumn('action', function($data)
            {
                $html = '<a href="'.url('/admin/editCity/'.$data->id) .'" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x"><i class="fas fa-edit"></i></a>
                <a href="'.url('/admin/deleteCity/'.$data->id) .'" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x" onclick="return confirm('."'Are you sure you want to delete this city?'".');"><i class="fas fa-trash"></i></a>
                ';
                return $html;
            })
            ->rawColumns(['id','city_name', 'action'])
            ->make(true);
       
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $city=city::where('id','=',$id)->first();
        return view('admin::city.cityUpdate',compact('city'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request)
    {
        $rules = array(
            'name' => 'required|regex:/^[a-zA-Z ]+$/u|unique:city,city_name,'.$request->city_id.'|max:255',
        );
        $customeMessage = array(
            'name.required' => 'City name can not be an empty.',
        );
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $city=city::where('id','=',$request->city_id)->first();

        if(!empty($city))
        {
            $city->update([
                'city_name' =>ucfirst($request->name),
            ]);
            return redirect('admin/city')->with('success','City updated successfully');
        }else
        {
            return redirect('admin/city')->with('success','Some error');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {   
        try{
            $affected = DB::delete("DELETE FROM city WHERE id = {$id}");
            return redirect('/admin/city')->with('success','City deleted successfully.');
        }catch(\Illuminate\Database\QueryException  $ex){
            return redirect('/admin/city')->with('error','Cannot delete this city.');
        }
    }
}
