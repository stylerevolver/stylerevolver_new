<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use App\models\appointment;
use App\models\product;
use App\models\sales;
use App\models\sales_details;
use App\models\sales_payment;
use App\models\customize_product;
use App\models\company;
use Illuminate\Support\Facades\Mail;
use App\Mail\AppointmentVisited;
use App\User;
use DB;
use Auth;
use PDF;


class AppointmentAdminController extends Controller
{
    public function index()
    {
        return view('admin::appointment.appointmentShow');
    }

    public function show()
    {
         $appointment=appointment::select('id','complete_status','visited_status','appointment_date','appointment_time','user_id')
            ->get();
         return \DataTables::of($appointment)
            ->editColumn('id', function($data)
            {
                return $data->id;
            })
             ->editColumn('first_name', function($data)
            {
                return $data->appointmentUser->first_name;
            })
            ->editColumn('complete_status', function($data)
            {
                return $data->complete_status;
            })
            ->editColumn('visited_status', function($data)
            {
                return $data->visited_status;
            })
            ->editColumn('appointment_date', function($data)
            {
                return date('d-m-Y',strtotime($data['appointment_date']));
            })
             ->editColumn('appointment_time', function($data)
            {
                return date('h:i A',strtotime($data->appointment_time));
            })
             ->editColumn('action', function($data)
            {
                $html="";
                if (($data->complete_status == "Confirm_Pending" || $data->complete_status == "Pending") && ($data->visited_status == "Pending") ) {
                    $customize_product=customize_product::where('price','=',NULL)->where('appointment_id',$data->id)->count();
                    $html.='<a href="'.url('/admin/customizedOrder/'.$data->id) .'" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x">Details</a> ';
                    if($customize_product == 0){
                        $html.= '<a href="'.url('/admin/updateStatus/'.$data->id) .'"  class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x"><i class="fa fa-check" aria-hidden="true"></i></a>';
                    }
                }else{
                    $html = '<a href="'.url('/admin/customizedOrder/'.$data->id) .'" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x">Details</a> <a href="#"  class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x"><i class="fa fa-thumbs-up" aria-hidden="true"></i></a>';
                }
                return $html;
            })
            ->rawColumns(['id','first_name','complete_status','visited_status','appointment_date','appointment_time','action'])
            ->make(true);
    }




    public function edit($id)
    {
        $total=customize_product::select('appointment_id', \DB::raw('sum(price) as total'))
                ->groupBy('appointment_id')
                ->where('appointment_id','=',$id)
                ->first();
        if(empty($total)){
            return redirect('/admin/Appointments')->with('error','Customize product not found'); 
        }

        $status=appointment::find($id);
        $status->update([
               'complete_status'=>'Complete',
               'visited_status'=>'Visited',
        ]);
        $user=User::where('id','=',$status->user_id)->first();
        $maildata['first_name']=$user['first_name'];
        $maildata['last_name']=$user['last_name'];
        $maildata['appointment_time']=$status->appointment_time;
        $maildata['appointment_date']=$status->appointment_date;
        $sales=sales::create([
            'sales_date' =>date("Y-m-d",time()),
            'ammount' =>$total->total,
            'sgst' =>'2.5',
            'cgst' =>'2.5',
            'user_id' =>$status->user_id,
            'status' =>'Complete',
        ]);
        if($sales){
            $cus_pro=customize_product::where('appointment_id','=',$id)->get();
            foreach ($cus_pro as $data) {
                $sales_details=sales_details::create([
                    'sales_id' =>$sales->id,
                    'customize_product_id' =>$data->id,
                    'price' =>$data->price,
                    'qty' =>'1',
                ]);
               // $affected = DB::delete("DELETE FROM customize_product WHERE id = {$data->id}");
            }
            $sales_payment=sales_payment::create([
                'amount' =>$sales->ammount,
                'payment_date' =>date("Y-m-d",time()),
                'transaction_date' =>NULL,
                'transaction_id' =>0,
                'payment_type_id'=>1,
                'sales_id' =>$sales->id,
            ]);
        }
        $id_sales=$sales->id;
         $company=company::first();
        $sales=sales::join('users','users.id','=','sales.user_id')
            ->where('sales.id','=',$id_sales)
            ->select('users.first_name','users.last_name','users.address','sales.sales_date','sales.id as sales_id','users.id as user_id','users.email','sales.sgst','sales.cgst')
            ->first();
        $details=sales_details::join('customize_product as p','p.id','=','sales_details.customize_product_id')
                    ->where('sales_details.sales_id','=',$id_sales)
                    ->select('sales_details.sales_id','p.customize_product_name','sales_details.price','sales_details.qty')
                    ->get();
        //echo '<pre>';print_r($details);exit();
        $type="view";
        $pdf = PDF::loadView('Invoice.customize_invoice',compact('company','sales','details','type'));
        $name=$user['first_name'].$id_sales.'.pdf';
        //dd($user['email']);
        //pdf genrate
        $user_email=$user['email'];
        try {
            Mail::send('emails.appointmentVisited', $maildata, function($message)use($user_email,$pdf,$name,$maildata) {
                $message->to($user_email)
                ->subject("Appointment")
                ->attachData($pdf->output(), $name);
            });
            //Mail::to($user['email'])->send(new AppointmentVisited($maildata,$files))->attachData($pdf->output(),$name);
        } catch (Exception $e) {
            return redirect('/admin/Appointments')->with('error','Appointment mail not send.');    
        }
        return redirect('/admin/Appointments')->with('success','Appointment successfully updated...!');
    }


    public function customizedProductIndex($id)
    {
        $customize_product=customize_product::join('appointment as a','a.id','=','customize_product.appointment_id')
                    ->where('customize_product.appointment_id','=',$id)
                    ->select('customize_product.id','customize_product.customize_product_name','customize_product.customize_product_image','customize_product.appointment_id','customize_product.description','customize_product.price','customize_product.appointment_id')
                    ->get();
        return view('admin::customizedOrder.customizedOrdershow',compact('customize_product'));
    }

   


    public function customizedProductDetailsEdit($id)
    { 
        $customize_product=customize_product::where('id','=',$id)->first();
        return view('admin::customizedOrder.customizedProductDetailsUpdate',compact('customize_product'));
    }
    public function customizedProductDetailsUpdate(Request $request)
    {
        if ($request->hasFile('image')) {
                $rules = array(
                    'name' => 'required|regex:/^[a-zA-Z ]+$/u|max:255',
                    'image' => 'required|image|mimes:jpeg,png,jpg',
                    'description' => 'required',
                    'price' => 'required',
                );
                $customeMessage = array(
                    'name.required' => 'customized product name can not be an empty',
                    'name.string' => 'customized product name is must be in string value',
                    'image.required' =>'customize product image can not be an empty',
                    'description.required' =>'description can not be an empty', 
                    'price.required' =>'price can not be an empty', 
                    
                );
         }else{
               $rules = array(
                    'name' => 'required|regex:/^[a-zA-Z ]+$/u|max:255',
                    'description' => 'required',
                    'price' => 'required',
                );
                $customeMessage = array(
                    'name.required' => 'customized product name can not be an empty',
                    'name.string' => 'customized product name is must be in string value',
                    'description.required' =>'description can not be an empty', 
                    'price.required' =>'price can not be an empty', 
                    
                );
        }
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        if ($request->hasFile('image')) {
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            if(request()->image->move(public_path('images'), $imageName)){
                $image_name=$imageName;
            }else{
                $message="Image not uploaded please try again.";
                return redirect()->back()->withErrors($message)->withInput();
            }
        }else{
                $image_name=$request->pro_image;
        }
            
        $customize_product=customize_product::where('id','=',$request->id)->first();
        if(!empty($customize_product))
        {
            $customize_product->update([
                'customize_product_name' =>ucfirst($request->name),
                'customize_product_image' =>$image_name,
                'description' =>$request->description,
                'price' =>(int)$request->price,
            ]);
                return redirect('admin/customizedOrder/'.$request->app_id)->with('success','Updated successfully');
        }else{
            return redirect('admin/updateCustomizedProductDetails/'.$request->id)->with('success','Some error');
        }
           
    }
    
    public function Appointment(Request $request){
        return view('admin::appointment.appointmentShow');
    }
    
    public function AppointmentReport(Request $request){
        $appointment=appointment::get();
        //$pdf = PDF::loadView('admin::appointment.customizedOrdershow',compact('appointment'));  
        //return $pdf->download('AppointmentReport.pdf');
        return view('admin::appointment.appointmentShow',compact('appointment'));
    }
}
