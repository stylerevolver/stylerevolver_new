<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\models\sales;
use App\models\product;
use App\models\sales_details;
use App\models\sales_payment;
use App\models\company;
use DB;
use PDF;

class SalesController extends Controller
{ 
    public function index()
    {
        return view('admin::sales.salesShow');
    }

    public function show()
    {
        $sales=sales::join('users as u','u.id','=','sales.user_id')
        ->select('sales.id as sales_id','sales.sales_date','u.first_name','u.contact_no','sales.status')
        ->get();
         return \DataTables::of($sales)
            ->editColumn('id', function($data)
            {
                return $data->sales_id;
            })
             ->editColumn('first_name', function($data)
            {
                return $data->first_name;
            })
             ->editColumn('contact_no', function($data)
            {
                return $data->contact_no;
            })
            ->editColumn('sales_date', function($data)
            {
                return date('d-m-Y',strtotime($data['sales_date']));
            })
              ->editColumn('details', function($data)
            {
                $details = '<a href="'.url('/admin/salesDetails/'.$data->sales_id) .'" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x">Details</a>';
                return $details;
            })
              ->editColumn('status', function($data)
            {
                return $data->status;
            })
              ->editColumn('payment', function($data)
            {
                $payment = '<a href="'.url('/admin/salesPayment/'.$data->sales_id).'" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x">Payment</a>';
                return $payment;
            })
              ->editColumn('Invoice', function($data)
            {
                $Invoice = '<div style="text-align:center;margin-left:8px"><a href="'.url('/viewinvoiceFront/'.$data->sales_id).'" data-text="Invoice" style="font-size: 20px;" target="_blank"><i class="fa fa-eye" aria-hidden="true" style="font-size: 20px;color:#7BCB4D"></i></a>   <a href="'.url('/invoiceFront/'.$data->sales_id).'" data-text="Invoice" style="font-size: 20px;"><i class="fa fa-download" aria-hidden="true" style="font-size: 20px;color:red"></i></a></div>';
                return $Invoice;

                /*
                $Invoice = '<a href="'.url('/admin/InvoiceDownload/'.$data->sales_id).'" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x">Invoice</a>';
                return $Invoice;*/
            })
            ->rawColumns(['id','sales_date','first_name','details','contact_no','status','payment','Invoice'])
            ->make(true);
    }

    public function salesDetails($id)
    {
        $user=sales::join('users as u','u.id','=','sales.user_id')->where('sales.id','=',$id)->first();
        $details=sales_details::join('sales as s','s.id','=','sales_details.sales_id')
                    ->join('product as p','p.id','=','sales_details.product_id')
                    ->where('sales_details.sales_id','=',$id)
                    ->select('sales_details.sales_id','p.product_name','sales_details.price','sales_details.qty')
                    ->get();
        return view('admin::sales.salesDetailsShow',compact('details','user'));  
    }

    public function salesPayment($id)
    {
        $user=sales::join('users as u','u.id','=','sales.user_id')->where('sales.id','=',$id)->first();
        $sp=sales_payment::join('sales as s','s.id','=','sales_id')
                    ->join('payment_type as pt','pt.id','=','payment_type_id')
                    ->join('users as u','u.id','=','s.user_id')
                    ->where('sales_payment.sales_id','=',$id)
                    ->select('sales_payment.id','sales_payment.amount','u.first_name','u.last_name','sales_payment.payment_date','sales_payment.transaction_date','sales_payment.transaction_id','pt.payment_type')
                    ->get();
        //echo "<pre>"; print_r($sp->all());exit();
        return view('admin::sales.salesPaymentShow',compact('sp','user'));
    }
    public function Invoice(Request $request)
    {
        return view('admin::sales.InvoiceShow');
    }
    public function InvoiceDownload($id)
    {
        $company=company::first();
        $sales=sales::join('users','users.id','=','sales.user_id')
            ->where('sales.id','=',$id)
            ->select('users.first_name','users.last_name','users.address','sales.sales_date','sales.id as sales_id','users.id as user_id','users.email','sales.sgst','sales.cgst')
            ->first();
        $details=sales_details::join('sales as s','s.id','=','sales_details.sales_id')
                    ->join('product as p','p.id','=','sales_details.product_id')
                    ->where('sales_details.sales_id','=',$id)
                    ->select('sales_details.sales_id','p.product_name','sales_details.price','sales_details.qty')
                    ->get();
        //$product=product::get();
        $name=$sales->first_name.'.pdf';
        $pdf = PDF::loadView('Invoice.Invoice',compact('company','sales','details'));  
        return $pdf->download('InvoiceDownload.pdf');
        //return view('Invoice.Invoice',compact('product','company','sales','details'));
    }
}

