<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;
use Modules\Admin\Entities\Admin;
use Illuminate\Support\Facades\Mail;
use App\Mail\forgotPassword;
use Auth;
use DB;


class AdminLoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        return view('admin::login');
    }


    public function loginAdmin(Request $request)
    {
        // Validate the form data
        $request->validate([
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);
        // Attempt to log the user in
        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            // if successful, then redirect to their intended location
            return redirect()->intended(route('admin.dashboard'));
        }
        else
        {
            return redirect()->back()->with('error', 'Username and password is incorrect');
        }

    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect()->route('admin.auth.login');
    }

    public function forgotPassword()
    {
        return view('admin::forgetPasswordAdmin');
    }

    public function forgotPasswordstore(Request $request)
    {
        $rules = array(
            'email' => 'required|email',
        );
        $customeMessage = array(
            'email.required' => 'Admin email id can not be an empty.',
        );
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $data=array();
        $usercount=Admin::Where('email','=',$request->email)->first();
        if(!empty($usercount)){
            $tokan=str_random(50);
            $usercount->update([
                'remember_token' => $tokan,
            ]);
            $data['remember_token'] = $tokan;
            $data['email'] = $request->email;
            try{
                Mail::to($request->email)->send(new forgotPassword($data));
                return redirect('/admin/forgotPassword')->with('success','Reset password link sent successfully, please check your mail..!');
            }
            catch(\Exception $e)
            {
                return redirect('/admin/forgotPassword')->with('error','Mail not send some error occur.');
            }
        }else{
            return redirect('/admin/forgotPassword')->with('error','Email id not exists');
        }
    }
    public function newpassword($email,$tokan){
        $usercount=Admin::Where('email','=',$email)
                    ->Where('remember_token','=',$tokan)
                    ->first();
        $email=$email;
        if($usercount != "" && !empty($usercount)){
            return view('admin::newpassword',compact('email'));
        }else{
            return redirect('/admin/forgotPassword')->with('error','Please try again..!');
        }   
    }
    public function storepassword(Request $request){
         $rules = array(
            'email' => 'required|email',
            'password' => 'required_with:password_confirmation|same:password_confirmation|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
            'password_confirmation' => 'required',
        );
        $customeMessage = array(
            'email.required' => 'Admin email id can not be an empty.',
        );
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $usercount=Admin::Where('email','=',$request->email)->first();
        if(!empty($usercount)){
            $usercount->update([
                'remember_token' => "",
                'password' => Hash::make($request->password)
            ]);
            return redirect('/admin/login')->with('success','Password successfully updated!');
        }else{
            return redirect('/admin/login')->with('success','Please try again..!');
        }
    }
}
