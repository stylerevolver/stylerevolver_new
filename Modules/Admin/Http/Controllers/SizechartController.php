<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use App\models\size_chart;
use App\models\subcategory;
use DB;


class SizechartController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admin::size_chart.sizechartShow');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $subcategory=subcategory::get();
        return view('admin::size_chart.sizechartAdd',compact('subcategory'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required|regex:/^[a-zA-Z -]+$/u|max:255|string',
            'length' => 'required|regex:/^[0-9 ]+$/u|digits_between:0,100',
            'waist' => 'required|regex:/^[0-9 ]+$/u|digits_between:0,100',
            'chest' => 'required|regex:/^[0-9 ]+$/u|digits_between:0,100',
            'hips' =>  'required|regex:/^[0-9 ]+$/u|digits_between:0,100',
            'subcategory' => 'required',
            );
        $customeMessage = array(
            'name.required' => 'Size name can not be empty.',
            'name.string' => 'Size name only varchar.',
            'length.required' => 'Length can not be empty.',
            'length.digits_between' => 'Length must between in 0 to 100.',
            'waist.required' => 'Waist can not be empty.',
            'waist.digits_between' => 'Waist must between in 0 to 100.',
            'chest.required' => 'Chest can not be empty.',
            'chest.digits_between' => 'Chest must between in 0 to 100.',
            'hips.required' => 'Hips can not be empty.',
            'hips.digits_between' => 'Hips must between in 0 to 100.',
            'subcategory.required' => 'Subcategory can not be empty.',
        );
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $size_chart = size_chart::create([
            'size_name' => ucfirst($request->name),
            'length' => $request ->length,
            'waist' => $request ->waist,
            'chest' => $request ->chest,
            'hips' => $request ->hips,
            'subcategory_id' => (int)$request->subcategory,
        ]);
        /*session::flash('message','Sizechart successfully added');
        session::flash('alert-class','alert success');*/
         return redirect('/admin/size_chart')->with('success','Size-Chart created successfully');
        /*$notification = array(
            'message' => 'Size-Chart created successfully!',
            'alert-type' => 'success'
        );
        return redirect('admin/size_chart')->with($notification);*/


    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show()
    {
        $subcategory=subcategory::get();
         $size_chart=size_chart::join('subcategory as s','s.id','=','size_chart.subcategory_id')
                        ->select('s.subcategory_name','size_chart.id','size_chart.size_name','size_chart.length','size_chart.waist','size_chart.chest','size_chart.hips')->get();
         return \DataTables::of($size_chart)
            ->editColumn('id', function($data)
            {
                return $data->id;
            })
            ->editColumn('size_name', function($data)
            {
                return $data->size_name;
            })
            ->editColumn('length', function($data)
            {
                return $data->length;
            })
            ->editColumn('waist', function($data)
            {
                return $data->waist;
            })
            ->editColumn('chest', function($data)
            {
                return $data->chest;
            })
            ->editColumn('hips', function($data)
            {
                return $data->hips;
            })
            ->editColumn('subcategory_name', function($data)
            {
                return $data->subcategory_name;
            })
 
            ->editColumn('action', function($data)
            {
                $html = '<a href="'.url('/admin/editSizechart/'.$data->id) .'" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x"><i class="fas fa-edit"></i></a>
                <a href="'.url('/admin/deleteSizechart/'.$data->id) .'" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x" onclick="return confirm('."'Are you sure you want to delete this size_chart?'".');"><i class="fas fa-trash"></i></a>
                ';
                return $html;
            })
            ->rawColumns(['id','size_name','length','waist','chest','hips','subcategory_name','action'])
            ->make(true);

       
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $subcategory=subcategory::get();
        $size_chart=size_chart::where('id','=',$id)->first();
        return view('admin::size_chart.sizechartUpdate',compact('size_chart','subcategory'));

    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request)
    {
        $rules = array(
            
            'name' => 'required|regex:/^[a-zA-Z -]+$/u|max:255|string',
            'length' => 'required|regex:/^[0-9 ]+$/u|digits_between:0,100',
            'waist' => 'required|regex:/^[0-9 ]+$/u|digits_between:0,100',
            'chest' => 'required|regex:/^[0-9 ]+$/u|digits_between:0,100',
            'hips' =>  'required|regex:/^[0-9 ]+$/u|digits_between:0,100',
            'subcategory' => 'required',
            );
        $customeMessage = array(
            'name.required' => 'Size name can not be empty.',
            'name.string' => 'Size name only varchar.',
            'length.required' => 'Length can not be empty.',
            'length.digits_between' => 'Length must between in 0 to 100.',
            'waist.required' => 'Waist can not be empty.',
            'waist.digits_between' => 'Waist must between in 0 to 100.',
            'chest.required' => 'Chest can not be empty.',
            'chest.digits_between' => 'Chest must between in 0 to 100.',
            'hips.required' => 'Hips can not be empty.',
            'hips.digits_between' => 'Hips must between in 0 to 100.',
            'subcategory.required' => 'Subcategory can not be empty.',
        );
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $size_chart=size_chart::where('id','=',$request->sizechart_id)->first();

        if(!empty($size_chart))
        {
            $size_chart->update([
                'size_name' =>ucfirst($request->name),
                'length' => $request ->length,
                'waist' => $request ->waist,
                'chest'=> $request ->chest,
                'hips'=> $request ->hips,
                'subcategory_id'=> (int)$request->subcategory,
            ]);
            return redirect('admin/size_chart')->with('success','Size-Chart updated successfully');
        }else
        {
            return redirect('admin/size_chart')->with('success','Some error');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try{
            $affected = DB::delete("DELETE FROM size_chart WHERE id = {$id}");
            return redirect('/admin/size_chart')->with('success','Size-Chart deleted successfully');
        }catch(\Illuminate\Database\QueryException  $ex){
            return redirect('/admin/size_chart')->with('error','Cannot delete this Size-Chart');
        }

    }
}
