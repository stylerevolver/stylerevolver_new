<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use App\models\payment_type;
use DB;

class PaymentTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
         return view('admin::payment_type.paymentTypeShow');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::payment_type.paymentTypeAdd');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required|regex:/^[a-zA-Z ]+$/u|unique:payment_type,payment_type|max:255',
        );
        $customeMessage = array(
            'name.required' => 'Payment Type can not be an empty.',
        );
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $payment_type = payment_type::create([
            'payment_type' => ucfirst($request->name),
        ]);
         return redirect('/admin/paymentType')->with('success','Payment Type Created successfully');
        /*$notification = array(
            'message' => 'Payment Type created successfully!',
            'alert-type' => 'success'
        );
        return redirect('admin/paymentType')->with($notification);*/
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show()
    {
        $payment_type=payment_type::get();
         return \DataTables::of($payment_type)
            ->editColumn('id', function($data)
            {
                return $data->id;
            })
            ->editColumn('payment_type', function($data)
            {
                return $data->payment_type;
            })
            ->editColumn('action', function($data)
            {
                $html = '<a href="'.url('/admin/editPaymentType/'.$data->id) .'" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x"><i class="fas fa-edit"></i></a>
                <a href="'.url('/admin/deletePaymentType/'.$data->id) .'" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x" onclick="return confirm('."'Are you sure you want to delete this paymentType?'".');"><i class="fas fa-trash"></i></a>
                ';
                return $html;
            })
            ->rawColumns(['id','payment_type', 'action'])
            ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $payment_type=payment_type::where('id','=',$id)->first();
        return view('admin::payment_type.paymentTypeUpdate',compact('payment_type'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request)
    {
         $rules = array(
            'name' => 'required|regex:/^[a-zA-Z ]+$/u|unique:payment_type,payment_type,'.$request->paymentType_id.'|max:255',
        );
        $customeMessage = array(
            'name.required' => 'payment type can not be an empty.',
        );
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $payment_type=payment_type::where('id','=',$request->paymentType_id)->first();

        if(!empty($payment_type))
        {
            $payment_type->update([
                'payment_type' =>ucfirst($request->name),
            ]);
            return redirect('admin/paymentType')->with('success','Payment Type updated successfully');
        }else
        {
            return redirect('admin/paymentType')->with('success','Some error');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try{
            $affected = DB::delete("DELETE FROM payment_type WHERE id = {$id}");
            return redirect('/admin/payment_type')->with('success','Payment Type deleted successfully');
        }catch(\Illuminate\Database\QueryException  $ex){
            return redirect('/admin/payment_type')->with('error','Cannot delete this Payment Type');
        }
    }
}
