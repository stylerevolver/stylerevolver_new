<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use App\models\feedback;
use App\models\user;
use App\models\product;
use DB;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admin::feedback.feedbackShow');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        //return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show()
    {
        $feedback=feedback::join('users as u','u.id','=','feedback.user_id')
            ->join('product as p','p.id','=','feedback.product_id')
            ->select('u.first_name','p.product_name','feedback.feedback_description','feedback.feedback_date')
        ->get();
         return \DataTables::of($feedback)
            ->editColumn('first_name', function($data)
            {
                return $data->first_name;
            })
            ->editColumn('product_name', function($data)
            {
                return $data->product_name;
            })
            
            ->editColumn('feedback_description', function($data)
            {
                return $data->feedback_description;
            }) 
            ->editColumn('feedback_date', function($data)
            {
                return date('d-m-Y',strtotime($data['feedback_date']));
            })
            
           /* ->editColumn('action', function($data)
            {
                $html = '<a href="'.url('/admin/deleteFeedback/'.$data->user_id) .'" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x" onclick="return confirm('."'Are you sure you want to delete this feedback?'".');"><i class="fas fa-trash"></i></a>
                ';
                return $html;
            })*/
            ->rawColumns(['first_name','product_name','feedback_description','feedback_date'/*,'action'*/])
            ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
}
