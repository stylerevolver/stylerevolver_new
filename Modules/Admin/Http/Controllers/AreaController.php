<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use App\models\area;
use App\models\city;
use DB;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admin::area.areaShow');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $city=city::get();
        return view('admin::area.areaAdd',compact('city'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //echo '<pre>';print_r($request->all());exit();
         $rules = array(
            'name' => 'required|regex:/^[a-zA-Z ]+$/u|unique:area,area_name|max:255|string',
            'pincode' => 'required|regex:/^[0-9 ]+$/u|digits_between:6,6',
            'cityname' => 'required'
        );
        $customeMessage = array(
            'name.required' => 'Area name can not be empty.',
            'name.string' => 'Area name only varchar.',
            'pincode.required' => 'Pincode can not be empty.',
            'pincode.digits_between' => 'Pincode must be 6 digits.',
            'cityname.required' => 'city can not be empty.',
        );
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $area = area::create([
            'area_name' => ucfirst($request->name),
            'pincode' => $request ->pincode,
            'city_id' => $request ->cityname,
        ]);
         return redirect('/admin/area')->with('success','Area created successfully');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show()
    {

        $area=area::join('city as c','c.id','=','area.city_id')
        ->select('area.id as area_id','area_name','pincode','c.city_name')
        ->get();
         return \DataTables::of($area)
            ->editColumn('id', function($data)
            {
                return $data->area_id;
            })
            ->editColumn('area_name', function($data)
            {
                return $data->area_name;
            })
             ->editColumn('pincode', function($data)
            {
                return $data->pincode;
            })
              ->editColumn('city_name', function($data)
            {
                return $data->city_name;
            })
            
            
            
            ->editColumn('action', function($data)
            {
                $html = '<a href="'.url('/admin/editArea/'.$data->area_id) .'" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x"><i class="fas fa-edit"></i></a>
                <a href="'.url('/admin/deleteArea/'.$data->area_id) .'" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x" onclick="return confirm('."'Are you sure you want to delete this area?'".');"><i class="fas fa-trash"></i></a>
                ';
                return $html;
            })
            ->rawColumns(['id','area_name','pincode','city_name','action'])
            ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $city=city::get();
        $area=area::where('id','=',$id)->first();
        return view('admin::area.areaUpdate')->with('area',$area)->with('city',$city);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request)
    {
         $rules = array(
            'name' => 'required|regex:/^[a-zA-Z ]+$/u|unique:area,area_name,'.$request->area_id.'|max:255',
            'pincode' => 'required|regex:/^[0-9 ]+$/u|digits_between:6,6',
            'cityname' => 'required'
        );
        $customeMessage = array(
            'name.required' => 'Area name can not be an empty.',
            'name.string' => 'Area name only varchar.',
            'pincode.required' => 'pincode can not be empty.',
            'pincode.digits_between' => 'Pincode must be 6 digits.',
            'cityname.required' => 'City can not be empty.',

        );
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $area=area::where('id','=',$request->area_id)->first();

        if(!empty($area))
        {
            $area->update([
                'area_name' =>ucfirst($request->name),
                'pincode' => $request ->pincode,
                'city_id' => $request ->cityname,
            ]);
            return redirect('admin/area')->with('success','Area updated successfully');
        }else
        {
            return redirect('admin/area')->with('success','Some error');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try{
            $affected = DB::delete("DELETE FROM area WHERE id = {$id}");
            return redirect('/admin/area')->with('success','Area deleted successfully');
        }catch(\Illuminate\Database\QueryException  $ex){
            return redirect('/admin/area')->with('error','Cannot delete this area');
        }
    }
}
