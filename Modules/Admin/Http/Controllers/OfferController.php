<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use App\models\offer;
use App\models\product;
use DB;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admin::offer.offerShow');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $product=product::get();
        return view('admin::offer.offerAdd',compact('product'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'couponcodet' => 'required',
            'offerpercentage' => 'required|regex:/^[0-9 ]+$/u',
            'startdate' => 'required|date|after_or_equal:today',
            'enddate' => 'required|date|after_or_equal:startdate',
            'productname' => 'required',
        );
        $customeMessage = array(
            'couponcodet.required' => 'Coupon code can not be an empty',
            'offerpercentage.required' => 'Offer Percentage can not be empty.',
            'startdate.required' => 'Strarting Date can not be empty.',
            'startdate.date' => 'Strarting Date must be in date format.',
            'enddate.required' => 'Ending Date can not be empty.',
            'enddate.date' => 'Ending Date must be in date format.',
            'productname.required' => 'Product can not be empty.',
        );
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $offer = offer::create([
            'couponcode' => ucfirst($request->couponcodet),
            'offer_percentage' => $request->offerpercentage,
            'start_date' => $request->startdate,
            'end_date' => $request->enddate,
            'product_id' => $request->productname,
        ]);

        return redirect('/admin/offer')->with('success','Offer Created successfully');
    }

    
    public function show()
    {
        $offer=offer::get();
         return \DataTables::of($offer)
            ->editColumn('id', function($data)
            {
                return $data->id;
            })
            ->editColumn('couponcode', function($data)
            {
                return $data->couponcode;
            })
             ->editColumn('offer_percentage', function($data)
            {
                return $data->offer_percentage;
            })
             ->editColumn('start_date', function($data)
            {
                return date('d-m-Y',strtotime($data['start_date']));
            })
             ->editColumn('end_date', function($data)
            {
                return date('d-m-Y',strtotime($data['end_date']));
            })
              ->editColumn('product_name', function($data)
            {
                return $data->product->product_name;
            })
            ->editColumn('action', function($data)
            {
                $html = '<a href="'.url('/admin/editOffer/'.$data->id) .'" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x"><i class="fas fa-edit"></i></a>
                <a href="'.url('/admin/deleteOffer/'.$data->id) .'" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x" onclick="return confirm('."'Are you sure you want to delete this offer?'".');"><i class="fas fa-trash"></i></a>
                ';
                return $html;
            })
            ->rawColumns(['id','couponcode','offer_percentage','start_date','end_date','product_name','action'])
            ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $product=product::get();
        $offer=offer::where('id','=',$id)->first();
        return view('admin::offer.offerUpdate')->with('offer',$offer)->with('product',$product);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request)
    {
        $rules = array(
            //'couponcode' => 'required',
            'offerpercentage' => 'required|regex:/^[0-9 ]+$/u',
            'startdate' => 'required|date|after_or_equal:today',
            'enddate' => 'required|date|after_or_equal:startdate',
            'productname' => 'required'
        );
        $customeMessage = array(
            //'couponcode.required' => 'Coupon code can not be an empty',
            'offerpercentage.required' => 'Offer Percentage can not be an empty.',
            'startdate.required' => 'Starting Date can not be empty',
            'startdate.date' => 'Starting Date must be in date format',
            'enddate.required' => 'Ending Date can not be empty',
            'enddate.date' => 'Ending Date must be in date format',
            'productname.required' => 'Product can not be empty',

        );
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $offer=offer::where('id','=',$request->offer_id)->first();

        if(!empty($offer))
        {
            $offer->update([
                //'couponcode' =>ucfirst($request->couponcode),
                'offer_percentage' =>ucfirst($request->offerpercentage),
                'start_date' => $request ->startdate,
                'end_date' => $request ->enddate,
                'product_id' => $request ->productname,
            ]);
            return redirect('admin/offer')->with('success','Offer updated successfully');
        }else
        {
            return redirect('admin/offer')->with('success','Some error');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try{
            $affected = DB::delete("DELETE FROM offer WHERE id = {$id}");
            return redirect('/admin/offer')->with('success','Offer deleted successfully');
        }catch(\Illuminate\Database\QueryException  $ex){
            return redirect('/admin/offer')->with('error','Cannot delete this Offer');
        }
    }
}
