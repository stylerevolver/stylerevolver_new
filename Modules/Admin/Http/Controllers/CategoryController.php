<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use App\models\category;
use DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admin::category.categoryShow');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::category.categoryAdd');

    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required|regex:/^[a-zA-Z ]+$/u|unique:category,category_name|max:255',
        );
        $customeMessage = array(
            'name.required' => 'Category name can not be an empty.',
        );
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $category = category::create([
            'category_name' => ucfirst($request->name),
        ]);
         return redirect('/admin/category')->with('success','Category created successfully');
        /*$notification = array(
            'message' => 'Category created successfully!',
            'alert-type' => 'success'
        );
        return redirect('admin/category')->with($notification);*/
    }
    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show()
    {
        $category=category::get();
         return \DataTables::of($category)
            ->editColumn('id', function($data)
            {
                return $data->id;
            })
            ->editColumn('category_name', function($data)
            {
                return $data->category_name;
            })
            ->editColumn('action', function($data)
            {
                $html = '<a href="'.url('/admin/editCategory/'.$data->id) .'" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x"><i class="fas fa-edit"></i></a>
                <a href="'.url('/admin/deleteCategory/'.$data->id) .'" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x" onclick="return confirm('."'Are you sure you want to delete this category?'".');"><i class="fas fa-trash"></i></a>
                ';
                return $html;
            })
            ->rawColumns(['id','category_name', 'action'])
            ->make(true);
       
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $category=category::where('id','=',$id)->first();
        return view('admin::category.categoryUpdate',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request)
    {
        $rules = array(
            'name' => 'required|regex:/^[a-zA-Z ]+$/u|unique:category,category_name,'.$request->category_id.'|max:255',
        );
        $customeMessage = array(
            'name.required' => 'Category name can not be an empty.',
        );
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $category=category::where('id','=',$request->category_id)->first();

        if(!empty($category))
        {
            $category->update([
                'category_name' =>ucfirst($request->name),
            ]);
            return redirect('admin/category')->with('success','Category updated successfully');
        }else
        {
            return redirect('admin/category')->with('success','Some error');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try{
            $affected = DB::delete("DELETE FROM category WHERE id = {$id}");
            return redirect('/admin/category')->with('success','Category deleted successfully');
        }catch(\Illuminate\Database\QueryException  $ex){
            return redirect('/admin/category')->with('error','Cannot delete this category');
        }
    }
}
