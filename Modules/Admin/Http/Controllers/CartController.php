<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use App\models\cart;
use App\models\user;
use App\models\product;
use DB;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admin::cart.cartShow');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        //return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show()
    {
       $cart=cart::join('users as u','u.id','=','cart.user_id')
            ->join('product as p','p.id','=','cart.product_id')
            ->select('p.product_name','cart.product_qty','cart.price','u.first_name')
        ->get();
         return \DataTables::of($cart)
            ->editColumn('first_name', function($data)
            {
                return $data->first_name;
            })
            ->editColumn('product_name', function($data)
            {
                return $data->product_name;
            })
            
            ->editColumn('product_Qty', function($data)
            {
                return $data->product_qty;
            }) 
            ->editColumn('price', function($data)
            {
                return '₹'.$data->price;
            })
            
            /*->editColumn('action', function($data)
            {
                $html = '<a href="'.url('/admin/deleteFeedback/'.$data->user_id) .'" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x" onclick="return confirm('."'Are you sure you want to delete this cart?'".');"><i class="fas fa-trash"></i></a>
                ';
                return $html;
            })*/
            ->rawColumns(['first_name','product_name','product_Qty','price'/*,'action'*/])
            ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
       /* try{
            $affected = DB::delete("DELETE FROM cart WHERE user_id = {$user_id}");
            return redirect('/admin/cart')->with('success','Aart deleted successfully');
        }catch(\Illuminate\Database\QueryException  $ex){
            return redirect('/admin/cart')->with('error','Cannot delete this cart');
        }*/
    }
}
