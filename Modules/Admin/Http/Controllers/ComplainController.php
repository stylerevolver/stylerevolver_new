<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use App\models\complain;
use App\User;
use App\models\sales;
use App\models\product;
use Illuminate\Support\Facades\Mail;
use App\Mail\ComplainResolved;
use DB;


class ComplainController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admin::complain.complainShow');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
       // return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show()
    {
        $complain=complain::join('users as u','u.id','=','complain.user_id')
            ->join('sales as s','s.id','=','complain.sales_id')
            ->join('product as p','p.id','=','complain.product_id')
            ->select('complain.id','complain.complain_date','complain.description','complain.complain_status','u.first_name','complain.sales_id','p.product_name')
        ->get();
         return \DataTables::of($complain)
            ->editColumn('id', function($data)
            { 
                return $data->id;
            })
            ->editColumn('complain_date', function($data)
            {
                return date('d-m-Y',strtotime($data['complain_date']));
            })
            ->editColumn('description', function($data)
            {
                return $data->description;
            })
            ->editColumn('complain_status', function($data)
            {
                return $data->complain_status;
            })
            ->editColumn('first_name', function($data)
            {
                return $data->first_name;
            })
            ->editColumn('sales_id', function($data)
            {
                return $data->sales_id;
            })
            ->editColumn('product_name', function($data)
            {
                return $data->product_name;
            })
            ->editColumn('action', function($data)
            {
                if($data->complain_status == "Pending"){
                 $html= '<a href="'.url('/admin/updateComplain/'.$data->id) .'" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x"><i class="fa fa-comments" aria-hidden="true" style="font-size: 20px;color:green"></i></a>';
                }else{
                    $html="-";
                }
                return $html;
            })
            ->rawColumns(['id','sales_id','first_name','product_name','description','complain_date','complain_status','action'])
            ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $complain=complain::find($id);
        $complain->update([
            'complain_status'=>'Complate',
        ]);
        $product=product::where('id','=',$complain->product_id)->value('product_name');
        $user=User::where('id','=',$complain->user_id)->first();
        $data['first_name']=$user->first_name;
        $data['last_name']=$user->last_name;
        $data['pro_name']=$product;
        $data['msg']=$complain->description;
        try {
            Mail::to($user->email)->send(new ComplainResolved($data));
        } catch (Exception $e) {
            return redirect('/admin/complain')->with('error','Complain mail not send.');    
        }
        return redirect('/admin/complain')->with('success','Complain successfully updated..!');    
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
         /*try{
            $affected = DB::delete("DELETE FROM complain WHERE user_id = {$user_id}");
            return redirect('/admin/complain')->with('success','Complain deleted successfully');
        }catch(\Illuminate\Database\QueryException  $ex){
            return redirect('/admin/complain')->with('error','Cannot delete this Complain');
        }*/
    }
    
}
