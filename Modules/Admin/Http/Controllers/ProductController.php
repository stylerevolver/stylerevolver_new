<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use App\models\product;
use App\models\category;
use App\models\subcategory;
use App\models\size_chart;
use App\models\company;
use App\models\color;
use App\models\product_image;
use App\models\product_color;
use DB;
use File;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admin::product.productShow');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $product=product::get();
        $subcategory=subcategory::get();
        $size_chart=size_chart::get();
        $company=company::get();
        $color=color::get();
        $category=category::get();

        return view('admin::product.productAdd',compact('subcategory','size_chart','company','color'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required|regex:/^[0-9a-zA-Z ]+$/u|unique:product,product_name|max:255',
            'image' => 'required|image|mimes:jpeg,png,jpg',
            'price' => 'required',
            'qty' => 'required',
            'desc' => 'required',
            'subcategory' => 'required',
            'sizechart' => 'required',
            'company' => 'required',
            'color' => 'required',
        );
        $customeMessage = array(
            'name.required' => 'Product name can not be an empty.',
            'image.required' => 'Product image can not be an empty.',
            'price.required' => 'Price can not be empty.',
            'qty.required' => 'Quantity can not be empty.',
            'desc.required' => 'Description can not be empty.',
            'subcategory.required' => 'Subcategory can not be empty.',
            'sizechart.required' => 'Size can not be empty.',
            'company.required' => 'Company can not be empty.',
            'color.required' => 'Color can not be empty.',
        );
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $images       =       array();
        $imageName = request()->image->getClientOriginalName().time().'.'.request()->image->getClientOriginalExtension();
        if(request()->image->move(public_path('images'), $imageName)){
            $image_name=$imageName;
        }else{
            $message="Image not uploaded please try again.";
            return redirect()->back()->withErrors($message)->withInput();
        }
        $imageName=implode('|',$images);
        $product = product::create([
            'product_name' => ucfirst($request->name),
            'product_image' => $image_name,
            'product_price' => (int)$request->price,
            'product_qty' => (int)$request->qty,
            'description' => $request->desc,
            'subcategory_id' => $request->subcategory,
            'size_id' => $request->sizechart,
            'company_id' => $request->company,
            'color_id' => implode('|', $request->color),
        ]);
        $pro_id=$product->id;
        $pro_qty=$product->qty;
        foreach ($request->color as $key => $col_id) {
            $pro_color=product_color::create([
                'product_id' =>  $pro_id,
                'color_id' =>$col_id,
                'qty' => $pro_qty
            ]);
        }
        return redirect('/admin/product')->with('success','Product rceated successfully');
    }
    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show()
    {
        $category=category::get();
        $product=product::join('subcategory as s','s.id','=','product.subcategory_id')
            ->join('size_chart as sc','sc.id','=','product.size_id')
            ->join('category','category.id','=','s.category_id')
            ->join('company as c','c.id','product.company_id')
            ->select('product.id','product.product_name','product.product_image','product.product_price','product.product_qty','s.subcategory_name','sc.size_name','c.company_name','product.color_id','category.category_name','product.featured_filed','product.description')
            ->get();
         return \DataTables::of($product)
            ->editColumn('id', function($data)
            {
                return $data->id;
            })
            ->editColumn('product_name', function($data)
            {
                return $data->product_name;
            })
            ->editColumn('fetured', function($data)
            {
                if($data->featured_filed == "Yes"){
                    $color="red";
                }else{
                    $color="#da9fa7";
                }
                $html='<a href="#" class="fetured" id="fetured'.$data->id.'" data-id="'.$data->id.'" style="color:'.$color.';font-size:35px;"><i class="fas fa-star"></i></a>';
                return $html;
            })
            ->editColumn('product_image', function($data)
            {
                $html="";
                $url = url('/images/'.$data->product_image);   
                $html.='<div style="margin:5px;"><img src="'.$url.'" height="70" width="70" alt="'.$data->product_image.'"></div>';
                return $html;
            })
            ->editColumn('product_images', function($data)
            {
                $html="-";
                $images=product_image::where('product_id','=',$data->id)->get();
                if(count($images) > 0){
                    $html = '<a href="#" data-id="'.$data->id.'" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x more_images"><i class="fas fa-image"></i></a>';
                }
                return $html;
            })
            ->editColumn('product_price', function($data)
            {
                return '₹'.$data->product_price;
            }) 
            ->editColumn('product_Qty', function($data)
            {
                return $data->product_qty;
            }) 
            ->editColumn('description', function($data)
            {
                return $data->description;
            }) 
            ->editColumn('subcategory_name', function($data)
            {
                return $data->subcategory_name;
            })
            ->editColumn('category_name',function($data)
            {
                return $data->category_name;
            })
            ->editColumn('size_name', function($data)
            {
                return $data->size_name;
            })
            ->editColumn('company_name', function($data)
            {
                return $data->company_name;
            })
            ->editColumn('color_name', function($data)
            {
                $html="";
                $color_id=explode('|', $data->color_id);
                foreach ($color_id as $key => $value) {
                    $color=color::where('id','=',$value)->value('color_name');
                    $html.='<div style="background-color:'.$color.';width:50px;height:50px;border:1px solid #fff;"></div>';
                }
                return $html;
            })
            ->editColumn('action', function($data)
            {
                $html = '<a href="'.url('/admin/editProduct/'.$data->id) .'" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x"><i class="fas fa-edit"></i></a>
                <a href="'.url('/admin/deleteProduct/'.$data->id) .'" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x" onclick="return confirm('."'Are you sure you want to delete this product?'".');"><i class="fas fa-trash"></i></a>
                ';
                return $html;
            })
            ->rawColumns(['id','product_name','fetured','product_image','product_images','product_price','product_Qty','subcategory_name','category_name','size_name','company_name','color_name','action'])
            ->make(true);
    }


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $product=product::where('id','=',$id)->first();
        $subcategory=subcategory::get();
        $size_chart=size_chart::where('subcategory_id','=',$product->subcategory_id)->get();
        $company=company::get();
        $color=color::get();
        return view('admin::product.productUpdate',compact('subcategory','size_chart','company','color','product'));
    }


    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request)
    {

        if ($request->hasFile('image')) {
            $rules = array(
                'name' => 'required|regex:/^[0-9a-zA-Z ]+$/u|unique:product,product_name,'.$request->product_id.'|max:255',
                'image' => 'required|image|mimes:jpeg,png,jpg',
                'price' => 'required',
                'qty' => 'required',
                'desc' => 'required',
                'subcategory' => 'required',
                'sizechart' => 'required',
                'company' => 'required',
                'color' => 'required',
            );
            $customeMessage = array(
                'name.required' => 'Product name can not be an empty.',
                'image.required' => 'Product image can not be an empty.',
                'price.required' => 'Price can not be empty.',
                'qty.required' => 'Quantity can not be empty.',
                'desc.required' => 'Description can not be empty.',
                'subcategory.required' => 'Subcategory can not be empty.',
                'sizechart.required' => 'Size can not be empty.',
                'company.required' => 'Company can not be empty.',
                'color.required' => 'Color can not be empty.',
            );
        }else{
            $rules = array(
                'name' => 'required|regex:/^[0-9a-zA-Z ]+$/u|unique:product,product_name,'.$request->product_id.'|max:255',
                'price' => 'required',
                'qty' => 'required',
                'desc' => 'required',
                'subcategory' => 'required',
                'sizechart' => 'required',
                'company' => 'required',
                'color' => 'required',
            );
            $customeMessage = array(
                'name.required' => 'Product name can not be an empty.',
                'price.required' => 'Price can not be empty.',
                'qty.required' => 'Quantity can not be empty.',
                'desc.required' => 'Description can not be empty.',
                'subcategory.required' => 'Subcategory can not be empty.',
                'sizechart.required' => 'Size can not be empty.',
                'company.required' => 'Company can not be empty.',
                'color.required' => 'Color can not be empty.',
            );
        }
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $product=product::where('id','=',$request->product_id)->first();
        if ($request->hasFile('image')) {
            $imageName = request()->image->getClientOriginalName().time().'.'.request()->image->getClientOriginalExtension();
            if(request()->image->move(public_path('images'), $imageName)){
                $image_name=$imageName;
            }else{
                $message="Image not uploaded please try again.";
                return redirect()->back()->withErrors($message)->withInput();
            }
        }else{
            $image_name=$request->product_image_name;
        }
        if(!empty($product))
        {
            $product->update([
                'product_name' => ucfirst($request->name),
                'product_image' => $image_name,
                'product_price' => (int)$request->price,
                'product_qty' => (int)$request->qty,
                'description' => $request->desc,
                'subcategory_id' => (int)$request->subcategory,
                'size_id' => (int)$request->sizechart,
                'company_id' => (int)$request->company,
                'color_id' => implode('|', $request->color),
            ]);
            return redirect('admin/product')->with('success','Product updated successfully');
        }else
        {
            return redirect('admin/product')->with('success','Some error');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try{
            $affected = DB::delete("DELETE FROM product WHERE id = {$id}");
            return redirect('/admin/product')->with('success','Product deleted successfully');
        }catch(\Illuminate\Database\QueryException  $ex){
            return redirect('/admin/product')->with('error','Cannot delete this product');
        }
    }

    public function Product(Request $request)
    {
        return view('admin::product.productShow');
    }

    public function moreProductImages(Request $request)
    {
        $product=product_image::where('product_id','=',$request->product_id)->get();
        $rander="more";
        $view=view('admin::product.productRander',compact('product','rander'));
        echo $view->render();exit();
    }
    public function feturedProduct(Request $request)
    {
        $product=product::where('id','=',$request->id)->first();
        $rander="more";
        $status=$product->featured_filed;
        if($status == "No"){
            $status_update="Yes";
        }elseif($status == ""){
            $status_update="Yes";
        }else{
            $status_update="No";
        }
        $product->update([
            'featured_filed' => $status_update
        ]);
        echo json_encode(['id'=>$product->id,'status'=>$product->featured_filed]);exit;
    }


    public function deleteProductImages(Request $request)
    {
        $id=$request->id;
        try{
            $affected = DB::delete("DELETE FROM product_images WHERE id = {$id}");
            $product=product_image::where('product_id','=',$request->product_id)->get();
            return redirect('/admin/product')->with('success','Product image successfully deleted');
        }catch(\Illuminate\Database\QueryException  $ex){
            return redirect('/admin/product')->with('error','Product image not deleted');
            //return redirect('/admin/area')->with('error','Cannot delete this area');
        }
       /* $rander="delete";
        $view=view('admin::product.productRander',compact('product','rander'));
        echo $view->render();exit();*/
    }

    public function multiple_products(Request $request)
    {
        $product=product::get();
        return view('admin::product.multiple_products',compact('product'));
    }

    public function multiple_products_store(Request $request){
        $rules = array( 
            'image.*' => 'required|image|mimes:jpeg,png,jpg',
            'productname' => 'required',
        );
        $customeMessage = array(
            'image.required' => 'Product image can not be an empty.',
            'productname.required' => 'Please select product.',
        );

        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $images       =       array();
        if($files     =       $request->file('image')) {
            foreach($files as $file) {
                $name     =    time().$file->getClientOriginalName();
                $destinationPath = public_path('/images');
                if($file->move($destinationPath, $name)) {
                    $images[]=$name;
                }else{
                    if(!empty($images)){
                        foreach ($images as $key => $imag) {
                            $path = public_path()."/images/".$imag;
                            unlink($path);
                        }
                    }
                    $message="image not uploaded please try again.";
                    return redirect()->back()->withErrors($message)->withInput();
                }
            }
        }
        if(!empty($images)){
            foreach ($images as $key => $imag) {
                $product = product_image::create([
                    'product_id' => $request->productname,
                    'image_name' => $imag
                ]);
            }
            return redirect('/admin/product')->with('success','Product Created successfully');
        }else{
            return redirect('/admin/product')->with('error','Some error please try again..!');
        }
    }

    public function subcategoryChange(Request $request){
        $size_id=$request->size_id;
        $size_chart=size_chart::where('subcategory_id','=',$request->id)->get();
        if(count($size_chart) > 0){
            $view=view('admin::product.sizechartDropdown',compact('size_chart','size_id'));
            echo $view->render();exit();
        }else{
            echo "error";exit();
        }
    }
}