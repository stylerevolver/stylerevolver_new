<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\models\area;
use App\models\city;
use App\models\company;
use App\models\appointment;
use App\models\feedback;
use App\models\rating;
use App\models\product;
use App\models\category;
use App\models\complain;
use App\models\subcategory;
use App\models\customize_product;
use App\models\sales;
use App\models\sales_details;
use App\User;
use DB;
use PDF;
use Auth;

class ReportController extends Controller
{
    /*Area*/
    public function AreaReport()
    {
        $company=company::first();
        $Reporttitle="Area Report";
        $area=area::join('city as c','c.id','=','area.city_id')
                ->select('area.area_name','area.pincode','c.city_name')
                ->get();

        $pdf = PDF::loadView('admin::report.areaReportShow',compact('company','area','Reporttitle'));  
        return $pdf->download('areaReportShow.pdf');
        //return view('admin::report.areaReportShow',compact('company','area'));
    }

    /* Appointment*/
    public function AppointmentReport(){

        $company=company::first();
        $Reporttitle="Appointment Report";
        $appointment=appointment::get();

        $pdf = PDF::loadView('admin::report.appointmentReport',compact('company','appointment','Reporttitle'));  
        return $pdf->download('appointmentReport.pdf');
        //return view('admin::report.appointmentReport',compact('company','appointment','Reporttitle'));
    } 

    /*Customer*/
    public function CustomerReport(){

        $company=company::first();
        $Reporttitle="Customer Report";
        $customer=User::join('area as a','a.id','=','users.area_id')
                ->select('users.first_name','users.email','users.address','users.gender','users.dob','users.contact_no','users.area_id')
                ->get();

        $pdf = PDF::loadView('admin::report.customerReportShow',compact('company','Reporttitle','customer'));  
        return $pdf->download('CustomerReport.pdf');
        //return view('admin::report.customerReportShow',compact('company','Reporttitle','customer'));
    }

    /*Complain*/
    public function ComplainReport(){

        $company=company::first();  
        $Reporttitle="Complain Report";
        $complain=complain::join('users as u','u.id','=','complain.user_id')
            ->join('product as p','p.id','=','complain.product_id')
            ->select('u.first_name','p.product_name','complain.description','complain.complain_date','complain.complain_status')
            ->get();

        $pdf = PDF::loadView('admin::report.complainReportShow',compact('company','Reporttitle','complain'));
        return $pdf->download('complainReport.pdf');
        //return view('admin::report.complainReportShow',compact('company','Reporttitle','complain'));
    }

    public function showComplainReport(){
        $users=complain::select('complain.user_id',\DB::raw('count(*) as total'))
              ->groupBy('complain.user_id')
              ->get();
        $product=complain::select('complain.product_id',\DB::raw('count(*) as total'))
              ->groupBy('complain.product_id')
              ->get();
        $complain_date=complain::select('complain_date',\DB::raw('count(*) as total'))->groupBy('complain_date')->get();
        return view('admin::complainReport.complainReportShow1',compact('users','product','complain_date'));
    }

    public function customeComplainReport(Request $request){
        //username
        $Reporttitle="";
        $company=company::first();
        if($request->username != "" && $request->productname == "" && $request->complain_from_date == "" && $request->complain_to_date == ""){
            $Reporttitle="User Wise Complain Report";
            $complain=complain::where('user_id','=',$request->username)
                ->get();
        }
        //procduct name
        elseif($request->username == "" && $request->productname != "" && $request->complain_from_date == "" && $request->complain_to_date == ""){
            $Reporttitle="Product Wise Complain Report";
            $complain=complain::where('product_id','=',$request->productname)
                ->get();
        }

        //from date
        elseif($request->username == "" && $request->productname == "" && $request->complain_from_date != "" && $request->complain_to_date == ""){
            $Reporttitle="From Date Wise Complain Report";
            $complain=complain::whereDate('complain_date','=',$request->complain_from_date)
                ->get();
        }
        //to date
        elseif($request->username == "" && $request->productname == "" && $request->complain_from_date == "" && $request->complain_to_date != ""){
            $Reporttitle="To Date Wise Complain Report";
            $complain=complain::whereDate('complain_date','=',$request->complain_to_date)
                ->get();   
        }
        //from and to date
        elseif($request->username == "" && $request->productname == "" && $request->complain_from_date != "" && $request->complain_to_date != ""){
            $Reporttitle="From And To Date Wise Complain Report";
            $complain=complain::whereDate('complain_date','>=',$request->complain_from_date)->whereDate('complain_date','<=',$request->complain_to_date)->get();
        }
        //username and product
        elseif($request->username != "" && $request->productname != "" && $request->complain_from_date == "" && $request->complain_to_date == ""){
            $Reporttitle="User And Product Wise Complain Report";
            $complain=complain::where('product_id','=',$request->productname)->where('user_id','=',$request->username)->get();
        }

        //user and from date
        elseif($request->username != "" && $request->productname == "" && $request->complain_from_date != "" && $request->complain_to_date == ""){
            $Reporttitle="User And From Date Wise Complain Report";
            $complain=complain::whereDate('complain_date','=',$request->complain_from_date)->where('user_id','=',$request->username)->get();
        }
        //user and to date
        elseif($request->username != "" && $request->productname == "" && $request->complain_from_date == "" && $request->complain_to_date != ""){
            $Reporttitle="User And To Date Wise Complain Report";
            $complain=complain::whereDate('complain_date','=',$request->complain_to_date)->where('user_id','=',$request->username)->get();
        }
        //user, from and to date
        elseif($request->username != "" && $request->productname == "" && $request->complain_from_date != "" && $request->complain_to_date != ""){
            $Reporttitle="User, From Date And To Date Wise Complain Report";
            $complain=complain::whereDate('complain_date','>=',$request->complain_from_date)->whereDate('complain_date','<=',$request->complain_to_date)->where('user_id','=',$request->username)->get();
        }

        //product from
        elseif($request->username == "" && $request->productname != "" && $request->complain_from_date != "" && $request->complain_to_date == ""){
            $Reporttitle="Product And From Date Wise Complain Report";
            $complain=complain::whereDate('complain_date','=',$request->complain_from_date)->where('product_id','=',$request->productname)->get();
        }
        //product to
        elseif($request->username == "" && $request->productname != "" && $request->complain_from_date == "" && $request->complain_to_date != ""){
            $Reporttitle="Product And To Date Wise Complain Report";
            $complain=complain::whereDate('complain_date','=',$request->complain_to_date)->where('product_id','=',$request->productname)->get();
        }
        //product, from and to
        elseif($request->username == "" && $request->productname != "" && $request->complain_from_date != "" && $request->complain_to_date != ""){
            $Reporttitle="Product,From Date And To Date Wise Complain Report";
            $complain=complain::whereDate('complain_date','>=',$request->complain_from_date)->whereDate('complain_date','<=',$request->complain_to_date)->where('product_id','=',$request->productname)
                ->get();
        }

        //prodct, username and from date
        elseif($request->username != "" && $request->productname != "" && $request->complain_from_date != "" && $request->complain_to_date == ""){
            $Reporttitle="Product, User and From Date Wise Complain Report";
            $complain=complain::where('product_id','=',$request->productname)->where('user_id','=',$request->username)->whereDate('complain_date','=',$request->complain_from_date)->get();
        }
        //prodct, username and to date
        elseif($request->username != "" && $request->productname != "" && $request->complain_from_date == "" && $request->complain_to_date != ""){
           $Reporttitle="Product, User and To Date Wise Complain Report";
            $complain=complain::where('product_id','=',$request->productname)->where('user_id','=',$request->username)->whereDate('complain_date','=',$request->complain_to_date)->get();
        }

        elseif($request->username != "" && $request->productname != "" && $request->complain_from_date != "" && $request->complain_to_date != ""){
           $Reporttitle="Product, User, From Date and To Date Wise Complain Report";
            $complain=complain::where('product_id','=',$request->productname)
            ->where('user_id','=',$request->username)
            ->whereDate('complain_date','=',$request->complain_to_date)
            ->whereDate('complain_date','=',$request->complain_from_date)
            ->get();
        }
        //all
        else{
            $Reporttitle="Complain Report";
            $complain=complain::get();
        }
        $today_date=current_date();
        if(isset($request->download)){
            $pdf = PDF::loadView('admin::report.complainReportShow',compact('company','Reporttitle','complain','today_date'));  
            return $pdf->download('ComplainReport.pdf');
        }else{
            $pdf = PDF::loadView('admin::report.complainReportShow',compact('company','Reporttitle','complain','today_date'));  
            return $pdf->stream('ComplainReport.pdf');
        }
    }
    /*Feedback*/
    public function FeedbackReport(){

        $company=company::first();
        $Reporttitle="Feedback Report";
        $feedback=feedback::join('users as u','u.id','=','feedback.user_id')
            ->join('product as p','p.id','=','feedback.product_id')
            ->select('u.first_name','p.product_name','feedback.feedback_description','feedback.feedback_date')
            ->get();

        $pdf = PDF::loadView('admin::report.feedbackReportShow',compact('company','Reporttitle','feedback'));
        return $pdf->download('feedbackReport.pdf');
        //return view('admin::report.feedbackReportShow',compact('company','Reporttitle','feedback'));
    }

    public function showFeedbackReport(){

        $users=feedback::select('feedback.user_id',\DB::raw('count(*) as total'))
              ->groupBy('feedback.user_id')
              ->get();
        $product=feedback::select('feedback.product_id',\DB::raw('count(*) as total'))
              ->groupBy('feedback.product_id')
              ->get();
        $feedback_date=feedback::select('feedback_date',\DB::raw('count(*) as total'))->groupBy('feedback_date')->get();
        return view('admin::feedbackReport.feedbackReport1Show',compact('users','product','feedback_date'));

    }

    public function customeFeedbackReport(Request $request){
    
        $Reporttitle="";
        $company=company::first();
        if($request->username != "" && $request->productname == "" && $request->feedback_from_date == "" && $request->feedback_to_date == ""){
            $Reporttitle="User Wise Feedback Report";
            $feedback=feedback::where('user_id','=',$request->username)
                ->get();
        }
        //procduct name
        elseif($request->username == "" && $request->productname != "" && $request->feedback_from_date == "" && $request->feedback_to_date == ""){
            $Reporttitle="Product Wise Feedback Report";
            $feedback=feedback::where('product_id','=',$request->productname)
                ->get();
        }
        //from date
        elseif($request->username == "" && $request->productname == "" && $request->feedback_from_date != "" && $request->feedback_to_date == ""){
            $Reporttitle="From Date Wise Feedback Report";
            $feedback=feedback::whereDate('feedback_date','=',$request->feedback_from_date)
                ->get();
        }
        //to date
        elseif($request->username == "" && $request->productname == "" && $request->feedback_from_date == "" && $request->feedback_to_date != ""){
            $Reporttitle="To Date Wise Feedback Report";
            $feedback=feedback::whereDate('feedback_date','=',$request->feedback_to_date)
                ->get();   
        }
        //from and to date
        elseif($request->username == "" && $request->productname == "" && $request->feedback_from_date != "" && $request->feedback_to_date != ""){
            $Reporttitle="From And To Date Wise Feedback Report";
            $feedback=feedback::whereDate('feedback_date','>=',$request->feedback_from_date)
            ->whereDate('feedback_date','<=',$request->feedback_to_date)
            ->get();
        }
        //username and product
        elseif($request->username != "" && $request->productname != "" && $request->feedback_from_date == "" && $request->feedback_to_date == ""){
            $Reporttitle="User And Product Wise Feedback Report";
            $feedback=feedback::where('product_id','=',$request->productname)->where('user_id','=',$request->username)->get();
        }

        //user and from date
        elseif($request->username != "" && $request->productname == "" && $request->feedback_from_date != "" && $request->feedback_to_date == ""){
            $Reporttitle="User And From Date Wise Feedback Report";
            $feedback=feedback::whereDate('feedback_date','=',$request->feedback_from_date)->where('user_id','=',$request->username)->get();
        }
        //user and to date
        elseif($request->username != "" && $request->productname == "" && $request->feedback_from_date == "" && $request->feedback_to_date != ""){
            $Reporttitle="User And To Date Wise Feedback Report";
            $feedback=feedback::whereDate('feedback_date','=',$request->feedback_to_date)->where('user_id','=',$request->username)->get();
        }
        //user, from and to date
        elseif($request->username != "" && $request->productname == "" && $request->feedback_from_date != "" && $request->feedback_to_date != ""){
            $Reporttitle="User, From Date And To Date Wise Feedback Report";
            $feedback=feedback::whereDate('feedback_date','>=',$request->feedback_from_date)->whereDate('feedback_date','<=',$request->feedback_to_date)->where('user_id','=',$request->username)->get();
        }

        //product from
        elseif($request->username == "" && $request->productname != "" && $request->feedback_from_date != "" && $request->feedback_to_date == ""){
            $Reporttitle="Product And From Date Wise Feedback Report";
            $feedback=feedback::whereDate('feedback_date','=',$request->feedback_from_date)->where('product_id','=',$request->productname)->get();
        }
        //product to
        elseif($request->username == "" && $request->productname != "" && $request->feedback_from_date == "" && $request->feedback_to_date != ""){
            $Reporttitle="Product And To Date Wise Feedback Report";
            $feedback=feedback::whereDate('feedback_date','=',$request->feedback_to_date)->where('product_id','=',$request->productname)->get();
        }
        //product, from and to
        elseif($request->username == "" && $request->productname != "" && $request->feedback_from_date != "" && $request->feedback_to_date != ""){
            $Reporttitle="Product,From Date And To Date Wise Feedback Report";
            $feedback=feedback::whereDate('feedback_date','>=',$request->feedback_from_date)->whereDate('feedback_date','<=',$request->feedback_to_date)->where('product_id','=',$request->productname)
                ->get();
        }
        //prodct, username and from date
        elseif($request->username != "" && $request->productname != "" && $request->feedback_from_date != "" && $request->feedback_to_date == ""){
            $Reporttitle="Product, User and From Date Wise Feedback Report";
            $feedback=feedback::where('product_id','=',$request->productname)->where('user_id','=',$request->username)->whereDate('feedback_date','=',$request->feedback_from_date)->get();
        }
        //prodct, username and to date
        elseif($request->username != "" && $request->productname != "" && $request->feedback_from_date == "" && $request->feedback_to_date != ""){
           $Reporttitle="Product, User and To Date Wise Feedback Report";
            $feedback=feedback::where('product_id','=',$request->productname)->where('user_id','=',$request->username)->whereDate('feedback_date','=',$request->feedback_to_date)->get();
        }

        elseif($request->username != "" && $request->productname != "" && $request->feedback_from_date != "" && $request->feedback_to_date != ""){
           $Reporttitle="Product, User, From Date and To Date Wise Feedback Report";
            $feedback=feedback::where('product_id','=',$request->productname)
            ->where('user_id','=',$request->username)
            ->whereDate('feedback_date','=',$request->feedback_to_date)
            ->whereDate('feedback_date','=',$request->feedback_from_date)
            ->get();
        }
        //all
        else{
            $Reporttitle="Feedback Report";
            $feedback=feedback::get();
        }
        $today_date=current_date();
        if(isset($request->download)){
            $pdf = PDF::loadView('admin::report.feedbackReportShow',compact('company','Reporttitle','feedback','today_date'));  
            return $pdf->download('FeedbackReport.pdf');
        }else{
            $pdf = PDF::loadView('admin::report.feedbackReportShow',compact('company','Reporttitle','feedback','today_date'));  
            return $pdf->stream('FeedbackReport.pdf');
        }
    }

    /*Rating*/
    public function RatingReport(){

        $company=company::first();
        $Reporttitle="Rating Report";
        $rating=rating::join('users as u','u.id','=','rating.user_id')
            ->join('product as p','p.id','=','rating.product_id')
            ->select('u.first_name','p.product_name','rating.rating','rating.rating_date')
            ->get();

        $pdf = PDF::loadView('admin::report.ratingReportShow',compact('company','Reporttitle','rating'));  
        return $pdf->download('RatingReport.pdf');
    
    }

    /*Customize Product*/
    public function CustomizeProductReport($id){

        $company=company::first();
        $Reporttitle="Customize Product Report";
        $customize_product=customize_product::join('appointment as a','a.id','=','customize_product.appointment_id')
                        ->where('customize_product.appointment_id','=',$id)
                        ->select('customize_product.id','customize_product.customize_product_name','customize_product.customize_product_image','customize_product.appointment_id','customize_product.description','customize_product.price','customize_product.appointment_id')
                        ->get();

        $pdf = PDF::loadView('admin::report.customizeProductReport',compact('company','Reporttitle','customize_product'));  
        return $pdf->download('CustomizeReport.pdf');
    }


    public function showreport(){

        $users=rating::select('rating.user_id',\DB::raw('count(*) as total'))
              ->groupBy('rating.user_id')
              ->get();
        $product=rating::select('rating.product_id',\DB::raw('count(*) as total'))
              ->groupBy('rating.product_id')
              ->get();
        $rating_date=rating::select('rating_date',\DB::raw('count(*) as total'))->groupBy('rating_date')->get();
        return view('admin::rating_report.rating_reportshow',compact('users','product','rating_date'));
    }

    public function customRatingReport(Request $request){
        //username
        $Reporttitle="";
        $company=company::first();
        if($request->username != "" && $request->productname == "" && $request->rating_from_date == "" && $request->rating_to_date == ""){
            $Reporttitle="User Wise Rating Report";
            $rating=rating::where('user_id','=',$request->username)->get();
        }
        //procduct name
        elseif($request->username == "" && $request->productname != "" && $request->rating_from_date == "" && $request->rating_to_date == ""){
            $Reporttitle="Product Wise Rating Report";
            $rating=rating::where('product_id','=',$request->productname)
                ->get();
        }
        //from date
        elseif($request->username == "" && $request->productname == "" && $request->rating_from_date != "" && $request->rating_to_date == ""){
            $Reporttitle="From Date Wise Rating Report";
            $rating=rating::whereDate('rating_date','=',$request->rating_from_date)->get();
        }
        //to date
        elseif($request->username == "" && $request->productname == "" && $request->rating_from_date == "" && $request->rating_to_date != ""){
            $Reporttitle="To Date Wise Rating Report";
            $rating=rating::whereDate('rating_date','=',$request->rating_to_date)->get();   
        }

        //from and to date
        elseif($request->username == "" && $request->productname == "" && $request->rating_from_date != "" && $request->rating_to_date != ""){
            $Reporttitle="From And To Date Wise Rating Report";
            $rating=rating::whereDate('rating_date','>=',$request->rating_from_date)->whereDate('rating_date','<=',$request->rating_to_date)->get();
        }
        //username and product
        elseif($request->username != "" && $request->productname != "" && $request->rating_from_date == "" && $request->rating_to_date == ""){
            $Reporttitle="User And Product Wise Rating Report";
            $rating=rating::where('product_id','=',$request->productname)->where('user_id','=',$request->username)->get();
        }

        //user and from date
        elseif($request->username != "" && $request->productname == "" && $request->rating_from_date != "" && $request->rating_to_date == ""){
            $Reporttitle="User And From Date Wise Rating Report";
            $rating=rating::whereDate('rating_date','=',$request->rating_from_date)->where('user_id','=',$request->username)->get();
        }
        //user and to date
        elseif($request->username != "" && $request->productname == "" && $request->rating_from_date == "" && $request->rating_to_date != ""){
            $Reporttitle="User And To Date Wise Rating Report";
            $rating=rating::whereDate('rating_date','=',$request->rating_to_date)->where('user_id','=',$request->username)->get();
        }
        //user, from and to date
        elseif($request->username != "" && $request->productname == "" && $request->rating_from_date != "" && $request->rating_to_date != ""){
            $Reporttitle="User, From Date And To Date Wise Rating Report";
            $rating=rating::whereDate('rating_date','>=',$request->rating_from_date)->whereDate('rating_date','<=',$request->rating_to_date)->where('user_id','=',$request->username)->get();
        }

        //product from
        elseif($request->username == "" && $request->productname != "" && $request->rating_from_date != "" && $request->rating_to_date == ""){
            $Reporttitle="Product And From Date Wise Rating Report";
            $rating=rating::whereDate('rating_date','=',$request->rating_from_date)->where('product_id','=',$request->productname)->get();
        }
        //product to
        elseif($request->username == "" && $request->productname != "" && $request->rating_from_date == "" && $request->rating_to_date != ""){
            $Reporttitle="Product And To Date Wise Rating Report";
            $rating=rating::whereDate('rating_date','=',$request->rating_to_date)->where('product_id','=',$request->productname)->get();
        }
        //product, from and to
        elseif($request->username == "" && $request->productname != "" && $request->rating_from_date != "" && $request->rating_to_date != ""){
            $Reporttitle="Product,From Date And To Date Wise Rating Report";
            $rating=rating::whereDate('rating_date','>=',$request->rating_from_date)->whereDate('rating_date','<=',$request->rating_to_date)->where('product_id','=',$request->productname)
                ->get();
        }
        //prodct, username and from date
        elseif($request->username != "" && $request->productname != "" && $request->rating_from_date != "" && $request->rating_to_date == ""){
            $Reporttitle="Product, User and From Date Wise Rating Report";
            $rating=rating::where('product_id','=',$request->productname)->where('user_id','=',$request->username)->whereDate('rating_date','=',$request->rating_from_date)->get();
        }
        //prodct, username and to date
        elseif($request->username != "" && $request->productname != "" && $request->rating_from_date == "" && $request->rating_to_date != ""){
           $Reporttitle="Product, User and To Date Wise Rating Report";
            $rating=rating::where('product_id','=',$request->productname)->where('user_id','=',$request->username)->whereDate('rating_date','=',$request->rating_to_date)->get();
        }

        elseif($request->username != "" && $request->productname != "" && $request->rating_from_date != "" && $request->rating_to_date != ""){
           $Reporttitle="Product, User, From Date Wise and To Date Wise Rating Report";
            $rating=rating::where('product_id','=',$request->productname)
                    ->where('user_id','=',$request->username)
                    ->whereDate('rating_date','=',$request->rating_from_date)
                    ->get();
                   // dd($request->username,$request->productname,$request->rating_from_date,$request->rating_to_date);
                    //echo '<pre>';print_r($rating);exit();
        }
        //all
        else{
            $Reporttitle="Rating Report";
            $rating=rating::get();
        }
        $today_date=current_date();
        if(isset($request->download)){
            $pdf = PDF::loadView('admin::report.ratingReportShow',compact('company','Reporttitle','rating','today_date'));  
            return $pdf->download('RatingReport.pdf');
        }else{
            $pdf = PDF::loadView('admin::report.ratingReportShow',compact('company','Reporttitle','rating','today_date'));  
            return $pdf->stream('RatingReport.pdf');
        }
    }

    /*Customer Report*/
    public function showCustomerReport()
    {
        $area=area::get();
        return view('admin::customerReport.customerReportShow1',compact('area'));
    }

    public function customeCustomerReport(Request $request)
    {
        //area name
        $Reporttitle="";
        $company=company::first();
        if($request->areaname != "" && $request->monthname == "")
        {
            $Reporttitle="Area Wise Customer Report";
            $users=User::where('area_id','=',$request->areaname)->get();
        }

        //Month name
        elseif($request->areaname == "" && $request->monthname != "")
        {
            $Reporttitle="Month Wise Customer Report";
            $users=User::whereMonth('created_at','=',$request->monthname)
                ->get();
        }
        elseif($request->areaname != "" && $request->monthname != "")
        {
            $Reporttitle="Month And User Wise Customer Report";
            $users=User::where('area_id','=',$request->areaname)
                    ->whereMonth('created_at','=',$request->monthname)
                    ->get();
        }
        else
        {
            $Reporttitle="Customer Report";
            $users=User::get();
        }
        $today_date=current_date();
        if(isset($request->download)){
            $pdf = PDF::loadView('admin::report.customerReportShow',compact('company','Reporttitle','users','today_date'));  
            return $pdf->download('Customer.pdf');
        }else{
            $pdf = PDF::loadView('admin::report.customerReportShow',compact('company','Reporttitle','users','today_date'));  
            return $pdf->stream('Customer.pdf');
        }
    }

    
    public function ShowCustomizeProduct()
    {
        $users=customize_product::join('appointment as a','a.id','=','customize_product.appointment_id')
                    ->join('users as u','u.id','=','a.user_id')
                    ->select('u.first_name','u.last_name','u.id')
                    ->get();
        
        $app=appointment::get();
        return view('admin::report.CustomizeProductReportShow',compact('users','app'));
    }

    public function customizeProduct(Request $request)
    {
        $company=company::first();
        $Reporttitle="";
        if ($request->custname != ""  && $request->appointmentdate == "")
        {
            $Reporttitle="User Wise Customize Product Report";
            $customize_product=customize_product::join('appointment as a','a.id','=','customize_product.appointment_id')
                    ->join('users as u','u.id','=','a.user_id')
                    ->where('user_id','=',$request->custname)
                    ->select('u.first_name','u.last_name','customize_product.customize_product_name','customize_product.description','customize_product.price')
                    ->get();
        }
        elseif ($request->custname == "" && $request->appointmentdate != "")
        {
            $Reporttitle="Appointment Wise Customize Product Report";
            $customize_product=customize_product::join('appointment as a','a.id','=','customize_product.appointment_id')
                                ->join('users as u','u.id','=','a.user_id')
                                ->where('appointment_id','=',$request->appointmentdate)
                                ->select('u.first_name','u.last_name','customize_product.customize_product_name','customize_product.description','customize_product.price')
                                ->get();
        }
        elseif ($request->custname != "" && $request->appointmentdate != "")
        {
            $Reporttitle="Appointment Wise And User Wise Customize Product Report";
            $customize_product=customize_product::join('appointment as a','a.id','=','customize_product.appointment_id')
                                ->join('users as u','u.id','=','a.user_id')
                                ->where('appointment_id','=',$request->appointmentdate)
                                ->where('user_id','=',$request->custname)
                                ->select('u.first_name','u.last_name','customize_product.customize_product_name','customize_product.description','customize_product.price')
                                ->get();
        }
        else
        {
            $Reporttitle="Customize Product Report";
            $customize_product=customize_product::join('appointment as a','a.id','=','customize_product.appointment_id')
                    ->join('users as u','u.id','=','a.user_id')
                    ->select('u.first_name','u.last_name','customize_product.customize_product_name','customize_product.description','customize_product.price')
                    ->get();
            
        }
        $today_date=current_date();
        if (isset($request->download)) 
        {
            $pdf = PDF::loadView('admin::report.customizeProductReport',compact('company','Reporttitle','customize_product','today_date'));
            return $pdf->download('CustomizeProduct.pdf');
        }
        else
        {
            //echo '<pre>';print_r($customize_product);exit();
            $pdf = PDF::loadView('admin::report.customizeProductReport',compact('company','Reporttitle','customize_product','today_date'));
            return $pdf->stream('CustomizeProduct.pdf');
        }

    }

    public function ShowProduct()
    {
        $category=category::get();
        $subcategory=subcategory::get();
        return view('admin::report.ShowProduct',compact('subcategory','category'));
    }
    /*Product*/
    public function ProductReport(Request $request){

        $company=company::first();
        $Reporttitle="";
        $category=category::get();
            //category wise
        if($request->categoryname != "" && $request->subcategoryname == "")
        {
            $Reporttitle="Category Wise Product Report";
            $product=product::join('subcategory as s','s.id','=','product.subcategory_id')
                ->join('color as cl','cl.id','=','product.color_id')
                ->join('size_chart as sc','sc.id','=','product.size_id')
                ->join('category','category.id','=','s.category_id')
                ->join('company as c','c.id','product.company_id')
                ->where('category_id','=',$request->categoryname)
                ->select('product.id','product.product_name','product.product_image','product.product_price','product.product_qty','s.subcategory_name','sc.size_name','c.company_name','cl.color_name','category.category_name')
                ->get();

        }
        //subcategory wise
        elseif ($request->categoryname == "" && $request->subcategoryname != "") 
        {
            $Reporttitle="Subcategory Wise Product Report";
            $product=product::join('subcategory as s','s.id','=','product.subcategory_id')
                ->join('color as cl','cl.id','=','product.color_id')
                ->join('size_chart as sc','sc.id','=','product.size_id')
                ->join('category','category.id','=','s.category_id')
                ->join('company as c','c.id','product.company_id')
                ->where('product.subcategory_id','=',$request->subcategoryname)
                ->select('product.id','product.product_name','product.product_image','product.product_price','product.product_qty','s.subcategory_name','sc.size_name','c.company_name','cl.color_name','category.category_name')
                ->get();
        }
        //category nd subcategory wise
        elseif ($request->categoryname != "" && $request->subcategoryname != "") 
        {
            $Reporttitle="Category Wise And Subcategory Wise Product Report";
            $product=product::join('subcategory as s','s.id','=','product.subcategory_id')
                ->join('color as cl','cl.id','=','product.color_id')
                ->join('size_chart as sc','sc.id','=','product.size_id')
                ->join('category','category.id','=','s.category_id')
                ->join('company as c','c.id','product.company_id')
                ->where('category_id','=',$request->categoryname)
                ->where('product.subcategory_id','=',$request->subcategoryname)
                ->select('product.id','product.product_name','product.product_image','product.product_price','product.product_qty','s.subcategory_name','sc.size_name','c.company_name','cl.color_name','category.category_name')
                ->get();
        }
        //all
        else
        {
            $Reporttitle="Product Report";
            $product=product::join('subcategory as s','s.id','=','product.subcategory_id')
                ->join('color as cl','cl.id','=','product.color_id')
                ->join('size_chart as sc','sc.id','=','product.size_id')
                ->join('category','category.id','=','s.category_id')
                ->join('company as c','c.id','product.company_id')
                ->select('product.id','product.product_name','product.product_image','product.product_price','product.product_qty','s.subcategory_name','sc.size_name','c.company_name','cl.color_name','category.category_name')
                ->get();

        }

        $today_date=current_date();
        if(isset($request->download))
        { 
            $pdf = PDF::loadView('admin::report.productReportShow',compact('company','Reporttitle','category','product','today_date'));  
            return $pdf->download('ProductReport.pdf');
        }
        else
        {
            $pdf = PDF::loadView('admin::report.productReportShow',compact('company','Reporttitle','category','product','today_date'));  
            return $pdf->stream('ProductReport.pdf');
        }
        
    }
}
