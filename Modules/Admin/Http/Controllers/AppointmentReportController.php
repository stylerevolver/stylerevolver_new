<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\models\appointment;
use App\models\company;
use App\User;
use DB;
use PDF;

class AppointmentReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admin::index');
    }

    public function reportshow()
    {
         $users=appointment::select('appointment.user_id',\DB::raw('count(*) as total'))
              ->groupBy('appointment.user_id')
              ->get();
         $appointment_date=appointment::select('appointment_date',\DB::raw('count(*) as total'))->groupBy('appointment_date')->get();
        return view('admin::report.customAppointment',compact('users','appointment_date'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $company=company::first();
        //user
        $Reporttitle="Appointment Report";
        $company=company::first();
        if($request->username != "" && $request->appointment_date == ""){
            $Reporttitle="User Wise Appointment Report";
            $appointment=appointment::where('user_id','=',$request->username)
                    ->get();
        }
        else if($request->username == "" && $request->appointment_date != ""){
            $Reporttitle="Appointment Date Wise Report";
            $appointment=appointment::where('appointment_date','=',$request->appointment_date)
                    ->get();
        }
        else if($request->username != "" && $request->appointment_date != ""){
            $Reporttitle="User And Appointment  Date Wise Report";
            $appointment=appointment::where('user_id','=',$request->username)
                    ->where('appointment_date','=',$request->appointment_date)
                    ->get();
        }else{
            $Reporttitle="Appointment Report";
            $appointment=appointment::get();
        }
        $today_date=current_date();
        if(isset($request->download)){
            $pdf = PDF::loadView('admin::report.appointmentReport',compact('company','appointment','Reporttitle','today_date'));  
            return $pdf->download('appointmentReport.pdf');
        }else{
            $pdf = PDF::loadView('admin::report.appointmentReport',compact('company','appointment','Reporttitle','today_date'));  
            return $pdf->stream('appointmentReport.pdf');
            
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
