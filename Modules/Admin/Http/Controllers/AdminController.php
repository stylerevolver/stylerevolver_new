<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use App\models\product;
use App\models\offer;
use Modules\Admin\Entities\Admin;
use App\User;
use App\models\complain;
use App\models\sales;
use App\models\appointment;
use DB;
use Auth;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $product=product::get()->count();
        $offer=offer::get()->count();
        $userCount = User::whereHas('roles', function($query){
            $query->where('name', 'customer');
        })->count();
        /*$salescount=sales::select('ammount', \DB::raw('sum(ammount) as total'))
                ->where('status','=','Complete')
                ->groupBy('id')
                ->get();
                print_r($salescount);exit();*/
        $complain =complain::where('complain_status','=','done')->get()->count(); 
        $appointment =appointment::where('complete_status','=','done')->get()->count();
        return view('admin::dashboard',compact('product','offer','userCount','complain','appointment'));
    }
    public function updateprofile()
    {
        $updateprofile=Admin::where('id','=',\Auth::guard('admin')->user()->id)->first();
        return view('admin::updateprofile.updateprofile',compact('updateprofile'));
    }

    public function updatepro(Request $request){
        //echo '<pre>';print_r($request->all());exit();
        if ($request->hasFile('image')) {
            $rules = array(
                'name' => 'required|string|max:255|regex:/^[a-zA-Z ]+$/u|unique:admins,name,'.$request->adminId,
                'email' => 'required|unique:admins,name,'.$request->adminId.'|email',
                'image' => 'required|image|mimes:jpeg,png,jpg',
            );
            $customeMessage = array(
                'name.required' => 'Name can not be an empty.',
                'name.string' => 'Name is must be in string value',
                'name.unique' => 'Name must be unique',
                'email.required' => 'Email can not be an empty.',
                'email.unique' => 'Email is must be unique',
            );
        }else{
            $rules = array(
                'name' => 'required|string|max:255|regex:/^[a-zA-Z ]+$/u|unique:admins,name,'.$request->adminId,
                'email' => 'required|email|unique:admins,name,'.$request->adminId,
            );
            $customeMessage = array(
                'name.required' => 'Name can not be an empty.',
                'name.string' => 'Name is must be in string value',
                'name.unique' => 'Name must be unique',
                'email.required' => 'Email can not be an empty.',
                'email.unique' => 'Email is must be unique',
            );
        }
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        if ($request->hasFile('image')) {
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            if(request()->image->move(public_path('images'), $imageName)){
                $image_name=$imageName;
            }else{
                $message="Image not uploaded please try again.";
                return redirect()->back()->withErrors($message)->withInput();
            }
        }else{
            $image_name=$request->admin_image;
        }
        
        $updateprofile=Admin::where('id','=',$request->adminId)->first();
        if(!empty($updateprofile))
        {
            $updateprofile->update([
                'name' => ucfirst($request->name),
                'email' => $request->email,
                'profile_picture' =>$image_name
            ]);
            return redirect('admin/updateprofile')->with('success','Your profile is updeted successfully');
        }else{
            return redirect('admin/updateprofile')->with('error','Some error');
            }
       
    }
}
