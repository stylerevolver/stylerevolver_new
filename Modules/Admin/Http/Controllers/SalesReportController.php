<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\models\sales;
use App\models\sales_details;
use App\models\company;
use App\User;
use DB;
use PDF;

class SalesReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admin::index');
    }

    public function reportshow()
    {
        $company=company::first();
        $Reporttitle="Sales Report";
        $sales=sales::join('users as u','u.id','=','sales.user_id')
            ->leftjoin('sales_details as sd','sales.id','=','sd.sales_id')
            ->join('product as p','p.id','=','sd.product_id')
            ->select('u.first_name','p.product_name','sales.ammount','sales.sales_date')
            ->get();

        $pdf = PDF::loadView('admin::report.salesreportshow',compact('company','Reporttitle','sales'));  
        return $pdf->stream('salesreport.pdf');
    }



    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function showsales()
    {
        $users=sales::select('sales.user_id',\DB::raw('count(*) as total'))
              ->groupBy('sales.user_id')
              ->get();
        $product=sales_details::select('sales_details.product_id',\DB::raw('count(*) as total'))
              ->groupBy('sales_details.product_id')
              ->get();
        $products=sales_details::join('sales as s','s.id','=','sales_details.sales_id')
                    ->join('product as p','p.id','=','sales_details.product_id')
                    ->select('p.product_name','p.id')
                    ->get();
        $sales_date=sales::select('sales.sales_date',\DB::raw('count(*) as total'))
              ->groupBy('sales.sales_date')
              ->get();
    
        return view('admin::report.salesreport',compact('users','product','sales_date','ammount','products'));

    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function customesales(Request $request)
    {
        $Reporttitle="Sales Report";
        $company=company::first();
        //user wise
        if($request->username != "" && $request->productname == "" && $request->sales_date == "")
        {
            $Reporttitle="User Wise Sales Report";
            $sales=sales::where('user_id','=',$request->username)->get();
        }
        //procduct name
        elseif($request->username == "" && $request->productname != "" && $request->sales_date == "" )
        {
            $Reporttitle="Product Wise Sales Report";
            $sales=sales::join('sales_details as sd','sd.sales_id','=','sales.id')
                    ->where('sd.product_id','=',$request->productname)->get();
        }
        //sales date
        elseif($request->username == "" && $request->productname == "" && $request->sales_date != "" )
        {
            $Reporttitle="Date Wise Sales Report";
            $sales=sales::where('sales_date','=',$request->sales_date)->get();
        }
        //product and date
        elseif($request->username == "" && $request->productname != "" && $request->sales_date != "")
        {
            $Reporttitle="Product Wise & Date Wise Sales Report";
            $sales=sales::join('sales_details as sd','sd.sales_id','=','sales.id')
                    ->where('sd.product_id','=',$request->productname)
                    ->where('sales_date','=',$request->sales_date)->get();
        }
         //username and date
        elseif($request->username != "" && $request->productname == "" && $request->sales_date != "")
        {
            $Reporttitle="User Wise & Date Wise Sales Report";
            $sales=sales::where('user_id','=',$request->username)
                    ->where('sales_date','=',$request->sales_date)->get();
        }
        //username and product
        elseif($request->username != "" && $request->productname != "" && $request->sales_date == "")
        {
            $Reporttitle="User Wise & Product Wise Sales Report";
            $sales=sales::join('sales_details as sd','sd.sales_id','=','sales.id')
                    ->where('sd.product_id','=',$request->productname)
                    ->where('user_id','=',$request->username)->get();
        }
         //all
        elseif ($request->username != "" && $request->productname != "" && $request->sales_date != "")
        {
            $Reporttitle="User Wise,Product Wise & Date Wise Sales Report";
            $sales=sales::join('sales_details as sd','sd.sales_id','=','sales.id')
                    ->where('sd.product_id','=',$request->productname)
                    ->where('user_id','=',$request->username)
                    ->where('sales_date','=',$request->sales_date)
                    ->get();
        }
        else{
            $Reporttitle="Sales Report";
            $sales=sales::get();
        }
        if(isset($request->download)){
            $pdf = PDF::loadView('admin::report.salesreportshow',compact('company','Reporttitle','sales'));  
            return $pdf->download('SalesReport.pdf');
        }else{
            $pdf = PDF::loadView('admin::report.salesreportshow',compact('company','Reporttitle','sales'));  
            return $pdf->stream('SalesReport.pdf');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
