<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use App\Rules\AdminMatchOldPassword;
use Illuminate\Support\Facades\Hash;
use Modules\Admin\Entities\Admin;
use DB;
use Auth;


class ChangePasswordController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    
    public function index()
    {
        $admin=Admin::where('id','=',\Auth::guard('admin')->user()->id)->first();
        return view('admin::changePassword',compact('admin'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'current_password' => ['required', new AdminMatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);
        $admin=Admin::where('id','=',$request->adminId)->first();
        if(!empty($admin))
        {
            $admin->update([
                'password' => Hash::make($request->new_password),
            ]);
            return redirect('admin/resetPassword')->with('msg','Your password changed successfully...!');
        }
        else{
            return redirect('admin/resetPassword')->with('error','Some error');
        }
    }

    
}

