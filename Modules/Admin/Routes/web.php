
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->middleware(['guest:admin'])->group(function() 
{
	Route::get('/login', 'AdminLoginController@login')->name('admin.auth.login');
	Route::post('/login', 'AdminLoginController@loginAdmin')->name('admin.auth.loginCheck');
    
    Route::get('/forgotPassword','AdminLoginController@forgotPassword');
    Route::post('/forgotPassword','AdminLoginController@forgotPasswordstore');
    Route::get('/adminForgotPassword/{email}/{tokan}/','AdminLoginController@newpassword');
    Route::post('/adminForgotPassword/','AdminLoginController@storepassword');
});


Route::prefix('admin')->middleware(['auth:admin'])->group(function() 
{
    Route::get('/', 'AdminController@index')->name('admin.dashboard');

    //updatepro
    Route::get('/updateprofile','AdminController@updateprofile');
    Route::post('/updatepro','AdminController@updatepro');
    
    //user
    Route::get('/user','cityController@viewUser');
    Route::get('/all-user-json','cityController@jsonUser');
    Route::get('/User/{id}','CityController@User');
    Route::get('/UserReport','CityController@UserReport')->name('UserReport');


    //sales
    Route::get('/sales','SalesController@index');
    Route::get('/all-sales-json','SalesController@show');

    //customized product
    Route::get('/customizedOrder/{id}','AppointmentAdminController@customizedProductIndex');
    Route::get('/all-customizedOrder-json/','AppointmentAdminController@customizedProductShow');
    Route::get('/updateCustomizedProductDetails/{id}','AppointmentAdminController@customizedProductDetailsEdit');
    Route::post('/updateCustomizedProductDetails','AppointmentAdminController@customizedProductDetailsUpdate');
    
    //appointment
    Route::get('/Appointments/','AppointmentAdminController@index');
    Route::get('/all-appointment-json/','AppointmentAdminController@show');
    Route::get('/updateStatus/{id}','AppointmentAdminController@edit');
    
    //sales details,salesPayment,Invoice
    Route::get('/salesDetails/{id}','SalesController@salesDetails');
    Route::get('/salesPayment/{id}','SalesController@salesPayment');
    Route::get('/Invoice/{id}','SalesController@Invoice');
    Route::get('/InvoiceDownload/{id}','SalesController@InvoiceDownload')->name('InvoiceDownload');


    //area
    Route::get('/area','AreaController@index');
    Route::get('/addArea','AreaController@create');
    Route::post('/storeArea','AreaController@store');
    Route::get('/all-area-json','AreaController@show');
    Route::get('/editArea/{id}','AreaController@edit');
    Route::post('/updateArea','AreaController@update');
    Route::get('/deleteArea/{id}','AreaController@destroy');
    

    //cart
    Route::get('/cart','CartController@index');
    Route::get('/all-cart-json','CartController@show');
    Route::get('/deleteCart/{id}','CartController@destroy');

    //city
    Route::get('/city','CityController@index');
    Route::get('/addCity','CityController@create');
    Route::post('/storeCity','CityController@store');
    Route::get('/all-city-json','CityController@show');
    Route::get('/editCity/{id}','CityController@edit');
    Route::post('/updateCity','CityController@update');
    Route::get('/deleteCity/{id}','CityController@destroy');
    
    //color
    Route::get('/color','ColorController@index');
    Route::get('/addColor','ColorController@create');
    Route::post('/storeColor','ColorController@store');
    Route::get('/all-color-json','ColorController@show');
    Route::get('/editColor/{id}','ColorController@edit');
    Route::post('/updateColor','ColorController@update');
    Route::get('/deleteColor/{id}','ColorController@destroy');

    //company
    Route::get('/company','CompanyController@index');
    Route::get('/addCompany','CompanyController@create');
    Route::post('/storeCompany','CompanyController@store');
    Route::get('/all-company-json','CompanyController@show');
    Route::get('/editCompany/{id}','CompanyController@edit');
    Route::post('/updateCompany','CompanyController@update');
    Route::get('/deleteCompany/{id}','CompanyController@destroy');

    //category
    Route::get('/category','CategoryController@index');
    Route::get('/addCategory','CategoryController@create');
    Route::post('/storeCategory','CategoryController@store');
    Route::get('/all-category-json','CategoryController@show');
    Route::get('/editCategory/{id}','CategoryController@edit');
    Route::post('/updateCategory','CategoryController@update');
    Route::get('/deleteCategory/{id}','CategoryController@destroy');

    //complain
    Route::get('/complain','ComplainController@index');
    Route::get('/all-complain-json','ComplainController@show');
    Route::get('/updateComplain/{id}','ComplainController@edit');
    
    //feedback
    Route::get('/feedback','FeedbackController@index');
    Route::get('/all-feedback-json','FeedbackController@show');

    //offer
     Route::get('/offer','OfferController@index');
    Route::get('/addOffer','OfferController@create');
    Route::post('/storeOffer','OfferController@store');
    Route::get('/all-offer-json','OfferController@show');
    Route::get('/editOffer/{id}','OfferController@edit');
    Route::post('/updateOffer','OfferController@update');
    Route::get('/deleteOffer/{id}','OfferController@destroy');

   //paymentType
    Route::get('/paymentType','PaymentTypeController@index');
    Route::get('/addPaymentType','PaymentTypeController@create');
    Route::post('/storePaymentType','PaymentTypeController@store');
    Route::get('/all-paymentType-json','PaymentTypeController@show');
    Route::get('/editPaymentType/{id}','PaymentTypeController@edit');
    Route::post('/updatePaymentType','PaymentTypeController@update');
    Route::get('/deletePaymentType/{id}','PaymentTypeController@destroy');

    //product
    Route::get('/product','ProductController@index');
    Route::get('/addProduct','ProductController@create');
    Route::post('/storeProduct','ProductController@store');
    Route::get('/all-product-json','ProductController@show');
    Route::get('/editProduct/{id}','ProductController@edit');
    Route::post('/updateProduct','ProductController@update');
    Route::get('/deleteProduct/{id}','ProductController@destroy');
    Route::get('/moreProductImages/','ProductController@moreProductImages');
    Route::post('/deleteProductImages/','ProductController@deleteProductImages');
    Route::get('/feturedProduct/','ProductController@feturedProduct');
    Route::post('/subcategoryChange/','ProductController@subcategoryChange');
    

    //rating
    Route::get('/rating','RatingController@index');
    Route::get('/all-rating-json','RatingController@show');
    Route::get('/deleteRating/{id}','RatingController@destroy');
    

    //size_chart
    Route::get('/size_chart','SizechartController@index');
    Route::get('/addSizechart','SizechartController@create');
    Route::post('/storeSizechart','SizechartController@store');
    Route::get('/all-sizechart-json','SizechartController@show');
    Route::get('/editSizechart/{id}','SizechartController@edit');
    Route::post('/updateSizechart','SizechartController@update');
    Route::get('/deleteSizechart/{id}','SizechartController@destroy');

    //subcategory
    Route::get('/subcategory','SubcategoryController@index');
    Route::get('/addSubcategory','SubcategoryController@create');
    Route::post('/storeSubcategory','SubcategoryController@store');
    Route::get('/all-subcategory-json','SubcategoryController@show');
    Route::get('/editSubcategory/{id}','SubcategoryController@edit');
    Route::post('/updateSubcategory','SubcategoryController@update');
    Route::get('/deleteSubcategory/{id}','SubcategoryController@destroy');


   //report
    
    
    //custom reating report
    Route::get('/rating_report','ReportController@showreport');
    Route::post('/customRatingReport','ReportController@customRatingReport');
    
    //appointment_report
    Route::get('/customAppointment','AppointmentReportController@reportshow');
    Route::post('/customAppointment','AppointmentReportController@store');

    //custome feedback report
    Route::get('/feedbackReport','ReportController@showFeedbackReport');
    Route::post('/customeFeedbackReport','ReportController@customeFeedbackReport');

    //complian_report
    Route::get('/complianReport','ReportController@showcomplain');

    //custome complian report
    Route::get('/customeComplainReport','ReportController@showComplainReport');
    Route::post('/complainReports','ReportController@customeComplainReport');


    //customer
    Route::get('/customerReport','ReportController@showCustomerReport');
    Route::post('/customeCustomerReport','ReportController@customeCustomerReport');

    //sales_report
    //Route::get('/salesreport','SalesReportController@reportshow');
    Route::get('/salesreportshow','SalesReportController@showsales');
    Route::post('/salesreportshow','SalesReportController@customesales');



    //multiple products
    Route::get('/multiple_products','ProductController@multiple_products');
    Route::post('/multiple_products_store','ProductController@multiple_products_store');


    //custome Product Report
    Route::get('/prodctReport','ReportController@showProductReport');
    Route::post('/customeProductReport','ReportController@customeProductReport');

    //change password
    Route::get('/resetPassword', 'ChangePasswordController@index');
    Route::post('/resetPassword', 'ChangePasswordController@store');

   
    //customize product report
    Route::get('/CustomizeProduct','ReportController@ShowCustomizeProduct');
    Route::post('/CustomizeProduct','ReportController@customizeProduct');

    //product report
    Route::get('/productReportShow','ReportController@ShowProduct');
    Route::post('/productReportShow','ReportController@ProductReport');
});