$(document).on('click','#logout',function(e){
    if(confirm("Are you sure you want to logout!")){
        e.preventDefault(); 
        document.getElementById('logout-form').submit();
    }
});
$(document).on('change','#subcategory',function(e){
	var u=window.location.origin;
	if(this.value != ""){
		var id=this.value;
		var type=$(this).data('info');
		var p_id=$(this).data('id');
		jQuery("#find_ajax_loading").show();
	    $.ajax({
			headers:{
				'X-CSRF-TOKEN':jQuery('meta[name="csrf-token"]').attr('content')
			},
			url:u+'/admin/subcategoryChange',
			type:'POST',
			data:{
				id:id,
				size_id:p_id,
				type:type
			},
			success:function(data){
				jQuery("#find_ajax_loading").hide();
				console.log(data);
				if($.trim(data) != "error")
				{	
					$("#sizechart").html(data);
					$("#sizechart").show();
					$(".sizechart_div").show();
					$(".s_error_message").html("");
					$(".pro_submit").attr('disabled',false);
					//toastr.success('Cart add successfully');
					console.log('done');
				}else{
					$("#sizechart").hide();
					$(".s_error_message").html("Please select another sub-category");
					$(".sizechart_div").show();
					console.log('error');
				}
			}
		});
	}else{
		$(".sizechart_div").hide();
	}
});