@extends('welcome')
@section('content')
<div class="banner-top">
	<div class="container">
		<h2 class="" data-wow-delay=".5s">My Order</h2>
		<h3 class="" data-wow-delay=".5s"><a href="{{ url('/home') }}">Home</a><label>/</label>My Order</h3>
		<div class="clearfix"> </div>
	</div>
</div>
<div class="check-out">	 
	<div class="container"> 
		 @if(session('success'))
            <div class="alert alert-success alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
              {{ session('success') }}
            </div>
          @elseif(session('error'))
              <div class="alert alert-error alert-dismissib">
                <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                {{ session('error') }}
            </div>                              
          @endif
		<div class="col-md-3">
			@include('myAccountLeftMenu')
		</div>
		<div class="col-md-9">
			<table class="table" data-wow-delay=".5s">
				<thead>
					<tr>
						<th class="t-head">No.</th>
						<th class="t-head">Date</th>
						<th class="t-head">Amount</th>
						<th class="t-head">Details</th>
						<th class="t-head">View Invoice</th>
						<th class="t-head">Download Invoice</th>
						<th class="t-head">Compain</th>
					</tr>
				</thead>
				<tbody>
					@if(count($sales)>0)
						@foreach($sales as $key=> $data)
	            		<tr>
		            		<td class="t-data" style="text-align: center;"> {{$key+1}} </td>
		            		<td class="t-data" style="text-align: center;"> {{date('d-m-Y', strtotime($data->sales_date))}}</td>
		            		<td class="t-data" style="text-align: center;">₹ {{$data->ammount}} </td>
		            		<td class="t-data" style="text-align: center;"> <a href="{{url('/myOrderDetails/'.$data->id)}}" data-text="Details" style="font-size: 20px;"><i class="fa fa-info-circle" aria-hidden="true" style="font-size: 20px;color:green"></i></a></td>
		            		<td class="t-data" style="text-align: center;"> <a href="{{url('/viewinvoiceFront/'.$data->id)}}" data-text="Invoice" style="font-size: 20px;" target="_blank"><i class="fa fa-eye" aria-hidden="true" style="font-size: 20px;color:#7BCB4D"></i></a></td>
		            		<td class="t-data" style="text-align: center;"> <a href="{{url('/invoiceFront/'.$data->id)}}" data-text="Invoice" style="font-size: 20px;"><i class="fa fa-download" aria-hidden="true" style="font-size: 20px;color:red"></i></a></td>
		            		<td class="t-data" style="text-align: center;"> <a href="{{url('/complain/'.$data->id)}}" data-text="Complain" style="font-size: 20px;"><i class="fa fa-comments" aria-hidden="true" style="font-size: 20px;color:red"></i></a></td>
						</tr>
	            		@endforeach
	            	@else
		            	<tr>
		            		<td colspan="5" style="text-align: center;font-size: 25px;">No Order Found...!</td>
		            	</tr>
		            	<tr>
		            		<td colspan="5" style="text-align: center;font-size: 25px;"><a href="{{url('/home')}}" class="btn btn-warning">Go To Home</a></td>
		            	</tr>
	            	@endif
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@section('PageCSS')
<style type="text/css">
	.close1{
		top:40px !important;
	}
	th.t-head{
		font-size: 18px !important;
		background-color: #FF7000;
	}
	td.t-data{
		font-size: 18px ;
	}

</style>
@endsection