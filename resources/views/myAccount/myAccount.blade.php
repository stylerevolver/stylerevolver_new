@extends('welcome')
@section('content')
<div class="banner-top">
	<div class="container">
		<h2 class="" data-wow-delay=".5s">My Account</h2>
		<h3 class="" data-wow-delay=".5s"><a href="{{ url('/home') }}">Home</a><label>/</label>MyAccount</h3>
		<div class="clearfix"> </div>
	</div>
</div>
<div class="myaccount">	 
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="col-md-3">
					<a href="{{url('/myprofile')}}" data-text="My Account" class="but-hover1 item_add Sh">My Account</a><br>
					
					<a href="{{url('/myAccount')}}" data-text="My Wishlist" class="but-hover1 item_add Sh">My Wishlist</a><br>
					
					<a href="{{url('/cart')}}" data-text="My Cart" class="but-hover1 item_add Sh">My Cart</a><br>
					
					<a href="{{url('/myOrder')}}" data-text="My Order" class="but-hover1 item_add Sh">My Order</a><br>

					<a href="{{url('/myOffers')}}" data-text="Offers" class="but-hover1 item_add Sh">Offers</a><br>

					<a href="{{url('/myAppointment')}}" data-text="My Appoinment" class="but-hover1 item_add Sh">My Appoinment</a><br>
					
					<a href="{{url('/changePassword')}}" data-text="Reset Pssword" class="but-hover1 item_add Sh">Reset Pssword</a><br>
					
					<!-- <a href="#" data-text="Payment Details" class="but-hover1 item_add Sh">Payment Details</a><br>

					<a href="#" data-text="Customize product Details" class="but-hover1 item_add Sh">Customize product Details</a><br> -->

					<a href="{{url('/updatepropic')}}" data-text="Profile Pic Update" class="but-hover1 item_add Sh">Profile Pic Update</a><br>
				</div>	
			</div>
			<div class="col-md-9">
				<!-- ahiyia dara useprofile nu karjo--->
			</div>
		</div>
	</div>
</div>
@endsection
@section('PageCSS')
	<style type="text/css">
		.myaccount{
			padding: 5em;
		}
	</style>
@endsection