@extends('welcome')
@section('content')
<div class="banner-top">
	<div class="container">
		<h2 class="" data-wow-delay=".5s">More Feedback</h2>
		<h3 class="" data-wow-delay=".5s"><a href="{{ url('/home') }}">Home</a><label>/</label>Feedback<label>/</label></h3>
		<div class="clearfix"> </div>
	</div>
</div>
<div class="product">
	<div class="container more_feedback_div">
		<div class="row">
			<div class="col-md-12">					
				@foreach($feedback as $data)
					<div class="col-md-4 from-group">
						<div class="top-comment-left morefeedback from-control">
							<img class="img-responsive" src="{{asset('/images/'.$data['profile_pic'])}}" alt="" onerror="" onerror="this.src='{{asset("download.png")}}';">
						</div>

						<div class="top-comment-right from-control">
							<h6>
								<a href="#">{{$data['first_name']}}</a> 
								<span style="text-align: right;">{{date('d-m-Y', strtotime($data['feedback_date']))}}</span>
							</h6>
							<span>Product   :  </span><span>{{$data['product_name']}}</span>
							<span>Feedback  :  </span><span>{{$data['feedback_description']}}</span>
						</div>
					</div>
				@endforeach
				<div class="clearfix"></div>
			</div>
		<div>
		<div class="pagination">
			{!! $feedback->links() !!}
		</div>	
	</div>
</div>
</div>
</div>
@endsection

@section('PageCSS')
<style type="text/css">
	.more_feedback_div{
		margin-top: 20px;
		margin-bottom: 20px;
	}
	.morefeedback{
		margin-bottom: 30px;
	}
</style>
@endsection
