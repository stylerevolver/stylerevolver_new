@extends('welcome')
@section('content')
<div class="banner-top">
    <div class="container">
        <h2 class="fadeInLeft" data-wow-delay=".5s">Update Profile Pic</h2>
        <h3 class="fadeInRight" data-wow-delay=".5s"><a href="{{ url('/home')}}">Home</a><label>/</label>Update Profile Pic</h3>
    </div>
</div>
<div class="check-out">	 
	<div class="container">
		 @if(session('success'))
            <div class="alert alert-success alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
              {{ session('success') }}
            </div>
          @elseif(session('error'))
              <div class="alert alert-warning alert-dismissib">
                <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                {{ session('error') }}
            </div> 
          @endif
          @error('image')
              <div class="alert alert-warning alert-dismissib">
                <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                {{ $message }}
               </div>
            @enderror                             
		<form method="post" action="{{ url('/updatepropic') }}" enctype="multipart/form-data">
            {{csrf_field()}} 
			<div class="col-md-3">
				@include('myAccountLeftMenu')
			</div>
			<div class="col-md-9">
				<div class="col-md-5">
					<input class="form-control m-input" name="image" id="image" type="file" value="{{ old('image') }}"  placeholder="Please enter your image">
					@error('image')
	                    <span class="text-danger" role="alert">
	                        <strong>{{ $message }}</strong>
	                    </span>
                    @enderror
				</div>
				<div class="m-form__actions">
					<div class="row">
						<div class="col-3"></div>
							<div class="col-2">
								<button type="submit" input type="button" class="btn btn-success">Submit</button>
							</div>
					</div>
					<input type="hidden" name="profile_pic" value="{{Auth::user()->profile_pic}}">
					<img src="{{asset('Profilephoto/'.\Auth::user()->profile_pic)}}" height="300" width="300" style="margin: 10px"></image>
				</div>
			</div>
		</form>
	</div>
</div>

@endsection