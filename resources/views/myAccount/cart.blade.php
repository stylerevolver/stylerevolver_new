@extends('welcome')
@section('content')
	<div class="banner-top">
		<div class="container">
			<h2 class="" data-wow-delay=".5s">Cart</h2>
			<h3 class="" data-wow-delay=".5s"><a href="{{ url('/home') }}">Home</a><label>/</label>Cart</h3>
			<div class="clearfix"> </div>
		</div>
	</div>
	<div class="check-out main_div">	 
		<div class="container"> 
			@if(session('success'))
		        <div class="alert alert-success alert-dismissible">
		          <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
		          {{ session('success') }}
		        </div>
          	@elseif(session('error'))
	            <div class="alert alert-error alert-dismissib">
	                <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
	                {{ session('error') }}
	            </div>
          	@endif
			<div class="col-md-3">
				@include('myAccountLeftMenu')
			</div>
			@php
				$total=0;
			@endphp
			<div class="col-md-9">
				<table class="table" data-wow-delay=".5s">
					<tr>
						<th class="t-head">Item</th>
						<th class="t-head">Name</th>
						<th class="t-head">Color</th>
						<th class="t-head">Price</th>
						<th class="t-head">Quantity</th>
						<th class="t-head">Total</th>
						<th class="t-head">Action</th>
					</tr>

				@if(count($cart)>0)
				    @foreach($cart as $key=> $c)

					    <tr class="cross cart_{{$c->cart_id}}">
							<td class="t-data">
								<input type="hidden" name="cart_id_array[]" value="{{ $c->cart_id }}">
								<input type="hidden" name="cart_id" id="cart_id" value="{{ $c->cart_id }}">
								<a href="{{ url('/product/'.$c->product_id)}}" class="at-in">
									<img src="{{asset('/images/'.$c->product_image)}}" class="img-responsive" alt="" height="150" width="90">
								</a>
							</td>
							<td class="t-data">
								{{ $c->product_name }}
							</td>
							<td class="t-data">
								<div style="background-color:{{ $c->color_name}};width:50px;height:50px;border:1px solid #fff;"></div>
								
							</td>
							<td class="t-data product_price{{$c->cart_id}}">{{ $c->product_price }}</td>
							<td class="t-data">
								<div class="quantity"> 
									<div class="quantity-select">            
										<a href="javascript:void(0)" class="decrement" data-id="{{$c->cart_id}}"><i class="entry value-minus"></i></a>
										<div class="entry value qty_value{{$c->cart_id}}">{{ $c->product_qty }}</div>									
										<a href="javascript:void(0)" class="increment" data-id="{{$c->cart_id}}"><i class="entry value-plus active"></i></a>
									</div>
								</div>
							</td>
							<td class="t-data camount{{$key}} amount{{$c->cart_id}}">₹ <span>{{ $c->product_price * $c->product_qty }}</span></td>
							@php
								$count=$c->product_price * $c->product_qty;
								$total+=$count;
							@endphp
							<td class="t-data">
								 <a href="#" data-id="{{$c->cart_id}}" class="btn btn-danger btn-sm" id="cart_delete">
							 	<i class="fa fa-trash-o"></i>
							 </a>
							</td>
					  	</tr>
				  	@endforeach
			@else
				<tr>
					<td colspan="5" style="text-align: center;vertical-align: center"><h3>No Prodcut found..</h3></td>
				</tr>
				<tr>
					<td colspan="5" style="text-align: center;vertical-align: center"><a href="{{url('/home')}}" class="btn btn-warning">Go To Home</a></td>
				</tr>
			@endif
				</table>
				@if(count($cart)>0)
					<div class="cart-total">
						<h5 class="continue" >Cart Total</h5>
						<div class="price-details">
							<h3>Price Details</h3>
							<span>Total</span>
							@php
								$cgst=(($total*2.5)/100);
								$sgst=(($total*2.5)/100);
								$offer=(($total*10)/100);
							@endphp
							<span class="total1">₹ {{number_format($total, 2, '.', ',')}}</span>
							<p class="sgst"> SGST(2.5%)  : ₹ {{number_format($sgst, 2, '.', ',')}}</p>
							<p class="cgst"> CGST(2.5%)  : ₹ {{number_format($cgst, 2, '.', ',')}}</p>
							<p class="offer">OFFER(10%)  : ₹ {{number_format($offer, 2, '.',',')}}</p>
							<!-- <span>Offer</span>
							<span class="offer"></span> -->
							{{-- <span>Delivery Charges</span>
							<span class="total1">150.00</span> --}}
							<div class="clearfix"></div>				 
						</div>	
						<ul class="total_price">
						   <li class="last_price"> <h4>TOTAL</h4></li>	
						   @php
								$total=$total+$sgst+$cgst-$offer;
							@endphp
						   <li class="last_price">₹<span>{{number_format($total, 2, '.', ',')}}</span></li>
						   <div class="clearfix"> </div>
						</ul>
						<a href="{{ url('/placeorder/cart')}}">Produced By Cart</a>
					</div>
				@endif
			</div>
		</div>
	</div>
@endsection
@section('pageJs')
	<script type="text/javascript">
		$(document).ready(function(c) {
			$('.close1').on('click', function(c){
				$('.cross').fadeOut('slow', function(c){
					$('.cross').remove();
				});
			});	  
		});
		jQuery(document).on('click','#cart_delete',function(){
			if(confirm('Are you sure you want to delete this Product?')){
				jQuery(".gif").show();
				var wish_list_id=jQuery(this).data('id');
				$.ajax({
					
					type:'get',
					url:'{{url("/removeCart/")}}'+'/'+wish_list_id,
					data:{
						id:wish_list_id
					},
					success:function(data){
						if (data != "error"){
							$(".cart_"+data.trim()).remove();
							var count_row=jQuery('.cross').length;
							var total_amount=amo=0;
							for (var i = 0; i < count_row; i++) {
								amo=jQuery(".camount"+i+' span').html();
								total_amount+=parseInt(amo);
							}
							if(total_amount != "" && total_amount > 0){
								var sgst=(total_amount*2.5)/100;
								var cgst=(total_amount*2.5)/100;
								var offer=(total_amount*10)/100;
								total_amount1=total_amount+sgst+cgst-offer;
								jQuery('.total1').html('₹'+total_amount);
								jQuery('.sgst').html('SGST(2.5%) : ₹'+sgst);
								jQuery('.cgst').html('CGST(2.5%) : ₹'+cgst);
								jQuery('.offer').html('OFFER(10%) : ₹'+offer);
								jQuery('.last_price span').html(total_amount1);
							}else{
								jQuery(".cart-total").hide();
								jQuery(".table").append('There is no Product in Cart.');

							}
						}else{
							 console.log('error');
						}
						jQuery(".gif").hide();
					}
				});
			}
		})
		jQuery('.product_price').html();
		$(document).ready(function(c) {
			$('.increment').on('click', function(){
				var cart_id=jQuery(this).data('id');
				var qty=parseInt(jQuery('.qty_value'+cart_id).html());
				qty=qty+1;
				if(qty > 1){
					jQuery(".gif").show();
					jQuery(".cart-total").hide();
					$.ajax({
						headers: {
				            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
			            },
					   type:'POST',
					   url:'{{url("/upadatecartqty/")}}',
					   data:{
					   		cart_id:cart_id,
					   		qty:qty,
					   	},
					  success:function(data){

					      if(data != "error"){
							var array=JSON.parse(data);
							var id=array.id;
							var product_qty=array.product_qty;
					      	var amount="amount"+id;
					      	var price=jQuery(".product_price"+id).html();
					      	var total=product_qty*price;
					      	console.log(id);
					      	console.log(amount);
					      	console.log(product_qty);
					      	console.log(price);
					      	console.log(total);
					      	jQuery("."+amount+' span').html(total);
					      	console.log(data);
							var count_row=jQuery('.cross').length;
							var total_amount=0;
							for (var i = 0; i < count_row; i++) {
								var amo=jQuery(".camount"+i+' span').html();
								total_amount+=parseInt(amo);
							}
							if(total_amount != "" && total_amount > 0){
								var sgst=(total_amount*2.5)/100;
								var cgst=(total_amount*2.5)/100;
								var offer=(total_amount*10)/100;
								total_amount1=total_amount+sgst+cgst-offer;
								jQuery('.total1').html('₹'+total_amount);
								jQuery('.sgst').html('SGST(2.5%) : ₹'+sgst);
								jQuery('.cgst').html('CGST(2.5%) : ₹'+cgst);
								jQuery('.offer').html('OFFER(10%) : ₹'+offer);
								jQuery('.last_price span').html(total_amount1);
							}else{
								jQuery(".cart-total").hide();
								jQuery(".table").append('There is no Product in Cart.');

							}
					      }
						jQuery(".gif").hide();
						jQuery(".cart-total").show();
					   }
					 });
				}
				var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10)+1;
				divUpd.text(newVal);
			});
			$('.decrement').on('click', function(){
				var cart_id=jQuery(this).data('id');
				var qty=parseInt(jQuery('.qty_value'+cart_id).html());
				if(qty > 1){
					jQuery(".gif").show();
					jQuery(".cart-total").hide();
					$.ajax({
						headers: {
				            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
			            },
					   type:'POST',
					   url:'{{url("/upadatecartqty/")}}',
					   data:{
					   		cart_id:cart_id,
					   		qty:qty-1,
					   	},
					   success:function(data){

					      if(data != "error"){
							var array=JSON.parse(data);
							var id=array.id;
							var product_qty=array.product_qty;
					      	var amount="amount"+id;
					      	var price=jQuery(".product_price"+id).html();
					      	var total=product_qty*price;
					      	jQuery("."+amount+' span').html(total);
					      	var count_row=jQuery('.cross').length;
							var total_amount=0;
							for (var i = 0; i < count_row; i++) {
								var amo=jQuery(".camount"+i+' span').html();
								total_amount+=parseInt(amo);
							}
							if(total_amount != "" && total_amount > 0){
								var sgst=(total_amount*2.5)/100;
								var cgst=(total_amount*2.5)/100;
								var offer=(total_amount*10)/100;
								total_amount1=total_amount+sgst+cgst-offer;
								jQuery('.total1').html('₹'+total_amount);
								jQuery('.sgst').html('SGST(2.5%) : ₹'+sgst);
								jQuery('.cgst').html('CGST(2.5%) : ₹'+cgst);
								jQuery('.offer').html('OFFER(10%) : ₹'+offer);
								jQuery('.last_price span').html(total_amount1);
							}else{
								jQuery(".cart-total").hide();
								jQuery(".table").append('There is no Product in Cart.');

							}
					      }

						jQuery(".gif").hide();
						jQuery(".cart-total").show();
					   }
					 });
				}
				var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10)-1;
				if(newVal>=1) divUpd.text(newVal);
			});
		});
	</script>
@endsection
@section('PageCSS')
<style type="text/css">
	.name h5{
		margin-top: 0px !important;
	}
	.close1{
		top:40px !important;
	}
	th.t-head{
		font-size: 18px !important;
		background-color: #FF7000;
	}
</style>
@endsection