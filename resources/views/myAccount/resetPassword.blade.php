@extends('welcome')
@section('content')
<div class="banner-top">
    <div class="container">
        <h2 class="" data-wow-delay=".5s">Change Password</h2>
        <h3 class="" data-wow-delay=".5s"><a href="{{ url('/home') }}">Home</a><label>/</label>Change Password</h3>
        <div class="clearfix"> </div>
    </div>
</div>
<div class="changePassword">  
    <div class="container"> 
        <div class="col-md-3">
            @include('myAccountLeftMenu')
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading" style="border-bottom: 1px solid black;margin-bottom: 10px;background-color: #FF7000;color:white;padding: 7px;border-radius: 5px">
                    <h2>Change password</h2>
                </div>
                <div class="clearfix"> </div>
                <div class="panel-body">
                    <form method="POST" action="{{ url('/changePassword') }}" style="box-sizing: 1px">
                        @if(session('msg'))
                            <div class="alert alert-success alert-dismissible">
                              <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                              {{ session('msg') }}
                            </div>
                        @endif
                        @csrf 
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Current Password</label>

                            <div class="col-md-6">
                                <input required id="password" type="password" class="form-control @error('current_password') is-invalid @enderror" name="current_password" autocomplete="current-password">
                                @error('current_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">New Password</label>

                            <div class="col-md-6">
                                <input required id="new_password" type="password" class="form-control @error('new_password') is-invalid @enderror" name="new_password" autocomplete="current-password">
                                @error('new_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Confirm Password</label>

                            <div class="col-md-6">
                                <input required id="new_confirm_password" type="password" class="form-control @error('new_confirm_password') is-invalid @enderror" name="new_confirm_password" autocomplete="current-password">
                                @error('new_confirm_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 text-center">
                                <button type="submit" class="btn btn-warning" style="background-color: #FF7000">
                                    Update Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('PageCSS')
    <style type="text/css">
        .changePassword{
            padding: 5em;
        }
    </style>
@endsection