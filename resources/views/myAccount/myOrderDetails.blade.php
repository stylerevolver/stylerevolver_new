@extends('welcome')
@section('content')
<div class="banner-top">
	<div class="container">
		<h2 class="" data-wow-delay=".5s">My Order Details</h2>
		<h3 class="" data-wow-delay=".5s"><a href="{{ url('/home') }}">Home</a><label>/</label>My Order</h3>
		<div class="clearfix"> </div>
	</div>
</div>
<div class="check-out">	 
	<div class="container"> 
		<div class="col-md-3">
			@include('myAccountLeftMenu');
		</div>
		<div class="col-md-9">
			<table class="table" data-wow-delay=".5s">
				<tr>
					<th class="t-head">No.</th>
					<th class="t-head">Product Name</th>
					<th class="t-head">Product Image</th>
					<th class="t-head">price</th>
					<th class="t-head">Quantity</th>
					<th class="t-head">Total</th>
				</tr>
	            <tbody>
	                @foreach($details as $data)
		                <tr>
		                    <td>{{$data['sales_id']}}</td>	
		                    <td>{{$data['product_name']}}</td>
		                    <td><img src="{{asset('/images/'.$data->product_image)}}" height="70" width="70" alt="{{$data->product_name}}"></td>
		                    <td>₹{{$data['price']}}</td>
		                    <td>{{$data['qty']}}</td>
		                    <td>₹{{($data->price)*($data->qty)}}</td>
		                </tr>
	                @endforeach
	            </tbody>
				<tfoot>
				    <tr>
					    <td></td>
					    <td></td>
					    <td></td>
					    <td></td>
					    <td></td>
				    </tr>
				</tfoot>
			</table>
		</div>
		<div class="col-md-3">
			
		</div>
	</div>
</div>
@endsection

@section('PageCSS')
<style type="text/css">
	
	.close1{
		top:40px !important;
	}
	th.t-head{
		font-size: 18px !important;
		background-color: #FF7000;
	}
	td.t-data{
		font-size: 18px ;

</style>
@endsection