@extends('welcome')
@section('content')
@php
	$total=0;
@endphp
	<div class="banner-top">
		<div class="container">
			<h2 class="" data-wow-delay=".5s">Place Order</h2>
			<h3 class="" data-wow-delay=".5s"><a href="{{ url('/home') }}">Home</a><label>/</label>Place Order</h3>
			<div class="clearfix"> </div>
		</div>
	</div>

<div class="container placeorder_class">
	<div class="row">
		<div class="row">
			<form method="post" action="{{ url('/placeorder') }}">
				{{csrf_field()}}
				<input type="hidden" name="user_id" value="{{$users->id}}">
				<div class="col-md-12">
					<div class="form-group">
						<span style="font-size: 24px;font-weight: 700;border-bottom: 1px solid;width: 100%">Order details</span>
					</div>
					<table class="table" data-wow-delay=".5s">
						<tr>
							<th class="t-head">Item</th>
							<th class="t-head">Name</th>
							<th class="t-head">Color</th>
							<th class="t-head">Price</th>
							<th class="t-head">Quantity</th>
							<th class="t-head">Total</th>
						</tr>
						 @foreach($details as $key => $data)
						    <tr class="cross wishlist_{{$data->id}}">
								<td class="t-data">
									<input type="hidden" name="details_array[{{$key}}][id]" value="{{ $data->id }}">
									<input type="hidden" name="details_array[{{$key}}][qty]" value="{{ $data->qty }}">
									<input type="hidden" name="details_array[{{$key}}][price]" value="{{ $data->product_price }}">
									<input type="hidden" name="details_array[{{$key}}][proid]"  value="{{ $data->proid }}">
									<input type="text" name="product_id[]" class="product_id" value="{{ $data->proid }}">
									<a href="{{ url('/product/'.$data->proid)}}" class="at-in">
										<img src="{{asset('/images/'.$data->product_image)}}" class="img-responsive" alt="" height="50" width="50">
									</a>
								</td>
								<td class="t-data">{{ $data->product_name }}</td>
								<td class="t-data">
									<div style="background-color:{{ $data->color_name}};width:50px;height:50px;border:1px solid #fff;"></div>
								</td>
								<td class="t-data product_price{{ $data->id }}">{{ $data->product_price }}</td>
								<td class="t-data">{{$data->qty}}</td>
								<td class="t-data camount{{$key}} amount{{ $data->id}}">₹ <span>{{ $data->product_price * $data->qty }}</span></td>
								@php
									$count=$data->product_price * $data->qty;
									$total+=$count;
								@endphp
							</tr>
					  	@endforeach
					</table>
				</div>
				<div class="clearfix"></div>
				<div class="col-md-12">
					<div class="col-md-6">
						<div class="form-group">
							<span style="font-size: 24px;font-weight: 700;border-bottom: 1px solid;width: 100%">Billing Address</span>
						</div>
            			<div class="form-group">
                            <label for="name" class="col-md-1 col-form-label text-md-right">Name</label>
                               <input class="form-control m-input" name="name" id="name" type="text" value="{{$users->first_name}}" tabindex="1"placeholder="Please enter your name">
								@error('name')
	                            <span class="text-danger" role="alert">
	                                <strong>{{ $message }}</strong>
	                           		</span>
	                            @enderror
                        </div>
                 
						<div class="form-group ">
							<label for="pincode" class="col-md-1 col-form-label text-md-right">Pincode</label>
								<input type="number" class="form-control m-input" name="pincode" value="{{$users->area->pincode}}" tabindex="2" placeholder="Please enter Pincode">
								@error('pincode')
		                        <span class="text-danger" role="alert">
		                            <strong>{{ $message }}</strong>
		                        </span>
		                        @enderror
						</div>
                    	<div class="form-group ">
							<label for="email" class="col-md-1 col-form-label text-md-right">Email</label>
								<input class="form-control m-input" name="email" id="email" type="email" value="{{$users->email}}" tabindex="6" placeholder="Please enter Email Id">
								@error('email')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
						</div>


						
						<div class="form-group">
							<label for="example-text-input" class="col-md-1 col-form-label">Contact</label>
								<input type="number" class="form-control m-input" name="contact" value="{{$users->contact_no}}" tabindex ="7" placeholder="Please enter Contact">
								@error('contact')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
						</div>

						<div class="form-group">
							<label for="address" class="col-md-1 col-form-label text-md-right">Address</label>
							<textarea class="form-control m-input" name="address" id="address" tabindex="3" placeholder="Please enter address">{{$users->address}}</textarea>
							@error('address')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
						</div>


						<div class="form-group">
							<label for="example-text-input" class="col-md-1 col-form-label">Area</label>
							<select name="area" id="area" class="form-control" tabindex="4" placeholder="Please enter Area ">
								<option></option>
								@foreach($area as $a)
									<option value="{{$a->id}}" {{ $users->area_id == $a->id ? 'selected="selected"' : '' }}>{{ $a->area_name}}</option>
                                @endforeach
							</select>
							@error('area')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<span style="font-size: 24px;font-weight: 700;border-bottom: 1px solid;width: 100%">Shipping Address</span>
						</div>
						<div class="form-group">
							<input type="checkbox" name="Shipping" id="Shipping"><label for="Shipping">Order To another address</label>	
						</div>
						<div class="shipping_part" style="display: none">
							<div class="form-group">
								<label for="address" class="col-md-1 col-form-label text-md-right">Address1</label>
								<textarea class="form-control m-input" name="shipping_address1" id="shipping_address1" tabindex="3" placeholder="Please enter address1 Like (Strret address)"></textarea>
								@error('shipping_address')
		                            <span class="text-danger" role="alert">
		                                <strong>{{ $message }}</strong>
		                            </span>
	                            @enderror
	                        </div>
	                       	<div class="form-group">
								<label for="address" class="col-md-1 col-form-label text-md-right">Address2</label>
								<textarea class="form-control m-input" name="shipping_address2" id="shipping_address2" tabindex="3" placeholder="Please enter address like(Near pleace, behind, opposite place name )"></textarea>
								@error('shipping_address')
		                            <span class="text-danger" role="alert">
		                                <strong>{{ $message }}</strong>
		                            </span>
	                            @enderror
	                        </div>
	                        <div class="form-group">
								<label for="example-text-input" class="col-form-label">Shipping Area</label>
								<select name="shippingarea" id="shippingarea" class="form-control" tabindex="4" placeholder="Please enter Area ">
									<option></option>
									@foreach($area as $a)
										<option value="{{$a->id}}">{{ $a->area_name}}</option>
	                                @endforeach
								</select>
								@error('shippingarea')
	                                <span class="text-danger" role="alert">
	                                    <strong>{{ $message }}</strong>
	                                </span>
	                            @enderror
							</div>
						</div>
					</div>
				</div>
						<div class="col-md-12 cart-total"> 
							@if(count($details)>0)
							<div class="col-md-6">
								<div class="">
									<span>Apply couponcode</span>
									<input type="text" id="offer" class="offer" name="offer"><input type="button" class="Apply" name="Apply" value="Apply">
									<div class="price-details">
										<h3>Price Details</h3>
										<span>Total</span>
										@php
											$cgst=(($total*2.5)/100);
											$sgst=(($total*2.5)/100);
											/*$offer=(($total*10)/100);*/
										@endphp
										<span class="total1">₹ {{number_format($total, 2, '.', ',')}}</span>
										<span>SGST(2.5%) :</span>
										<span class="sgst">₹ {{number_format($sgst, 2, '.', ',')}}</span>
										<span>CGST(2.5%) :</span>
										<span class="cgst">₹ {{number_format($cgst, 2, '.', ',')}}</span>
										{{-- <span>Offer(10%)</span>
										<span class="offer">₹ {{number_format($offer, 2, '.',',')}}</span> --}}
										<div class="offer_div" style="display: none;">
											<span>SUB Total:</span>
											<span class="subtotal"></span>
											<span>Offer per :</span>
											<span class="offper"></span>
											<span>Offer amount:</span>
											<span class="offamo"></span>
										</div>
										{{-- <span>Delivery Charges</span>
										<span class="total1">150.00</span> --}}
										<div class="clearfix"></div>				 
									</div>	



									<ul class="total_price">
									   <li class="last_price"> <h4>TOTAL</h4></li>	
									   	@php
											$total=$total+$sgst+$cgst;
										@endphp
									   <li class="last_price">₹<span>{{number_format($total, 2, '.', ',')}}</span></li>
									   <div class="clearfix"> </div>
									   <input type="hidden" name="grandtotal" id="grandtotal" value="{{$total}}">
									   <input type="hidden" name="offer" value="">
									   <input type="hidden" name="type" value="{{$wishOrCart}}">
									</ul>
								</div>
							</div>
								<div class="col-md-6">
									<div class="paymnet-details">
										<input type="radio" name="paymnet" id="cod" value="cod" checked><label for="cod"><img src="{{asset('/cod.jpg')}}" height="150" width="150">Cash On Delivery</label>
									</div>	
								</div>
							@endif
						</div>
					
				<div class="col-md-12">
					<div class="col-md-6 text-center">
						<input type="submit" name="submit" value="submit" class="btn btn-success form-control">
					</div>
					<div class="col-md-6 text-center">
						<input type="reset" name="reset" value="reset" class="btn btn-danger form-control">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>



@endsection
@section('PageCSS')
<style type="text/css">
	
	.close1{
		top:40px !important;
	}
	th.t-head{
		font-size: 18px !important;
		background-color: #FF7000;
	}
	.price-details h3 {
	    color: #000;
	    font-size: 2.2em;
	    margin-bottom: 1em;
	}
	.cart-total {
	   	border: 1px solid;
	    padding: 25px;
	    border-radius: 10px;
	    margin-bottom:10px; 
   }
   .placeorder_class{
	   	margin-top: 10px;
	   	margin-bottom: 10px; 
   }
</style>
@endsection
@section('pageJs')
	<script type="text/javascript">
		$(document).ready(function(){
	        $('input[name="Shipping"]').click(function(){
	            if($(this).prop("checked") == true){
	                $(".shipping_part").show();
	                $("#shipping_address1").attr('required',true);
	                $("#shipping_address2").attr('required',true);
	                $("#shippingarea").attr('required',true);
	            }
	            else if($(this).prop("checked") == false){
	                $(".shipping_part").hide();
	                $("#shipping_address1").attr('required',false);
	                $("#shipping_address2").attr('required',false);
	                $("#shippingarea").attr('required',false);
	            }
	        });
	    });

	    //ajax for offer chk
	    jQuery(document).on('click','.Apply',function(){
	    	if(jQuery("#offer").val() != ""){
		    	var couponcode = jQuery("#offer").val();
		    	var product_id = jQuery('.product_id').val();
		    	var grandtotal = jQuery('#grandtotal').val();
				var product_id = $('input[name^=product_id]').map(function(idx, elem) {
				    return $(elem).val();
				  }).get();
		    	$.ajax({
		    		headers:{
		    			'X-CSRF-TOKEN':jQuery('meta[name="csrf-token"]').attr('content')
		    		},
		    		type:'POST',
		    		url:'{{url("/couponCodeCheck")}}',
		    		data:{
		    			couponcode:couponcode,
		    			product_id:product_id,
		    			grandtotal:grandtotal
		    		},
		    		success:function(responce)
		    		{
		    			var data=JSON.parse(responce);
		    			if(data.status != "success" && data.success == "error"){

		    			}else{
		    				jQuery('#grandtotal').val(data.grand_total);
		    				jQuery('.last_price span').html(data.grand_total);
		    				jQuery('.offper').html(data.offper+"%");
		    				jQuery('.offamo').html("₹"+data.offamo);
		    				jQuery('.subtotal').html("₹"+data.subtotal);
		    				jQuery(".offer_div").show();
		    			}
		    			console.log(data);
		    			console.log(data.status);
		    			/*if(data != "error"){
		    				jQuery('#cp').text(data);
		    			}*/
		    		}
		    	})
	    	}
	    })
    </script>
@endsection