@extends('welcome')
@section('content')
<div class="banner-top">
    <div class="container">
        <h2 class="fadeInLeft" data-wow-delay=".5s">Profile Update</h2>
        <h3 class="fadeInRight" data-wow-delay=".5s"><a href="{{ url('/home')}}">Home</a><label>/</label>Profile Update</h3>
    </div>
</div>
<div class="check-out">  
    <div class="container"> 
        <div class="col-md-3">
            @include('myAccountLeftMenu')
        </div>
        <div class="col-md-9" style="background-color:#f1f3f5;padding: 20px;margin-bottom: 20px">
            <form method="post" action="{{ url('/myprofile') }}">
                {{csrf_field()}}
                @if(session('success'))
                    <div class="alert alert-success alert-dismissible">
                      <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                      {{ session('success') }}
                    </div>
                @elseif(session('warning'))
                    <div class="alert alert-warning alert-dismissible">
                      <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                      {{ session('warning') }}
                    </div>
                @endif
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br>
                @endif
                <div class="col-md-12 login-do1">
                    <div class="col-md-6">
                        <div class="row">
                            <input type="hidden" name="user_id" value="{{$profileupdate->id}}">
                           {{--  <div class="form-group">
                                <label for="username">User name</label>
                                <input type="text" id="username" placeholder="Enter youre username" required autocomplete="username" name="username" class="form-control" tabindex="1" value="{{$profileupdate->name}}">
                                @error('username')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div> --}}
                            <div class=" form-group">
                                <label for="first_name">First Name</label>
                                <input type="text" name="first_name" id="first_name" placeholder="Enter Your First name" required autocomplete="false" class="form-control" tabindex="2" value="{{$profileupdate->first_name }}">
                                @error('first_name')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                             <div class="form-group">
                                <label for="lname">Last Name</label>
                                <input type="text" name="lname" id="lname" placeholder="Enter your Last name" required autocomplete="false" class="form-control" tabindex="3" value="{{$profileupdate->last_name }}">
                                @error('lname')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror                                
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" placeholder="Enter your Email" required autocomplete="false" name="email" id="email" class="form-control" tabindex="4" value="{{$profileupdate->email }}">
                                @error('email')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="phone">Phone number</label>
                                <input type="number" min="0" placeholder="Enter your phone" name="phone" id="phone" required autocomplete="false" class="form-control" tabindex="5" value="{{$profileupdate->contact_no  }}">
                                @error('phone')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="dob">Date Of Birth</label>
                                <input type="date" placeholder="Select birth date" required autocomplete="false" class="form-control" id="dob" name="dob" tabindex="8" value="{{$profileupdate->dob }}">
                                @error('dob')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="gender">Gender</label><br>
                            <input type="radio" name="gender"  value="male" required  tabindex="6" {{ $profileupdate->gender == "male" ? 'checked' : '' }}  tabindex="6">Male
                            <input type="radio" name="gender" value="female" required tabindex="7" {{ $profileupdate->gender == "female" ? 'checked' : '' }} tabindex="7">Female
                            @error('gender')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                       
                        <div class="form-group">
                            <label for="address">Address</label>
                            <textarea name="address" id="address" placeholder="Enter your address" required class="form-control" cols="10"  tabindex="9">{{ $profileupdate->address  }}</textarea>
                            @error('address')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="areaname">Area</label>
                            <select name="areaname" id="areaname" required class="form-control" tabindex="12">
                                <option value="">Select area</option> 
                                @foreach($area as $a)
                                    <option value="{{$a->id}}" {{ $profileupdate->area_id == $a->id ? 'selected="selected"' : '' }}>{{ $a->area_name}}</option>
                                @endforeach
                            </select>
                            @error('areaname')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="s_q">Security Question</label>
                            <select name="s_q" id="s_q" required class="form-control" tabindex="10">
                                <option selected value=""></option>
                                <option value="nicename" @if($profileupdate->security_question == "nicename") selected @endif>Nickname</option>
                                <option value="color" @if($profileupdate->security_question == "color") selected @endif>Color</option>
                            </select>
                            @error('s_q')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                         <div class="form-group">
                            <label for="s_a">Security Answer</label>
                            <input type="text" placeholder="Enter security answer" required autocomplete="false" class="form-control" id="s_a" name="s_a" value="{{$profileupdate->security_answer}}" tabindex="11">
                            @error('s_a')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                       
                    </div>
                </div>
                <div class="col-md-2 form-group">
                </div>
                <div class="col-md-4 form-group">
                    <input type="submit" value="Submit" name="submit" class="btn btn-info form-control" tabindex="13">
                </div>
                <div class="col-md-4 form-group">
                    <input type="reset" value="reset" name="reset" id="reset" class="btn btn-warning form-control" tabindex="14">
                </div> 
                <div class="col-md-2 form-group">
                </div> 
            </form>
        </div>
    </div>
</div>
@endsection
@section('pageJs')
    <script type="text/javascript">
        $(document).on('click',"#reset",function(){
            location.reload();
        })
    </script>
@endsection
@section('PageCSS')
   <style type="text/css">
        .header_regs{
            font-size: 65px;
            color: blue;
            font-weight: bolder;                                                                                
            margin-bottom:30px;
        }
        .profileupdate{
            padding-top:10px; 
            padding-bottom:10px;
        }
    </style>

@endsection