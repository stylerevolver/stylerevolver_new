<!DOCTYPE html>
<html>
<head>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	<meta content="width=device-width" name="viewport" />
	<title>Style Revolver</title>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<style type="text/css">/* -------------------------------------
        INLINED WITH https://putsmail.com/inliner
    ------------------------------------- */
    /* -------------------------------------
        RESPONSIVE AND MOBILE FRIENDLY STYLES
    ------------------------------------- */
    @media only screen and (max-width: 620px) {
      table[class=body] h1 {
        font-size: 28px !important;
        margin-bottom: 10px !important; }
      table[class=body] p,
      table[class=body] ul,
      table[class=body] ol,
      table[class=body] td,
      table[class=body] span,
      table[class=body] a {
        font-size: 16px !important; }
      table[class=body] .wrapper,
      table[class=body] .article {
        padding: 10px !important; }
      table[class=body] .content {
        padding: 0 !important; }
      table[class=body] .container {
        padding: 0 !important;
        width: 100% !important; }
      table[class=body] .main {
        border-left-width: 0 !important;
        border-radius: 0 !important;
        border-right-width: 0 !important; }
      table[class=body] .btn table {
        width: 100% !important; }
      table[class=body] .btn a {
        width: 100% !important; }
      table[class=body] .img-responsive {
        height: auto !important;
        max-width: 100% !important;
        width: auto !important; }}
    /* -------------------------------------
        PRESERVE THESE STYLES IN THE HEAD
    ------------------------------------- */
    @media all {
      .ExternalClass {
        width: 100%; }
      .ExternalClass,
      .ExternalClass p,
      .ExternalClass span,
      .ExternalClass font,
      .ExternalClass td,
      .ExternalClass div {
        line-height: 100%; }
      .apple-link a {
        color: inherit !important;
        font-family: inherit !important;
        font-size: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
        text-decoration: none !important; }
      .btn-primary table td:hover {
        background-color: #34495e !important; }
      .btn-primary a:hover {
        background-color: #34495e !important;
        border-color: #34495e !important; } }
	</style>
</head>
<body style="background-color:#f6f6f6;font-family:'avenir','helvetica',sans-serif;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
<table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#f6f6f6;width:100%;">
	<tbody>
		<tr>
			<td style="font-family:'avenir','helvetica',sans-serif;font-size:14px;vertical-align:top;"></td>
			<td class="container" style="font-family:'avenir','helvetica',sans-serif;font-size:14px;vertical-align:top;display:block;max-width:580px;padding:10px;width:580px;Margin:0 auto !important;">
			<div class="content" style="box-sizing:border-box;display:block;Margin:0 auto;max-width:580px;padding:10px;"><!-- START CENTERED WHITE CONTAINER -->
			<table class="main" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#fff;border-radius:3px;width:100%;"><!-- START MAIN CONTENT AREA -->
				<tbody>
          <tr style="background-color:#0079d1; padding: 20px; text-align: center;">
              <td style="padding: 20px;">
                <h1 style="font-family:'avenir','helvetica',sans-serif;font-size:25px; color: #fff;margin:0; padding:0;">Style Revolver</h1>
              </td>
            </tr>
					<tr>
						<td class="wrapper" style="font-family:'avenir','helvetica',sans-serif;font-size:14px;vertical-align:top;box-sizing:border-box;padding:20px;">
						<table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
							<tbody>
								<tr>
									<td style="font-family:'avenir','helvetica',sans-serif;font-size:14px;vertical-align:top;">
									 <p style="font-family:'avenir','helvetica',sans-serif;font-size:18px;font-weight:bold;margin:0;Margin-bottom:15px;">Your order sucessfully placed</p>
                   <p style="font-family:'avenir','helvetica',sans-serif;font-size:18px;font-weight:bold;margin:0;Margin-bottom:15px;">We hope you like our services...</p>
                   <p style="font-family:'avenir','helvetica',sans-serif;font-size:18px;font-weight:bold;margin:0;Margin-bottom:15px;">Thank You..!</p>
                </td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<!-- END MAIN CONTENT AREA -->
				</tbody>
			</table>
			<!-- START FOOTER -->

			<div class="footer" style="clear:both;padding-top:10px;text-align:center;width:100%;">
			<table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
				<tbody>
					<tr>
						<td class="content-block powered-by" style="font-family:'avenir','helvetica',sans-serif;font-size:14px;vertical-align:top;color:#999999;text-align:center;">Powered by Style Revolver.</td>
					</tr>
				</tbody>
			</table>
			</div>
			<!-- END FOOTER --><!-- END CENTERED WHITE CONTAINER --></div>
			</td>
			<td style="font-family:'avenir','helvetica',sans-serif;font-size:14px;vertical-align:top;"></td>
		</tr>
	</tbody>
</table>
</body>
</html>
