@extends('welcome')
@section('content')
<div class="banner-top">
	<div class="container">
		<h2 class="" data-wow-delay=".5s">My Offer</h2>
		<h3 class="" data-wow-delay=".5s"><a href="{{ url('/home') }}">Home</a><label>/</label>Offers</h3>
		<div class="clearfix"> </div>
	</div>
</div>
<div class="offer">
	<div class="container">
		<div class="row">
			<div class="col-12">
				@php 
					$i=0;
				@endphp
				@foreach($offer as $data)
						<div class="col-md-3 col-sm-3 custom_offer">
							<h4 class="">Coupon Code : <span class="couponcode{{$i}}">{{$data->couponcode}}</span><span class="copy_clipboard" data-id="{{$i}}"><i class="fa fa-copy"></i></span></h4>
							<h4>Offer Persentage   : {{$data->offer_percentage}}%</h4>
							<div class="Validity">
								<h4>Product</h4>
								<div class="validate">
									<span class="">Name : {{$data->product->product_name}}</span>
								</div>
							</div>
							<div class="Validity">
								<h4>Validity</h4>
								<div class="validate">
									<span class="left_val">Start Date : </span><span class="right_val">{{date('d-m-Y', strtotime($data->start_date))}}</span>
								</div>
								<div class="validate">
									<span class="left_val"> End Date : </span><span class="right_val">{{date('d-m-Y', strtotime($data->end_date))}}</span>
								</div>
							</div>
						</div>
						@php
							$i++;
						@endphp
				@endforeach
				<div class="clearfix"></div>
			</div>
		</div>
				<div class="pagination">
					{!! $offer->links() !!}
				</div>
	</div>			
</div>
@endsection

@section('pageJs')
	<script type="text/javascript">
		jQuery(document).on('click','.copy_clipboard',function(){
			var id=$(this).data('id');
			var c_id=".couponcode"+id;
			var code=$(c_id).html();
			var $temp = $("<input>");
			$("body").append($temp);
			$temp.val($(c_id).html()).select();
			document.execCommand("copy");
			$temp.remove();
			toastr.success('Coupon Code Successfully copied..! ');
		})
	</script>
@endsection
@section('PageCSS')
<style type="text/css">
	.offer {
	    padding-top: 25px;
	    padding-bottom: 25px;
	}
	.custom_offer {
	    float: left;
	    background-color: #FF7000;
	    color: white;
	    margin: 10px;
	    padding: 10px;
	    width: 22%;
	}

	.Validity {
	    width: 100%;
	    display: block;
	    margin-top: 4px;
	}

	.right_val {
	    width: 49%;
	    float: right;
	}
	.Validity h4 {
	    padding: 5px 0px 5px 0px;
	    border-top: 1px solid;
	    border-bottom: 1px solid;
	    text-align: center;
	}
	.copy_clipboard {
	    float: right;
	    font-size: 30px;
	    color: black;
	}
</style>
@endsection