<div class="top-comment-left">
	<img class="img-responsive" src="{{asset('/images/'.$feedback_render->profile_pic)}}" alt="">
</div>
<div class="top-comment-right">
	<h6><a href="#">{{$feedback_render->first_name}}</a> <span style="text-align: right;">{{date('d-m-Y', strtotime($feedback_render->feedback_date))}}</span></h6>
	<p>{{$feedback_render->feedback_description}}</p>
</div>