@extends('welcome')
@section('content')
<!--banner-->
<!--banner-->
@php
	$product_id=$id;
@endphp
<div class="banner-top">
	<div class="container">
		<h2 class="" data-wow-delay=".5s">Product Details</h2>
		<h3 class="" data-wow-delay=".5s"><a href="{{ url('/home') }}">Home</a><label>/</label>Product Details</h3>
		<div class="clearfix"> </div>
	</div>
</div>
	<!--content-->

	<div class="product">
		<div class="container">
			<div class="col-md-12" data-wow-delay=".5s">
				<div class="col-md-5 grid-im">		
					<div class="flexslider">
			  			<ul class="slides">
			  				<li data-thumb="{{asset('images/'.$pro->product_image)}}" onerror="this.src='{{asset("download.png")}}';">
						        <div class="thumb-image"> <img src="{{asset('images/'.$pro->product_image)}}" onerror="this.src='{{asset("download.png")}}';" data-imagezoom="true" class="img-responsive"> </div>
						    </li>
						   	@if(count($images)>1) 
						   		@foreach($images as $key=> $image)
								    <li data-thumb="{{asset('images/'.$image->image_name)}}" onerror="this.src='{{asset("download.png")}}';">
								        <div class="thumb-image"> <img src="{{asset('images/'.$image->image_name)}}" onerror="this.src='{{asset("download.png")}}';" data-imagezoom="true" class="img-responsive"> </div>
								    </li>
							    @endforeach
						   	@endif
						</ul>
					</div>
				</div>	
				<div class="col-md-7 single-top-in">
					<div class="span_2_of_a1 simpleCart_shelfItem">
						<h3>{{$pro->product_name}}</h3>
			    		<div class="price_single">
					  		<span class="reducedfrom item_price">&#8377 {{$pro->product_price}}</span>
					 		
					 		<div class="clearfix"></div>
						</div>
					@auth 
						<div class="rating_msg" style="padding: 5px;margin-bottom: 5px;display: none">
							<span style="color:red;" class="rating_msg">Rating Successfully</span>
						</div>
					  	<div>
					  	@php
						  	if($boolean){
						  		$id="stars";
						  	}else{
						  		$id="";
						  	}
						@endphp
					  	@if(empty($rating))
					  		<section class='rating-widget'>
								<div class='rating-stars text-center'>
								    <ul id='{{$id}}'>
								      <li class='star' title='Poor' data-value='1'>
								        <i class='fa fa-star fa-fw'></i>
								      </li>
								      <li class='star' title='Fair' data-value='2'>
								        <i class='fa fa-star fa-fw'></i>
								      </li>
								      <li class='star' title='Good' data-value='3'>
								        <i class='fa fa-star fa-fw'></i>
								      </li>
								      <li class='star' title='Excellent' data-value='4'>
								        <i class='fa fa-star fa-fw'></i>
								      </li>
								      <li class='star' title='WOW!!!' data-value='5'>
								        <i class='fa fa-star fa-fw'></i>
								      </li>
								    </ul>
								  </div>
							</section>
						@else
							@php
								if(isset($rating_user->ratings_average)){
									$ra=$rating_user->ratings_average;
									$ra=round($ra);
								}else{
									$ra=round($rating->ratings_average);
								}
							@endphp
							<section class='rating-widget'>
								  <div class='rating-stars text-center'>
								    <ul id='{{$id}}'>
								    	<li class="star {{ $ra >= 1 ? 'selected' : '' }}" title='Poor' data-value='1' >
									        <i class='fa fa-star fa-fw'></i>
								      </li>
								      <li class="star {{ $ra >= 2 ? 'selected' : '' }}" title='Fair' data-value='2' >
								        <i class='fa fa-star fa-fw'></i>
								      </li>
								      <li class="star {{ $ra >= 3 ? 'selected' : '' }}" title='Good' data-value='3'>
								        <i class='fa fa-star fa-fw'></i>
								      </li>
								      <li class="star  {{ $ra >= 4 ? 'selected' : '' }}" title='Excellent' data-value='4'>
								        <i class='fa fa-star fa-fw'></i>
								      </li>
								      <li class="star  {{ $ra >= 5 ? 'selected' : '' }}" title='WOW!!!' data-value='5'>
								        <i class='fa fa-star fa-fw'></i>
								      </li>
								    </ul>
								  </div>
							</section>
						@endif
						</div>
					@else
						@php
							if(isset($rating->ratings_average)){
								$ra=round($rating->ratings_average);
							}else{
								$ra=0;
							}
						@endphp
						<section class='rating-widget'>
							<div class='rating-stars text-center'>
							    <ul id=''>
							    	<li class="star {{ $ra >= 1 ? 'selected' : '' }}" title='Poor' data-value='1' >
								        <i class='fa fa-star fa-fw'></i>
							      </li>
							      <li class="star {{ $ra >= 2 ? 'selected' : '' }}" title='Fair' data-value='2' >
							        <i class='fa fa-star fa-fw'></i>
							      </li>
							      <li class="star {{ $ra >= 3 ? 'selected' : '' }}" title='Good' data-value='3'>
							        <i class='fa fa-star fa-fw'></i>
							      </li>
							      <li class="star  {{ $ra >= 4 ? 'selected' : '' }}" title='Excellent' data-value='4'>
							        <i class='fa fa-star fa-fw'></i>
							      </li>
							      <li class="star  {{ $ra >= 5 ? 'selected' : '' }}" title='WOW!!!' data-value='5'>
							        <i class='fa fa-star fa-fw'></i>
							      </li>
							    </ul>
							</div>
						</section>
					@endauth
						@if(!empty($rating->ratings_average))
							<p> AVERAGE RATING : <span class="avg_rating">{{round($rating->ratings_average,2,PHP_ROUND_HALF_ODD)}}</span></p>
						@else
							<p> AVERAGE RATING : <span class="avg_rating">0</span></p>
						@endif
						<div class="clearfix"> </div>
					</div>
		   			<div class="sap_tabs">	
					    <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
							<ul class="resp-tabs-list">
								  <li class="resp-tab-item" aria-controls="tab_item-1" role="tab"><span>Additional Information</span></li>
								  <li class="resp-tab-item" aria-controls="tab_item-2" role="tab"><span>Reviews</span></li>
								  <div class="clearfix"></div>
							</ul>				  	 
							<div class="resp-tabs-container">
								<h2 class="resp-accordion" role="tab" aria-controls="tab_item-1"><span class="resp-arrow"></span>Additional Information</h2>
								<div class="tab-1 resp-tab-content" aria-labelledby="tab_item-1">
									<div class="facts1">
										<div class="color">
											<table>
												<tr>
													<td><b>Description</b></td>
													<td><span></span></td>
													<td><span>{{$pro->description}}</span></td>
												</tr>
											</table>		
													<div class="clearfix"></div>
											
										</div>
										<div class="color">
											<p><b>Size</b></p>
											<span >{{$pro->size_name}}</span>
											<div class="clearfix"></div>
										</div>
										<div class="color">
											<p><b>Color</b></p>
											@foreach($color_data as $key=> $cl)
												<input type="radio" name="color" id="color{{$key}}" tabindex="8" value="{{$cl['id']}}"><label style="background-color:{{$cl['name']}};height: 25px;width: 25px;margin-left:10px;"></label>
                                           	@endforeach   
											<div class="clearfix"></div>
										</div>
										<span class="error" style="color:red;font-size: 25px;display: none">Please select color</span><br/>
							        	@auth
								 			<a href="javascript:void(0)" data-id="{{ $pro->id}}" data-price="{{$pro->product_price}}" data-text="Add To Cart" class="but-hover1 item_add addcart">Add To Cart</a>
											<a href="javascript:void(0)" data-id="{{ $pro->id}}" data-price="{{$pro->product_price}}" data-text="Add To Wishlist" class="but-hover1" id="addwishlist" style="margin-right: 8px" disabled>Add To Wishlist</a>
										@else
											<a href="{{ url('/login')}}" data-id="{{ $pro->id}}" data-price="{{$pro->product_price}}" data-text="Add To Cart" class="but-hover1 item_add" >Add To Cart</a>
											<a href="{{ url('/login')}}" data-id="{{ $pro->id}}" data-price="{{$pro->product_price}}" data-text="Add To Wishlist" class="but-hover1 item_add wish" >Add To Wishlist</a>
										@endauth
						        	</div>
								</div>
									<h2 class="resp-accordion" role="tab" aria-controls="tab_item-2"><span class="resp-arrow"></span>Reviews</h2>
									<div class="tab-1 resp-tab-content" aria-labelledby="tab_item-2">
									<div class="comments-top-top">
									@foreach($feedback as $key=> $data)					
										@if($key <= 1)
											<div class="top-comment-left">
												<img class="img-responsive feedback_img" src="{{asset('/images/'.$data->profile_pic)}}" alt="" height="110">
											</div>
											<div class="top-comment-right">
												<h6><a href="#">{{$data->first_name}}</a> <span style="text-align: right;">{{date('d-m-Y', strtotime($data->feedback_date))}}</span></h6>
												<p>{{$data->feedback_description}}</p>
												<div><br></div>
											</div>
										@endif	
									@endforeach
									<div class="feedback_append">
									</div>
									</div>
										@auth
											@php
												$style="display:block";
												foreach($feedback as $data){
													if($data->user_id == \Auth::user()->id){
														$style="display:none";
													}
												}
												if($boolean){

												}else{
													$style="display:none";
												}
											@endphp
												<div class="col-md-12">
													<div class="col-md-6">
														<a href="#" data-toggle="modal" data-target="#exampleModal" style="{{$style}}" class="feedback_btn btn btn-primary">Add Reviews</a>
													</div>
										@endauth
													@if(count($feedback)>2)
														<div class="col-md-6">
															<a href="{{url('/moreFeedBack/'.$product_id)}}" class="btn btn-primary" style="display: inline-block;">More Feedback</a>
														</div>
													@endif
												</div>

									</div>
					  		</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="clearfix"> 
				<center><h1><p style="color:orange">Related Products</p></h1></center>

				</div>
				<div class=" col-md-si" style="margin-top:10px;margin-bottom:10px;">

					@foreach ($product as $product_data)
						<div class="col-md-4 col-sm-4 item-grid simpleCart_shelfItem custom_pro">
							<div class="grid-pro">
								<div  class=" grid-product " >
									<figure>		
										@php
					  						$images=explode("|",$product_data['product_image']);
					  					@endphp
										<a href="{{ url('/product/'.$product_data['id'])}}">
											<div class="grid-img">
												<img  src="{{asset('/images/'.$images[0])}}" class="" height="175" width="140" alt="">
											</div>
											<div class="grid-img">
												<img  src="{{asset('/images/'.$images[0])}}" class="like{{$product_data['id']}}" height="175" width="140" alt="">
											</div>			
										</a>		
									</figure>	
								</div>
								<div class="{{$product_data['subcategory_name']}}">
									<h3><a href="{{ url('/product/'.$product_data['id'])}}">{{$product_data['product_name']}}</a></h3>
									<p ><b>&#8377 </b><em class="item_price">{{$product_data['product_price']}}</em></p>
									@auth
										<a href="{{ url('/product/'.$product_data['id'])}}" data-id="{{ $product_data['id']}}" data-price="{{$product_data['product_price']}}" data-text="Add To Cart" class="but-hover1" >Add To Cart</a>
									@else
										<a href="{{ url('/login')}}" data-id="{{ $product_data['id']}}" data-price="{{$product_data['product_price']}}" data-text="Add To Cart" class="but-hover1 item_add" >Add To Cart</a>
									@endauth

								</div>
							</div>
						</div>
					@endforeach
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>


  
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
	    <div class="modal-content">
		    <div class="modal-header">
	    	  <h2 class="modal-title" id="exampleModalLabel">Add Feedback</h2>
		    </div>
	      	<div class="modal-body">
	      	<div class="row">
	    		<div class="col-12">
		    		<div class="col-md-3">
		    			<label for="review">Feedback</label>
		    		</div>
		    		<div class="col-md-9">
		    			<textarea name="review" id="review" placeholder="Enter the feedback" class="form-control"></textarea>
		    			<span style="color: red;display: none" class="feederror"></span>
		    			@foreach($pro as $data)
			    			<input type="hidden" name="product_id" id="product_id" value="{{$pro->id}}">
			    		@endforeach
			    	</div>
		    	</div>
	      	</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary reviewsubmit">Save changes</button>
	      </div>
    	</div>
  	</div>
</div>



@endsection
@section('pageJs')
<!-- //footer -->
<script src="{{asset('js/imagezoom.js')}}"></script>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script defer src="{{asset('js/jquery.flexslider.js')}}"></script>
<link rel="stylesheet" href="{{asset('css/flexslider.css')}}" type="text/css" media="screen" />

<script src="{{asset('js/easyResponsiveTabs.js')}}" type="text/javascript"></script>
<script>
$(document).ready(function () {
    $('#horizontalTab').easyResponsiveTabs({
        type: 'default', //Types: default, vertical, accordion           
        width: 'auto', //auto or any width like 600px
        fit: true   // 100% fit in a container
    });
});
			   
// Can also be used with $(document).ready()
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    controlNav: "thumbnails"
  });
});

jQuery(document).on('click','.addWishlist',function(){
	jQuery("#find_ajax_loading").show();
	var product_id=jQuery(this).data('id');
	var product_price=jQuery(this).data('price');
	$.ajax({
		headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            },
		type:'POST',
		url:'{{url("/wishlistadd")}}',
		data:{
			product_id:product_id,
			price:product_price
		},
		success:function(data){
			jQuery("#find_ajax_loading").hide();
			if (data != "error"){
				jQuery('.like'+data.trim()).css({'color': "#f44336"});
				jQuery('.like'+data.trim()).parent().removeClass('addWishlist')
				toastr.success('Wishlist add successfully');
			}else{
				 console.log('error');
			}
		}
	});
});
		
jQuery(document).on('click','#addwishlist',function(){
	if ($("input[name='color']").is(':checked')) {
		jQuery("#find_ajax_loading").show();
		var product_id=jQuery(this).data('id');
		var product_price=jQuery(this).data('price');
		var color_id=$("input[name='color']:checked"). val();
		$.ajax({
			headers: {
	            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
	            },
			type:'POST',
			url:'{{url("/wishlistadd")}}',
			data:{
				product_id:product_id,
				price:product_price,
				color_id:color_id,
			},
			success:function(data){
				$(".error").hide();
				if (data == "error"){
					console.log('error');
				}
				toastr.success('Wishlist add successfully');
				jQuery("#find_ajax_loading").hide();
			}
		});
	}else{
		$(".error").show();
	}
});

jQuery(document).on('click','.addcart',function(){
	if ($("input[name='color']").is(':checked')) {
		var color_id=$("input[name='color']:checked"). val();
		jQuery("#find_ajax_loading").show();
		var product_id=jQuery(this).data('id');
		var product_price=jQuery(this).data('price');
		$.ajax({
			headers:{
				'X-CSRF-TOKEN':jQuery('meta[name="csrf-token"]').attr('content')
			},
			url:'{{ url("/addcart")}}',
			type:'POST',
			data:{
				product_id:product_id,
				price:product_price,
				color_id:color_id,
			},
			success:function(data){
				jQuery("#find_ajax_loading").hide();
				$(".error").hide();
				if(data != "error")
				{
					toastr.success('Cart add successfully');
					console.log('done');
				}else{
					 console.log('error');
				}
			}
		});
	}else{
		$(".error").show();
	}
})

jQuery(document).on('click','.reviewsubmit',function(){
	if($("textarea#review").val() != ""){
		jQuery(".feederror").hide();
		jQuery(".feederror").html('');
		var desc = $("textarea#review").val();
		var productid = jQuery("#product_id").val();
		jQuery("#find_ajax_loading").css('z-index',1250)
		jQuery("#find_ajax_loading").show();
		$.ajax({
		   type:'get',
		   url:'{{url("/feedbackadd/")}}',
		   data:{
		   		desc:desc,
		   		product_id:productid
		   	},
		   success:function(data){
		      if(data == "error"){
		      	$("textarea#review").val('');
		      	jQuery("textarea#review").css("border-color","red");
				jQuery(".feederror").html('');
		      	//jQuery('#exampleModal').modal('');
		      }else{
		      	jQuery(".feedback_append").html(data);
		      	toastr.success('Feedback add successfully');
		      	jQuery(".feedback_btn").hide();
		      	jQuery('#exampleModal').modal('hide');
		      }
		      jQuery("#find_ajax_loading").hide();
		   }
		 });
	}else{
		jQuery("textarea#review").css("border-color","red");
		jQuery(".feederror").show();
	}
})

//rating js start
$(document).ready(function(){
    $('#stars li').on('mouseover', function(){
	    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
	   
	    // Now highlight all the stars that's not after the current hovered star
	    $(this).parent().children('li.star').each(function(e){
	      if (e < onStar) {
	        $(this).addClass('hover');
	      }
	      else {
	        $(this).removeClass('hover');
	      }
	    });
    }).on('mouseout', function(){
	    $(this).parent().children('li.star').each(function(e){
	      $(this).removeClass('hover');
	    });
  	});
  
  
  	/* 2. Action to perform on click */
  	$('#stars li').on('click', function(){
	    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
	    var stars = $(this).parent().children('li.star');
	    
	    for (i = 0; i < stars.length; i++) {
	      $(stars[i]).removeClass('selected');
	    }
	    
	    for (i = 0; i < onStar; i++) {
	      $(stars[i]).addClass('selected');
	    }
	   	var rating =onStar;
		var productid = jQuery("#product_id").val();
		jQuery("#find_ajax_loading").show();
		$.ajax({
			headers: {
	            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            },
			type:'POST',
			url:'{{url("/ratingadd")}}',
		    data:{
		   		rating_controller_variable:rating,
		   		product_id_controller_variable:productid
		   	},
		    success:function(data){
		    	jQuery("#find_ajax_loading").hide();
		    	var data1=JSON.parse(data);
		    	console.log(data1.rating_user);
		    	console.log(data1.rating_group);
		       	if(data1 != "error"){
		     		for (i = 0; i < stars.length; i++) {
			    		$(stars[i]).removeClass('selected');
			    	}
			    	for (i = 0; i < data1.rating_user; i++) {
			      		$(stars[i]).addClass('selected');
			    	}
  					$('.avg_rating').html(data1.rating_group);
				    var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
				    var msg = "";
				    toastr.success('Rating add successfully');
				    $('.rating_msg').show().delay( 5000 ).hide( 0 );
  				}
		   }
		});
  	});
});
//rating js end

function responseMessage(msg) {
  $('.success-box').fadeIn(200);  
  $('.success-box div.text-message').html("<span>" + msg + "</span>");
}
</script>
@endsection
@section('PageCSS')
<style type="text/css">
	/*Rating css start*/
a {
  color: tomato;
  text-decoration: none;
}

a:hover {
  color: #2196f3;
}

/* Rating Star Widgets Style */
.rating-stars ul {
  list-style-type:none;
  padding:0;
  
  -moz-user-select:none;
  -webkit-user-select:none;
}
.rating-stars ul > li.star {
  display:inline-block;
}

/* Idle State of the stars */
.rating-stars ul > li.star > i.fa {
  font-size:2.5em; /* Change the size of the stars */
  color:#ccc; /* Color on idle state */
}

/* Hover state of the stars */
.rating-stars ul > li.star.hover > i.fa {
  color:#FFCC36;
}

/* Selected state of the stars */
.rating-stars ul > li.star.selected > i.fa {
  color:#FF912C;
}
.rating-widget ul{
	text-align: left;
}
	/*Rating css end*/
a.wish {
    margin-right: 10px;
}
.top-comment-left {
    margin-bottom: 10px;
}

.top-comment-right {margin-bottom: 10px;}

.custom_pro{
	margin-bottom:15px; 
}
img.img-responsive.feedback_img {
    height: 110px;
}
</style>
@endsection