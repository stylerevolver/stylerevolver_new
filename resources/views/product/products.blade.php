@extends('welcome')
@section('content')
<div class="banner-top">
	<div class="container">
		<h2 class="" data-wow-delay=".5s">Products</h2>
		<h3 class="" data-wow-delay=".5s"><a href="{{ url('/home') }}">Home</a><label>/</label>Products<label>/{{$subcategory}}</label></h3>
		<div class="clearfix"> </div>

	</div>
</div>
<div class="product">
	<div class="container">
		<div class="col-md-12">
			<div class="mid-popular">
				@foreach ($product as $product_data)
					<div class="col-md-3 col-sm-3 item-grid simpleCart_shelfItem custom_pro">
						<div class="grid-pro">
							<div  class=" grid-product " >
								@php
			  						$images=explode("|",$product_data['product_image']);
			  					@endphp
								<figure>		
									<a href="{{ url('/product/'.$product_data['id'])}}">
										<div class="grid-img">
											<img  src="{{asset('/images/'.$images[0])}}" class="img-responsive" height="175" width="140" alt="">
										</div>
										<div class="grid-img">
											<img  src="{{asset('/images/'.$images[0])}}" class="img-responsive like{{$product_data['id']}}" height="175" width="140" alt="">
										</div>			
									</a>		
								</figure>	
							</div>
							<div class="{{$product_data['subcategory_name']}}">
								<h3><a href="{{ url('/product/'.$product_data['id'])}}">{{$product_data['product_name']}}</a></h3>
								<p ><b>&#8377 </b><em class="item_price">{{$product_data['product_price']}}</em></p>
								@auth
									<a href="{{ url('/product/'.$product_data['id'])}}" data-id="{{ $product_data['id']}}" data-price="{{$product_data['product_price']}}" data-text="Add To Cart" class="but-hover1 item_add" >Add To Cart</a>
								@else
									<a href="{{ url('/product/'.$product_data['id'])}}" data-id="{{ $product_data['id']}}" data-price="{{$product_data['product_price']}}" data-text="Add To Cart" class="but-hover1 item_add" >Add To Cart</a>
								@endauth
							</div>
						</div>
					</div>
				@endforeach
					<div class="clearfix"></div>
			</div>
			{!! $product->links() !!}
		</div>

		<div class="pagination">
		</div>
	</div>			
</div>
@endsection
@section('PageCSS')
	<style type="text/css">
		.custom_pro {
	    	margin-bottom: 15px;
		}
	</style>
@endsection
@section('pageJs')
	<script type="text/javascript">
		jQuery(document).on('click','.addWishlist',function(){
			jQuery("#find_ajax_loading").show();
			var product_id=jQuery(this).data('id');
			var product_price=jQuery(this).data('price');
			$.ajax({
				headers: {
		            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
		            },
				type:'POST',
				url:'{{url("/wishlistadd")}}',
				data:{
					product_id:product_id,
					price:product_price
				},
				success:function(data){
					jQuery("#find_ajax_loading").hide();
					if (data != "error"){
						jQuery('.like'+data.trim()).css('color',"#f44336");
						jQuery('.like'+data.trim()).parent().removeClass('addWishlist')
					}else{
						 console.log('error');
					}
				}
			});
		});

		jQuery(document).on('click','.addcart',function(){
			jQuery("#find_ajax_loading").show();
			var product_id=jQuery(this).data('id');
			var product_price=jQuery(this).data('price');
			$.ajax({
				headers:{
					'X-CSRF-TOKEN':jQuery('meta[name="csrf-token"]').attr('content')
				},
				url:'{{ url("/addcart")}}',
				type:'POST',
				data:{
					product_id:product_id,
					price:product_price
				},
				success:function(data){
					jQuery("#find_ajax_loading").hide();
					if(data != "error")
					{
						console.log('done');
					}else{
						 console.log('error');
					}
				}
			});
		})
	</script>
@endsection