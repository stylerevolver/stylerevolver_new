@php
	$admin=adminData();
@endphp
<div class="footer">
	<div class="container">
		<div class="footer-top">
			<div class="col-md-9 footer-top1">
				<h4>The Motive Of Style Revolver is....</h4>
				<p>To make comfort for the customer and give the best perfomance to them choice..</p>
			</div>
			<div class="col-md-3 footer-top2">
				<a href="{{url('/contact')}}">Contact Us</a>
			</div>
			<div class="clearfix"> </div>
		</div>
		<div class="footer-grids">
			<div class="col-md-4 footer-grid" data-wow-delay=".5s">
				<h3>About Us</h3>
				<p>Style Revolver will be a web application from where customer can purchase various designer clothes.
				<span> customer can also give  customized order by taking appointment.</span></p>
			</div>
			<div class="col-md-4 footer-grid" data-wow-delay=".6s">
				<h3>Contact Info</h3>
				<ul>
					<li><i class="glyphicon glyphicon-map-marker" ></i>{{$admin[0]['company_address']}}</li>
					<li class="foot-mid"><i class="glyphicon glyphicon-envelope" ></i><a href="contactto:{{$admin[0]['company_email']}}">{{$admin[0]['company_email']}}</a></li>
					<li><i class="glyphicon glyphicon-earphone" ></i>+91{{$admin[0]['company_contact']}}</li>
				</ul>
			</div>
		<!-- 	<div class="col-md-4 footer-grid" data-wow-delay=".7s">
				<h3>Sign up for newsletter </h3>
				<form>
					<input type="text" placeholder="Email"  required="">
					<input type="submit" value="Submit">
				</form>
			</div> -->
			<div class="clearfix"> </div>
		</div>
		<div class="copy-right animated wow fadeInUp" data-wow-delay=".5s">
			<p>&copy {{date('Y')}} All rights reserved | Design by <a href="#">Stylerevoler</a></p>
		</div>
	</div>
</div>