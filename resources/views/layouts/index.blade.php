@section('PageCSS')
<link href="{{asset('/css/owl.carousel.css')}}" rel="stylesheet">
<style type="text/css">
    .custom .item img{
        height: 395px !important;
    }
</style>
@endsection
@extends('welcome')
@section('content')
<div class="flex-center position-ref full-height">
    <!-- banner -->
    @if(session('message'))
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>{{ session('message') }}
        </div>
    @endif
    <div class="banner">
        <div class="banner-right">
            <ul id="flexiselDemo2"> 
                @foreach($product as $product_data)
                    <li>
                        <div class="banner-grid">
                            <h2>Featured Products</h2>
                            <div class="wome">
                                <a href="{{ url('/product/'.$product_data['id'])}}" ><img class="img-responsive" src="{{asset('/images/'.$product_data['product_image'])}}" alt="" height="315" width="235" /></a>
                                <div class="women simpleCart_shelfItem">
                                    {{-- <a href="{{ url('/product/'.$product_data['id'])}}"><img src="{{asset('/images/'.$images[0])}}" alt=""></a> --}}
                                    <h6 ><a href="{{ url('/product/'.$product_data['id'])}}">{{ $product_data['product_name']}}</a></h6>
                                    <p class="ba-price">{{-- <del>₹100.00</del> --}}<em class="item_price">₹{{ $product_data['product_price']}}</em></p>
                                    <a href="{{ url('/product/'.$product_data['id'])}}" data-text="Add To Cart" class="but-hover1 item_add">Add To Cart</a>
                                </div>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
<!-- //banner -->
<!--content-->
<div class="content">
    <div class="content-head">
        <div class="col-md-6 col-m1 animated wow fadeInLeft" data-wow-delay=".1s">
            <div class="col-1">
                <div class="col-md-6 col-2">
                    <a href="{{ url('/category/'.$subcategory[0]['id'])}}"><img src="{{asset('/images/1576778326.jpeg')}}" class="img-responsive" alt=""></a>
                </div>
                <div class="col-md-6 col-p">
                    <h5>For All Collections</h5>
                    <p>Everything we make with our hands...<br>Everything has a nice touch of home... <br>Hope you like these things you need...</p>
                    
                    <a href="{{ url('/category/'.$subcategory[0]['id'])}}" class="shop" data-hover="Shop Now">Shop Now</a>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="col-1">
                <div class="col-md-6 col-p">
                    <h5>For All Collections</h5>
                    <p>Everything we make with our hands...<br>Everything has a nice touch of home... <br>Hope you like these things you need...</p>
                    
                    <a href="{{ url('/category/'.$subcategory[1]['id'])}}" class="shop" data-hover="Shop Now">Shop Now</a>
                </div>
                <div class="col-md-6 col-2">
                    <a href="{{ url('/category/'.$subcategory[1]['id'])}}"><img src="{{asset('/images/pi.jpg')}}" class="img-responsive" alt=""></a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-6 col-m2 animated wow fadeInRight" data-wow-delay=".1s">
           <div id="owl-demo" class="owl-carousel custom">
            @foreach($new_product as $product)
                <div class="item">             
                @php
                    $images=explode("|",$product['product_image']);
                @endphp                          
                    <a href="{{ url('/product/'.$product['id'])}}"><img class="img-responsive" src="{{asset('/images/'.$images[0])}}" alt=""  width="250" height="395" /></a>  
                    <a href="{{ url('/product/'.$product['id'])}}" class="shop-2" >Shop Now</a>
                </div>
            @endforeach
            </div>
        </div>
        <div class="clearfix"></div>
    </div>  
</div>
<!---->
<div class="content-top">
    <div class="card col-12 custom_cat_show">
       <h5 class="card-body">{{ $subcategory[0]['subcategory_name'] }}</h5>
    </div>
    <div class="col-md-12 animated wow animated wow fadeInRight" data-wow-delay=".1s">
        @foreach($product_category as $product_cat)
            @php
                $images_cat=explode("|",$product_cat['product_image']);
            @endphp
            <div class="col-sm-3 item-grid simpleCart_shelfItem">
                <div class="grid-pro">
                    <div  class=" grid-product " >
                        <figure>        
                            <a href="{{ url('/product/'.$product_cat['id'])}}">
                                <div class="grid-img">
                                    <img  src="{{asset('/images/'.$images_cat[0])}}" class="img-responsive" alt="">
                                </div>
                                <div class="grid-img">
                                    <img  src="{{asset('/images/'.$images_cat[0])}}" class="img-responsive"  alt="">
                                </div>          
                            </a>        
                        </figure>   
                    </div>
                    <div class="women">
                        <h6><a href="{{ url('/product/'.$product_cat['id'])}}">{{ $product_cat['product_name']}}</a></h6>
                        <p >{{-- <del>₹100.00</del> --}}<em class="item_price">₹{{ $product_cat['product_price']}}</em></p>
                        <a href="{{ url('/product/'.$product_cat['id'])}}" data-text="Add To Cart" class="but-hover1 item_add">Add To Cart</a>
                    </div>
                </div>
            </div>
        @endforeach
        
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>
    <!----->
<div class="content-top">
    <div class="card col-12 custom_cat_show">
       <h5 class="card-body">{{ $subcategory[1]['subcategory_name'] }}</h5>
    </div>
    <div class="col-md-12 animated wow fadeInLeft" data-wow-delay=".1s">
        @foreach($product_category1 as $product_cat)
            @php
                $images_cat=explode("|",$product_cat['product_image']);
            @endphp
            <div class="col-sm-3 item-grid simpleCart_shelfItem">
                <div class="grid-pro">
                    <div  class=" grid-product " >
                        <figure>        
                            <a href="{{ url('/product/'.$product_cat['id'])}}">
                                <div class="grid-img">
                                    <img  src="{{asset('/images/'.$images_cat[0])}}" class="img-responsive" alt="">
                                </div>
                                <div class="grid-img">
                                    <img  src="{{asset('/images/'.$images_cat[0])}}" class="img-responsive"  alt="">
                                </div>          
                            </a>        
                        </figure>   
                    </div>
                    <div class="women">
                        <h6><a href="{{ url('/product/'.$product_cat['id'])}}">{{ $product_cat['product_name']}}</a></h6>
                        <p >{{-- <del>₹100.00</del> --}}<em class="item_price">₹{{ $product_cat['product_price']}}</em></p>
                        <a href="{{ url('/product/'.$product_cat['id'])}}" data-text="Add To Cart" class="but-hover1 item_add">Add To Cart</a>
                    </div>
                </div>
            </div>
        @endforeach
        
        <div class="clearfix"></div>
    </div>
   
    <div class="clearfix"></div>
</div>

@endsection
@section('pageJs')
    <script type="text/javascript" src="{{asset('js/jquery.flexisel.js')}}"></script>
    <script src="{{asset('js/owl.carousel.js')}}"></script>
    <script type="text/javascript">
        $(window).load(function() {
            $("#flexiselDemo2").flexisel({
                visibleItems: 1,
                animationSpeed: 1000,
                autoPlay: true,
                autoPlaySpeed: 5000,            
                pauseOnHover: true,
                enableResponsiveBreakpoints: true,
                responsiveBreakpoints: { 
                    portrait: { 
                        changePoint:480,
                        visibleItems: 1
                    }, 
                    landscape: { 
                        changePoint:640,
                        visibleItems: 1
                    },
                    tablet: { 
                        changePoint:768,
                        visibleItems: 1
                    }
                }
            });
        });
        $(document).ready(function() {
            $("#owl-demo").owlCarousel({
                items : 2,
                lazyLoad : false,
                autoPlay : true,
                navigation : true,
                navigationText :  true,
                pagination : false,
            });
        });
    </script>
@endsection