@php
	$admin=adminData();
	$category=cateData();
	$subcategory=subcatData();
@endphp
<div class="header">
	<div class="header-grid">
		<div class="container">
			<div class="header-left">
				<ul>
					<li><i class="glyphicon glyphicon-envelope" ></i><a href="mailto:{{$admin[0]['company_email'] }}">{{$admin[0]['company_email'] }}</a></li>
					<li><i class="glyphicon glyphicon-earphone" ></i>+91 {{ $admin[0]['company_contact'] }}</li>
				</ul>
			</div>
			<div class="header-right header_cus">
				<div class="header-right1">
					<ul>
						@auth
							<li><i class="glyphicon glyphicon-log-in" ></i>Welcome <u>{{ucfirst(Auth::user()->first_name)}}</u></u></li>
							<li><i class="glyphicon glyphicon-log-in" ></i>
								<a href="javascript:void(0);" id="logout">Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                          	</li>
                        	<li><a href="{{url('/myprofile')}}"><i class="fa fa-cogs" aria-hidden="true" title="Settings"> Settings</i></a></li>
                      	    <li><a href="{{url('/cart')}}"><i class="fa fa-shopping-cart" aria-hidden="true" title="Cart"></i></a></li>
						@else

							<li><i class="glyphicon glyphicon-log-in" ></i><a href="{{ route('login')}}">Login</a></li>
							<li><i class="glyphicon glyphicon-book" ></i><a href="{{route('register')}}">Register</a></li>
						@endauth
					</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<div class="container">
		<div class="logo-nav">
			<nav class="navbar navbar-default">
				<div class="navbar-header nav_2">
					<button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div class="navbar-brand logo-nav-left ">
						<h1 class="animated wow pulse" data-wow-delay=".5s"><a href="{{url('/home')}}">STYLE REVOLVER</a></h1>
					</div>
				</div> 
				<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
					<ul class="nav navbar-nav">
						<li class="active"><a href="{{ url('/home')}}" class="act">Home</a></li>	
						<!-- Mega Menu -->

						@foreach($subcategory as $data)
							@if(!empty($data['subcategory']))
						        <li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ $data['category_name'] }}<b class="caret"></b></a>
									<ul class="dropdown-menu multi">
										<div class="row">
											<div class="col-sm-3">
												<ul class="multi-column-dropdown">
													<h6>{{ $data['category_name'] }}</h6>
													@foreach($data['subcategory'] as $subcategory)
														<li><a href="{{ url('/category/'.$subcategory['id']) }}">{{$subcategory['subcategory_name']}}</a></li>
													@endforeach
												</ul>
											</div>
											<div class="clearfix"></div>
										</div>
										
									</ul>
								</li>
							@endif
						@endforeach
						@auth
							<li><a href="{{url('/appointment')}}">Appointment</a></li>
						@endauth
						<li><a href="{{ url('/about')}}"> About Us</a></li>
						<li><a href="{{ url('/contact')}}">Contact Us</a></li>
						<li><a href="{{ url('/Offer')}}">Offer</a></li>
					</ul>
				</div>
			</nav>
		</div>
	</div>
</div>
<style type="text/css">
	.dropdown-menu.multi{
		min-width: 500px !important;
	}
	.header_cus{
		width: 40%;
	}
</style>
<script type="text/javascript">
	$(document).on('click','#logout',function(e){
		if(confirm("Are you sure you want to logout!")){
			e.preventDefault(); 
			document.getElementById('logout-form').submit();
		}
	})
</script>