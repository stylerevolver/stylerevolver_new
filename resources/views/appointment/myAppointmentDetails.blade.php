@extends('welcome')
@section('content')
	<div class="banner-top">
		<div class="container">
			<h2 class="" data-wow-delay=".5s">My Customized Product Details</h2>
			<h3 class="" data-wow-delay=".5s"><a href="{{ url('/home') }}">Home</a><label>/</label>Cart</h3>
			<div class="clearfix"> </div>
		</div>
	</div>

<div class="check-out">	 
	<div class="container"> 
		<div class="col-md-3">
			@include('myAccountLeftMenu');
		</div>
		<div class="col-md-9">
			<table class="table" data-wow-delay=".5s">
				<tr>
					<th class="t-head">Id</th>
                    <th class="t-head">Customized Order Name</th>
                    <th class="t-head">Customized Product Image</th>
                    <th class="t-head">Descriptioin</th>
                    <th class="t-head">Estimate Price</th>
				</tr>
				<tbody>
					@foreach($customize_product as $key=> $data)
                     	<tr>
                            <td>{{$key+1}}</td>
                            <td>{{$data->customize_product_name}}</td>   
                            <td><img src="{{url('/images/'.$data->customize_product_image)}}" height="50" width="50" /></td>  
                            <td>{{$data->description}}</td>   
                            <td>₹{{$data->price}}</td>      
                        </tr>
                    @endforeach
				</tbody>
			</table>
		</div>
		<div class="col-md-3">
			
		</div>
	</div>
</div>
@endsection
@section('pageJs')
	
@endsection
@section('PageCSS')
<style type="text/css">
	.name h5{
		margin-top: 0px !important;
	}
	.close1{
		top:40px !important;
	}

	.close1{
		top:40px !important;
	}
	th.t-head{
		font-size: 18px !important;
		background-color: #FF7000;
	}
	td.t-data{
		font-size: 18px ;
</style>
@endsection