@extends('welcome')
@section('content')


<div class="banner-top">
	<div class="container">
		<h2 class="" data-wow-delay=".5s">Appointment Details</h2>
		<h3 class="" data-wow-delay=",5s"><a href="{{url('/home/appointment')}}">Home</a><label>/</label>Appointment</h3>
		<div class="clearfix"> </div>
	</div>
</div>


<div class="contact">
	<div class="container">
		<div class="col-md-12" data-wow-delay=".5s">
			<form class="form-group" action="{{url('/appointmentStore')}}" method="post">
				{{csrf_field()}}
				@if($errors->any())
		          	<div class="alert alert-danger">
		            	<ul>
		              		@foreach ($errors->all() as $error)
		                		<li>{{ $error }}</li>
		              		@endforeach
		            	</ul>
		          	</div>
		          	<br>
		       @endif
				<div class="row">
					<div class="col-md-12">
						<h3 class="label_app">Slots Available Click On Prefrable Data To Book The Slot</h3>
					</div>
				</div>
				<div class="clearfix"> </div>
				<div class="row ">
					<div class="col-md-6">
						<h3 class="lable_app">Appointment Date</h3>
      					<div id="datetimepicker12"></div>
      					<input type="hidden" name="time" id="date_appointment" value="{{date('m/d/Y')}}" class="form-control">
      					{{-- @error('time')
	                   		<span class="text-danger" role="alert">
	                        	<strong>{{ $message }}</strong>
	                   		</span>
                 		@enderror --}}
	      			</div>
					<div class="col-md-6 app_time">
			  			<h3 class="lable_app">Appointment Time</h3>
			  			{{-- @error('appointmentTime')
	                   		<span class="text-danger" role="alert" style="font-size: 32px;margin-left: 20px;">
	                        	<strong>{{ $message }}</strong>
	                   		</span>
                 		@enderror --}}
                 		@php
                 		$cuttent_time=date_added();
                 		/*echo strtotime($cuttent_time);echo "<br>";
                 		echo strtotime('20:00:00');*/
                 		@endphp
                 		
						<div class="row form-group appointment_label">
							<div class="col-md-2"></div>
							<div class="col-md-4">
								<label class="slot_availble"></label>
								<span>Slot Availble</span>
							</div>
							<div class="col-md-4">
								<label class="no_slot_availble"></label>
								<span>No Slot Availble</span>
							</div>
							<div class="col-md-2"></div>
						</div>
			  			<div class="col-md-4 form-group app_time_slots">
				  		 	<input type="radio" name="appointmentTime"  id="radio0" class="time_app_radio form-control" style="display:none;" value="09:00:00" @if(in_array('09:00:00',$appointment) || strtotime($cuttent_time) >= strtotime('09:00:00')) disabled @endif>
					  		<label for="radio0">09:00 AM</label>
				  		</div>
			  			<div class="col-md-4 form-group app_time_slots">
				  		 	<input type="radio" name="appointmentTime"  id="radio1" class="time_app_radio form-control" style="display:none;" value="10:00:00" @if(in_array('10:00:00',$appointment)|| strtotime($cuttent_time) >= strtotime('10:00:00')) disabled @endif>
					  		<label for="radio1">10:00 AM</label>
				  		</div>
				  		<div class="col-md-4 form-group app_time_slots">
					 	 	<input type="radio" name="appointmentTime" id="radio2" class="time_app_radio form-control " style="display:none;" value="11:00:00" @if(in_array('11:00:00',$appointment)|| strtotime($cuttent_time) >= strtotime('11:00:00')) disabled @endif> 
					  		<label for="radio2">11:00 AM</label>
				  		</div>
				  		<div class="col-md-4 form-group app_time_slots">
					 	 	<input type="radio" name="appointmentTime" id="radio3" class="time_app_radio form-control " style="display:none;" value="12:00:00" @if(in_array('12:00:00',$appointment)|| strtotime($cuttent_time) >= strtotime('12:00:00')) disabled @endif> 
					  		<label for="radio3">12:00 AM</label>
				  		</div>
				  		<div class="col-md-4 form-group app_time_slots">
					 	 	<input type="radio" name="appointmentTime" id="radio4" class="time_app_radio form-control " style="display:none;" value="13:00:00" @if(in_array('13:00:00',$appointment)|| strtotime($cuttent_time) >= strtotime('13:00:00')) disabled @endif> 
					  		<label for="radio4">01:00 PM</label>
				  		</div>
				  		<div class="col-md-4 form-group app_time_slots">
							<input type="radio" name="appointmentTime" id="radio5" class="time_app_radio form-control " style="display:none;" value="14:00:00" @if(in_array('14:00:00',$appointment)|| strtotime($cuttent_time) >= strtotime('14:00:00')) disabled @endif> 
					  		<label for="radio5">02:00 PM</label>
				  		</div>
				  		<div class="col-md-4 form-group app_time_slots">
						    <input type="radio" name="appointmentTime" id="radio6" class="time_app_radio form-control " style="display:none;" value="15:00:00" @if(in_array('15:00:00',$appointment)|| strtotime($cuttent_time) >= strtotime('15:00:00')) disabled @endif> 
					  		<label for="radio6">03:00 PM</label>
				  		</div>
				  		<div class="col-md-4 form-group app_time_slots">
						    <input type="radio" name="appointmentTime" id="radio7" class="time_app_radio form-control " style="display:none;" value="16:00:00" @if(in_array('16:00:00',$appointment)|| strtotime($cuttent_time) >= strtotime('16:00:00')) disabled @endif> 
					  		<label for="radio7">04:00 PM</label>
				  		</div>
				  		<div class="col-md-4 form-group app_time_slots">
						    <input type="radio" name="appointmentTime" id="radio8" class="time_app_radio form-control " style="display:none;" value="17:00:00" @if(in_array('17:00:00',$appointment)|| strtotime($cuttent_time) >= strtotime('17:00:00')) disabled @endif> 
					  		<label for="radio8">05:00 PM</label>
				  		</div>
				  		<div class="col-md-4 form-group app_time_slots">
						    <input type="radio" name="appointmentTime" id="radio9" class="time_app_radio form-control " style="display:none;" value="18:00:00" @if(in_array('18:00:00',$appointment)|| strtotime($cuttent_time) >= strtotime('18:00:00')) disabled @endif> 
					  		<label for="radio9">06:00 PM</label>
				  		</div>
				  		<div class="col-md-4 form-group app_time_slots">
						    <input type="radio" name="appointmentTime" id="radio10" class="time_app_radio form-control o" style="display:none;" value="19:00:00" @if(in_array('19:00:00',$appointment)|| strtotime($cuttent_time) >= strtotime('19:00:00')) disabled @endif> 
					  		<label for="radio10">07:00 PM</label>
				  		</div>
				  		<div class="col-md-4 form-group app_time_slots">
						    <input type="radio" name="appointmentTime" id="radio11" class="time_app_radio form-control o" style="display:none;" value="20:00:00" @if(in_array('20:00:00',$appointment)|| strtotime($cuttent_time) >= strtotime('20:00:00')) disabled @endif> 
					  		<label for="radio11">08:00 PM</label>
				  		</div>
						
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<input type="submit" name="app_button" id="app_button" Value="submit" class="form-control btn btn-warning">
					</div>
					<div class="col-md-6">
						<input type="reset" name="app_button" id="reset_button" Value="Reset" class="form-control btn btn-danger">
					</div>
				</div>
	   		</form>
	   	</div>
	</div>
</div>
@endsection
@section('pageJs')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
<script type="text/Javascript">
	$(function () {
        $('#datetimepicker12').datetimepicker({
            inline: true,
            format: 'dd/mm/yyyy',
            minDate: moment(),
            maxDate: moment().add(30, 'days'),
            daysOfWeekDisabled: [0]
        }).on('dp.change', function (ev) {
		    appointment_date_change(ev) ;//your function call
		});
    });
    var date = new Date();
	var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
	$(document).on('click',"#reset_button",function(e){
    	location.reload(true);
    });
    function appointment_date_change(data){
    	var current_time="{{ date_added() }}";
    	var current_date="{{ cur_date_added() }}";
    	//alert(current_time);
    	$("#date_appointment").val(data.date.format('L'));
		var selectedDate=data.date.format('L');
		var time_array=[];
    	$('.time_app_radio').each(function(){
    		$(this).css('background-color','#26eb51');
    		$("input[name=appointmentTime]").attr("checked",false);
			time_array.push($(this).val());
		});

			//jQuery("#find_ajax_loading").show();
			$.ajax({
				headers: {
		            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
	            },
				type:'POST',
			    url:'{{url("/checkAppointmentTime/")}}',
			    data:{
					selectedDate:selectedDate,
					time_array:time_array			   			
			   	},
			    success:function(responce){
			    	var data=JSON.parse(responce);
			    	console.log(data);
			    	if(data != ""){
			    		$('.time_app_radio').each(function(){
				    		$("input[name=appointmentTime]").attr("disabled",false);
				    		$(this).css('background-color','#26eb51');
						});
						for (var i = 0; i < data.app.length; i++) {
							if(jQuery.inArray(data.app[i].appointment_time,time_array) !== -1){
								$("input[name=appointmentTime][value='"+data.app[i].appointment_time+"']").attr("disabled",true);
							}
						}
						for (var i = 0; i < data.disabled_slot.length; i++) {
							if(jQuery.inArray(data.disabled_slot[i],time_array) !== -1){
								$("input[name=appointmentTime][value='"+data.disabled_slot[i]+"']").attr("disabled",true);
							}
						}
					}
				jQuery("#find_ajax_loading").hide();
			   }
			});
    }
</script>
@endsection
@section('PageCSS')
	<link rel="stylesheet" type="text/css" href="{{asset('/css/prettify-1.0.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('/css/bootstrap-datetimepicker.css')}}">
	<style type="text/css">
		.label_app{
		    color: #01CFCF;
		    text-align: center;
		    margin-bottom: 20px;
		    font-size: 40px;
		    font-weight: 800;
		}
		input[type=radio]:checked + label {
			color: #FFF;
			background-color: #01CFCF;
			font-style: normal;
		}
		input[type=radio]:disabled + label {
			color: #FFF;
			background-color: #2c2e2e;
			font-style: normal;
		} 
		.app_time label {
		    border: 1px solid;
		    border-radius: 5px;
		    padding: 10px;
		    margin: 5px;
		    clear: both;
		    content: none;
		    width: 100%;
		    text-align: center;
		    background-color: #26eb51;
		    color: white;
		}
		.picker-switch.accordion-toggle {
		    display: none;
		}
		#datetimepicker12 {
		    border: 1px solid;
		    padding: 10px;
		    border-radius: 10px;
		}
		.lable_app {
		    margin-bottom: 16px;
		    text-align: center;
		}
		.appointment_label span {
		    text-align: center;
		    margin-left: 20px;
		}

		label.slot_availble {
		    background-color:#26eb51;
		}

		label.no_slot_availble {
			background-color: #2c2e2e;
		}
	</style>
@endsection

