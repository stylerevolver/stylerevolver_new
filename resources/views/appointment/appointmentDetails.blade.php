@extends('welcome')
@section('content')


<div class="banner-top">
	<div class="container">
		<h2 class="" data-wow-delay=".5s">Appointment Details</h2>
		<h3 class="" data-wow-delay=",5s"><a href="{{url('/home/appointment')}}">Home</a><label>/</label>Appointment</h3>
		<div class="clearfix"> </div>
	</div>
</div>


<div class="appointment">
		<div class="container">
			 @if(session('success'))
	            <div class="alert alert-success alert-dismissible">
	              <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
	              {{ session('success') }}
	            </div>
	          @elseif(session('error'))
	              <div class="alert alert-error alert-dismissib">
	                <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
	                {{ session('error') }}
	            </div>                              
	          @endif
				<div class="col-md-12" style="margin-bottom:10px"> 
					<h3 style="text-align: center;border-bottom: 1px solid;border-width: 5px;">Customize Product</h3>
				</div>
			<form action="{{url('/store_CustomizeProduct')}}" method="post" enctype="multipart/form-data" id="customizeProduct">
				{{ csrf_field() }}
				<input type="hidden" name="app_id" value="{{$appointment}}">
				<div class="col-12">
					<div class="col-md-3"> 
						<div class="form-group">
							<label for="name">ProductName</label>
							<select name="name" id="name" class="form-control m-input m-input--air select2_class name"  placeholder="Please enter Product name">
								<option></option>
								@foreach($product as $p)
                                    <option value="{{$p->subcategory_name}}">{{$p->subcategory_name}}</option>
                                @endforeach
                                    <option value="other">other</option>
							</select>
							{{--<input type="type" id="name" class="form-control" name="name" required value="{{ old('name') }}">--}}
							@error('name')
								<span class="text-danger" role="alert">
	                            	<strong>{{ $message }}</strong>
	                       		</span>
							@enderror
						</div>
					</div>
					<div class="col-md-3"> 
						<div class="form-group">
							<label for="pro_image">Product Image</label>
							<input type="file" id="pro_image" class="form-control" name="pro_image" value="{{ old('pro_image') }}" required>
							@error('pro_image')
								<span class="text-danger" role="alert">
	                            	<strong>{{ $message }}</strong>
	                       		</span>
							@enderror
						</div>
					</div>
					<div class="col-md-3"> 
						<div class="form-group">
							<label for="pro_image">Product Description</label>
							<textarea name="pro_descption" class="form-control" required>{{ old('pro_descption') }}</textarea>
							@error('pro_descption')
								<span class="text-danger" role="alert">
	                            	<strong>{{ $message }}</strong>
	                       		</span>
							@enderror
						</div>
					</div>
					<div class="col-md-3"> 
						<label>&nbsp</label>
						<input type="submit" id="submit" class="btn btn-warning form-control" name="submit">
					</div>
				</div>
	   		</form>
		   	<div class="row">
		   	@if(count($appointment_detils)>0)
		   		<table id="appointment_details" class="table" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
		   			<thead>
		   				<tr>
		   					<th>Id</th>
		   					<th>Product Image</th>
		   					<th>Product Name</th>
		   					<th>Description</th>
		   					<th>Action</th>
		   				</tr>
		   			</thead>
		   			<tbody>
		   				@foreach($appointment_detils as $key=> $details)
			   				<tr>
			   					<td>{{($key)+1}}</td>
			   					<td><img src="{{asset('/images/'.$details->customize_product_image)}}" height="70" width="70"></td>
			   					<td>{{$details->customize_product_name}}</td>
			   					<td>{{$details->description}}</td>
		   						<td><a href="{{url('/updateMyAppointmentDetails/'.$details->customize_product_id)}}" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x"><i class="fa fa-wrench" aria-hidden="true"></i></a> <a href="{{url('deleteCustomizeProduct/'.$details->customize_product_id)}}" class="btn btn-danger" onclick='return confirm("Are you sure you want to delete this customize product?")'><i class="fa fa-trash" aria-hidden="true" title="Delete"></i></a></td>
			   				</tr>
			   				@endforeach
		   			</tbody>
		   		</table>
   			@endif
		   	</div>
		   	<div class="row">
		   		<div class="text-center" style="margin: 15px">
		   			<a href="{{url('/appointmentStatus/'.$appointment)}}" class="btn btn-success" id="bookAppo">Confirm booked appointment</a>
		   		</div>
		   	</div>
		</div>
</div>

@endsection
@section('pageJs')
<script type="text/Javascript">
 	jQuery(document).on('submit','#customizeProduct',function(e){
 		$("#submit").attr('disabled',true);
	})
	jQuery(document).on('click','#bookAppo',function(e){
 		$(this).attr('disabled',true);
	})

	jQuery("#name").select2({
	    placeholder: "Select a Product Name",
	    allowClear: true
	});
</script>
@endsection
@section('PageCSS')
	<style type="text/css">
		.appointment{
			padding-top: 20px;
			padding-bottom: : 20px;
		}
		
	</style>
@endsection