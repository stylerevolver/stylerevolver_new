@extends('welcome')
@section('content')
	<div class="banner-top">
		<div class="container">
			<h2 class="" data-wow-delay=".5s">Update Customize Product Details</h2>
			<h3 class="" data-wow-delay=".5s"><a href="{{ url('/home') }}">Home</a><label>/</label><a href="#">Appointment</a></h3>
			<div class="clearfix"> </div>
		</div>
	</div>

	<div class="check-out">	 
	<div class="container"> 
		
		<div class="col-md-9">
							<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{ url('/updateMyAppointmentDetails') }}" enctype="multipart/form-data">{{csrf_field()}}
				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label for="name" class="col-3 col-form-label">
						Customized Product Name
						</label>
						<div class="col-9">
							<select name="name" id="name" class="form-control m-input m-input--air select2_class name"  placeholder="Please enter Product name">
								<option></option>
								@foreach($product as $p)
                                    <option value="{{$p->subcategory_name}}">{{$p->subcategory_name}}</option>
                                @endforeach
                                    <option value="other">other</option>
							</select>
							{{--<input class="form-control m-input" name="name" id="name" type="text" tabindex="1" value="{{ $customize_product->customize_product_name}}" placeholder="Please enter Product name">--}}
							@error('name')
                                     <span class="text-danger" role="alert">
                                         <strong>{{ $message }}</strong>
                                     </span>
                                   @enderror
						</div>
					</div>
				</div>
				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label for="image" class="col-3 col-form-label">
						Customized Product	Image
						</label>
						<div class="col-9">
							<input class="form-control m-input" name="image" id="image" type="file" tabindex="2" value="{{asset($customize_product->customize_product_image)}}" placeholder="Please enter Product">
										@error('image')
                                       	<span class="text-danger" role="alert">
                                            <strong>{{ $message }}</strong>
                                       	</span>
                                     	@enderror
									</div>
								</div>
							</div>

							<div class="m-portlet__body">
								<div class="form-group m-form__group row">
									<label for="description" class="col-3 col-form-label">
									Description
									</label>
									<div class="col-9">
										<textarea tabindex="2" class="form-control m-input" name="description" id="description"  placeholder="">{{ $customize_product->description}}</textarea>
										@error('description')
		                                    <span class="text-danger" role="alert">
		                                       		<strong>{{ $message }}</strong>
		                                   	</span>
                                   		@enderror
									</div>
								</div>
							</div>


							<div class="m-portlet__body">
								<div class="form-group m-form__group row">
									<div class="col-9">
			                          		<img src="{{asset('/images/'.$customize_product->customize_product_image)}}" height="100" width="100" alt="{{$customize_product->customize_product_name}}">
									</div>
								</div>
							</div>



							<div class="m-portlet__foot m-portlet__foot--fit">
								<div class="m-form__actions">
									<div class="row">
										<div class="col-3"></div>
										<div class="col-9">
                        					<input type="hidden" name="pro_image" id="pro_image" value="{{$customize_product->customize_product_image}}">
											<input type="hidden" name="id" value="{{$customize_product->id}}">
											<input type="hidden" name="app_id" value="{{$customize_product->appointment_id}}">
											<button type="submit" class="btn btn-success" tabindex="4">
												Submit
											</button>
											<a href="{{ url('/appointmentDetails')}}"><button type="button" class="btn btn-secondary" tabindex="4">
                                            	Cancel
                                        	</button></a>
										<!-- 	<a href="{{url('/admin/customizedProductDetails')}}" class="btn btn-secondary" tabindex='5'>Cancel</a> -->
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		



@endsection

@section('pageJs')
<script type="text/Javascript">
	jQuery("#name").select2({
	    placeholder: "Select a Product Name",
	    allowClear: true
	});
</script>
@endsection