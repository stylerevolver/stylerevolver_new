@extends('welcome')
@section('content')
	<div class="banner-top">
		<div class="container">
			<h2 class="" data-wow-delay=".5s">My Appoinment</h2>
			<h3 class="" data-wow-delay=".5s"><a href="{{ url('/home') }}">Home</a><label>/</label>Cart</h3>
			<div class="clearfix"> </div>
		</div>
	</div>

<div class="check-out">	 
	<div class="container"> 
		<div class="col-md-3">
			@include('myAccountLeftMenu');
		</div>
		<div class="col-md-9">
			<table class="table" data-wow-delay=".5s">
				<tr>
					<th class="t-head">Id</th>
					<th class="t-head">Appointment Stastus</th>
					<th class="t-head">Visited Stastus</th>
					<th class="t-head">Date</th>
					<th class="t-head">Time</th>
					<th class="t-head">Action</th>
				</tr>
				<tbody>
					@foreach($appointment as $data)
	            	<tr>
	            		<td class="t-data"> {{$data->id}} </td>
	            		<td class="t-data"> {{$data->complete_status}} </td>
	            		<td class="t-data"> {{$data->visited_status}} </td>
	            		<td class="t-data"> {{$data->appointment_date}} </td>
	            		<td class="t-data"> {{$data->appointment_time}} </td>
	            		<td class="t-data"> <a href="{{url('/myAppointmentDetails/'.$data->id)}}" data-text="Details" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x but-hover1 item_add">Details</a></td>
					</tr>
	            	@endforeach
				</tbody>
			</table>
		</div>
		<div class="col-md-3">
			
		</div>
	</div>
</div>
@endsection
@section('pageJs')
	
@endsection
@section('PageCSS')
<style type="text/css">
	.name h5{
		margin-top: 0px !important;
	}
	.close1{
		top:40px !important;
	}

	.close1{
		top:40px !important;
	}
	th.t-head{
		font-size: 18px !important;
		background-color: #FF7000;
	}
	td.t-data{
		font-size: 18px ;
</style>
@endsection