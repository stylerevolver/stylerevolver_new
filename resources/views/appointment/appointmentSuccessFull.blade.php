@extends('welcome')
@section('content')


<div class="banner-top">
	<div class="container">
		<h2 class="" data-wow-delay=".5s">Appointment</h2>
		<h3 class="" data-wow-delay=",5s"><a href="{{url('/home')}}">Home</a><label>/</label>Appointment</h3>
		<div class="clearfix"> </div>
	</div>
</div>


<div class="contact">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				@if($appointment->complete_status == "Confirm_Pending")
					<h2>Appointment Successfully  booked...</h2>
					<div class="text-center"> 
						<a href="{{url('/home')}}" class="btn btn-warning">Go to Home</a>
					</div>
					{{-- <script type="text/javascript">window.open('/myAppointment','_self');</script> --}}
				@elseif($appointment->complete_status == "Pending")
					<div class="text-center"> 
						<div class="col-md-5 text-center block_custom">
							<h4>You can also add the your own one or more product design, magerment</h4>
							<a href="{{url('/appointmentDetails')}}" class="btn btn-success">Add Customize Product</a>
						</div>
						<div class="col-md-2"></div>
						<div class="col-md-5 text-center block_custom">
							<h4>If you can't be add the own product so confirm the appointment</h4>
							<a href="{{url('/appointmentStatus/'.$appointment->id)}}" class="btn btn-success">Confirm booked appointment</a>
						</div>
					</div>
				@elseif($appointment->complete_status == "Complete")
					<h2>Order your own customize products..</h2>
					<div class="text-center"> 
						<a href="{{url('/appointment')}}" class="btn btn-warning">Click hear</a>
					</div>
				@else 
					<h2>Order your own customize products..</h2>
					<div class="text-center"> 
						<a href="{{url('/appointment')}}" class="btn btn-warning">Click hear</a>
					</div>					
				@endif
			</div>
		</div>
	</div>
</div>
@endsection
@section('pageJs')
@endsection
@section('PageCSS')
	<style type="text/css">
		.block_custom{
			border: 1px solid black;padding: 10px;border-radius: 5px;background-color: orange;color:black;
		}
		.block_custom h4{
			margin: 10px;
		}
	</style>
@endsection

