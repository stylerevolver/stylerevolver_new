@php
	$admin=adminData();
@endphp
@extends('welcome')
@section('content')

<!--banner-->
<div class="banner-top">
	<div class="container">
		<h2 class="">Contact</h2>
		<h3 class=""><a href="{{url('/home')}}">Home</a><label>/</label>Contact</h3>
		<div class="clearfix"> </div>
	</div>
</div>
<!-- contact -->
	<div class="contact">
		<div class="container">
				@if(session('success'))
		        <div class="alert alert-success alert-dismissible">
		          <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
		          {{ session('success') }}
		        </div>
		      @elseif(session('error'))
		          <div class="alert alert-error alert-dismissib">
		            <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
		            {{ session('error') }}
		        </div>                              
		      @endif
			<div class="col-md-8 contact-grids1" data-wow-delay=".5s">
				<form method="post" action="{{url('/contactUsMail')}}">
					{{ csrf_field() }}
					<div class="contact-form2">
						<h4>Name</h4>
						<input type="text" name="name" id="name" placeholder="" required>
						@error('name')
	                       	<span class="text-danger" role="alert">
	                   			<strong>{{ $message }}</strong>
	           		 		</span>
	          		 	@enderror
					</div>
					<div class="contact-form2">
						<h4>Email</h4>
						<input type="email" name="email" id="email" placeholder="" required>
						@error('email')
                           	<span class="text-danger" role="alert">
                       			<strong>{{ $message }}</strong>
               		 		</span>
              		 	@enderror
					</div>
					<div class="contact-form2">
						<h4>Subject</h4>
						<input type="text" name="subject" id='subject' placeholder="" required>
						@error('subject')
	                       	<span class="text-danger" role="alert">
	                   			<strong>{{ $message }}</strong>
	           		 		</span>
              		 	@enderror
					</div>
					<div class="contact-me ">
						<h4>Message</h4>
						<textarea type="text" name="message" id="message" placeholder="" required> </textarea>
						@error('message')
	                       	<span class="text-danger" role="alert">
	                   			<strong>{{ $message }}</strong>
	           		 		</span>
              		 	@enderror
					</div>
					<input type="submit" value="Submit" >
				</form>
			</div>
			
			<div class="col-md-4 contact-grids">
				<div class=" contact-grid" data-wow-delay=".5s">
					<div class="contact-grid1">
						<div class="contact-grid2 ">
							<i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>
						</div>
						<div class="contact-grid3">
							<h4>Address</h4>
							<p>{{$admin[0]['company_address']}}</p>
						</div>
					</div>
				</div>
				<div class=" contact-grid" data-wow-delay=".5s">
					<div class="contact-grid1">
						<div class="contact-grid2 contact-grid4">
							<i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>
						</div>
						<div class="contact-grid3">
							<h4>Call Us</h4>
							<p>+91{{$admin[0]['company_contact']}}</p>
						</div>
					</div>
				</div>
				<div class=" contact-grid" data-wow-delay=".5s">
					<div class="contact-grid1">
						<div class="contact-grid2 contact-grid5">
							<i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>
						</div>
						<div class="contact-grid3">
							<h4>Email</h4>
							<p><a href="contactto:{{$admin[0]['company_email']}}">{{$admin[0]['company_email']}}</a></p>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	 <!-- <div class="map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3770.4179283150966!2d74.77714145050848!3d19.089313056450937!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bdcb020b798612f%3A0x92a95728ed2ba6a9!2sShri%20Ram%20Colony%2C%20Bhingar%2C%20Ahmednagar%2C%20Maharashtra%20414002!5e0!3m2!1sen!2sin!4v1581576769654!5m2!1sen!2sin" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen></iframe>
		</div > -->


@endsection