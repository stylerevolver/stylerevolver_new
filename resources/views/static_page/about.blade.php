@extends('welcome')
@section('content')
<div class="banner-top">
	<div class="container">
		<h2 class="">About</h2>
		<h3 class=""><a href="{{url('/home')}}">Home</a><label>/</label><a href="javascript:void(0)" style="color: white">About<label>
		<div class="clearfix"> </div>
	</div>
</div>
<div class="contact">
	<div class="container">
		<div class="row  about">
			<div class="col-md-6 ">
			   	<img src="{{asset('/admin-asset/images/about.jpg')}}" alt=" " class="img-responsive" height="370px !important" />
			</div>
			 <div class="col-md-6">
			    <h5 style="font-size: 35px"><a href="{{ url('/home')}}"><u>Style Revolver</u></a></h5>
			   <p>Style Revolver is a webpage where you can buy various designer cloths and also you can give your own disign by taking appointment.Also You can customize your order.You can get your product at anytime from anywhere</p>
				<H5>Vision <span>&</span> Mision</H5>
				<p>We provide various Designer clothes to our clients in order to provide them with full satisfaction. We have stringent measures and we use them to assure the quality of our products at all level of manufacturing and delivery.</p>
			
			</div>
			<div class="clearfix"></div>
		</div>   
	</div>
</div>

@endsection
@section('PageCSS')
<style type="text/css">
	.about p {
    margin: 1em 0 2em;
    color: #545454;
    line-height: 2em;
}

.about h5 {
       font-size: 1.5em;
       text-transform: uppercase;
    color: #212121;
    letter-spacing: 2px;
    font-weight: 700;
    margin-bottom: 1.5em;
}
.about img{
	height: 370px !important;
}
</style>
@endsection