@php
  $admin=adminData();
@endphp
@extends('welcome')
@section('content')

<div class="banner-top">
  <div class="container">
    <h2 class="">Complain</h2>
    <h3 class=""><a href="{{url('/home')}}">Home</a><label>/</label>Complain</h3>
    <div class="clearfix"> </div>
  </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if(session('success'))
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                  {{ session('success') }}
                </div>
            @elseif(session('error'))
                <div class="alert alert-error alert-dismissib">
                    <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                    {{ session('error') }}
                </div>                              
            @endif
            <form action="{{url('/complainSubmit')}}" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="sales_id" value="{{$id}}">
                <div class="m-portlet__body">
                    <div class="form__group row">
                        <label for="name" class="col-md-1 col-form-label text-md-right">ProductName</label>
                        <div class="col-md-4">
                            <select name="name" id="name" class="form-control m-input m-input--air select2_class" tabindex="1" placeholder="Please enter Product Name ">
                              <option></option>
                              @foreach($product as $p)
                                <option value="{{$p->pro_id}}">{{$p->product_name}}</option>
                              @endforeach
                            </select>
                            @error('name')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <br>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <label for="msg" class="col-md-1 col-form-label text-md-right">Message</label>
                        <div class="col-md-4">
                            <textarea class="form-control m-input" name="msg" id="msg" tabindex="2" placeholder="Please enter Message"></textarea>
                            @error('msg')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                           @enderror
                        </div>                    
                    </div>
                </div>
                <button type="submit" class="btn btn-success" tabindex="4">Submit</button>
                <button type="reset" class="btn btn-secondary" tabindex="4">Cancel</button>
            </form>
        </div>
    </div>
</div>
@endsection