<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>STYLEREVOLER</title>
        <link rel="shortcut icon" href="{{ asset('images/style_logo.jpeg') }}" />
        @include('layouts.head')
    </head>
    <body>
        <div id="find_ajax_loading" style="" class="gif">
            <div class="loader">
                <img src="{{asset('/loading.gif')}}" width="200" height="200" >
            </div>
        </div>
        @include('layouts.header')
        @yield('content')
@include('layouts.footer')
    </body>
</html>
