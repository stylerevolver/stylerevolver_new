@extends('welcome')
@section('content')
<div class="banner-top">
        <div class="container">
            <h2 class="" data-wow-delay=".5s">Reset Password</h2>
            <h3 class="" data-wow-delay=".5s"><a href="{{ url('/home') }}">Home</a><label>/</label>Reset Password</h3>
            <div class="clearfix"> </div>
        </div>
    </div>
<div class="container" style="margin-top: 15px;">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading" style="border-bottom: 1px solid black;margin-bottom: 10px;background-color: #FF7000;text-align: center;color:white;padding: 7px;border-radius: 5px">
                <h2>Reset password</h2>
            </div>

            <div class="card-body" style="margin-left:15px;">

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0 text-center">
                            <button type="submit" class="btn btn-warning">
                                {{ __('Send Password Reset Link') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
