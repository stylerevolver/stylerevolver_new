@extends('welcome')
@section('content')
<div class="banner-top">
    <div class="container">
        <h2 class="fadeInLeft" data-wow-delay=".5s">Login</h2>
        <h3 class="fadeInRight" data-wow-delay=".5s"><a href="index.html">Home</a><label>/</label>Login</h3>
        <div class="clearfix"> </div>
    </div>
</div>
<div class="login_page">
    <div class="container">
        <div class="row justify-content-center">
                @if($errors->first('message'))
                    <div class="alert alert-warning alert-dismissible">
                      <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                      {{ $errors->first('message') }}
                    </div>
                @elseif($errors->has('success'))
                    <div class="alert alert-error alert-dismissib">
                        <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                        {{ $errors->first('success') }}
                    </div>                              
                @elseif($errors->has('active'))
                    <div class="alert alert-error alert-dismissib">
                        <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                        You must be active to your account.
                    </div>                              
                @endif
                @if(session('success'))
                    <div class="alert alert-success alert-dismissible">
                      <a href="#" class="close" data-dismiss="alert" aria-lable="close"></a>
                      {{ session('success') }}
                    </div>
                @endif
            <div class="card-body">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="col-12">
                        <div class="col-md-6 login-do1" data-wow-delay=".5s">
                            <div class="login-mail">
                                <input type="text" placeholder="Email" required="" name="email" value="{{ old('email') }}" autocomplete="email" autofocus  class="form-contro @error('email') is-invalid @enderror">
                                <i class="glyphicon glyphicon-envelope"></i>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="login-mail">
                                <input id="password" type="password" class="@error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                <i class="glyphicon glyphicon-lock"></i>
                                 @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            @if (Route::has('password.request'))
                                <a class="btn btn-warning" href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a>
                            @endif
                            @if($errors->has('active'))
                                 <a href="{{url('resendActivationLink/'.$errors->first('active'))}}" class="form-controls btn btn-danger">Resend The Activation Link</a>
                            @endif
                        </div>
                        <div class="col-md-6">
                            

                            <div class="login-do">
                                <label class="hvr-sweep-to-top login-sub"><input type="submit" value=" {{ __('Login') }}"></label>
                                <p>Do not have an account?</p>
                                <a href="{{url('register')}}" class="hvr-sweep-to-top">Signup</a>
                                </div>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('PageCSS')
   <style type="text/css">
        .login_page{
            padding-top:60px;
            padding-bottom:60px;
        }
    </style>
@endsection