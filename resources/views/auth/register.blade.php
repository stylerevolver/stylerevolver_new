@extends('welcome')
@section('content')
<div class="banner-top">
    <div class="container">
        <h2 class="fadeInLeft" data-wow-delay=".5s">Register</h2>
        <h3 class="fadeInRight" data-wow-delay=".5s"><a href="index.html">Home</a><label>/</label>Register</h3>
        <div class="clearfix"> </div>
    </div>
</div>
<div class="login">
        <div class="container" style="background-color:#f1f3f5;padding: 20px;margin-bottom: 20px">
        <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data" id="register">
            @csrf
            <div class="row">
                <div class="col-md-12 login-do1">
                    <div class="col-md-6">
                        <div class="row">
                            {{--<div class="form-group">
                                <label for="username">Username</label>
                                <input type="text" id="username" placeholder="Enter youre username" required autocomplete="off" name="username" class="form-control" value="{{old('username')}}">
                                @error('username')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>--}}
                            <div class=" form-group">
                                <label for="first_name">First Name</label>
                                <input type="text" name="first_name" id="first_name" placeholder="Enter Your First name" required autocomplete="false" class="form-control" value="{{old('first_name')}}">
                                @error('first_name')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="lname">Last Name</label>
                                <input type="text" name="lname" id="lname" placeholder="Enter your Last name" required autocomplete="false" class="form-control" value="{{old('lname')}}">
                                @error('lname')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" placeholder="Enter your Email" required autocomplete="false" name="email" id="email" class="form-control" value="{{old('email')}}">
                                @error('email')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="phone">Phone number</label>
                                <input type="number" min="0" placeholder="Enter your phone" name="phone" id="phone" required autocomplete="false" class="form-control" value="{{old('phone')}}">
                                @error('phone')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" placeholder="Enter your password" name="password" id="password" required autocomplete="false" class="form-control" value="{{old('password')}}">
                                @error('password')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="password-confirm">Confirm Password</label>
                                <input type="password" placeholder="Enter your confirm password" name="password_confirmation" id="password-confirm" required autocomplete="false" class="form-control" value="{{old('password_confirmation')}}">
                                @error('password_confirmation')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 fo">
                            <div class="form-group">
                                <label for="gender">Gender</label><br>
                                <input type="radio" name="gender"  value="male"  @if(old('gender') == "male") checked @endif required>Male
                                <input type="radio" name="gender" value="female"  @if(old('gender') == "female") checked @endif required>Female
                                @error('gender')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="dob">Date Of Birth</label>
                                <input type="date" placeholder="Select birth date" required autocomplete="false" class="form-control" id="dob" name="dob" value="{{old('dob')}}">
                                @error('dob')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="address">Address</label>
                                <textarea name="address" id="address" placeholder="Enter your address" required class="form-control" cols="10">{{old('address')}}</textarea>
                                @error('address')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="areaname">Area</label>
                                <select name="areaname" id="areaname" required class="form-control">
                                    <option selected value="">Select area</option> 
                                    @foreach($area as $a)
                                        <option value="{{$a->id}}" {{$a->id == old('areaname') ? 'selected="selected"' : '' }}>{{$a->area_name}}</option>
                                    @endforeach
                                </select>
                                @error('areaname')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                           <div class="form-group">
                                <label for="s_q">Security Question</label>
                                <select name="s_q" id="s_q" required class="form-control">
                                    <option selected value="">Choose option</option>
                                    <option value="nicename" {{'nicename' == old('s_q') ? 'selected="selected"' : '' }}>Nickname</option>
                                    <option value="color" {{'color' == old('s_q') ? 'selected="selected"' : '' }}>Color</option>
                                </select>
                                @error('s_q')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                             <div class="form-group">
                                <label for="s_a">Security Answer</label>
                                <input type="text" placeholder="Enter security answer" required autocomplete="false" class="form-control" id="s_a" name="s_a" value="{{old('s_a')}}">
                                @error('s_a')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                      {{--   <div class="form-group">
                            <label for="pic">Profile Photo</label>
                            <input type="file" placeholder="Select image" name="pic" id="pic" required autocomplete="false" class="form-control" accept="image/png, image/jpeg" value="{{old('pic')}}">
                            @error('pic')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="image_preview"></div> --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 login-do1">
                    <div class="col-md-2"></div>
                    <div class="col-md-4 form-group">
                        <input type="submit" value="Submit" name="submit" id="submit" class="btn btn-info form-control">
                    </div>
                    <div class="col-md-4 form-group">
                        <a href="{{ route('login')}}" class="btn btn-primary form-control" id="login_btn">Login</a>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
            <div class="clearfix"> </div>
            </form>
        </div>


    </div>
@endsection
@section('PageCSS')
   <style type="text/css">
        .header_regs{
            font-size: 65px;
            color: blue;
            font-weight: bolder;
            margin-bottom:30px;
        }
    </style>

@endsection
@section('pageJs')
    <script type="text/javascript">
        $(document).on('submit','#register',function(e){
            $("#submit").attr('disabled',true);
            $("#login_btn").attr('disabled',true);
        })
    </script>
@endsection