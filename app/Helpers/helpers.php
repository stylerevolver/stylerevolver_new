<?php

use App\Models\Role;
use Modules\Admin\Entities\admin;
use App\models\company;
use App\models\category;
use App\models\subcategory;


if (! function_exists('getRoleID')) {
    function getRoleID($role='') {
        $role = Role::where('name', $role)->select('id')->first();
        if(!empty($role))
        	return $role->id;
    	else
    		return NULL;
    }
}
if (! function_exists('adminData')) {
	function adminData(){
		$admin = company::select('company_name','company_email','company_contact','company_address')->get()->toArray();

		return $admin;
	}
}

if (! function_exists('cateData')) {
    function cateData(){
        $category = category::select('category_name')->get()->toArray();
        return $category;
    }
}

if(! function_exists('subcatData')){
    function subcatData(){
        //anu copy na karva jta tmne khyal nii ave
        $subcategory = category::with('subcategory')->get()->toArray();
        return $subcategory;
    }
}

    function date_added(){
        date_default_timezone_set('Asia/Calcutta');//or choose your location
        return date('H:i:s');
    }
    function cur_date_added(){
        date_default_timezone_set('Asia/Calcutta');//or choose your location
        return date('Y-m-d');
    }
    function current_date(){
        date_default_timezone_set('Asia/Calcutta');//or choose your location
        return date('d-m-Y');
    }

?>
