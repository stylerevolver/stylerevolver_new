<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class welcomeMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
         $this->data = $data;
    }

    /** 
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //echo '<pre>';print_r($this->data);exit();
        return $this->view('emails.welcome')->subject('Account Verification')->with('data',$this->data);
    }
}
