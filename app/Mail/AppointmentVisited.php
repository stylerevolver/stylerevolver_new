<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\models\sales;
use App\models\sales_details;
use App\models\company;
use PDF;
class AppointmentVisited extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
       
        $this->data = $data;
        $this->files = $files;
        //$this->sales_id=$data['sales_id'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email =$this->view('emails.appointmentVisited')->subject('Appointment Status')->with('data',$this->data);
        return $email;
         //dd("hii");
    }
}
