<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\area;
use App\Models\Role;
use App\Models\RoleUser;
use Illuminate\Support\Facades\Input;
use App\Mail\welcomeMail;
use Carbon\Carbon;
use Redirect;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    public function showRegistrationForm(){
            $area='';
            $area=area::all();
            return view('auth.register')->with('area',$area);
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
   

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function register(Request $request)
    {
        $rules = array(
 /*           'username' => 'required|string|max:255|regex:/^[a-zA-Z ]+$/u|unique:users,name',*/
            'first_name' => 'required|string|max:255|regex:/^[a-zA-Z ]+$/u',
            'lname' => 'required|string|max:255|regex:/^[a-zA-Z ]+$/u',
            'email' => 'required|string|max:255|unique:users,email|email',
            'phone' => 'required|regex:/^[0-9 ]+$/u|digits_between:10,11|unique:users,contact_no',
            'password' => 'required_with:password_confirmation|same:password_confirmation|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
            'password_confirmation' => 'required',
            'gender' => 'required',
            'dob'=>'required|before:13 years ago',
            'address' => 'required',
            'areaname' =>'required',
            's_q' =>'required',
            's_a' =>'required',
        );
        $customeMessage = array(
            /*'username.required' => 'User name can not be an empty.',                                   */
            'username.string' => 'User name is must be in string value',
            'username.unique' => 'User name must be unique',
            'first_name.required' => 'First name can not be an empty',
            'first_name.string' => 'First name is must be in string value',
            'lname.required' => 'Last name can not be an empty',
            'lname.string' => 'Last name is must be in string value',
            'email.required' => 'Email can not be an empty.',
            'email.unique' => 'Email is must be unique',
            'phone.required' => 'Phone number can not be an empty',
            'phone.regex' => 'Invalid value',
            'password.required' => 'password can not be an empty',
            'password.same' => 'Confirm password and passwordshould be same',
            'password_confirmation.required' => 'Confirm password can not be an empty',
            'gender.required' => 'Gender can not be an empty',
            'dob.required' => 'Date Of Birth can not be an empty',
            'address.required' => 'Address can not be an empty',
            'areaname.required' => 'Area name can not be an empty',
            's_q.required' => 'Security Question can not be an empty',
            's_a.required' => 'Security Answer can not be an empty',
         );
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails()){
          return redirect()->back()->withErrors($validator)->withInput();
        }
        event(new Registered($user = $this->create($request->all())));
        $request->session()->flash('message', 'Registered successfully, please check your mail...!');
        return $this->registered($request, $user)?: redirect($this->redirectPath());
    }
    protected function create(array $data){
        $user = new User();
        $user->name = "-";
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->address = $data['address'];
        $user->security_question = $data['s_q'];
        $user->security_answer = $data['s_a'];
        $user->first_name = ucfirst($data['first_name']);
        $user->last_name = ucfirst($data['lname']);
        $user->gender = $data['gender'];
        $user->dob = $data['dob'];
        $user->contact_no = $data['phone'];
        $user->area_id = $data['areaname'];
        $user->verify_tokan = str_random(50);
        $user->profile_pic= "user_d.png";
       
        $request = request();
        /*if($request->file('pic') != null)
        {
            $profile_file = $request->file('pic');
            $filename = time() . '_' . pathinfo($profile_file->getClientOriginalName(), PATHINFO_FILENAME) . '.' . $profile_file->getClientOriginalExtension();
            $profile_file->move(public_path('profilephoto'), $filename);
            $user->profile_pic =$filename;
        }*/
        $user->save();

        $data['verify_tokan'] = $user->verify_tokan;
        
     
        $role_name = Role::where('name', '=', 'customer')->first();
        if(!empty($user) && !$user->hasRole('customer')){
                $user->attachRole($role_name);
        }
        try{
            Mail::to($user->email)->send(new welcomeMail($data));
            return $user;
        }
        catch(\Exception $e)
        {
           return $user;
        }
       return $user;
    }
}
