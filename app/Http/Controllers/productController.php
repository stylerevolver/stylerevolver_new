<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\models\company;
use App\models\subcategory;
use App\models\product;
use App\models\size_chart;
use App\models\color;
use App\models\users;
use App\models\feedback;
use App\models\wishlist;
use App\models\rating;
use App\models\product_image;
use App\models\sales_details;
use DB;

class productController extends Controller
{
	
    public function show($id){
        $product=product::join('subcategory as s','s.id','=','product.subcategory_id')
        		->join('size_chart as sc','sc.id','=','product.size_id')
        		->join('company as c','c.id','=','product.company_id')
        		->select('product.id','product.product_name','product.product_image','product.product_price','product.product_qty','s.subcategory_name','sc.size_name','c.company_name','product.description')
                ->where('product.subcategory_id','=',$id)
        		->paginate(12);
            $subcategory=subcategory::where('id','=',$id)->value('subcategory_name');

        if (\Auth::user()) {
            $wishlist=wishlist::where('user_id','=',\Auth::User()->id)->select('product_id')->get();
            if(count($wishlist) >0){
                foreach ($wishlist as $key => $value) {
                    $wishlist_ar[]=$value->product_id;
                }
            }else{
                $wishlist_ar[]=0;
            }
            return view('product.products',compact('product','wishlist_ar','subcategory'));
        }else{
            return view('product.products',compact('product','subcategory'));
        }
    }


    public function singlePage($id)
    {
        
        $feedback=feedback::join('users as u','u.id','=','user_id')
                ->select('feedback.user_id','feedback.product_id','feedback.feedback_description','feedback.feedback_date','u.first_name','u.last_name','u.profile_pic')
                ->where('feedback.product_id','=',$id)
                ->get();

        $subcategory=subcategory::get();

        $size_chart=size_chart::get();

        $wishlist=wishlist::get();
        if(\Auth::User()){
            $rating_user=rating::where('product_id','=',$id)->where('user_id','=',\Auth::User()->id)->select(DB::raw('AVG(rating) as ratings_average'))->groupBy('product_id')->first();
            $rating=rating::where('product_id','=',$id)->select(DB::raw('AVG(rating) as ratings_average'))->groupBy('product_id')->first();
            if(empty($rating)){
                $rating=[];
            }
            if(empty($rating_user)){
                $rating_user=[];
            }
            $chk=sales_details::join('sales as s','s.id','=','sales_details.sales_id')
                ->where('s.user_id','=',\Auth::User()->id)
                ->where('sales_details.product_id','=',$id)
                ->get();
                if(count($chk)>0){
                    $boolean=true;
                }else{
                    $boolean=false;    
                }
        }else{
            $rating=rating::where('product_id','=',$id)->select(DB::raw('AVG(rating) as ratings_average'))->groupBy('product_id')->first();
            if(empty($rating)){
                $rating=[];
            }
            $rating_user=[];
            $boolean=false;    
        }
        
        $pro=product::join('subcategory as s','s.id','=','product.subcategory_id')
            ->join('size_chart as sc','sc.id','=','product.size_id')
            ->join('company as c','c.id','=','product.company_id')
            ->select('product.id','product.product_name','product.product_image','product.product_price','product.product_qty','product.subcategory_id','sc.size_name','product.company_id','product.color_id','product.size_id','s.subcategory_name','c.company_name','product.description')
            ->where('product.id','=',$id)
            ->first();
            
        $color_data=[];
        if($pro->color_id != ""){
            $color_array=explode('|',$pro->color_id);
            foreach ($color_array as $key => $value) {
                $color_name=color::where('id','=',$value)->first();
                $color_data[$key]['id']=$color_name->id;
                $color_data[$key]['name']=$color_name->color_name;
            }
        }
        $images=product_image::where('product_id','=',$id)->get();

        $product=product::join('subcategory as s','s.id','=','product.subcategory_id')
                ->join('size_chart as sc','sc.id','=','product.size_id')
                ->join('company as c','c.id','=','product.company_id')
                ->select('product.id','product.product_name','product.product_image','product.product_price','product.product_qty','s.subcategory_name','sc.size_name','c.company_name','product.description')
                ->where('product.subcategory_id','=',$pro->subcategory_id)
                ->whereNotIn('product.id',[$id])
                ->take(3)
                ->get();
         if (\Auth::user()) {
            $wishlist=wishlist::where('user_id','=',\Auth::User()->id)->select('product_id')->get();
            if(count($wishlist) >0){
                foreach ($wishlist as $key => $value) {
                    $wishlist_ar[]=$value->product_id;
                }
            }else{
                $wishlist_ar[]=0;
            }
            //dd($feedback);
            return view('product.single',compact('subcategory','size_chart','pro','feedback','wishlist','rating','rating_user','product','wishlist_ar','id','color_data','images','boolean'));
        }else{
            return view('product.single',compact('subcategory','size_chart','pro','feedback','wishlist','rating','rating_user','product','id','color_data','images','boolean'));
        }

        /*return view('product.single',compact('subcategory','size_chart','pro','feedback','wishlist','rating','rating_user','product'));*/
    }
}

    