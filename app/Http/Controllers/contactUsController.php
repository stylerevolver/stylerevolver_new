<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\contactUs;
use DB;

class contactUsController extends Controller
{
   function contact_us()
    {
    	return view('static_page.contact');
    }


    public function store(Request $request)
    {
        //echo '<pre>';print_r($request->all());exit();
         $rules = array(
            'name' => 'required|regex:/^[a-zA-Z ]+$/u|max:255|string',
            'email' => 'required|email|max:255|unique:email,',
            'subject' => 'required',
            'message' => 'required',
        );
        $customeMessage = array(
            'name.required' => 'Name can not be empty.',
            'name.string' => 'Name only varchar.',
            'email.required' => 'email can not be empty.',
            'subject.required' => 'subject can not be empty',
            'message.required' => 'message can not be emoty',
        );
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $contact = contact_us::create([
            'name' => ucfirst($request->name),
            'email' => $request ->email,
            'subject' => $request ->subject,
            'message' => $request ->message,
        ]);
         return redirect('/contact')->with('success',' Request sent successfully');
    }
}
