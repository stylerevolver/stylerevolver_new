<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\feedback;
use App\models\wishlist;
use App\models\cart;
use App\models\rating;
use App\models\city;
use App\models\area;
use App\models\offer;
use App\models\sales;
use App\models\sales_details;
use App\models\shipping_address;
use App\models\payment_type;
use App\models\sales_payment;
use App\models\company;
use App\models\product;
use App\User;
use Illuminate\Support\Facades\Validator;
use DB;
use PDF;
use Illuminate\Support\Facades\Mail;
class reviewController extends Controller
{
    public function addfeedback(Request $request){
        $product_id=$request->product_id;
        $user_id=\Auth::User()->id;
        
        $feedback=feedback::create([
            'user_id' => \Auth::User()->id,
            'product_id' => $request->product_id, 
            'feedback_description'=> $request->desc,
            'feedback_date' => date("Y-m-d", time())
        ]);
         $feedback_render=feedback::join('users as u','u.id','=','feedback.user_id')
            ->join('product as p','p.id','=','feedback.product_id')
            ->where('feedback.user_id','=',$feedback->user_id)
            ->where('feedback.product_id','=',$feedback->product_id)
            ->select('p.product_name','feedback.feedback_description','feedback.feedback_date','u.profile_pic','u.first_name','u.last_name')
            ->first();
        if($feedback_render){
            $view = view('product.feedbackRender',compact('feedback_render'));
            echo $view->render();
        }else{
            echo "error";
        }
        exit(); 
    }

    
    public function myOrder(){
        return view('myAccount.myOrder');
    }

    public function ratingadd(Request $request){
        $userid=\Auth::User()->id;
        $pro_id=$request->product_id_controller_variable;
        $rating_data=$request->rating_controller_variable;
        $rating=rating::where('user_id','=',$userid)->where('product_id','=',$pro_id)->first();
        if(empty($rating)){
            $rating=rating::create([
                'user_id' => $userid,
                'product_id' =>$pro_id,
                'rating_date' =>date("Y-m-d", time()),
                'rating' => $rating_data
            ]);
        }else{
             $rating->update([
                'user_id' => $userid,
                'product_id' =>$pro_id,
                'rating_date' =>date("Y-m-d", time()),
                'rating' => $rating_data
            ]);
        }
        if($rating){
            $rating_user=rating::where('product_id','=',$pro_id)->where('user_id','=',\Auth::User()->id)->select(DB::raw('AVG(rating) as ratings_average'))->groupBy('product_id')->first();
            $rating_count=rating::where('product_id','=',$pro_id)->select(DB::raw('AVG(rating) as ratings_average'))->groupBy('product_id')->first();
            echo json_encode(['rating_group'=>$rating_count->ratings_average,'rating_user'=>round($rating_user->ratings_average)]);exit();
        }else{
            echo "error";exit();    
        }
    }

   
    public function morefeedback($id)
    {
        $feedback=feedback::join('product as p','p.id','=','feedback.product_id')
            ->join('users as u','u.id','=','feedback.user_id')
            ->where('feedback.product_id','=',$id)
            ->select('feedback.user_id','feedback.product_id','feedback.feedback_date','feedback.feedback_description','u.first_name','u.last_name','p.product_name','u.profile_pic')
            ->paginate(12);
        return view('myAccount.morefeedback',compact('feedback'));
    }

    public function placeorder($parameter,Request $request)
    {

        $userid=\Auth::User()->id;
        $offer=offer::get();
        $city=city::get();
        $area=area::get();
        $users=User::where('id','=',\Auth::User()->id)->first();
        $wishOrCart=$parameter;
        if($wishOrCart == "cart"){
              $details=cart::join('product as p','p.id','=','cart.product_id')
            ->join('color as c','c.id','=','p.color_id')
            ->where('cart.user_id','=',\Auth::User()->id)
            ->select('p.product_name','p.id as proid','c.color_name','p.product_image','p.product_price','cart.product_qty as qty','cart.id as id')
            ->get();
            if(count($details)<=0){
                return redirect('/cart')->with('error','Cart is empty..!');
            }
        }else{
             $details=wishlist::join('product as p','p.id','=','wishlist.product_id')
                ->join('color as c','c.id','=','p.color_id')
                ->where('wishlist.user_id','=',\Auth::User()->id)
                ->select('p.product_name','p.product_image','p.product_price','c.color_name','wishlist.qty as qty','wishlist.id as id','p.id as proid')
                ->get();
            if(count($details)<=0){
                return redirect('/wishlist')->with('error','Wishlist is empty..!');
            }
        }
        return view('myAccount.placeorder',compact('city','area','users','userid','details','wishOrCart','offer'));
    }

    public function placeorderSuccess(){
        return view('static_page.placeorderSuccess');
    }
    public function getplaceorder(Request $request)
    {
        if(isset($request->Shipping)){
            $rules = array(
                'name' => 'required|regex:/^[a-zA-Z ]+$/u',
                'pincode' => 'required|regex:/^[0-9 ]+$/u|digits_between:6,6',
                'email' => 'required|email',
                'contact' => 'required|regex:/^[0-9 ]+$/u|digits_between:10,11',
                'address' => 'required|string',
                'area' =>'required',
                'shipping_address1' => 'required|string',
                'shipping_address2' =>'required|string',
                'shippingarea' =>'required',
            );
            $customeMessage = array(
                'name.required' => 'Name can not be empty.',
                'name.string' => 'Name only varchar.',
                'pincode.required' => 'Pincode can not be empty.',
                'pincode.digits_between' => 'Pincode must be 6 digits.',
                'email.required' => 'Email address can not be empty.',
                'contact.required'=>'Contact can not be empty',
                'contact.digits_between' => 'Contact must be 10 or 11 digits.',
                'address.required' => 'Billing address can not be empty.',
                'address.string' => 'Billing address must be varchar value.',
                'shipping_address1.required' => 'Shipping address can not be empty.',
                'shipping_address1.string' => 'Shipping address must be varchar value.',
                'shipping_address2.required' => 'Shipping address 1 can not be empty.',
                'shipping_address2.string' => 'Shipping address 1 must be varchar value.',
                'area.required' => 'Area can not be empty.',
                'shippingarea.required' => 'Shipping area can not be empty.',
            );
        }else{
            $rules = array(
                'name' => 'required|regex:/^[a-zA-Z ]+$/u',
                'pincode' => 'required|regex:/^[0-9 ]+$/u|digits_between:6,6',
                'email' => 'required|email',
                'contact' => 'required|regex:/^[0-9 ]+$/u|digits_between:10,11',
                'address' => 'required|string',
                'area' =>'required',
            );
            $customeMessage = array(
                'name.required' => 'Name can not be empty.',
                'name.string' => 'Name only varchar.',
                'pincode.required' => 'Pincode can not be empty.',
                'pincode.digits_between' => 'Pincode must be 6 digits.',
                'email.required' => 'Email address can not be empty.',
                'contact.required'=>'Contact can not be empty',
                'contact.digits_between' => 'Contact must be 10 or 11 digits.',
                'address.required' => 'Billing address can not be empty.',
                'address.string' => 'Billing address must be varchar value.',
                'area.required' => 'Area can not be empty.',
            );
        }
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $sales=sales::create([
            'sales_date'=>date("Y-m-d", time()),
            'ammount' =>$request->grandtotal,
            'sgst' =>'2.5',
            'cgst' =>'2.5',
            'status' =>'Complete',
            'user_id' =>\Auth::User()->id,
        ]);
        $global_sales_id=$sales->id;
        if($sales){
            foreach ($request->details_array as $key => $value) {
                $sales_details=sales_details::create([
                    'sales_id' =>$sales->id,                
                    'product_id' =>$value['proid'],
                    'qty' =>$value['qty'],
                    'price' =>$value['price'] 
                ]);
                if($request->type =="cart"){
                    try{
                        $affected = DB::delete("DELETE FROM cart WHERE id = {$value['id']}");
                    }catch(\Illuminate\Database\QueryException  $ex){
                        //return redirect('/placeorder/'.$request->type)->with('error','Cannot delete this Product');
            
                    }
                }else{
                    try{
                        $affected = DB::delete("DELETE FROM wishlist WHERE id = {$value['id']}");
                    }catch(\Illuminate\Database\QueryException  $ex){
                        //return redirect('/placeorder/'.$request->type)->with('error','Cannot delete this Product');
                    }
                }
            }
        }
        if(isset($request->Shipping)){ 
            $shipping_address=shipping_address::create([
                'address1'=>$request->shipping_address1,
                'address2' =>$request->shipping_address2,
                'user_id' =>\Auth::User()->id,
                'area_id' =>$request->shippingarea,
                'sales_id' =>$sales->id,
            ]);
        }
        $pay_id=payment_type::first();
        $sales_payment=sales_payment::create([
            'amount' => $request->grandtotal,
            'payment_date' =>date("Y-m-d", time()),
            'transaction_date' => NULL,
            'transaction_id' => NULL,
            'payment_type_id' => $pay_id->id,
            'sales_id' => $sales->id
        ]);
        $user=User::where('id','=',$sales->user_id)->first();
        $maildata['first_name']=$user['first_name'];
        $maildata['last_name']=$user['last_name'];
        $company=company::first();
        $sales=sales::join('users','users.id','=','sales.user_id')
            ->where('sales.id','=',$global_sales_id)
            ->select('users.first_name','users.last_name','users.address','sales.sales_date','sales.id as sales_id','users.id as user_id','users.email','sales.sgst','sales.cgst')
            ->first();
        $details=sales_details::join('sales as s','s.id','=','sales_details.sales_id')
                    ->join('product as p','p.id','=','sales_details.product_id')
                    ->where('sales_details.sales_id','=',$global_sales_id)
                    ->select('sales_details.sales_id','p.product_name','sales_details.price','sales_details.qty')
                    ->get();
               // dd($sales,$details,$sales);
        $type="download";

        $pdf = PDF::loadView('Invoice.Invoice',compact('company','sales','details','type'));
        $name=$sales['first_name'].$sales->id.'.pdf';
        $user_email=$sales['email'];
        try {
            Mail::send('emails.appointmentVisited', $maildata, function($message)use($user_email,$pdf,$name,$maildata) {
                $message->to($user_email)
                ->subject("Order")
                ->attachData($pdf->output(), $name);
            });
            //Mail::to($user['email'])->send(new AppointmentVisited($maildata,$files))->attachData($pdf->output(),$name);
        } catch (Exception $e) {
            return redirect('/admin/Appointments')->with('error','Appointment mail not send.');    
        }
        return redirect('/placeorderSuccess');
    }
    public function couponCodeCheck(Request $request){
        $product_id=implode(', ',$request->product_id);
        $total=$request->grandtotal;
        $pro_id=[];$pro_price_total=$offer_price_total=0;
        $offer=offer::where('couponcode','=',$request->couponcode)->select('product_id','offer_percentage')->get()->toArray();
        foreach ($offer as $key => $value) {
            if(in_array($value['product_id'],$request->product_id)){
                    $pro_id[]=$value['product_id'];
                    $off_per=$value['offer_percentage'];
            }
        }
        if(!empty($pro_id)){
            foreach ($pro_id as $key => $value) {
                $pro_price=product::where('id',$value)->select('product_price')->first();
                $pro_price_total+=$pro_price->product_price;
            }
        }else{
            return json_encode(['status'=>'error','message'=>'Offer code not valid fot this products']);
        }
        $offer_price_total=($pro_price_total*$off_per)/100;

        $grand_total=$total-$offer_price_total;
        return json_encode(['status'=>'success','message'=>'offer code successfully applied','grand_total'=>round($grand_total,2),'offper'=>$off_per,'offamo'=>round($offer_price_total,2),'subtotal'=>$total]);
        //dd($off_per,$total,$pro_price_total,$offer_price_total,$grand_total);
    }
}



