<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\models\appointment;
use App\models\customize_product;
use App\models\subcategory;
use App\User;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\AppointmentBooked;
use DB;
use Auth;

class AppointmentController extends Controller
{
     public function getAppointment()
    {
        $appointment_user=appointment::where('user_id','=',\Auth::user()->id)->count();
        if($appointment_user == 0 ){
            $appointment_time=appointment::whereDate('appointment_date','=',date("Y-m-d",time()))->where('complete_status','!=','Complete')->select('appointment_time')->get()->toArray();
            if(empty($appointment_time)){
                $appointment=[];
            }else{
                foreach ($appointment_time as $key => $value) {
                    $appointment[]=$value['appointment_time'];
                }
            }
            return view('appointment.appointment',compact('appointment'));
        }else{
            $appointment_user=appointment::where('user_id','=',\Auth::user()->id)->orderBy('created_at','DESC')->first();
            if($appointment_user->complete_status == "Complete"){
               $appointment_time=appointment::whereDate('appointment_date','=',date("Y-m-d",time()))->where('complete_status','!=','Complete')->select('appointment_time')->get()->toArray();
                if(empty($appointment_time)){
                    $appointment=[];
                }else{
                    foreach ($appointment_time as $key => $value) {
                        $appointment[]=$value['appointment_time'];
                    }
                }
                return view('appointment.appointment',compact('appointment')); 
            }else{
               // echo '<pre>';print_r($appointment_user->visited_status);exit();
                return redirect('appointmentSuccessfull');
            }
        }
    }

    public function StoreAppointment(Request $request)
    {
        if(!empty($request->all())){
            $rules = array(
                'time' => 'required|date',
                'appointmentTime' => 'required',
            );
              $customeMessage = array(
                'time.required' => 'Appointment date can not be an empty.',
                'time.date' => 'Please select proper appointment date format.',
                'appointmentTime.required' => 'Appointment time can not be empty.',
            );
            $validator = Validator::make($request->all(),$rules,$customeMessage);
            if($validator->fails())
            {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            $appointment=appointment::create([
                    'complete_status' =>'Pending',
                    'visited_status' =>'Pending',
                    'appointment_date' => Carbon::createFromFormat('d/m/Y',$request->time)->format('Y-d-m'),
                    'appointment_time' => $request->appointmentTime,
                    'user_id' =>\Auth::user()->id
                ]);
        }
        return redirect('appointmentSuccessfull');
    }
    
    public function giveAppointment(){
        $appointment_user=appointment::where('user_id','=',\Auth::user()->id)->orderBy('created_at','DESC')->first();
        if($appointment_user->complete_status == "Complete"){
            return redirect('/home');
        }else{
            $appointment=$appointment_user->id;
            $product=subcategory::get();
            $appointment_detils=customize_product::where('appointment_id','=',$appointment)
                        ->select('customize_product_name','customize_product_image','description','price','customize_product.id as customize_product_id')
                        ->get();
            return view('appointment.appointmentDetails',compact('appointment','appointment_detils','product'));        
        }
    }
    
    //status wise show the output
    public function appointmentSuccessfull(){
        $appointment=appointment::where('user_id','=',\Auth::user()->id)->orderBy('created_at','DESC')->first();
        if(!empty($appointment)){
            return view('appointment.appointmentSuccessFull',compact('appointment'));
        }else{
           return redirect('/home');
        }
    }


    //ondate change to check the slot avalible or not
    public function checkAppointmentTime(Request $request){
        $current_time=date_added();
        $disabled_slot=[];
        $current_date=cur_date_added();
    	$date=date("Y-m-d", strtotime($request->selectedDate));
    	$appointment=appointment::whereDate('appointment_date','=',$date)->where('complete_status','!=','Complete')->select('appointment_time')->get()->toArray();
    	if(empty($appointment)){
    		$appointment=[];
    	}
        if(strtotime($request->selectedDate) == strtotime($current_date)){
            foreach ($request->time_array as $key => $value) {
                if(strtotime($current_time) >= strtotime($value)){
                    $disabled_slot[]=$value;
                }    
            }
        }
        //dd($request->time_array,$disabled_slot);
    	echo json_encode(['app'=>$appointment,'disabled_slot'=>$disabled_slot]);exit();
    }

    //customize product stare
    public function storeCustomizeProduct(Request $request){
        $rules = array(
           /* 'name' => 'required|regex:/^[a-zA-Z ]+$/u|max:255',*/
            'name' => 'required',
            'pro_image' => 'required|image|mimes:jpeg,png,jpg',
            'pro_descption' => 'required'
        );
        $customeMessage = array(
            'name.required' => 'Product name can not be an empty.',
            'pro_image.required' => 'Product image can not be an empty.',
            'pro_image.mimes' => 'Product image must be an image.',
            'pro_image.image' => 'Product image must be an image.',
            'pro_descption.required' => 'Description can not be empty.',
        );
    
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $app_id=$request->app_id;
        if ($request->hasFile('pro_image')) {
             $imageName = time().'.'.request()->pro_image->getClientOriginalExtension();
            if(request()->pro_image->move(public_path('images'), $imageName)){
                $image_name=$imageName;
                $customize_product=customize_product::create([
                    'customize_product_name' => $request->name,
                    'customize_product_image' => $image_name,
                    'appointment_id' =>$app_id,
                    'description' => $request->pro_descption,
                    'price' => 0,
                ]);
            }else{
                $message="Image not uploaded please try again.";
                return redirect()->back()->withErrors($message)->withInput();
            }
        }
        return redirect('appointmentDetails');
    }

    //delete customize product
    public function deleteCustomizeProduct($customize_product_id){
        try{
            $affected = DB::delete("DELETE FROM customize_product WHERE id = ".$customize_product_id."");
            return redirect('appointmentDetails')->with('success','Customized product deleted');
        }catch(\Illuminate\Database\QueryException  $ex){
            return redirect('appointmentDetails')->with('success',"Can't be delete this product");
        }
    }


    //status change
    public function appointmentStatus($app_id){
        $app=appointment::find($app_id);
        $current_date=cur_date_added();
        $current_time=cur_date_added();
        //if($app->appointment_date > )
        if(!empty($app)){
            $app->update([
                'complete_status' =>'Confirm_Pending',
            ]);
            $user=User::find($app->user_id);
            $data['first_name']=$user['first_name'];
            $data['last_name']=$user['last_name'];
            $data['appointment_time']=date("h:i A", strtotime($app->appointment_time));
            $data['appointment_date']=$app->appointment_date;

            Mail::to($user['email'])->send(new AppointmentBooked($data));
            return redirect('appointmentSuccessfull');
        }else{
            return redirect('appointmentSuccessfull');
        }
    }

    public function myAppointment()
    {

        $userid=\Auth::User()->id;
        $users=User::where('id','=',\Auth::User()->id)->first();
        $appointment=appointment::join('users as u','u.id','=','appointment.user_id')
                ->where('appointment.user_id','=',$userid)
                ->select('appointment.id','appointment.complete_status','appointment.visited_status','appointment.appointment_date','appointment.appointment_time','appointment.user_id')
                ->get();
        return view('appointment.myAppointment',compact('userid','users','appointment'));
    }

    public function myAppointmentDetails($id)
    {
        $customize_product=customize_product::join('appointment as a','a.id','=','customize_product.appointment_id')
                    ->where('customize_product.appointment_id','=',$id)
                    ->select('customize_product.id','customize_product.customize_product_name','customize_product.customize_product_image','customize_product.appointment_id','customize_product.description','customize_product.price','customize_product.appointment_id')
                    ->get();
        return view('appointment.myAppointmentDetails',compact('customize_product'));
    }

    public function myAppointmentDetailsEdit($id)
    {
        $customize_product=customize_product::where('id','=',$id)->first();
        $product=subcategory::get();
        return view('appointment.myAppointmentDetailsUpdate',compact('customize_product','product'));
    }

    public function myAppointmentDetailsUpdate(Request $request)
    {
        if ($request->hasFile('image')) {
                $rules = array(
                    'name' => 'required',
                    'image' => 'required|image|mimes:jpeg,png,jpg',
                    'description' => 'required',
                  
                );
                $customeMessage = array(
                    'name.required' => 'customized product name can not be an empty',
                    'image.required' =>'customize product image can not be an empty',
                    'description.required' =>'description can not be an empty', 
                    
                    
                );
         }else{
               $rules = array(
                    'name' => 'required',
                    'description' => 'required',
                   
                );
                $customeMessage = array(
                    'name.required' => 'customized product name can not be an empty',
                    'description.required' =>'description can not be an empty', 
                    
                );
        }
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        if ($request->hasFile('image')) {
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            if(request()->image->move(public_path('images'), $imageName)){
                $image_name=$imageName;
            }else{
                $message="Image not uploaded please try again.";
                return redirect()->back()->withErrors($message)->withInput();
            }
        }else{
                $image_name=$request->pro_image;
        }
            
        $customize_product=customize_product::where('id','=',$request->id)->first();
        if(!empty($customize_product))
        {
            $customize_product->update([
                'customize_product_name' =>ucfirst($request->name),
                'customize_product_image' =>$image_name,
                'description' =>$request->description,
            ]);
                return redirect('/appointmentDetails/')->with('success','Updated successfully');
        }else{
            return redirect('/updateMyAppointmentDetails/'.$request->id)->with('success','Some error');
        }
    }
}