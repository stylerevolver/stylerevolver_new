<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\models\wishlist;
use App\models\cart;
use App\models\area;
use App\models\Role;
use App\models\RoleUser;
use App\models\shipping_address;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\ResendActivaionLink;
use DB;
use Auth;
use Hash;

class frontCartController extends Controller
{
    public function mycart(){
        $cart=cart::join('product as p','p.id','=','cart.product_id')
            ->join('color as c','c.id','=','cart.color_id')
            ->where('cart.user_id','=',\Auth::User()->id)
            ->select('p.product_name','p.id as product_id','p.product_image','p.product_price','c.color_name','cart.product_qty','cart.id as cart_id')
            ->get();
        return view('myAccount.cart',compact('cart'));
    }

    public function addcart(Request $request){
        $product_id=$request->product_id;
        $user_id=\Auth::User()->id;
        $count=cart::where('product_id','=',$request->product_id)->where('user_id','=',$user_id)->where('color_id',$request->color_id)->select('product_qty')->get();
        if(count($count)>0){
            $qty=$count[0]->product_qty;
            $cart=cart::where('product_id','=',$request->product_id)->where('user_id','=',$user_id)->where('color_id',$request->color_id)->first();
                $cart->update([
                'user_id' => \Auth::User()->id,
                'product_id' => $request->product_id,
                'product_qty' =>(int)$qty+1,
                'cart_date' => date("Y-m-d", time()),
                'price' => $request->price,
                'color_id' => $request->color_id
            ]);
        }else{
            $cart=cart::create([
                'user_id' => \Auth::User()->id,
                'product_id' => $request->product_id,
                'product_qty' => 1,
                'cart_date' => date("Y-m-d", time()),
                'price' => $request->price,
                'color_id' => $request->color_id
            ]);
        }
        if($cart){
            echo $cart->product_id;
        }else{
            echo "error";
        }
        exit();
    }

    public function removeCart($id){
        try{
            $affected = DB::delete("DELETE FROM cart WHERE id = {$id}");
             echo $id;exit();
            //return redirect('/cart')->with('success','Product deleted successfully');
        }catch(\Illuminate\Database\QueryException  $ex){
            echo "error";exit();
            //return redirect('/cart')->with('error','Cannot delete this Product');
        }
    }

    public function upadatecartqty(Request $request){
        $cart=cart::where('id','=',$request->cart_id)->first();
        if($cart){
            $cart->update([
                'product_qty'=>$request->qty,
            ]);
        echo json_encode(['id' => $cart->id , 'product_qty' => $cart->product_qty]);exit();
        }else{
            echo "error";exit();
        }
    }
}
