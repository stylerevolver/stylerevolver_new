<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Routing\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Mail\welcomeMail;
use App\Mail\complainMail;
use App\Mail\contactUs;
use App\models\complain;
use App\models\sales;
use App\models\company;
use App\models\product;
use App\models\offer;
use App\models\sales_details;
use App\models\subcategory;
use App\models\category;
use DB;
use PDF;
use App\User;



class StaticPageController extends Controller
{
    function about_us(){
    	return view('static_page.about');
    }
    function checkout(){
    	return view('product.checkout');
    }
    
    function home(){
        //first div for 3 product
        $product=product::where('product_qty','>',0)->where('featured_filed','=','Yes')->get();
        if(count($product) <= 2){
            $product=product::where('product_qty','>',0)->take(3)->inRandomOrder()->get();
        }
        $new_product=product::where('product_qty','>',0)->orderBy('created_at','DESC')->get();
        $subcategory=subcategory::take(2)->inRandomOrder()->get()->toArray();

        //get the random category and realted 3 random product
        //$category=subcategory::whereIn('category_id',[$category['']])->inRandomOrder()->get()->toArray();
        $product_category=product::where('subcategory_id','=',$subcategory[0]['id'])->take(4)->inRandomOrder()->get();
        $product_category1=product::where('subcategory_id','=',$subcategory[1]['id'])->take(4)->inRandomOrder()->get();
        /*$product_category=[];
        $product_category1=[];*/
        return view('layouts.index',compact('product','new_product','subcategory','product_category','product_category1'));
    }
    public function myaccount(){
        return view('myAccount.myAccount');
    }

    function myOrderShow(){
        $userid=\Auth::User()->id;
        $users=User::where('id','=',\Auth::User()->id)->first();
        $sales=sales::join('users as u','u.id','=','sales.user_id')
                ->where('sales.user_id','=',$userid)
                ->select('sales.id','sales.sales_date','sales.ammount')
                ->get();
        return view('myAccount.myOrder',compact('sales','userid','users'));
    }

    function myOrderDetails($id){
        $details=sales_details::join('sales as s','s.id','=','sales_details.sales_id')
                    ->join('product as p','p.id','=','sales_details.product_id')
                    ->where('sales_details.sales_id','=',$id)
                    ->select('sales_details.sales_id','p.product_name','p.product_image','sales_details.price','sales_details.qty')
                    ->get();
        return view('myAccount.myOrderDetails',compact('details'));
    }

    public function invoiceFront($id){
        $company=company::first();
        $sales=sales::join('users','users.id','=','sales.user_id')
            ->where('sales.id','=',$id)
            ->select('users.first_name','users.last_name','users.address','sales.sales_date','sales.id as sales_id','users.id as user_id','users.email','sales.sgst','sales.cgst')
            ->first();
        $details=sales_details::join('sales as s','s.id','=','sales_details.sales_id')
                    ->join('product as p','p.id','=','sales_details.product_id')
                    ->where('sales_details.sales_id','=',$id)
                    ->select('sales_details.sales_id','p.product_name','sales_details.price','sales_details.qty')
                    ->get();
        $type="download";
        $pdf = PDF::loadView('Invoice.Invoice',compact('company','sales','details','type'));  
        return $pdf->download('Invoice.pdf');
    }
    
    public function viewinvoiceFront($id){
        $company=company::first();
        $sales=sales::join('users','users.id','=','sales.user_id')
            ->where('sales.id','=',$id)
            ->select('users.first_name','users.last_name','users.address','sales.sales_date','sales.id as sales_id','users.id as user_id','users.email','sales.sgst','sales.cgst')
            ->first();
        $details=sales_details::join('sales as s','s.id','=','sales_details.sales_id')
                    ->join('product as p','p.id','=','sales_details.product_id')
                    ->where('sales_details.sales_id','=',$id)
                    ->select('sales_details.sales_id','p.product_name','sales_details.price','sales_details.qty')
                    ->get();
        $type="download";
        $pdf = PDF::loadView('Invoice.Invoice',compact('company','sales','details','type'));  
        return $pdf->stream('Invoice.pdf');
    }
    function complain($id,Request $request){
        $product=sales_details::join('product','product.id','=','sales_details.product_id')
                ->where('sales_details.sales_id','=',$id)
                ->select('product.product_name','product.id as pro_id','sales_details.sales_id','sales_details.product_id')
                ->get();
        return view('complain',compact('product','id'));
    }

    function complainSubmit(Request $request){
        $rules = array(
            'msg' => 'required',
            'name' => 'required'
        );
        $customeMessage = array(
            'name.required' => 'Please select product.',
            'msg.required' => "This field can't be empty.",
        );
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $complain=complain::create([
            'complain_date' =>date("Y-m-d", time()),
            'description'   =>$request->msg,
            'complain_status' =>'Pending',
            'user_id' =>\Auth::User()->id,
            'sales_id' =>$request->sales_id,
            'product_id' =>$request->name,
        ]);
        if($complain){
            $product=product::where('id','=',$request->name)->value('product_name');
            $data['first_name']=\Auth::User()->first_name;
            $data['last_name']=\Auth::User()->last_name;
            $data['pro_name']=$product;
            $data['msg']=$request->msg;
            try {
                Mail::to(\Auth::User()->email)->send(new complainMail($data));
            } catch (Exception $e) {
                //return redirect('/myOrder')->with('error','Area Created successfully');    
            }
            return redirect('/myOrder')->with('success','Complain successfully booked..!');    
        }else{
            return redirect('/myOrder')->with('error','Plase try again..!');    
        }

    }

    public function myOfferShow()
    {
        $today_date=cur_date_added();
        $code=offer::whereDate('end_date','>=',$today_date)->select(DB::raw('count(id)'),'couponcode')->groupBy('couponcode')->get();
        /*foreach ($code as $key => $value) {
        }*/
            $offer=offer::whereDate('end_date','>=',$today_date)->paginate(12);
        return view('product.myOfferShow',compact('offer'));
    }

    function contactUsMail(Request $request){
        $rules = array(
            'name' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'message' => 'required',
        );
        $customeMessage = array(
            'name.required' => "This field can't be empty.",
            'email.required' => "This field can't be empty.",
            'subject.required' => "This field can't be empty.",
            'message.required' => "This field can't be empty.",
        );
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $data['name']=$request->name;
        $data['subject']=$request->subject;
        $data['email']=$request->email;
        $data['message']=$request->message;
        try {
            Mail::to('stylerevolvernew3999@gmail.com')->send(new contactUs($data));
            return redirect('/contact')->with('success','Successfully Done..!');    
        } catch (Exception $e) {
            return redirect('/contact')->with('error','Please try again');    
        }
    }
}
