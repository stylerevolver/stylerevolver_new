<?php

namespace App\Http\Controllers;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\models\wishlist;
use App\models\cart;
use App\models\area;
use App\models\Role;
use App\models\RoleUser;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\ResendActivaionLink;
use DB;
use Auth;
use Hash;
class userController extends Controller
{
    
    public function myprofileupdate(Request $request){
          
        $area = area::get();
        $profileupdate=User::where('id','=',\Auth::User()->id)->first();
        return view('myAccount.profileupdate',compact('profileupdate','area'));
    }

   
    public function verifyuser($token){
        try{
            DB::table('users')->where('verify_tokan','=',$token)->update([
                'email_verified_at' => Carbon::now()->toDateTimeString(),
            ]);
            return redirect('/login')->with('success', 'Your account verified successfully');
        }
        catch (Exception $e)
        {
            return redirect('/login')->with('msg', 'Your account not verified');
        }
    }

    public function resendlink($id){
        $user=User::where('id','=',$id)->first();
        $user->verify_tokan = str_random(50);
        $user->save();
        $data['verify_tokan']=$user->verify_tokan;
        Mail::to($user->email)->send(new ResendActivaionLink($data));
         return redirect()
                    ->back()
                    ->withErrors(['message' => 'Link send successfully, please check mail...!']);
    }
    public function profileupdate(Request $request){
        $rules = array(
            /*'username' => 'required|string|max:255|regex:/^[a-zA-Z ]+$/u|unique:users,name,'.$request->user_id,*/
            'first_name' => 'required|string|max:255|regex:/^[a-zA-Z ]+$/u',
            'lname' => 'required|string|max:255|regex:/^[a-zA-Z ]+$/u',
            'email' => 'required|email|max:255|unique:users,email,'.$request->user_id,
            'phone' => 'required|regex:/^[0-9 ]+$/u|digits_between:10,11|unique:users,contact_no,'.$request->user_id,
            'gender' => 'required',
            'dob'=>'required||date_format:Y-m-d|before:13 years ago',
            'address' => 'required',
            'areaname' =>'required',
            's_q' =>'required',
            's_a' =>'required',
        );
        $customeMessage = array(
           /* 'username.required' => 'User name can not be an empty.',
            'username.string' => 'User name is must be in string value',
            'username.unique' => 'User name must be unique',*/
            'first_name.required' => 'First name can not be an empty',
            'first_name.string' => 'First name is must be in string value',
            'lname.required' => 'Last name can not be an empty',
            'lname.string' => 'Last name is must be in string value',
            'email.required' => 'Email can not be an empty.',
            'email.unique' => 'Email is must be unique',
            'phone.required' => 'Phone number can not be an empty',
            'phone.regex' => 'Invalid value',
            'gender.required' => 'Gender can not be an empty',
            'dob.required' => 'Date Of Birth can not be an empty',
            'address.required' => 'Address can not be an empty',
            'areaname.required' => 'Area name can not be an empty',
            's_q.required' => 'Security Question can not be an empty',
            's_a.required' => 'Security Answer can not be an empty',

        );
        $validator = Validator::make($request->all(),$rules,$customeMessage);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $profileupdate=User::where('id','=',$request->user_id)->first();
        if(!empty($profileupdate))
        {
            $profileupdate->update([
                'name' => "-",
                'first_name' => $request->first_name,
                'last_name' => $request->lname,
                'email' => $request->email,
                'contact_no' => $request->phone,
                'gender' => $request->gender,
                'dob' => $request->dob,
                'address' => $request->address,
                'area_id' =>(int)$request->areaname,
                'security_quetion' => $request->s_q,
                'security_answer' => $request->s_a,
            ]);

            return redirect('/myprofile')->with('success','Your profile is successfully updated');
        }else{
            return redirect('/myprofile')->with('warning','You can not update your profile');
        }
    }

    public function myupdatepropic(){

        $myprofileupdate=User::where('id','=',\Auth::User()->id)->first();
        return view('myAccount.updatepropic',compact('myprofileupdate'));
    }

    public function updatepropic(Request $request)
    {
        if ($request->hasFile('image')){
            $rules = array(
                'image' => 'required|image|mimes:jpeg,png,jpg',
            );
            $customeMessage = array(
                'image.required' => 'Profile image can not be an empty.',
            );
            $validator = Validator::make($request->all(),$rules,$customeMessage);
            if($validator->fails())
            {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            
            $profilepic = time().'.'.request()->image->getClientOriginalExtension();
            if(request()->image->move(public_path('profilephoto'), $profilepic)){
                $profile_pic=$profilepic;
            }else{
                $message="Profile pic not uploaded please try again.";

                return redirect()->back()->withErrors('error',$message)->withInput();
            }
        }else{
            $profilepic=$request->profile_pic;
        }
        $myprofileupdate=User::where('id','=',\Auth::user()->id)->first();
        if(!empty($myprofileupdate))
        {
            $myprofileupdate->update([
                'profile_pic' => $profilepic,
            ]);
            return redirect('updatepropic')->with('success','Your profile pic is updated successfully');
        }else{
            return redirect('updatepropic')->with('success','You can not update your profile pic');
        }
    } 
}
