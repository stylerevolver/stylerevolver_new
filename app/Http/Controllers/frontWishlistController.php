<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\feedback;
use App\models\wishlist;
use App\models\cart;
use App\models\rating;
use App\models\city;
use App\models\area;
use App\User;
use Illuminate\Support\Facades\Validator;
use DB;

class frontWishlistController extends Controller
{

    public function wishlist(){

        $wishlist=wishlist::join('product as p','p.id','=','wishlist.product_id')
                ->join('color as c','c.id','=','wishlist.color_id')
                ->where('wishlist.user_id','=',\Auth::User()->id)
                ->select('p.product_name','p.product_image','p.product_price','wishlist.qty','c.color_name','wishlist.id as wishid','p.id as proid')
                ->get();
        return view('myAccount.wishlist',compact('wishlist'));
    }

    public function wishlisttocart(Request $request){
        $id=$request->id;
        $wishlist=wishlist::where('id','=',$request->id)->first();
        $cart_count=cart::where('product_id','=',$wishlist->product_id)->where('user_id','=',$wishlist->user_id)->where('color_id','=',$wishlist->color_id)->count();
        if($cart_count > 0){
        	$cart=cart::where('product_id','=',$wishlist->product_id)->where('user_id','=',$wishlist->user_id)->where('color_id','=',$wishlist->color_id)->first();
        	$qty=$cart->product_qty;
        	$qty=$qty+$wishlist->qty;
       		$cart->update([
                'user_id' => \Auth::User()->id,
                'product_id' => $wishlist->product_id,
                'product_qty' => $qty,
                'cart_date' => date("Y-m-d", time()),
                'price' => $wishlist->price,
                'color_id' =>$wishlist->color_id,
            ]);
       	}else{
	        $cart=cart::create([
                'user_id' => \Auth::User()->id,
                'product_id' => $wishlist->product_id,
                'product_qty' => $wishlist->qty,
                'cart_date' => date("Y-m-d", time()),
                'price' => $wishlist->price,
                'color_id' =>$wishlist->color_id,
            ]);
        }
        if($cart){
            try{
                $affected = DB::delete("DELETE FROM wishlist WHERE id = {$id}");
                echo $request->id;exit();
            }catch(\Illuminate\Database\QueryException  $ex){
                echo "error";exit();
            }
        }
    }

    public function addWishlist(Request $request){
        $product_id=$request->product_id;
        $user_id=\Auth::User()->id;
        $count=wishlist::where('product_id','=',$request->product_id)->where('user_id','=',$user_id)->where('color_id','=',$request->color_id)->select('qty')->get();
        if(count($count)>0){
            $qty=$count[0]->qty;
            $wishlist=wishlist::where('product_id','=',$request->product_id)->where('user_id','=',$user_id)->where('color_id','=',$request->color_id)->first();
                $wishlist->update([
                'user_id' => \Auth::User()->id,
                'product_id' => $request->product_id,
                'qty' =>(int)$qty+1,
                'wishlist_date' => date("Y-m-d", time()),
                'price' => $request->price,
                'color_id' =>$request->color_id
            ]);
        }else{
            $wishlist=wishlist::create([
                'user_id' => \Auth::User()->id,
                'product_id' => $request->product_id,
                'wishlist_date' => date("Y-m-d", time()),
                'qty' =>1,
                'price' => $request->price,
                'color_id' =>$request->color_id
            ]);
        }
        if($wishlist){
            echo $wishlist->product_id;exit();
        }else{
            echo "error";exit();
        }
    }

    public function removeWishlist($id){
        try{
            $affected = DB::delete("DELETE FROM wishlist WHERE id = {$id}");
            echo $id;exit();
        }catch(\Illuminate\Database\QueryException  $ex){
            echo "error";exit();
            //return redirect('/wishlist')->with('error','Cannot delete this Product');
        }
    }

    public function updateqty(Request $request){
        $wishlist=wishlist::where('id','=',$request->wishlist_id)->first();
        if($wishlist){
            $wishlist->update([
                'qty'=>$request->qty,
            ]);
        	echo json_encode(['id' => $wishlist->id , 'product_qty' => $wishlist->qty]);exit();
        }else{
            echo "error";exit();
        }
    }
}
