<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Mail\Mailable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;
use App\Mail\UserBirthday;
use DB;
use Mail;
use App\User;

class BirthdayMail extends Command implements ShouldQueue
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'incevio:birthday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send birthday mail.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $month = date('m',strtotime(Carbon::now()));
        $date = date('d',strtotime(Carbon::now()));
        $user= User::whereMonth('dob', '=', $month)->whereDay('dob', '=', $date)->get();
        foreach ($user as $key => $customer) {
            Mail::to($customer->email)->send(new UserBirthday($customer));
        }
    }
}
