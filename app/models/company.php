<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class company extends Model
{
    protected $table='company';
    protected $fillable=['company_name','company_address','company_email','company_contact','area_id'];
}
