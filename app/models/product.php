<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class product extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table="product";
    protected $fillable=['product_name','product_image','product_price','product_qty','subcategory_id','size_id','company_id','color_id','featured_filed','description'];

    public function color(){
    	return $this->belongsTo('App\models\color','id','color_id');
    }
    public function category(){
    	return $this->belongsTo('App\models\category','subcategory_id');
    }
}
