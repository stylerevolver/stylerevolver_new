<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    protected $table='category';
    protected $fillable= ['category_name'];

    public function subcategory(){
    	return $this->hasMany('App\models\subcategory','category_id');
    }
}
