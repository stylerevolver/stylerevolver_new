<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class size_chart extends Model
{
    protected $table="size_chart";
    protected $fillable=['size_name','length','waist','chest','hips','subcategory_id'];
}
