<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class pages extends Model
{
    protected $table='pages';
    protected $fillable=['name','description','images'];
}
