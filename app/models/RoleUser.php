<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class RoleUser extends \Eloquent implements Auditable
{
	use \OwenIt\Auditing\Auditable;

    protected $guarded =[];
}

