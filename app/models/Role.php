<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustRole;
use OwenIt\Auditing\Contracts\Auditable;

class Role extends EntrustRole
{
    protected $fillable = [
        'name', 'display_name', 'description',
    ];

    protected $auditInclude = [
        'name', 'display_name', 'description',
    ];
    
	public function users()
    {
        return $this->belongsToMany('App\User');
    }
}