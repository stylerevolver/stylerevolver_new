<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class sales_details extends Model
{
    protected $table='sales_details';
    protected $fillable=['sales_id','product_id','qty','price','customize_product_id','color_id'];

    public function user(){
		return $this->hasOne('App\User','id','user_id');
    }

    public function product(){
		return $this->belongsTo('App\models\product','id','product_id');
    }
}
