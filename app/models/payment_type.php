<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class payment_type extends Model
{
    protected $table='payment_type';
    protected $fillable=['payment_type'];
}
