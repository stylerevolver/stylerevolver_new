<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class contectUs extends Model
{
    protected $table='contactUs';
    protected $fillable=['name','email','subject','message'];
}
