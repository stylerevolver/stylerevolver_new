<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class customize_product_details extends Model
{
    protected $table="customize_product_details";
    protected $fillable=['description','price','customize_product_id'];
}
