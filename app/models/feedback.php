<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class feedback extends Model
{
	protected $table='feedback';
    protected $fillable=['user_id','product_id','feedback_description','feedback_date'];

    public function user(){
		return $this->hasOne('App\User','id','user_id');
    }

    public function product(){
		return $this->hasOne('App\models\product','id','product_id');
    }
}
