<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class sales extends Model
{
    protected $table="sales";
    protected $fillable=['sales_date','delivery_address','delivery_date','ammount','delivery_charges','sgst','cgst','user_id','offer_id','offer_amount','status'];

    public function user(){
		return $this->hasOne('App\User','id','user_id');
    }
}
