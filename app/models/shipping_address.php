<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class shipping_address extends Model
{
    protected $table='shipping_address';
    protected $fillable=['address1','address2','user_id','area_id','latitude','longitude','sales_id'];
}
