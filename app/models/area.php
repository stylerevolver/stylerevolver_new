<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class area extends Model
{
    protected $table="area";
    protected $fillable=['area_name','pincode','city_id'];
}
