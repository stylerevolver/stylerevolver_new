<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class color extends Model
{
    protected $table='color';
    protected $fillable=['color_name','color_image','color_code'];
}
