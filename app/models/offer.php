<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class offer extends Model
{
    use SoftDeletes;

	protected $dates = ['deleted_at'];
    protected $table="offer";
    protected $fillable=['offer_percentage','start_date','end_date','product_id','couponcode'];

    public function product(){
		return $this->hasOne('App\models\product','id','product_id');
    }
}
