<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class product_image extends Model
{
    protected $table="product_images";
    protected $fillable=['image_name','product_id'];
}
