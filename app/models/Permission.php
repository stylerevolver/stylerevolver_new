<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustPermission;
use OwenIt\Auditing\Contracts\Auditable;

class Permission extends EntrustPermission implements Auditable
{
    use \OwenIt\Auditing\Auditable;
}
