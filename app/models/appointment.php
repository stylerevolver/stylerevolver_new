<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class appointment extends Model
{
    protected $table='appointment';
    protected $fillable=['complete_status','visited_status','appointment_date','appointment_time','user_id'];

    public function appointmentUser()
    {
    	return $this->hasOne('App\User','id','user_id');
   }
   
}
