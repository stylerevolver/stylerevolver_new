<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class cart extends Model
{
    protected $table='cart';
    protected $fillable=['cart_date','product_qty','price','product_id','user_id','color_id'];
}
