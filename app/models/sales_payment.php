<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class sales_payment extends Model
{
    protected $table='sales_payment';
    protected $fillable=['amount','payment_type','transaction_date','transaction_id','payment_type_id','sales_id','payment_date'];
}
