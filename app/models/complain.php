<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class complain extends Model
{
	protected $dates = ['deleted_at'];
    protected $table='complain';
    protected $fillable=['complain_date','description','complain_status','user_id','sales_id','product_id'];

    public function user(){
		return $this->hasOne('App\User','id','user_id');
    }

    public function product(){
		return $this->hasOne('App\models\product','id','product_id');
    }
}
