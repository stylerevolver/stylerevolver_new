<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class rating extends Model
{
    protected $table='rating';
    protected $fillable=['user_id','product_id','rating','rating_date'];

    public function user(){
		return $this->hasOne('App\User','id','user_id');
    }

    public function product(){
		return $this->hasOne('App\models\product','id','product_id');
    }
}
