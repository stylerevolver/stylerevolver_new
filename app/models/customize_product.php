<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class customize_product extends Model
{
    protected $table='customize_product';
    protected $fillable=['customize_product_name','customize_product_image','appointment_id','description','price'];
}
