<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class wishlist extends Model
{
    protected $table='wishlist';
    protected $fillable=['wishlist_date','qty','price','product_id','user_id','color_id'];
}
