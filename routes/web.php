<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','StaticPageController@home');

Auth::routes(['verify' => true]);
Route::get('/get-verification-code/{token}','userController@verifyuser');
Route::get('/home_auth', 'HomeController@index')->name('home');
Route::get('/home','StaticPageController@home');
Route::get('/about','StaticPageController@about_us');
Route::get('/contact','contactUsController@contact_us');
Route::post('/contactUs','contactUsController@store');
Route::get('/checkout/','StaticPageController@checkout');


Route::get('/category/{id}','productController@show');
Route::get('/product/{id}','productController@singlePage');

Route::get('/resendActivationLink/{id}','userController@resendlink');
Route::post('/checkAppointmentTime','AppointmentController@checkAppointmentTime');
Route::post('/contactUsMail','StaticPageController@contactUsMail');
    //myOffer
Route::get('/Offer','StaticPageController@myOfferShow');

Route::get('/invoiceFront/{id}','StaticPageController@invoiceFront');
Route::get('/viewinvoiceFront/{id}','StaticPageController@viewinvoiceFront');
Route::get('/moreFeedBack/{id}','reviewController@morefeedback');

//my account
Route::group( ['middleware' => 'auth' ], function()
{ 

    //wishlist
    Route::get('/wishlist','frontWishlistController@wishlist');
	Route::post('/wishlisttocart','frontWishlistController@wishlisttocart');
	Route::post('/wishlistadd','frontWishlistController@addWishlist');
	Route::get('/removeWishlist/{id}','frontWishlistController@removeWishlist');
    Route::post('/upadatewishlistqty/','frontWishlistController@updateqty');

    //cart
    Route::get('/cart','frontCartController@mycart');
	Route::post('/addcart','frontCartController@addcart');
	Route::get('/removeCart/{id}','frontCartController@removeCart');
    Route::post('upadatecartqty','frontCartController@upadatecartqty');
    //feedback
	Route::get('/feedbackadd','reviewController@addfeedback');

    Route::get('/placeorder/{parameter}','reviewController@placeorder');
    Route::get('/placeorderSuccess','reviewController@placeorderSuccess');
    Route::post('/placeorder','reviewController@getplaceorder');
    Route::post('/couponCodeCheck','reviewController@couponCodeCheck');

	Route::get('/settings','StaticPageController@myaccount');
    Route::get('/myprofile','userController@myprofileupdate');
    Route::post('/myprofile','userController@profileupdate');
    
    //change password
    Route::get('changePassword', 'changePasswordController@index');
	Route::post('changePassword', 'changePasswordController@store')->name('changePassword');
    
    //rating
    Route::post('ratingadd','reviewController@ratingadd');

    //userprofilepic
    Route::get('/updatepropic','userController@myupdatepropic');
    Route::post('/updatepropic','userController@updatepropic');
    

    Route::get('/complain/{id}','StaticPageController@complain');
    Route::post('/complainSubmit/','StaticPageController@complainSubmit');

    //appointment
    Route::get('/appointment','AppointmentController@getAppointment');
    Route::post('/appointmentStore','AppointmentController@StoreAppointment');

    //user wise appoinment
    Route::get('/myAppointment','AppointmentController@myAppointment');
    Route::get('/myAppointmentDetails/{id}','AppointmentController@myAppointmentDetails');
    Route::get('/updateMyAppointmentDetails/{id}','AppointmentController@myAppointmentDetailsEdit');
    Route::post('/updateMyAppointmentDetails','AppointmentController@myAppointmentDetailsUpdate');


    Route::get('/appointmentDetails','AppointmentController@giveAppointment');
    Route::get('/appointmentSuccessfull','AppointmentController@appointmentSuccessfull')->name('appointmentSuccessfull');
    Route::get('/deleteCustomizeProduct/{customize_id}','AppointmentController@deleteCustomizeProduct');
    Route::get('/appointmentStatus/{app_id}','AppointmentController@appointmentStatus');

    //customize product
    Route::post('/store_CustomizeProduct','AppointmentController@storeCustomizeProduct');
    
    //my order
    Route::get('/myOrder','StaticPageController@myOrderShow');
    Route::get('/myOrderDetails/{id}','StaticPageController@myOrderDetails');


});


