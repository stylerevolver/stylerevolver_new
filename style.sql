-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 26, 2022 at 03:18 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `style1`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_picture` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `profile_picture`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Suresh Chauhan', 'stylerevolvernew3999@gmail.com', '$2y$10$8LrWLfrKl/BM9AkdS0fEMevPdG0aWCeEOHvZqRvRlSYcCWtFV4bz2', '1584884449.jpg', '', '2020-02-29 00:25:59', '2020-04-02 05:48:39'),
(2, 'Super Admin', 'mittusinojiya38@gmail.com', '$2y$10$2X0PxnK3594cpxzgV87qqew0jZw2k4PY3CCSjxicOjRmUBcKxTwNO', NULL, '3E0oS2UOTlOtJXLN7c6BFOie65ij8PIi9hItIfy4w17L3zK6Vz', '2020-04-02 07:52:04', '2020-04-06 23:53:29'),
(3, 'Suresh Chauhan', 'styleRevolver3999@gmail.com', '$2y$10$zqSIqYbqnBJBaQ9CJL8r3euRfTmNnWMxWCam0gp.27p2t83StQHay', NULL, NULL, '2020-06-15 23:26:33', '2020-06-15 23:26:33');

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE `appointment` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `complete_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `visited_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `appointment_date` date NOT NULL,
  `appointment_time` time NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `appointment`
--

INSERT INTO `appointment` (`id`, `complete_status`, `visited_status`, `appointment_date`, `appointment_time`, `user_id`, `created_at`, `updated_at`) VALUES
(18, 'Complete', 'Visited', '2020-03-11', '09:00:00', 9, '2020-03-10 10:21:25', '2020-03-10 10:26:49'),
(19, 'Complete', 'Visited', '2020-03-11', '20:00:00', 4, '2020-03-10 21:28:22', '2020-03-10 21:29:33'),
(20, 'Complete', 'Visited', '2020-03-11', '19:00:00', 5, '2020-03-11 01:46:31', '2020-03-16 09:45:43'),
(21, 'Complete', 'Visited', '2020-03-12', '13:00:00', 6, '2020-03-11 01:48:30', '2020-03-15 01:06:33'),
(22, 'Complete', 'Visited', '2020-03-12', '12:00:00', 4, '2020-03-11 02:59:34', '2020-03-16 09:45:50'),
(23, 'Complete', 'Visited', '2021-03-04', '20:00:00', 6, '2020-03-16 08:18:14', '2020-03-16 08:51:17'),
(25, 'Complete', 'Visited', '2021-03-05', '16:00:00', 6, '2020-03-16 08:57:45', '2020-03-16 09:45:35'),
(26, 'Complete', 'Visited', '2021-03-08', '16:00:00', 4, '2020-03-19 09:26:33', '2020-03-19 09:27:57'),
(27, 'Complete', 'Visited', '2021-03-08', '13:00:00', 4, '2020-03-19 09:30:30', '2020-03-19 09:32:16'),
(28, 'Confirm_Pending', 'Pending', '2024-02-01', '09:00:00', 13, '2022-02-24 00:43:38', '2022-02-24 06:13:48'),
(29, 'Pending', 'Pending', '2023-02-12', '13:00:00', 13, '2022-02-24 01:04:46', '2022-02-24 01:04:46'),
(30, 'Pending', 'Pending', '2023-02-12', '13:00:00', 13, '2022-02-24 01:25:24', '2022-02-24 01:25:24');

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `area_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pincode` int(11) NOT NULL,
  `city_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`id`, `area_name`, `pincode`, `city_id`, `created_at`, `updated_at`) VALUES
(1, 'Shahibag', 380003, 1, '2020-02-29 00:32:41', '2020-02-29 00:32:41'),
(2, 'NavaVadaj', 380013, 1, '2020-02-29 00:33:05', '2020-02-29 00:33:05'),
(3, 'Bapunagar', 380024, 1, '2020-02-29 00:34:44', '2020-02-29 00:34:44'),
(4, 'Nikol', 382350, 3, '2020-02-29 00:36:22', '2020-03-10 06:38:01');

-- --------------------------------------------------------

--
-- Table structure for table `carolous`
--

CREATE TABLE `carolous` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `imagename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` bigint(20) NOT NULL,
  `imagepath` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cart_date` date NOT NULL,
  `product_qty` bigint(20) NOT NULL,
  `price` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `color_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `cart_date`, `product_qty`, `price`, `product_id`, `user_id`, `created_at`, `updated_at`, `color_id`) VALUES
(6, '2020-03-04', 1, 899, 30, 1, '2020-03-04 07:33:51', '2020-03-04 07:33:51', 3),
(9, '2020-03-07', 2, 599, 40, 2, '2020-03-07 03:34:06', '2020-03-07 03:34:07', 2),
(22, '2020-03-16', 2, 799, 15, 4, '2020-03-16 10:39:34', '2020-03-16 10:39:34', 10),
(24, '2020-03-17', 1, 999, 3, 6, '2020-03-17 05:30:38', '2020-03-17 05:30:38', 9),
(25, '2020-04-10', 1, 799, 45, 10, '2020-04-10 09:16:43', '2020-04-10 09:16:43', 6);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category_name`, `created_at`, `updated_at`) VALUES
(1, 'Men', '2020-02-29 00:50:42', '2020-02-29 00:51:04'),
(2, 'Women', '2020-02-29 00:50:52', '2020-02-29 00:51:35');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `city_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `city_name`, `created_at`, `updated_at`) VALUES
(1, 'Ahmedabad', '2020-02-29 00:30:50', '2020-02-29 00:30:50'),
(2, 'Surat', '2020-02-29 00:30:56', '2020-02-29 00:30:56'),
(3, 'Bhavnagar', '2020-02-29 00:31:07', '2020-02-29 00:31:07'),
(4, 'Baroda', '2020-02-29 00:31:22', '2020-02-29 00:31:22');

-- --------------------------------------------------------

--
-- Table structure for table `color`
--

CREATE TABLE `color` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `color_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `color_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `color`
--

INSERT INTO `color` (`id`, `color_name`, `color_image`, `created_at`, `updated_at`, `color_code`) VALUES
(1, 'Lightseagreen', '1582958013.jpg', '2020-02-29 01:03:33', '2020-03-16 10:04:53', '#20b2aa'),
(2, 'Deeppink', '1582958041.jpg', '2020-02-29 01:04:01', '2020-03-16 10:06:05', '#ff1493'),
(3, 'Coral', '1582958076.jpg', '2020-02-29 01:04:36', '2020-03-16 10:08:31', '#fe9d7c'),
(4, 'Lightpink', '1582958104.jpg', '2020-02-29 01:05:04', '2020-03-16 10:11:36', '#ff8ec7'),
(5, 'Yellow', '1582958292.jpg', '2020-02-29 01:08:12', '2020-03-16 10:12:55', '#ffff00'),
(6, 'Plum', '1582958337.jpg', '2020-02-29 01:08:57', '2020-03-16 10:14:10', '#d2a8f4'),
(7, 'Darkred', '1582958383.jpg', '2020-02-29 01:09:43', '2020-03-16 10:16:12', '#8B0000'),
(8, 'Maroon', '1582958494.jpg', '2020-02-29 01:11:34', '2020-03-16 10:18:36', '#800000'),
(9, 'Silver', 'color.jpg', '2020-02-29 02:02:09', '2020-03-16 10:28:24', '#c0c0c0'),
(10, 'Navy', 'color.jpg', '2020-02-29 06:39:15', '2020-03-16 10:29:46', '#000080'),
(11, 'Saddlebrown', 'color.jpg', '2020-02-29 06:39:30', '2020-03-16 10:37:16', '#8B4513'),
(12, 'Firebrick', 'color.jpg', '2020-02-29 06:39:44', '2020-03-16 10:36:31', '#b22222'),
(13, 'Darkmagenta', 'color.jpg', '2020-02-29 06:43:36', '2020-03-16 10:35:12', '#94239a'),
(14, 'Limegreen', 'color.jpg', '2020-02-29 06:43:56', '2020-03-16 10:33:57', '#32cd32'),
(15, 'Darkblue', 'color.jpg', '2020-03-01 02:54:45', '2020-03-16 10:32:23', '#004080'),
(16, 'Cyan', 'color.jpg', '2020-03-10 06:50:53', '2020-03-16 10:30:44', '#00ffff'),
(17, 'Black', 'color.jpg', '2020-03-16 09:49:32', '2020-03-16 10:12:22', '#000000');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_contact` bigint(20) NOT NULL,
  `area_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `company_name`, `company_address`, `company_email`, `company_contact`, `area_id`, `created_at`, `updated_at`) VALUES
(1, 'Dharti Tailor Selection', '31/2, RamColony, \r\nNear DenaBank, \r\nBhimjipura, \r\nNavaVadaj', 'stylerevolvernew3999@gmail.com', 9662794697, 2, '2020-02-29 00:39:16', '2020-02-29 00:40:31');

-- --------------------------------------------------------

--
-- Table structure for table `complain`
--

CREATE TABLE `complain` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `complain_date` date NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `complain_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `sales_id` bigint(20) UNSIGNED DEFAULT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `complain`
--

INSERT INTO `complain` (`id`, `complain_date`, `description`, `complain_status`, `user_id`, `sales_id`, `product_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '2020-03-10', 'Damaged product', 'Complate', 9, 30, 3, '2020-03-10 10:17:49', '2020-03-10 10:18:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contactus`
--

CREATE TABLE `contactus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customize_product`
--

CREATE TABLE `customize_product` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customize_product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customize_product_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `appointment_id` bigint(20) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customize_product`
--

INSERT INTO `customize_product` (`id`, `customize_product_name`, `customize_product_image`, `appointment_id`, `description`, `price`, `created_at`, `updated_at`) VALUES
(20, 'Gown', '1583855636.jpg', 18, 'blue gown', 2000, '2020-03-10 10:23:56', '2020-03-10 10:26:05'),
(21, 'Gown', '1583895526.jpg', 19, 'abcd', 0, '2020-03-10 21:28:46', '2020-03-10 21:29:00'),
(22, 'long gown', '1583911022.jpg', 20, 'abcd', 0, '2020-03-11 01:47:02', '2020-03-11 01:47:02'),
(23, 'frock', '1583911139.jpg', 21, 'abcd', 0, '2020-03-11 01:48:59', '2020-03-11 01:48:59'),
(24, 'anarkali', '1583915497.jpg', 22, 'acsghggs djdcj', 0, '2020-03-11 03:01:37', '2020-03-11 03:01:37'),
(26, 'Other', '1584368200.jpg', 23, 'other', 2000, '2020-03-16 08:46:40', '2020-03-16 08:50:32'),
(27, 'Blazer', '1584368263.jpg', 23, 'abcd', 1599, '2020-03-16 08:47:43', '2020-03-16 08:50:42'),
(28, 'Jacket', '1584368885.jpg', 25, 'dsgfdg', 0, '2020-03-16 08:58:05', '2020-03-16 08:58:05'),
(29, 'Anarkali', '1584629831.jpg', 26, 'abcd', 0, '2020-03-19 09:27:11', '2020-03-19 09:27:11'),
(30, 'Gown', '1584630093.jpg', 27, 'abcd', 1599, '2020-03-19 09:31:33', '2020-03-19 09:32:03'),
(31, 'Indowestern', '1645684544.png', 29, 'sfdsgfgsdfdsf', 0, '2022-02-24 01:05:44', '2022-02-24 01:05:44');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `feedback_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `feedback_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`user_id`, `product_id`, `feedback_description`, `feedback_date`, `created_at`, `updated_at`) VALUES
(3, 36, 'good', '2020-03-07', '2020-03-07 02:55:57', '2020-03-07 02:55:57'),
(4, 36, 'Wow...!!!', '2020-03-08', '2020-03-08 04:03:07', '2020-03-08 04:03:07'),
(4, 60, 'nice looking', '2020-03-08', '2020-03-08 03:02:35', '2020-03-08 03:02:35'),
(5, 32, 'Good looking', '2020-03-08', '2020-03-08 03:17:12', '2020-03-08 03:17:12'),
(5, 36, 'Wow...!!!', '2020-03-08', '2020-03-08 04:03:07', '2020-03-08 04:03:07'),
(6, 30, 'nice', '2020-03-08', '2020-03-08 03:28:15', '2020-03-08 03:28:15'),
(6, 36, 'Too Bakvas', '2020-03-08', '2020-03-08 03:02:35', '2020-03-08 03:02:35'),
(7, 36, 'Not Good', '2020-03-08', '2020-03-08 03:17:12', '2020-03-08 03:17:12'),
(8, 36, 'nice', '2020-03-08', '2020-03-08 03:28:15', '2020-03-08 03:28:15'),
(8, 51, 'Wow...!!!', '2020-03-08', '2020-03-08 04:03:07', '2020-03-08 04:03:07'),
(9, 36, 'Wow...!!!', '2020-03-08', '2020-03-08 04:03:07', '2020-03-08 04:03:07');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_user_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_11_10_112049_entrust_setup_tables', 1),
(4, '2019_11_10_113229_create_city_table', 1),
(5, '2019_11_10_113655_create_area_table', 1),
(6, '2019_12_02_112506_create_company_table', 1),
(7, '2019_12_02_114157_create_size_chart_table', 1),
(8, '2019_12_03_065207_create_category_table', 1),
(9, '2019_12_03_065728_create_subcategory_table', 1),
(10, '2019_12_03_070842_create_color_table', 1),
(11, '2019_12_03_071340_create_payment_type_table', 1),
(12, '2019_12_03_071754_create_product_table', 1),
(13, '2019_12_03_072244_create_offer_table', 1),
(14, '2019_12_03_072720_create_appointment_table', 1),
(15, '2019_12_03_100442_create_customize_product_table', 1),
(16, '2019_12_03_101738_create_customize_product_details_table', 1),
(17, '2019_12_03_104421_create_sales_table', 1),
(18, '2019_12_03_144550_create_sales_payment_table', 1),
(19, '2019_12_04_095139_create_wishlist_table', 1),
(20, '2019_12_04_101206_create_cart_table', 1),
(21, '2019_12_04_113115_create_feedback_table', 1),
(22, '2019_12_04_113511_create_complain_table', 1),
(23, '2019_12_04_115555_create_carolous_table', 1),
(24, '2019_12_04_120212_create_newslater_table', 1),
(25, '2019_12_04_120627_create_sales_details_table', 1),
(26, '2019_12_08_061035_create_admin_tabel', 1),
(27, '2019_12_21_143037_create_rating_table', 1),
(28, '2019_12_21_143657_add_del_address_to_sales_table', 1),
(29, '2019_12_21_145025_add_users_fields_to_user_table', 1),
(30, '2019_12_24_081010_create_pages_table', 1),
(31, '2019_12_26_102335_user_table', 1),
(32, '2020_01_12_093811_drop_wishlist_table', 1),
(33, '2020_01_12_111212_drop_cart_tabel', 1),
(34, '2020_01_12_185007_add_verify_tokan_to_users_tabel', 1),
(35, '2020_01_18_021823_drop_rating_table', 1),
(36, '2020_02_03_164122_create_shipping_address_table', 1),
(37, '2020_02_08_082603_add_user_id_toshippind_address_table', 1),
(38, '2020_02_11_051020_drop_customize_product_details_table', 1),
(39, '2020_02_11_051136_add_field_customize_product__table', 1),
(40, '2020_02_13_034339_add_field_to_shipping_table', 1),
(41, '2020_02_15_073529_add_featured_filed_to_product_table', 1),
(42, '2020_02_15_074446_add_status_filed_to_sales_table', 1),
(43, '2020_02_15_074530_add_code_filed_to_color_table', 1),
(44, '2020_02_15_164918_drop_foreign_key', 1),
(45, '2020_02_16_085259_create_contectUs_table', 1),
(46, '2020_02_16_110907_add_field_color_id_to_cart', 1),
(47, '2020_02_16_111534_add_field_color_id_to_wishlist', 1),
(48, '2020_02_16_111614_add_field_color_id_to_sales_details', 1),
(49, '2020_02_20_112108_create_product_images_table', 1),
(50, '2020_02_28_081434_change_files_nullable_sales_payment_table', 2),
(51, '2020_03_01_110323_add_field_couponcode_to_offer', 3),
(52, '2020_03_05_180434_drop_primary_key_sales_details_table', 4),
(53, '2020_03_07_101556_add_field_description_to_product', 5),
(54, '2020_03_12_162143_create_product_color_table', 6),
(55, '2020_03_16_123958_add_fields_color_id_to_sales_details', 7),
(56, '2020_04_03_045043_add_subcategory_id_to_size_chart_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `newslater`
--

CREATE TABLE `newslater` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `offer`
--

CREATE TABLE `offer` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `offer_percentage` double(15,8) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `product_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `couponcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offer`
--

INSERT INTO `offer` (`id`, `offer_percentage`, `start_date`, `end_date`, `product_id`, `created_at`, `updated_at`, `deleted_at`, `couponcode`) VALUES
(2, 20.00000000, '2020-03-02', '2020-03-23', '6', '2020-03-01 06:05:36', '2020-03-01 06:05:36', NULL, '123s45'),
(4, 15.00000000, '2020-03-03', '2020-03-22', '4', '2020-03-01 07:17:37', '2020-03-01 07:17:37', NULL, '123s45'),
(6, 20.00000000, '2020-03-19', '2020-03-23', '3', '2020-03-10 09:06:05', '2020-03-10 09:06:05', NULL, 'GObmXU'),
(7, 25.00000000, '2020-02-01', '2020-03-24', '5', '2020-03-22 07:52:36', '2020-03-22 07:52:36', NULL, 'AI8sSS'),
(8, 25.00000000, '2020-02-01', '2020-03-24', '9', '2020-03-22 07:52:36', '2020-03-22 07:52:36', NULL, 'AI8sSS'),
(9, 10.00000000, '2020-03-22', '2020-03-23', '3', '2020-03-22 07:55:36', '2020-03-22 07:55:36', NULL, 'KhCvrl'),
(10, 10.00000000, '2020-04-03', '2020-04-03', '4', '2020-03-22 07:55:36', '2020-04-02 07:19:57', NULL, 'KhCvrl'),
(11, 10.00000000, '2020-04-03', '2020-04-05', '4', '2020-04-02 07:34:29', '2020-04-02 07:34:29', NULL, 'N54ni2');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_type`
--

CREATE TABLE `payment_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `payment_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_type`
--

INSERT INTO `payment_type` (`id`, `payment_type`, `created_at`, `updated_at`) VALUES
(1, 'Cash On Delivery', '2020-02-29 00:43:41', '2020-02-29 00:43:41');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` double(15,8) NOT NULL,
  `product_qty` int(11) NOT NULL,
  `subcategory_id` bigint(20) UNSIGNED NOT NULL,
  `size_id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) UNSIGNED NOT NULL,
  `color_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `featured_filed` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Yes mean''s featured',
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `product_name`, `product_image`, `product_price`, `product_qty`, `subcategory_id`, `size_id`, `company_id`, `color_id`, `created_at`, `updated_at`, `deleted_at`, `featured_filed`, `description`) VALUES
(3, 'Gray  Casual Blezer', '1582977738b1.jpg', 999.00000000, 200, 1, 1, 1, '7|9', '2020-02-29 06:32:18', '2020-03-10 03:17:10', NULL, 'Yes', 'Black and gray checked single-breasted blazer, has a notched lapel, double button closure, long sleeves.'),
(4, 'Navy Blue Blazer', '1582978005b1.jpg', 899.00000000, 250, 1, 2, 1, '7', '2020-02-29 06:36:06', '2020-03-10 03:17:12', NULL, 'Yes', 'Navy blue single-breasted tuxedo blazer, has a shawl collar.'),
(5, 'FAVOROSKI', '1582978133b1.jpg', 799.00000000, 500, 1, 4, 1, '7|9', '2020-02-29 06:38:53', '2020-03-10 03:17:15', NULL, 'Yes', 'Purple single-breasted tuxedo blazer, button closure, long sleeves.'),
(6, 'Dark gray Blazer', '1582978244b1.jpg', 699.00000000, 300, 1, 4, 1, '11|12', '2020-02-29 06:40:44', '2020-03-09 05:04:44', NULL, NULL, 'Dark gray blazer with the little formal look,vented back hem, attached lining\r\nComes with a pocket square'),
(7, 'Royal Blue Blazer', '1582978320b1.jpg', 650.00000000, 310, 1, 1, 1, '10|11', '2020-02-29 06:42:00', '2020-03-09 05:05:56', NULL, NULL, 'Royal Blue single-breasted tuxedo blazer, has a shawl collar, button closure, long sleeves'),
(8, 'Close Neck IndoWastern', '1582978394I1.jpg', 1050.00000000, 230, 2, 1, 1, '7|9|12', '2020-02-29 06:43:14', '2020-03-09 05:19:17', NULL, NULL, 'Close Neck IndoWastern, has a mandarin collar, a full button placket, long sleeves, two flap pockets,'),
(9, 'Sherwani look Indowestern', '1582978514I1.jpg', 850.00000000, 300, 2, 4, 1, '10|11', '2020-02-29 06:45:14', '2020-03-10 02:41:05', NULL, NULL, 'Sherwani look blazer, has a mandarin collar, a full button placket, long sleeves, two flap pockets, with man nackless'),
(10, 'Golden Sherwani Look Indowestern', '1582978563I1.jpg', 950.00000000, 400, 2, 2, 1, '7|12', '2020-02-29 06:46:03', '2020-03-10 02:40:40', NULL, NULL, 'Golden Sherwani Look Blazer blazer, has a mandarin collar, a full button placket, long sleeves, two flap pockets,'),
(11, 'White and Gray Indowestern', '1582978696I1.jpg', 799.00000000, 220, 2, 4, 1, '9', '2020-02-29 06:48:16', '2020-03-10 02:41:24', NULL, NULL, 'White-Gray Blazer,one chest welt pocket, an attached lining,\r\nComes with a pocket square'),
(12, 'Proper Patola Look Indowestern', '1582978743I1.jpg', 699.00000000, 150, 2, 3, 1, '9|10', '2020-02-29 06:49:03', '2020-03-10 02:42:31', NULL, NULL, 'one chest welt pocket, an attached lining, and a double-vented back hem\r\nComes with a pocket square'),
(13, 'Black Shervani Type Indowestern', '1582978825I1.jpg', 999.00000000, 300, 2, 4, 1, '9|10', '2020-02-29 06:50:25', '2020-03-10 02:43:42', NULL, NULL, 'Blue woven design straight kurta, has a mandarin collar, button placket, long sleeves, curved hem, no slits'),
(14, 'LUXURAZI', '1582979153j1.jpg', 950.00000000, 350, 6, 1, 1, '9|10', '2020-02-29 06:50:56', '2020-03-09 08:25:42', NULL, NULL, 'Black solid two-piece suit\r\nBlack solid slim-fit blazer, has a notched lapel, single-breasted with double button closures, long sleeves, three pockets, and a double-vented back hem'),
(15, 'White Jacket', '1582979238j1.jpg', 799.00000000, 300, 6, 2, 1, '9|10', '2020-02-29 06:57:19', '2020-03-09 08:28:27', NULL, NULL, 'Beige self-design single-breasted formal blazer, has a notched lapel, long sleeves, 3 pockets on the front, an attached lining with two pockets and a double-vented hem\r\nComes with a lapel pin'),
(16, 'Red Jacket', '1582979321j1.jpg', 899.00000000, 150, 6, 2, 1, '7|9|10', '2020-02-29 06:58:41', '2020-03-09 08:29:56', NULL, NULL, 'Red single-breasted party blazer, has a shawl collar, button closure, long sleeves, one welt and two flap pockets, vented back hem, attached lining\r\nComes with a pocket square'),
(17, 'Dark Blue Jacket', '1582979402j1.jpg', 999.00000000, 250, 6, 4, 1, '7|9', '2020-02-29 07:00:02', '2020-03-10 02:48:24', NULL, NULL, 'Blue self-design bandhgala regular-fit blazer, has a mandarin collar, a full button placket, two flap pockets, one welt pocket, an attached lining, and a double-vented back hem.'),
(18, 'Blue jacket', '1582979476j1.jpg', 899.00000000, 230, 6, 4, 1, '7|9|10', '2020-02-29 07:01:16', '2020-03-09 08:35:35', NULL, NULL, 'Blue and white printed woven waistcoat, has a mandarin collar with a V-neck, a short button placket, sleeveless, and three pockets'),
(19, 'Pink Shervani', '1582979570sr1.jpg', 899.00000000, 200, 8, 4, 1, '7|9|10', '2020-02-29 07:02:51', '2020-03-09 08:42:32', NULL, NULL, 'Pink and cream-coloured sherwani set\r\nPink woven design sherwani, has a mandarin collar, a full-front button placket, long sleeves, one chest welt pocket, and multiple slits'),
(20, 'Red Cream Shervani', '1582979632sr1.jpg', 899.00000000, 199, 8, 3, 1, '7|10|12', '2020-02-29 07:03:52', '2020-03-09 08:41:56', NULL, NULL, 'faesfkkeaRed and cream-coloured sherwani set\r\nRed woven design sherwani, has a mandarin collar, a full-front button placket, long sleeves, one chest welt pocket, and multiple slits'),
(21, 'Manish Creations', '1582979721sr1.jpg', 599.00000000, 250, 8, 3, 1, '7|9|11', '2020-02-29 07:05:21', '2020-03-09 08:45:33', NULL, NULL, 'Teal green brocade pattern handicraft sherwani\r\nTeal green and black brocade pattern handicraft sherwani, has a stylised stand collar, long sleeves, structured shoulders'),
(22, 'Cream Shervani', '1582979807sr1.jpg', 699.00000000, 199, 8, 4, 1, '7|9', '2020-02-29 07:06:47', '2020-03-09 08:46:53', NULL, NULL, 'Cream-coloured and coral sherwani\r\nCoral-coloured solid sherwani, has a mandarin collar, a full button placket, long sleeves,'),
(23, 'Light color Shervani', '1582979859sr1.jpg', 699.00000000, 230, 8, 2, 1, '7|8', '2020-02-29 07:07:39', '2020-03-09 08:48:29', NULL, NULL, 'Cream-coloured and off-white self-design sherwani\r\nCream-coloured woven design sherwani, has a mandarin collar, long sleeves, straight hem, side slits'),
(24, 'Black Suit', '1582979925s1.jpg', 599.00000000, 150, 9, 4, 1, '7|9', '2020-02-29 07:08:45', '2020-03-09 08:49:32', NULL, NULL, 'Black solid woven suit\r\nBlack solid blazer, has a mandarin collar, single-breasted with a full button placket, long sleeves, three pockets, double-vented back hem'),
(26, 'Full Grey Shits', '1582980061s1.jpg', 899.00000000, 150, 9, 2, 1, '9|10', '2020-02-29 07:11:01', '2020-03-09 08:52:44', NULL, NULL, 'Grey solid urban-fit blazer, has a shawl collar, single-breasted with double-button closure, long sleeves, two flap pockets, one welt pocket, three inside pockets'),
(27, 'New Suit', '1582980218s1.jpg', 799.00000000, 200, 9, 2, 1, '7|9', '2020-02-29 07:13:38', '2020-03-09 08:54:10', NULL, NULL, 'Grey solid urban-fit blazer, has a shawl collar, single-breasted with double-button closure, long sleeves, two flap pockets, one welt pocket, three inside piockets,'),
(28, 'Wedding suits', '1582980292s1.jpg', 799.00000000, 199, 9, 2, 1, '7|8', '2020-02-29 07:14:52', '2020-03-09 08:56:31', NULL, NULL, 'Burgundy self-design party Suit\r\nBurgundy self-design party blazer, has a peaked lapel, single-breasted with double button closures, long sleeves,'),
(29, 'Formal suits', '1582980366s1.jpg', 1199.00000000, 200, 9, 5, 1, '7|10', '2020-02-29 07:16:06', '2020-03-09 08:57:34', NULL, NULL, 'Grey solid formal suit\r\nGrey solid blazer, has a notched lapel, single-breasted with button closure, long sleeves, three pockets,'),
(30, 'Pink Anarkali', '1582980462a1.jpg', 899.00000000, 120, 11, 2, 1, '2|3', '2020-02-29 07:17:42', '2020-03-09 08:59:55', NULL, NULL, 'Pink and taupe printed woven maxi dress, has a round neck, sleeveless, flared hem'),
(31, 'Printed Dress', '1582980537k1.jpg', 899.00000000, 200, 11, 3, 1, '4|5', '2020-02-29 07:18:57', '2020-03-09 09:01:16', NULL, NULL, 'Silver-Toned and navy blue solid knitted maxi dress, has a round neck, three-quarter sleeves,,, flared hem'),
(32, 'Black  Anarkali', '1582980600ak11.jpg', 699.00000000, 200, 11, 4, 1, '1|2', '2020-02-29 07:20:00', '2020-03-09 09:02:21', NULL, NULL, 'Black and Red printed woven  dress, has a mandarin collar, short sleeves, flared hem'),
(33, 'Printed Anarkali', '1582980673ak111.jpg', 799.00000000, 200, 11, 5, 1, '4|5', '2020-02-29 07:21:13', '2020-03-09 09:17:01', NULL, NULL, 'Beige unstitched dress material\r\nBeige embroidered kurta fabric\r\nBeige solid bottom fabric'),
(34, 'Yellow Anarkli', '1582980967k11.jpg', 799.00000000, 150, 11, 5, 1, '1|2', '2020-02-29 07:26:07', '2020-03-09 09:18:07', NULL, NULL, 'Yellow lehenga choli\r\nYellow blouse, sleeveless\r\nYellow ready to wear lehenga, flared hem'),
(35, 'Print Croptop', '1582981041ct11.jpg', 1499.00000000, 300, 12, 1, 1, '13|14', '2020-02-29 07:27:21', '2020-03-09 09:19:20', NULL, NULL, 'Cream-coloured lehenga choli\r\nCream-coloured solid blouse, sleeveless\r\nCream-coloured and gold-toned'),
(36, 'New Croptop', '1582981154ct11.jpg', 1299.00000000, 300, 12, 2, 1, '13|14', '2020-02-29 07:29:14', '2020-03-09 09:20:09', NULL, NULL, 'Beige and green embroidered unstitched dress material\r\nBeige and green embroidered kurta fabric\r\nGreen solid bottom fabric'),
(37, 'Designer Croptop', '1582981209ct111.jpg', 899.00000000, 150, 12, 2, 1, '4|5', '2020-02-29 07:30:09', '2020-03-09 09:22:15', NULL, NULL, 'Pink and golden Made to Measure lehenga with choli and dupatta\r\nPink and golden sequinned made to measure choli, has a round neck with zircon embellished detail, sleeveless,'),
(38, 'Grey Croptop', '1582981289ct01.jpg', 899.00000000, 200, 12, 5, 1, '3|4', '2020-02-29 07:31:29', '2020-03-09 09:23:49', NULL, NULL, 'Grey lehenga choli, blouse, three-quarter sleeves\r\nready to wear lehenga, flared hem'),
(40, 'Green Croptop', '1582982312k11.jpg', 599.00000000, 150, 11, 3, 1, '2|3', '2020-02-29 07:48:32', '2020-03-09 09:26:29', NULL, NULL, 'Green lehenga choli with dupatta, has zari\r\nGreen blouse, has a boat neck, short sleeves\r\nGreen semi-stitched lehenga, flared hem'),
(41, 'Blue Dress', '1582982366k111.jpg', 899.00000000, 150, 11, 5, 1, '3|4', '2020-02-29 07:49:26', '2020-03-09 09:27:29', NULL, NULL, 'Blue printed unstitched dress material\r\nBlue printed kurta fabric\r\nBlue printed bottom fabric\r\nBlue printeddupatta, has printed border'),
(42, 'Black Choli', '1582982418k101.jpg', 799.00000000, 100, 11, 3, 1, '6|14', '2020-02-29 07:50:18', '2020-03-09 09:28:47', NULL, NULL, 'Black and golden embroidered made to measure lehenga choli with dupatta\r\nBlack embroidered made to measure blouse, has a stylised boat neck'),
(43, 'Pink Sharara', '1582983039k01.jpg', 999.00000000, 200, 16, 2, 1, '13|14', '2020-02-29 07:52:37', '2020-03-09 09:31:31', NULL, NULL, 'A pair of pink and grey printed woven flared fit Palazzos, opaque, has a partially elasticated waistband with drawstring closure'),
(44, 'Green Sharara', '1582982620k1111.jpg', 599.00000000, 300, 16, 5, 1, '3|4', '2020-02-29 07:53:40', '2020-03-09 09:33:33', NULL, NULL, 'Green and light Pink woven design maxi flared skirt with tie-up detail, has a concealed zip closure, flared hem , an attached lining'),
(45, 'Blue Sharara', '1582983783srr11.jpg', 799.00000000, 300, 16, 4, 1, '5|6', '2020-02-29 08:13:03', '2020-03-09 09:34:55', NULL, NULL, 'A pair of Navy Blue mirror work woven flared fit Palazzos, opaque, and drawstring closure'),
(46, 'Printed Sharara', '1582984362srr1.jpg', 799.00000000, 100, 16, 3, 1, '4|6', '2020-02-29 08:22:42', '2020-03-09 09:36:07', NULL, NULL, 'Coral-coloured printed A-line maxi skirt, has an elasticated waistband with a drawstring fastening, an attached lining, side zip, flared hem.'),
(47, 'Studio sarara', '1582984429srr101.jpg', 699.00000000, 300, 16, 4, 1, '4|6', '2020-02-29 08:23:49', '2020-03-09 09:37:54', NULL, NULL, 'Coral-coloured printed A-line maxi skirt, has an elasticated waistband with a drawstring fastening, an attached lining, side zip, flared hem.'),
(48, 'Yellow Sharara', '1582984491srr144.jpg', 899.00000000, 200, 16, 2, 1, '2|3', '2020-02-29 08:24:51', '2020-03-09 09:38:55', NULL, NULL, 'Gold-coloured woven maxi skirt with embroidered detail, has a waistband with drawstring fastening, flared lace hemline, and a side zip closure'),
(49, 'Red Sharara', '1582984534srr155.jpg', 799.00000000, 150, 16, 2, 1, '3|4', '2020-02-29 08:25:34', '2020-03-09 09:40:30', NULL, NULL, 'Coral Red embellished A-Line maxi skirt, has a partially elasticated waistband, side zip closure, an attached lining, and flared hem'),
(50, 'Navy Blue sarara', '1582984644g11.jpg', 599.00000000, 300, 13, 4, 1, '2|3', '2020-02-29 08:27:24', '2020-03-09 09:42:08', NULL, NULL, 'Navy blue embellished A-line maxi skirt, has a waistband with side zip closure, an attached lining, flared hem'),
(51, 'Printed Wide Gown', '1582984694g111.jpg', 1599.00000000, 200, 13, 3, 1, '3|4', '2020-02-29 08:28:14', '2020-03-09 09:45:05', NULL, NULL, 'Peach-Coloured lehenga choli\r\nPeach-Coloured blouse, short sleeves\r\nPeach-Coloured ready to wear lehenga, flared hem'),
(52, 'Burgundy Gown', '1582984747g101.jpg', 899.00000000, 300, 13, 2, 1, '1|2', '2020-02-29 08:29:07', '2020-03-09 09:46:23', NULL, NULL, 'Burgundy and golden made to measure embelished lehenga choli with dupatta\r\nBurgundy and golden made to measure embellished choli,'),
(53, 'Pink Gown', '1582984797g1111.jpg', 999.00000000, 7, 13, 2, 1, '2|3', '2020-02-29 08:29:57', '2020-03-09 09:47:19', NULL, NULL, 'Pink lehenga choli with dupatta, has thread work\r\nPink blouse, has a round neck, three-quarter sleeves\r\nPink semi-stitched lehenga, flared hem'),
(54, 'Black Gown', '1582984847gw1.jpg', 1299.00000000, 300, 13, 3, 1, '1|2', '2020-02-29 08:30:47', '2020-03-09 10:09:11', NULL, NULL, 'Black and grey woven design ready to wear lehenga choli with dupatta\r\nBlack woven design blouse, has a shirt collar, short sleeves'),
(55, 'Wear Lehenga', '1582985274l11.jpg', 799.00000000, 120, 13, 2, 1, '1|2', '2020-02-29 08:37:54', '2020-03-09 10:11:57', NULL, NULL, 'Navy Blue lehenga choli\r\nGrey unstitched blouse, sleeveless\r\nNavy Blue ready to wear printed lehenga'),
(56, 'Wear Lehenga Gown', '1582985352l111.jpg', 899.00000000, 200, 13, 2, 1, '1|2', '2020-02-29 08:39:12', '2020-03-09 10:17:40', NULL, NULL, 'Pink printed lehenga choli with dupatta\r\nPink woven design unstitched blouse fabric\r\nPink semi-stitched printed lehenga, has concealed zip closure,'),
(58, 'Light Green Gown', '1582985479l101.jpg', 999.00000000, 200, 13, 4, 1, '1|2', '2020-02-29 08:41:19', '2020-03-09 10:19:56', NULL, NULL, 'Turquoise Blue lehenga choli\r\nTurquoise Blue blouse, three-quarter sleeves\r\nTurquoise Blue made to measure lehenga, flared hem'),
(59, 'Printed Ready Croptop', '1582985754l1111.jpg', 899.00000000, 300, 12, 4, 1, '2|3', '2020-02-29 08:45:34', '2020-03-09 10:23:41', NULL, NULL, 'pink lehenga choli with dupatta, has mirror work\r\nPink blouse, has a u-neck, sleeveless\r\nPink ready to wear lehenga, flared hem'),
(60, 'White Croptop', '1582985869l1444.jpg', 999.00000000, 200, 12, 2, 1, '2|3', '2020-02-29 08:47:49', '2020-03-09 10:24:36', NULL, NULL, 'Off-White & Gold-Toned printed lehenga choli\r\nOff-White & Gold-Toned printed blouse, sleeveless\r\nOff-White & Gold-Toned printed ready to wear lehenga, flared hem'),
(61, 'Gold croptop', '1582985978ct11.jpg', 899.00000000, 200, 12, 5, 1, '1|2', '2020-02-29 08:49:38', '2020-03-09 10:28:30', NULL, NULL, 'Rust and gold-toned self designed floor length jacket, three quarter sleeves, tie-up neck\r\nRust and gold-toned A-line floor length skirt'),
(62, 'Navy Croptop', '1582986134l11.jpg', 899.00000000, 300, 12, 1, 1, '1|2', '2020-02-29 08:52:14', '2020-03-09 10:29:29', NULL, NULL, 'Navy Blue & pink embellished lehenga choli with dupatta, has zari\r\nPink blouse, has a scoop neck, full sleeve'),
(63, 'Pataudi Blazer', '1583053266j1.jpg', 899.00000000, 200, 1, 2, 1, '7|12', '2020-03-01 03:31:06', '2020-03-09 10:30:53', NULL, NULL, 'Maroon self-design ethnic velvet bandhgala, has a mandarin collar, a full button placket, long sleeves, one inner pocket, one welt pocket, and two external pockets.'),
(65, 'Flower Printed Indowestern', '1583828589k1.jpg', 699.00000000, 300, 2, 4, 1, '11|12', '2020-03-10 02:53:09', '2020-03-10 02:53:09', NULL, NULL, 'Cream Sherwani Look Flower Printed Indowestern, It has a mandarin collar, a full button placket, long sleeves, two flap pockets.'),
(66, 'Plain Black Indowestern', '1583828719k1.jpg', 999.00000000, 250, 2, 3, 1, '7|10', '2020-03-10 02:55:19', '2020-03-10 02:55:19', NULL, NULL, 'Black Plaain Sherwani Look Indowestern,It  has a mandarin collar, a full button placket, long sleeves, two flap pockets,'),
(67, 'Black Plain Shervani', '1583828852k1.jpg', 899.00000000, 320, 8, 1, 1, '7|10', '2020-03-10 02:57:32', '2020-03-10 02:57:32', NULL, NULL, 'Black Plain Sherwani ,It has a mandarin collar, a full button placket, long sleeves, two flap pockets,'),
(68, 'Golden Black Shervani', '1583828982k1.jpg', 799.00000000, 300, 8, 1, 1, '7|12', '2020-03-10 02:59:42', '2020-03-10 02:59:42', NULL, NULL, 'Golden Black Sherwani ,It has a mandarin collar, a full button placket, long sleeves, two flap pockets,'),
(69, 'Printed Black Indowestern', '1583829074k1.jpg', 899.00000000, 300, 8, 4, 1, '7|10|11', '2020-03-10 03:01:14', '2020-03-10 03:01:14', NULL, NULL, 'Printed Sherwani , has a mandarin collar, a full button placket, long sleeves, two flap pockets,'),
(70, 'Grey Blazer', '1582986261s1.jpg', 899.00000000, 250, 1, 3, 1, '7|9', '2020-02-29 07:10:07', '2020-03-09 08:50:37', NULL, NULL, 'Grey and blue checked single-breasted formal Blazer\r\nGrey and blue checked blazer, has a notched lapel, single-breasted'),
(71, 'New Blazer', '1582980218s1.jpg', 799.00000000, 200, 1, 2, 1, '7|9', '2020-02-29 07:13:38', '2020-03-09 08:54:10', NULL, NULL, 'Grey solid urban-fit blazer, has a shawl collar, single-breasted with double-button closure, long sleeves, two flap pockets, one welt pocket, three inside piockets,'),
(72, 'Royal Blue Suit', '1582978320b1.jpg', 650.00000000, 310, 9, 1, 1, '10|11', '2020-02-29 06:42:00', '2020-03-09 05:05:56', NULL, NULL, 'Royal Blue single-breasted tuxedo suit, has a shawl collar, button closure, long sleeves'),
(74, 'Brown formal Jacket', '1583829897j1.jpg', 799.00000000, 150, 6, 4, 1, '7|8', '2020-03-10 03:14:57', '2020-03-10 03:14:57', NULL, NULL, 'Brown single-breasted party blazer, has a shawl collar, button closure, long sleeves, one welt and two flap pockets, vented back hem, attached lining Comes with a pocket squar'),
(75, 'Gray', '1583830232j1.jpg', 899.00000000, 300, 6, 2, 1, '7', '2020-03-10 03:16:40', '2020-03-10 03:20:32', NULL, NULL, 'Gray single-breasted party blazer, has a shawl collar, button closure, long sleeves, one welt and two flap pockets, vented back hem, attached lining Comes with a pocket squar'),
(76, 'Blue Jodhpuri  Style Jacket', '1583830649j1.jpg', 999.00000000, 200, 6, 5, 1, '1|9', '2020-03-10 03:27:29', '2020-03-10 03:27:29', NULL, NULL, 'Blue woven waistcoat, has a mandarin collar with a V-neck, a short button placket, sleeveless, and three pockets'),
(78, 'Pataudi Suit', '1583053266j1.jpg', 899.00000000, 200, 9, 2, 1, '7|12', '2020-03-01 03:31:06', '2020-03-09 10:30:53', NULL, NULL, 'Maroon self-design ethnic velvet bandhgala, has a mandarin collar, a full button placket, long sleeves, one inner pocket, one welt pocket, and two external pockets.'),
(79, 'Grey Suit', '1582986261s1.jpg', 899.00000000, 250, 9, 3, 1, '7|9', '2020-02-29 07:10:07', '2020-03-09 08:50:37', NULL, NULL, 'Grey and blue checked single-breasted formal suit\r\nGrey and blue checked blazer, has a notched lapel, single-breasted');

-- --------------------------------------------------------

--
-- Table structure for table `product_color`
--

CREATE TABLE `product_color` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `color_id` bigint(20) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_color`
--

INSERT INTO `product_color` (`id`, `product_id`, `color_id`, `qty`, `created_at`, `updated_at`) VALUES
(1, 3, 7, 200, NULL, NULL),
(2, 3, 7, 200, NULL, NULL),
(3, 4, 7, 250, NULL, NULL),
(4, 5, 7, 500, NULL, NULL),
(5, 5, 9, 500, NULL, NULL),
(8, 6, 11, 300, NULL, NULL),
(9, 6, 12, 300, NULL, NULL),
(10, 7, 10, 310, NULL, NULL),
(11, 7, 11, 310, NULL, NULL),
(12, 8, 7, 230, NULL, NULL),
(13, 8, 9, 230, NULL, NULL),
(14, 8, 12, 230, NULL, NULL),
(15, 9, 10, 300, NULL, NULL),
(16, 9, 11, 300, NULL, NULL),
(17, 10, 7, 400, NULL, NULL),
(18, 10, 12, 400, NULL, NULL),
(19, 11, 9, 220, NULL, NULL),
(20, 12, 9, 150, NULL, NULL),
(21, 12, 10, 150, NULL, NULL),
(22, 13, 9, 300, NULL, NULL),
(23, 13, 10, 300, NULL, NULL),
(24, 14, 9, 350, NULL, NULL),
(25, 14, 10, 350, NULL, NULL),
(26, 15, 9, 300, NULL, NULL),
(27, 15, 10, 300, NULL, NULL),
(28, 16, 7, 150, NULL, NULL),
(29, 16, 9, 150, NULL, NULL),
(30, 16, 10, 150, NULL, NULL),
(31, 80, 1, NULL, '2020-04-07 03:12:09', '2020-04-07 03:12:09'),
(32, 81, 1, NULL, '2020-04-07 07:54:32', '2020-04-07 07:54:32'),
(33, 81, 2, NULL, '2020-04-07 07:54:32', '2020-04-07 07:54:32');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `image_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `image_name`, `created_at`, `updated_at`) VALUES
(1, 3, '1582977738b2.jpg', NULL, NULL),
(2, 3, '1582977738b3.jpg', NULL, NULL),
(3, 4, '1582978005b2.jpg', NULL, NULL),
(4, 4, '1582978005b3.jpg', NULL, NULL),
(5, 5, '1582978133b3.jpg', NULL, NULL),
(6, 5, '1582978133b2.jpg', NULL, NULL),
(8, 6, '1582978244b2.jpg', NULL, NULL),
(9, 6, '1582978244b3.jpg', NULL, NULL),
(10, 7, '1582978320b2.jpg', NULL, NULL),
(11, 7, '1582978320b3.jpg', NULL, NULL),
(12, 8, '1582978394I2.jpg', NULL, NULL),
(13, 8, '1582978394I3.jpg', NULL, NULL),
(14, 9, '1582978514I2.jpg', NULL, NULL),
(15, 9, '1582978514I3.jpg', NULL, NULL),
(16, 10, '1582978563I2.jpg', NULL, NULL),
(17, 10, '1582978563I3.jpg', NULL, NULL),
(18, 11, '1582978696I2.jpg', NULL, NULL),
(19, 11, '1582978696I3.jpg', NULL, NULL),
(20, 12, '1582978743I2.jpg', NULL, NULL),
(21, 12, '1582978743I3.jpg', NULL, NULL),
(22, 13, '1582978825I2.jpg', NULL, NULL),
(23, 13, '1582978825I3.jpg', NULL, NULL),
(24, 14, '1582979153j2.jpg', NULL, NULL),
(25, 14, '1582979153j3.jpg', NULL, NULL),
(26, 15, '1582979238j2.jpg', NULL, NULL),
(27, 15, '1582979238j3.jpg', NULL, NULL),
(28, 16, '1582979321j2.jpg', NULL, NULL),
(29, 16, '1582979321j3.jpg', NULL, NULL),
(30, 17, '1582979402j2.jpg', NULL, NULL),
(31, 17, '1582979402j3.jpg', NULL, NULL),
(32, 18, '1582979476j2.jpg', NULL, NULL),
(33, 18, '1582979476j3.jpg', NULL, NULL),
(34, 19, '1582979570sr2.jpg', NULL, NULL),
(35, 19, '1582979571sr3.jpg', NULL, NULL),
(36, 20, '1582979632sr2.jpg', NULL, NULL),
(37, 20, '1582979632sr3.jpg', NULL, NULL),
(38, 21, '1582979721sr2.jpg', NULL, NULL),
(39, 21, '1582979721sr3.jpg', NULL, NULL),
(40, 22, '1582979807sr2.jpg', NULL, NULL),
(41, 22, '1582979807sr3.jpg', NULL, NULL),
(42, 23, '1582979859sr2.jpg', NULL, NULL),
(43, 23, '1582979859sr3.jpg', NULL, NULL),
(44, 24, '1582979925s2.jpg', NULL, NULL),
(45, 24, '1582979925s3.jpg', NULL, NULL),
(48, 26, '1582980061s2.jpg', NULL, NULL),
(49, 26, '1582980061s3.jpg', NULL, NULL),
(50, 27, '1582980218s2.jpg', NULL, NULL),
(51, 27, '1582980218s3.jpg', NULL, NULL),
(54, 28, '1582980292s2.jpg', NULL, NULL),
(55, 28, '1582980292s3.jpg', NULL, NULL),
(56, 29, '1582980366s2.jpg', NULL, NULL),
(57, 29, '1582980366s3.jpg', NULL, NULL),
(58, 30, '1582980462a2.jpg', NULL, NULL),
(59, 30, '1582980462a3.jpg', NULL, NULL),
(60, 31, '1582980537k2.jpg', NULL, NULL),
(61, 31, '1582980537k3.jpg', NULL, NULL),
(62, 32, '1582980600ak22.jpg', NULL, NULL),
(63, 32, '1582980600ak33.jpg', NULL, NULL),
(64, 33, '1582980673ak222.jpg', NULL, NULL),
(65, 33, '1582980673ak333.jpg', NULL, NULL),
(66, 34, '1582980967k22.jpg', NULL, NULL),
(67, 34, '1582980967k33.jpg', NULL, NULL),
(68, 35, '1582981041ct22.jpg', NULL, NULL),
(69, 35, '1582981041ct33.jpg', NULL, NULL),
(70, 36, '1582981154ct22.jpg', NULL, NULL),
(71, 36, '1582981154ct33.jpg', NULL, NULL),
(72, 37, '1582981209ct222.jpg', NULL, NULL),
(73, 37, '1582981209ct333.jpg', NULL, NULL),
(74, 38, '1582981289ct02.jpg', NULL, NULL),
(75, 38, '1582981289ct03.jpg', NULL, NULL),
(76, 40, '1582982312k22.jpg', NULL, NULL),
(77, 40, '1582982312k33.jpg', NULL, NULL),
(78, 41, '1582982366k222.jpg', NULL, NULL),
(79, 41, '1582982366k333.jpg', NULL, NULL),
(80, 42, '1582982418k202.jpg', NULL, NULL),
(82, 43, '1582983039k02.jpg', NULL, NULL),
(83, 43, '1582983039k03.jpg', NULL, NULL),
(84, 44, '1582982620k2222.jpg', NULL, NULL),
(85, 44, '1582982620k3333.jpg', NULL, NULL),
(86, 79, '1582986261s2.jpg', NULL, NULL),
(87, 79, '1582986261s3.jpg', NULL, NULL),
(88, 45, '1582983783srr222.jpg', NULL, NULL),
(89, 45, '1582983783srr333.jpg', NULL, NULL),
(90, 46, '1582984362srr2.jpg', NULL, NULL),
(91, 46, '1582984362srr3.jpg', NULL, NULL),
(92, 47, '1582984429srr202.jpg', NULL, NULL),
(93, 47, '1582984429srr303.jpg', NULL, NULL),
(94, 48, '1582984491srr244.jpg', NULL, NULL),
(95, 48, '1582984491srr344.jpg', NULL, NULL),
(96, 49, '1582984534srr255.jpg', NULL, NULL),
(97, 49, '1582984534srr355.jpg', NULL, NULL),
(98, 50, '1582984644g22.jpg', NULL, NULL),
(99, 50, '1582984644g33.jpg', NULL, NULL),
(100, 51, '1582984694g222.jpg', NULL, NULL),
(101, 51, '1582984694g333.jpg', NULL, NULL),
(102, 52, '1582984747g202.jpg', NULL, NULL),
(103, 52, '1582984747g303.jpg', NULL, NULL),
(104, 53, '1582984797g2222.jpg', NULL, NULL),
(105, 53, '1582984797g3333.jpg', NULL, NULL),
(106, 54, '1582984847gw2.jpg', NULL, NULL),
(107, 54, '1582984847gw3.jpg', NULL, NULL),
(108, 55, '1582985274l22.jpg', NULL, NULL),
(109, 55, '1582985274l33.jpg', NULL, NULL),
(110, 56, '1582985352l222.jpg', NULL, NULL),
(111, 56, '1582985352l333.jpg', NULL, NULL),
(112, 58, '1582985479l202.jpg', NULL, NULL),
(113, 58, '1582985479l303.jpg', NULL, NULL),
(114, 59, '1582985754l2222.jpg', NULL, NULL),
(115, 59, '1582985754l3333.jpg', NULL, NULL),
(116, 60, '1582985869l2444.jpg', NULL, NULL),
(117, 60, '1582985869l3444.jpg', NULL, NULL),
(118, 61, '1582985978ct22.jpg', NULL, NULL),
(119, 61, '1582985978ct33.jpg', NULL, NULL),
(120, 62, '1582986134l22.jpg', NULL, NULL),
(121, 62, '1582986134l33.jpg', NULL, NULL),
(122, 63, '1583053266j2.jpg', NULL, NULL),
(123, 63, '1583053266j3.jpg', NULL, NULL),
(124, 65, '1583828589k2.jpg', NULL, NULL),
(125, 65, '1583828589k3.jpg', NULL, NULL),
(126, 66, '1583828719k2.jpg', NULL, NULL),
(127, 66, '1583828719k3.jpg', NULL, NULL),
(128, 66, '1583828719k4.jpg', NULL, NULL),
(129, 67, '1583828852k2.jpg', NULL, NULL),
(130, 67, '1583828852k3.jpg', NULL, NULL),
(131, 68, '1583828982k2.jpg', NULL, NULL),
(132, 68, '1583828982k3.jpg', NULL, NULL),
(133, 69, '1583829074k2.jpg', NULL, NULL),
(134, 69, '1583829074k3.jpg', NULL, NULL),
(135, 70, '1582986261s2.jpg', NULL, NULL),
(136, 70, '1582986261s3.jpg', NULL, NULL),
(137, 71, '1582980218s2.jpg', NULL, NULL),
(138, 71, '1582980218s3.jpg', NULL, NULL),
(139, 72, '1582978320b2.jpg', NULL, NULL),
(140, 72, '1582978320b3.jpg', NULL, NULL),
(141, 74, '1583829897j2.jpg', NULL, NULL),
(142, 74, '1583829897j3.jpg', NULL, NULL),
(143, 75, '1583830232j2.jpg', NULL, NULL),
(144, 75, '1583830232j3.jpg', NULL, NULL),
(145, 76, '1583830649j2.jpg', NULL, NULL),
(146, 76, '1583830649j3.jpg', NULL, NULL),
(147, 78, '1583053266j2.jpg', NULL, NULL),
(148, 78, '1583053266j3.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE `rating` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `rating` double(8,2) NOT NULL,
  `rating_date` date NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rating`
--

INSERT INTO `rating` (`id`, `rating`, `rating_date`, `user_id`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 4.00, '2020-03-18', 9, 3, '2020-03-20 10:09:34', '2020-03-20 10:09:36'),
(2, 4.00, '2020-03-19', 3, 54, '2020-03-20 10:12:34', '2020-03-20 10:12:35'),
(3, 3.00, '2020-03-18', 3, 3, '2020-03-20 10:12:44', '2020-03-20 10:12:46'),
(4, 3.00, '2020-03-19', 4, 43, '2020-03-20 10:15:00', '2020-03-20 10:15:00'),
(5, 3.00, '2020-03-20', 4, 30, '2020-03-20 10:15:26', '2020-03-20 10:15:26'),
(6, 4.00, '2020-03-20', 4, 15, '2020-03-20 10:17:01', '2020-03-20 10:17:01'),
(7, 4.00, '2020-03-20', 5, 10, '2020-03-20 10:18:02', '2020-03-20 10:18:02'),
(8, 4.00, '2020-03-20', 6, 6, '2020-03-20 10:41:16', '2020-03-20 10:41:16'),
(9, 4.00, '2020-03-20', 6, 30, '2020-03-20 10:41:33', '2020-03-20 10:41:33'),
(10, 3.00, '2020-03-20', 6, 3, '2020-03-20 10:42:17', '2020-03-20 10:42:19'),
(11, 4.00, '2020-03-20', 7, 44, '2020-03-20 10:44:46', '2020-03-20 10:44:46'),
(12, 3.00, '2020-03-20', 8, 44, '2020-03-20 10:52:45', '2020-03-20 10:52:47'),
(13, 3.00, '2020-03-20', 8, 51, '2020-03-20 10:53:05', '2020-03-20 10:53:05');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin', 'Admin is allowed to view all details', '2020-02-29 00:19:00', '2020-02-29 00:19:00'),
(2, 'customer', 'customer', 'customer', '2020-02-29 00:19:00', '2020-02-29 00:19:00');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(3, 2),
(4, 2),
(5, 2),
(6, 2),
(7, 2),
(8, 2),
(9, 2),
(10, 2),
(13, 2);

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sales_date` date NOT NULL,
  `delivery_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_date` date DEFAULT NULL,
  `ammount` double(15,8) NOT NULL,
  `delivery_charges` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sgst` double NOT NULL,
  `cgst` double NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Pending,Complete,Cencel'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `sales_date`, `delivery_address`, `delivery_date`, `ammount`, `delivery_charges`, `sgst`, `cgst`, `user_id`, `created_at`, `updated_at`, `status`) VALUES
(30, '2020-03-10', NULL, NULL, 1898.10000000, NULL, 2.5, 2.5, 9, '2020-03-10 10:15:48', '2020-03-10 10:15:48', 'Complete'),
(32, '2020-03-11', NULL, NULL, 2847.15000000, NULL, 2.5, 2.5, 4, '2020-03-10 21:23:26', '2020-03-10 21:23:26', 'Complete'),
(43, '2020-03-11', NULL, NULL, 1708.10000000, NULL, 2.5, 2.5, 4, '2020-03-10 22:02:55', '2020-03-10 22:02:55', 'Complete'),
(44, '2020-03-11', NULL, NULL, 854.05000000, NULL, 2.5, 2.5, 4, '2020-03-10 22:06:17', '2020-03-10 22:06:17', 'Complete'),
(45, '2020-03-11', NULL, NULL, 854.05000000, NULL, 2.5, 2.5, 4, '2020-03-10 22:08:51', '2020-03-10 22:08:51', 'Complete'),
(46, '2020-03-11', NULL, NULL, 759.05000000, NULL, 2.5, 2.5, 4, '2020-03-10 22:48:40', '2020-03-10 22:48:40', 'Complete'),
(47, '2020-03-11', NULL, NULL, 759.05000000, NULL, 2.5, 2.5, 4, '2020-03-10 23:02:06', '2020-03-10 23:02:06', 'Complete'),
(48, '2020-03-11', NULL, NULL, 902.50000000, NULL, 2.5, 2.5, 5, '2020-03-11 01:45:33', '2020-03-11 01:45:33', 'Complete'),
(49, '2020-03-11', NULL, NULL, 2372.15000000, NULL, 2.5, 2.5, 6, '2020-03-11 01:50:05', '2020-03-11 01:50:05', 'Complete'),
(50, '2020-03-11', NULL, NULL, 569.05000000, NULL, 2.5, 2.5, 7, '2020-03-11 02:11:47', '2020-03-11 02:11:47', 'Complete'),
(52, '2020-03-16', NULL, NULL, 3599.00000000, NULL, 2.5, 2.5, 6, '2020-03-16 08:51:17', '2020-03-16 08:51:17', 'Complete'),
(56, '2020-03-17', NULL, NULL, 949.05000000, NULL, 2.5, 2.5, 6, '2020-03-17 05:25:43', '2020-03-17 05:25:43', 'Complete'),
(59, '2020-03-19', NULL, NULL, 1599.00000000, NULL, 2.5, 2.5, 4, '2020-03-19 09:32:16', '2020-03-19 09:32:16', 'Complete'),
(60, '2020-03-20', NULL, NULL, 7553.70000000, NULL, 2.5, 2.5, 3, '2020-03-20 10:11:40', '2020-03-20 10:11:40', 'Complete'),
(61, '2020-03-20', NULL, NULL, 2307.90000000, NULL, 2.5, 2.5, 8, '2020-03-20 10:51:19', '2020-03-20 10:51:19', 'Complete'),
(62, '2020-03-20', NULL, NULL, 2307.90000000, NULL, 2.5, 2.5, 8, '2020-03-20 10:51:27', '2020-03-20 10:51:27', 'Complete'),
(63, '2020-03-22', NULL, NULL, 1992.90000000, NULL, 2.5, 2.5, 10, '2020-03-22 11:54:09', '2020-03-22 11:54:09', 'Complete'),
(64, '2022-02-24', NULL, NULL, 1048.95000000, NULL, 2.5, 2.5, 13, '2022-02-24 00:45:12', '2022-02-24 00:45:12', 'Complete');

-- --------------------------------------------------------

--
-- Table structure for table `sales_details`
--

CREATE TABLE `sales_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sales_id` bigint(20) NOT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `price` double(15,8) NOT NULL,
  `customize_product_id` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `color_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales_details`
--

INSERT INTO `sales_details` (`id`, `sales_id`, `product_id`, `qty`, `price`, `customize_product_id`, `created_at`, `updated_at`, `color_id`) VALUES
(26, 30, 3, 2, 999.00000000, NULL, '2020-03-10 10:15:48', '2020-03-10 10:15:48', NULL),
(28, 32, 43, 3, 999.00000000, NULL, '2020-03-10 21:23:26', '2020-03-10 21:23:26', NULL),
(39, 43, 30, 2, 899.00000000, NULL, '2020-03-10 22:02:55', '2020-03-10 22:02:55', NULL),
(40, 44, 30, 1, 899.00000000, NULL, '2020-03-10 22:06:18', '2020-03-10 22:06:18', NULL),
(41, 45, 30, 1, 899.00000000, NULL, '2020-03-10 22:08:52', '2020-03-10 22:08:52', NULL),
(42, 46, 15, 1, 799.00000000, NULL, '2020-03-10 22:48:41', '2020-03-10 22:48:41', NULL),
(43, 47, 15, 1, 799.00000000, NULL, '2020-03-10 23:02:07', '2020-03-10 23:02:07', NULL),
(44, 48, 10, 1, 950.00000000, NULL, '2020-03-11 01:45:33', '2020-03-11 01:45:33', NULL),
(45, 49, 6, 1, 699.00000000, NULL, '2020-03-11 01:50:05', '2020-03-11 01:50:05', NULL),
(46, 49, 30, 2, 899.00000000, NULL, '2020-03-11 01:50:05', '2020-03-11 01:50:05', NULL),
(47, 50, 44, 1, 599.00000000, NULL, '2020-03-11 02:11:47', '2020-03-11 02:11:47', NULL),
(49, 52, NULL, 1, 2000.00000000, 26, '2020-03-16 08:51:17', '2020-03-16 08:51:17', NULL),
(50, 52, NULL, 1, 1599.00000000, 27, '2020-03-16 08:51:17', '2020-03-16 08:51:17', NULL),
(54, 56, 3, 1, 999.00000000, NULL, '2020-03-17 05:25:43', '2020-03-17 05:25:43', NULL),
(57, 59, NULL, 1, 1599.00000000, 30, '2020-03-19 09:32:17', '2020-03-19 09:32:17', NULL),
(58, 60, 3, 2, 999.00000000, NULL, '2020-03-20 10:11:40', '2020-03-20 10:11:40', NULL),
(59, 60, 54, 4, 1299.00000000, NULL, '2020-03-20 10:11:40', '2020-03-20 10:11:40', NULL),
(60, 61, 51, 1, 1599.00000000, NULL, '2020-03-20 10:51:21', '2020-03-20 10:51:21', NULL),
(61, 61, 44, 1, 599.00000000, NULL, '2020-03-20 10:51:21', '2020-03-20 10:51:21', NULL),
(62, 62, 51, 1, 1599.00000000, NULL, '2020-03-20 10:51:27', '2020-03-20 10:51:27', NULL),
(63, 62, 44, 1, 599.00000000, NULL, '2020-03-20 10:51:27', '2020-03-20 10:51:27', NULL),
(64, 63, 3, 1, 999.00000000, NULL, '2020-03-22 11:54:09', '2020-03-22 11:54:09', NULL),
(65, 63, 4, 1, 899.00000000, NULL, '2020-03-22 11:54:09', '2020-03-22 11:54:09', NULL),
(66, 64, 76, 1, 999.00000000, NULL, '2022-02-24 00:45:12', '2022-02-24 00:45:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sales_payment`
--

CREATE TABLE `sales_payment` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `amount` double(15,8) NOT NULL,
  `payment_date` date DEFAULT NULL,
  `transaction_date` date DEFAULT NULL,
  `transaction_id` bigint(20) DEFAULT NULL,
  `payment_type_id` bigint(20) UNSIGNED NOT NULL,
  `sales_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales_payment`
--

INSERT INTO `sales_payment` (`id`, `amount`, `payment_date`, `transaction_date`, `transaction_id`, `payment_type_id`, `sales_id`, `created_at`, `updated_at`) VALUES
(24, 1898.10000000, '2020-03-10', NULL, NULL, 1, 30, '2020-03-10 10:15:48', '2020-03-10 10:15:48'),
(26, 2847.15000000, '2020-03-11', NULL, NULL, 1, 32, '2020-03-10 21:23:27', '2020-03-10 21:23:27'),
(37, 1708.10000000, '2020-03-11', NULL, NULL, 1, 43, '2020-03-10 22:02:55', '2020-03-10 22:02:55'),
(38, 854.05000000, '2020-03-11', NULL, NULL, 1, 44, '2020-03-10 22:06:19', '2020-03-10 22:06:19'),
(39, 854.05000000, '2020-03-11', NULL, NULL, 1, 45, '2020-03-10 22:08:52', '2020-03-10 22:08:52'),
(40, 759.05000000, '2020-03-11', NULL, NULL, 1, 46, '2020-03-10 22:48:41', '2020-03-10 22:48:41'),
(41, 759.05000000, '2020-03-11', NULL, NULL, 1, 47, '2020-03-10 23:02:07', '2020-03-10 23:02:07'),
(42, 902.50000000, '2020-03-11', NULL, NULL, 1, 48, '2020-03-11 01:45:33', '2020-03-11 01:45:33'),
(43, 2372.15000000, '2020-03-11', NULL, NULL, 1, 49, '2020-03-11 01:50:05', '2020-03-11 01:50:05'),
(44, 569.05000000, '2020-03-11', NULL, NULL, 1, 50, '2020-03-11 02:11:47', '2020-03-11 02:11:47'),
(46, 3599.00000000, '2020-03-16', NULL, 0, 1, 52, '2020-03-16 08:51:17', '2020-03-16 08:51:17'),
(50, 949.05000000, '2020-03-17', NULL, NULL, 1, 56, '2020-03-17 05:25:43', '2020-03-17 05:25:43'),
(53, 1599.00000000, '2020-03-19', NULL, 0, 1, 59, '2020-03-19 09:32:17', '2020-03-19 09:32:17'),
(54, 7553.70000000, '2020-03-20', NULL, NULL, 1, 60, '2020-03-20 10:11:41', '2020-03-20 10:11:41'),
(55, 2307.90000000, '2020-03-20', NULL, NULL, 1, 61, '2020-03-20 10:51:21', '2020-03-20 10:51:21'),
(56, 2307.90000000, '2020-03-20', NULL, NULL, 1, 62, '2020-03-20 10:51:28', '2020-03-20 10:51:28'),
(57, 1992.90000000, '2020-03-22', NULL, NULL, 1, 63, '2020-03-22 11:54:09', '2020-03-22 11:54:09'),
(58, 1048.95000000, '2022-02-24', NULL, NULL, 1, 64, '2022-02-24 00:45:12', '2022-02-24 00:45:12');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_address`
--

CREATE TABLE `shipping_address` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `address1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `area_id` bigint(20) UNSIGNED DEFAULT NULL,
  `latitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sales_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `size_chart`
--

CREATE TABLE `size_chart` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `size_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `length` double(15,8) NOT NULL,
  `waist` double(15,8) NOT NULL,
  `chest` double(15,8) NOT NULL,
  `hips` double(15,8) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `subcategory_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `size_chart`
--

INSERT INTO `size_chart` (`id`, `size_name`, `length`, `waist`, `chest`, `hips`, `created_at`, `updated_at`, `subcategory_id`) VALUES
(1, 'S', 42.00000000, 31.00000000, 36.00000000, 38.00000000, '2020-02-29 00:48:15', '2020-02-29 00:48:15', 0),
(2, 'M', 42.00000000, 33.00000000, 38.00000000, 40.00000000, '2020-02-29 00:48:35', '2020-02-29 00:48:35', 0),
(3, 'L', 42.00000000, 35.00000000, 40.00000000, 42.00000000, '2020-02-29 00:49:23', '2020-02-29 00:49:23', 0),
(4, 'XL', 44.00000000, 37.00000000, 42.00000000, 44.00000000, '2020-02-29 00:49:44', '2020-02-29 00:49:44', 0),
(5, 'XXL', 46.00000000, 39.00000000, 44.00000000, 46.00000000, '2020-02-29 00:50:15', '2020-02-29 00:50:15', 0),
(6, 'L', 10.00000000, 20.00000000, 20.00000000, 20.00000000, '2020-04-05 02:35:01', '2020-04-05 02:35:01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE `subcategory` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subcategory_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subcategory`
--

INSERT INTO `subcategory` (`id`, `subcategory_name`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'Blazer', 1, '2020-02-29 00:53:14', '2020-02-29 00:53:14'),
(2, 'Indowestern', 1, '2020-02-29 00:53:43', '2020-02-29 00:53:43'),
(6, 'Jacket', 1, '2020-02-29 00:54:57', '2020-02-29 00:54:57'),
(8, 'Shervani', 1, '2020-02-29 00:55:23', '2020-02-29 00:55:23'),
(9, 'Suit', 1, '2020-02-29 00:55:34', '2020-02-29 00:55:34'),
(11, 'Anarkali', 2, '2020-02-29 00:58:18', '2020-02-29 00:58:18'),
(12, 'Crop Top', 2, '2020-02-29 00:58:34', '2020-02-29 00:58:34'),
(13, 'Gown', 2, '2020-02-29 00:58:46', '2020-02-29 00:58:46'),
(16, 'Sharara', 2, '2020-02-29 01:00:11', '2020-02-29 01:00:11');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `security_question` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `security_answer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `contact_no` bigint(20) NOT NULL,
  `profile_pic` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area_id` bigint(20) UNSIGNED DEFAULT NULL,
  `verify_tokan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `address`, `security_question`, `security_answer`, `first_name`, `last_name`, `gender`, `dob`, `contact_no`, `profile_pic`, `area_id`, `verify_tokan`) VALUES
(3, 'Shwetu', 'chavdashweta23@gmail.com', '2020-03-07 02:25:08', '$2y$10$.R3entW/Pb9vbZF1pbb82.M5T9ppt/4MiaZt9/SR6G.VcNCDDls3i', NULL, '2020-03-07 02:20:52', '2020-03-07 02:20:52', 'Shahibag', 'color', 'black', 'Shweta', 'Chavda', 'female', '1999-09-23', 6351909166, 'pcc.jpg', 1, 'ESZWR8yKhjcaAYYT0v3WU2AZrfJUQBMKy1sGOJ2NkVKJRLgaKK'),
(4, 'viru', 'viral123@mailinator.com', '2020-03-08 03:00:45', '$2y$10$T9dfe9XtfIyBdpB/RgTxJenPhA8Hqdx.B0ctu8OfGe2D6miV9LF/6', NULL, '2020-03-08 02:59:39', '2020-03-08 02:59:39', 'HanshPark', 'color', 'black', 'Viral', 'Chavda', 'female', '1995-09-02', 9876546790, 'pcc.jpg', 1, 'k3u5Otj84ds10lM8kIjY5rWhxBZdSn2OllVzc2R1Hv3SNruF49'),
(5, 'Anju', 'anjali123@mailinator.com', '2020-03-08 03:16:11', '$2y$10$/IIk3bM16JWP.dTzLTT5W.iXps.5TKHhKHfkW/DxbRbBB0sJ34x4.', NULL, '2020-03-08 03:14:49', '2020-03-08 03:14:49', 'Sanidhya park', 'nicename', 'guduu', 'Anjali', 'Patel', 'female', '2000-12-06', 7756890435, 'pcc.jpg', 4, 'jnd0ouA09b9VIGvqteVueEuHhiWl3toZazBFypj0VjeOf83lCL'),
(6, 'Sweetu', 'shweta123@mailinator.com', '2020-03-08 03:26:12', '$2y$10$TQdDoOLXapyG.C082XjeuuKaydFkSOLYJ9HfQO/XO7WxG7VmxfijC', NULL, '2020-03-08 03:25:53', '2020-03-15 01:04:56', 'Danev Park', 'nicename', 'shetuuu', 'Shwarti', 'Chavda', 'female', '1999-02-09', 8797656780, 'pcc.jpg', 3, 'nCJkYiZHLYFHJ39RT34sTwaayxGJywL2pUy9GTSQGs4Rb3d5mU'),
(7, 'Gopi', 'rajvi123@mailinator.com', '2020-03-08 03:36:30', '$2y$10$8LMBOgxb/XMmRiJFfhDoHOv7fqNqvC2eFqjy9X5t/Dp8LAfDxqB.S', NULL, '2020-03-08 03:35:14', '2020-03-08 03:35:14', 'Jinandhara', 'nicename', 'gopli', 'Rajvi', 'Sojitra', 'female', '2000-12-04', 9865468906, 'pcc.jpg', 2, 'VzA4KZoqi1ziP2O4x0QSkg2mi0fJXpA8hSGb96Kj3bOUNxKX6s'),
(8, 'Vedika', 'vedika123@mailinator.com', '2020-03-08 04:02:11', '$2y$10$F2jzoEqoWDd.1FJL4jtjF.WG5gnozqd.r3inx7q0ufNE8ORiBsRH.', NULL, '2020-03-08 04:01:49', '2020-03-08 04:01:49', 'Gajanan Society', 'color', 'red', 'Radhika', 'Kavar', 'female', '1997-03-01', 9812346790, 'pcc.jpg', 2, '7CpkFjtyTRwzRxKMpqMFKjU3yQ9rq15Z2ksURfBO7QjUJG5wGy'),
(9, 'D', 'dharti@mailinator.com', '2020-03-10 06:27:48', '$2y$10$FfGoL6zlWaJN4hFmAI1NSuAXSduqB9b2DTlOtqKC0W1//cRkTilVi', NULL, '2020-03-10 06:25:55', '2020-03-10 06:27:21', 'vadaj', 'color', 'black', 'Dharti', 'Chauhan', 'female', '1999-10-16', 1234567980, 'pcc.jpg', 1, 'i98s37uxwCtmCHHv8cKd4ATNPwyHvJL80nKdd0s96qKD3sIz0Q'),
(10, '-', 'prajapatidhruvi3959@gmail.com', '2020-03-22 00:41:44', '$2y$10$zBQjAKbLAlJFwHMdjG5jbON6T2g7XoNIUoCKYrqQDjymMaiyTID1K', NULL, '2020-03-22 00:40:55', '2020-03-22 01:27:16', '381 Mahadev vas-1, new vadaj', 'nicename', 'Hp', 'Hardik', 'Prajapati', 'male', '1999-01-31', 9558976162, '1584858241.jpg', 2, 'RaNg4i0pUkbn5uwu0JciGVIWBSqOXMmwbodCGd53qx0ZEt2VMl'),
(13, '-', 'hardikmca3125@gmail.com', '2022-02-24 00:43:06', '$2y$10$o2jEGkbTJOmy5PyEfmziwOKWkul3n6yms7O0ClndOrAUW3rlWI.fu', 'xyhYj5axZqgyQFwVUQspQJEiijixTLdysEh6PJBkW5C1P4XZW6InRgcOGW7G', '2022-02-24 00:42:51', '2022-02-24 01:27:32', 'Ahmedabad', 'color', 'test', 'Arati', 'Prajapati', 'female', '2000-09-17', 8780900308, 'user_d.png', 2, 'xqBKaKgTHssNaq32DHj4rbuR97BzxO19sMgvSw2FlRgJIFOSdd');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `wishlist_date` date NOT NULL,
  `qty` bigint(20) NOT NULL,
  `price` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `color_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`id`, `wishlist_date`, `qty`, `price`, `product_id`, `user_id`, `created_at`, `updated_at`, `color_id`) VALUES
(1, '2020-03-07', 3, 799, 15, 2, '2020-03-07 03:53:17', '2020-03-07 03:53:22', 9),
(3, '2020-03-08', 1, 799, 42, 5, '2020-03-08 03:17:45', '2020-03-08 03:17:45', 6),
(4, '2020-03-08', 1, 899, 30, 6, '2020-03-08 03:28:34', '2020-03-08 03:28:34', 2),
(5, '2020-03-08', 1, 899, 26, 8, '2020-03-08 04:03:34', '2020-03-08 04:03:34', 10),
(9, '2020-03-11', 1, 799, 15, 4, '2020-03-10 22:43:43', '2020-03-10 22:43:43', 9),
(11, '2020-03-11', 2, 950, 10, 5, '2020-03-11 01:45:09', '2020-03-11 01:45:10', 7);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `appointment_user_id_foreign` (`user_id`);

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`),
  ADD KEY `area_city_id_foreign` (`city_id`);

--
-- Indexes for table `carolous`
--
ALTER TABLE `carolous`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `company_company_email_unique` (`company_email`),
  ADD KEY `company_area_id_foreign` (`area_id`);

--
-- Indexes for table `complain`
--
ALTER TABLE `complain`
  ADD PRIMARY KEY (`id`),
  ADD KEY `complain_user_id_foreign` (`user_id`),
  ADD KEY `complain_sales_id_foreign` (`sales_id`),
  ADD KEY `complain_product_id_foreign` (`product_id`);

--
-- Indexes for table `contactus`
--
ALTER TABLE `contactus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contactus_email_unique` (`email`);

--
-- Indexes for table `customize_product`
--
ALTER TABLE `customize_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customize_product_appointment_id_foreign` (`appointment_id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`user_id`,`product_id`),
  ADD KEY `feedback_product_id_foreign` (`product_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newslater`
--
ALTER TABLE `newslater`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer`
--
ALTER TABLE `offer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_type`
--
ALTER TABLE `payment_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_subcategory_id_foreign` (`subcategory_id`),
  ADD KEY `product_size_id_foreign` (`size_id`),
  ADD KEY `product_company_id_foreign` (`company_id`);

--
-- Indexes for table `product_color`
--
ALTER TABLE `product_color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_images_product_id_foreign` (`product_id`);

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sales_user_id_foreign` (`user_id`);

--
-- Indexes for table `sales_details`
--
ALTER TABLE `sales_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_payment`
--
ALTER TABLE `sales_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sales_payment_payment_type_id_foreign` (`payment_type_id`),
  ADD KEY `sales_payment_sales_id_foreign` (`sales_id`);

--
-- Indexes for table `shipping_address`
--
ALTER TABLE `shipping_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shipping_address_user_id_foreign` (`user_id`),
  ADD KEY `shipping_address_area_id_foreign` (`area_id`),
  ADD KEY `shipping_address_sales_id_foreign` (`sales_id`);

--
-- Indexes for table `size_chart`
--
ALTER TABLE `size_chart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategory`
--
ALTER TABLE `subcategory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subcategory_category_id_foreign` (`category_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_email_unique` (`email`),
  ADD UNIQUE KEY `user_contact_no_unique` (`contact_no`),
  ADD KEY `users_area_id_foreign` (`area_id`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `appointment`
--
ALTER TABLE `appointment`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `carolous`
--
ALTER TABLE `carolous`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `color`
--
ALTER TABLE `color`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `complain`
--
ALTER TABLE `complain`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contactus`
--
ALTER TABLE `contactus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customize_product`
--
ALTER TABLE `customize_product`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `newslater`
--
ALTER TABLE `newslater`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `offer`
--
ALTER TABLE `offer`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_type`
--
ALTER TABLE `payment_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `product_color`
--
ALTER TABLE `product_color`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;

--
-- AUTO_INCREMENT for table `rating`
--
ALTER TABLE `rating`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `sales_details`
--
ALTER TABLE `sales_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `sales_payment`
--
ALTER TABLE `sales_payment`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `shipping_address`
--
ALTER TABLE `shipping_address`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `size_chart`
--
ALTER TABLE `size_chart`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `subcategory`
--
ALTER TABLE `subcategory`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `appointment`
--
ALTER TABLE `appointment`
  ADD CONSTRAINT `appointment_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `area`
--
ALTER TABLE `area`
  ADD CONSTRAINT `area_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`);

--
-- Constraints for table `company`
--
ALTER TABLE `company`
  ADD CONSTRAINT `company_area_id_foreign` FOREIGN KEY (`area_id`) REFERENCES `area` (`id`);

--
-- Constraints for table `complain`
--
ALTER TABLE `complain`
  ADD CONSTRAINT `complain_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `complain_sales_id_foreign` FOREIGN KEY (`sales_id`) REFERENCES `sales` (`id`),
  ADD CONSTRAINT `complain_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `customize_product`
--
ALTER TABLE `customize_product`
  ADD CONSTRAINT `customize_product_appointment_id_foreign` FOREIGN KEY (`appointment_id`) REFERENCES `appointment` (`id`);

--
-- Constraints for table `feedback`
--
ALTER TABLE `feedback`
  ADD CONSTRAINT `feedback_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `feedback_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  ADD CONSTRAINT `product_size_id_foreign` FOREIGN KEY (`size_id`) REFERENCES `size_chart` (`id`),
  ADD CONSTRAINT `product_subcategory_id_foreign` FOREIGN KEY (`subcategory_id`) REFERENCES `subcategory` (`id`);

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sales`
--
ALTER TABLE `sales`
  ADD CONSTRAINT `sales_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `sales_payment`
--
ALTER TABLE `sales_payment`
  ADD CONSTRAINT `sales_payment_payment_type_id_foreign` FOREIGN KEY (`payment_type_id`) REFERENCES `payment_type` (`id`),
  ADD CONSTRAINT `sales_payment_sales_id_foreign` FOREIGN KEY (`sales_id`) REFERENCES `sales` (`id`);

--
-- Constraints for table `shipping_address`
--
ALTER TABLE `shipping_address`
  ADD CONSTRAINT `shipping_address_area_id_foreign` FOREIGN KEY (`area_id`) REFERENCES `area` (`id`),
  ADD CONSTRAINT `shipping_address_sales_id_foreign` FOREIGN KEY (`sales_id`) REFERENCES `sales` (`id`),
  ADD CONSTRAINT `shipping_address_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `subcategory`
--
ALTER TABLE `subcategory`
  ADD CONSTRAINT `subcategory_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_area_id_foreign` FOREIGN KEY (`area_id`) REFERENCES `area` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
